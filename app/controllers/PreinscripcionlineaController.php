<?php
class PreinscripcionlineaController extends BaseController {

    public function getIndex()
    {
        if(Session::has('message_error_periodo')){
            $tab_active='periodo';
        }elseif(Session::has('message_error_personales')){
            $tab_active='personales';
        }elseif(Session::has('message_error_datossecundaria')){
            $tab_active='datossecundaria'; 
        }else{
            $tab_active='periodo';
        }        

        $tcedula=array(
            ''=>'SELECCIONAR',
            'v'=>'V',
            'e'=>'E');

        $nsobre=array(
            ''=>'SELECCIONAR',
            '100' =>'COSTA RICA - UBA',
            '101' =>'ECUADOR - UBA'
            );

        $tsexo=array(
            ''=>'SELECCIONAR',
            'M'=>'MASCULINO',
            'F'=>'FEMENINO');
        $ecivil=array(
            ''=>'SELECCIONAR',
            'S'=>'SOLTERO',
            'C'=>'CASADO',
            'D'=>'DIVORCIADO',
            'V'=>'VIUDO');

        $pais=array(
            ''=>'SELECCIONAR',
            '1'=>'VENEZUELA',
            '2'=>'OTROS');

        $pais_nac = array('' => 'SELECCIONAR' , ) + DB::table('db_paises')->lists('des_paises', 'ID');

        $estados = array('' => 'SELECCIONAR' , ) + DB::table('db_estados')->where('id_pais', '230')->lists('des_estados', 'ID');

        $ciudad=array(
            ''=>'SELECCIONAR'
            );

        $estados_nac=array(
            ''=>'SELECCIONAR'
            );

        $ciudad_nac=array(
            ''=>'SELECCIONAR'
            );

        $ciudad_ubi=array(
            ''=>'SELECCIONAR'
            );

        $ciudad_cs=array(
            ''=>'SELECCIONAR'
            );

        $especialidad=array(
            ''=>'SELECCIONAR',
            '1'=>'CIENCIAS',
            '2'=>'HUMANIDADES',
            '3'=>'OTRA');

        $carreras_uba = array('' => 'SELECCIONAR' , ) + DB::table('carreras_uba')->lists('carrera', 'id');

        $datos_carrera=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');

        $carreras = array('' => 'SELECCIONAR' , ) + DB::table('carreras')->lists('carrera', 'id');

        $nivel_estudio =array(
            ''=>'SELECCIONAR',
            '1'=>'T.S.U',
            '2'=>'Profesional');        

        $preinscripcion_config= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                //->where('estado', '=', '1')
                ->get();


            foreach($preinscripcion_config as $key ){
                $lapso=$key->lapso;                
                $cod_reg=$key->cod_reg;
                if ($cod_reg==0){
                    $lapso_reg=$key->lapso;
                }else{
                    $lapso_trim=$key->lapso;
                }
            }

        if (count($preinscripcion_config)>0){
            if (count($preinscripcion_config)==2){
                $tsolicitud=array(
                ''=>'SELECCIONAR',
                '1'=>'POR PRIMERA VEZ O REINGRESO',
                '2'=>'REINCORPORACI&Oacute;N(Por retiro de la Instituci&oacute;n)',
                '3'=>'EQUIVALENCIA');
                $mensaje='Solicitud de Inscripci&oacute;n Trimestral lapso '.$lapso_trim.' y Semestral para el lapso '.$lapso_reg ;
            }elseif($cod_reg==2){
                $tsolicitud=array(
                ''=>'SELECCIONAR',
                '1'=>'POR PRIMERA VEZ O REINGRESO',
                '2'=>'REINCORPORACI&Oacute;N(Por retiro de la Instituci&oacute;n)',
                '3'=>'EQUIVALENCIA');
                $mensaje='Solicitud de Inscripci&oacute;n Trimestral para el lapso '.$lapso_trim;
            }elseif($cod_reg==0){
                $tsolicitud=array(
                ''=>'SELECCIONAR',
                '2'=>'REINCORPORACI&Oacute;N',
                '3'=>'EQUIVALENCIA(S&oacute;lo Semestre)');
                $mensaje='Solicitud de Inscripci&oacute;n Semestral para el lapso '.$lapso_reg;
            }

            //calendario de citas

            $fechas_no = array();

            for ($i=26; $i <= 30; $i++) { 
                $fechas_no[] = '"'.$i.'-03-2018"';
            }

            $query_fechas = DB::select('SELECT fecha_cita,COUNT(*) AS n_citas FROM preinscritos WHERE lapso = ? AND (cod_nucleo = 0 AND cod_nucleo = 4) AND numsobre LIKE "00-%" GROUP BY fecha_cita HAVING n_citas = 50',array($lapso_trim));  

            foreach ($query_fechas as $value) {
                $fechas_no[] = '"'.date_format(date_create($value->fecha_cita),'d-m-Y').'"';
            }

            setlocale(LC_TIME, 'es_VE'); # Localiza en español es_Venezuela
            date_default_timezone_set('America/La_Paz');

            $fechas_no[] = '"25-09-2018"';
            $fechas_no[] = '"10-10-2018"';
            $elementos = implode(",", $fechas_no);

            //termina calendario

            $title='Preinscripcion';
            return View::make('preinscripcion.preinscripcionlinea', array(
                   'title'=>$title,
                   'mensaje'=>$mensaje,
                   'tab'=>$tab_active,
                   'tsolicitud'=>$tsolicitud,
                   'tcedula'=>$tcedula,
                   'tsexo'=>$tsexo,
                   'ecivil'=>$ecivil,
                   'pais'=>$pais,
                   'pais_nac'=>$pais_nac,
                   'estados' => $estados,
                   'estados_nac_inter' => $estados_nac,
                   'ciudad'=>$ciudad,
                   'ciudad_nac_inter'=>$ciudad_nac,
                   'ciudad_ubi'=>$ciudad_ubi,
                   'especialidad'=>$especialidad,
                   'ciudad_cs'=>$ciudad_cs,
                   'carreras_uba'=>$carreras_uba,
                   'carreras'=>$carreras,
                   'datos_carrera'=>$datos_carrera,
                   'carreras'=>$carreras,
                   'nivel_estudio' => $nivel_estudio,
                   'lapso'=>$lapso,
                   'fechas_no' => $elementos,
                   'nsobre' => $nsobre

            ));
        }else{
            $title='Proceso Inactivo';
            return View::make('preinscripcion.mensajepreinscripcion', array(
                   'title'=>$title,
            ));
        }    
        
    }
    //FUNCION QUE PROCESA LA PETICION DE AJAX PARA PROCESAR LA SOLICITUD (VISTA: DATOSERVICIO)
    public function postProcesarpreinsc()
    {
        $tsolicitud=Input::get('tsolicitud');
        $fecha=Input::get('fecha');
        $tcedula=Input::get('tcedula');
        $cedula=Input::get('cedula');
        $edad=Input::get('edad');
        $nsobre=Input::get('nsobre');
        $nombres=Input::get('nombres');
        $apellidos=Input::get('apellidos');
        $tsexo=Input::get('tsexo');
        $ecivil=Input::get('ecivil');
        $telefono=Input::get('telefono');
        $email=Input::get('email');
        $doc1=Input::get('doc1');
        $doc2=Input::get('doc2');
        $doc3=Input::get('doc3');
        $doc4=Input::get('doc4');
        $doc5=Input::get('doc5');
        $doc6=Input::get('doc6');
        $doc7=Input::get('doc7');
        $doc8=Input::get('doc8');
        $doc9=Input::get('doc9');

        $twitter=Input::get('twitter');
        if ($twitter == '') {
            $twitter = 'NO';
        }
        $facebook=Input::get('facebook');
        if ($facebook == '') {
            $facebook = 'NO';
        }

        $pais_nac=Input::get('pais_nac');
        if ($pais_nac==''){
            $estados_nac=null;
            $ciudad_nac=null;
        }else{
            $estados_nac=Input::get('estados_nac');
            if ($estados_nac==''){
                $estados_nac=null;
                $ciudad_nac=null;
            }else{        
                $ciudad_nac=Input::get('ciudad_nac');
                if ($ciudad_nac==''){
                    $ciudad_nac=null;
                }else{        
                    $ciudad_nac=Input::get('ciudad_nac');
                }
            }
        }
        $direcc_r=Input::get('direcc_r');
        $direcc_perm=Input::get('direcc_perm');
        $estados_ubi=Input::get('estados_ubi');
        $ciudad_ubi=Input::get('ciudad_ubi');
        $edad=Input::get('edad');
        $telefono_r=Input::get('telefono_r');
        $nom_institucion=Input::get('nom_institucion');
        $g_especialidad=Input::get('g_especialidad');

        if ($g_especialidad==3){
            $ex_especialidad=Input::get('ex_especialidad');
        }else{
            $ex_especialidad=null;
        }
        
        $promedio_b=Input::get('promedio_b');
        $fecha_g=Input::get('fecha_g');
        $estados_estu_secu=Input::get('estados_estu_secu');
        $ciudad_estu_secu=Input::get('ciudad_estu_secu');
        $rusnies=Input::get('rusnies');
        $carreras=Input::get('carreras');
        $carrera_uba=Input::get('carrera_uba');
        $otros_estudio=Input::get('otros_estudio');

        $otros_estudio=Input::get('otros_estudio');
        if ($otros_estudio==1){
            $nom_institucion=Input::get('nom_institucion');
            $otra_carreras=Input::get('otra_carreras');
            $fecha_ing=Input::get('fecha_ing');
            $nivel_estudio = Input::get('nivel_estudio');
        }else{
            $nom_institucion=null;
            $otra_carreras=null;
            $fecha_ing=null;
            $nivel_estudio =null;
        }
        if ($otros_estudio==1){
            $nom_institucion=Input::get('nom_institucion');
            $otra_carreras=Input::get('otra_carreras');
            $fecha_ing=Input::get('fecha_ing');
            $nivel_estudio = Input::get('nivel_estudio');
        }else{
            $fecha_ing=null;
        }

        $variables1 = array(
            'tipo de solicitud' =>$tsolicitud,
            'fecha de nacimiento'=>$fecha,
            'nacionalidad'=>$tcedula,
            'c&eacute;dula'=>$cedula
        );
        //reglas para validar cada variable
        $rules1 = array(
            'tipo de solicitud' => 'required',
            'fecha de nacimiento'=>'required',
            'nacionalidad'=>'required',
            'c&eacute;dula' => 'required|numeric|digits_between:6,11',
        );
         
        //en el caso que alguna vadilacion falle retornaremos al formulario.
        $validation1 = Validator::make($variables1, $rules1);
        if ($validation1->fails()){
            $error_periodo=true;
            $message_periodo = $validation1->messages();
        }else{
            $error_periodo=false;
        }
        $variables2 = array(
            'n&uacute;mero de sobre' => $nsobre,
            'primer y segundo nombre'=> $nombres,
            'primer y segundo apellido'=> $apellidos,
            'sexo'=>$tsexo,
            'estado civil'=>$ecivil,
            'tel&eacute;fono'=>$telefono,
            'correo electr&oacute;nico'=>$email,
            'twitter' => $twitter,
            'facebook' => $facebook,
            'pais de nacimiento'=>$pais_nac,
            'direcci&oacute;n de residencia'=>$direcc_r,
            'tel&eacute;fono de residencia'=>$telefono_r,
            'direcci&oacute;n permanente'=>$direcc_perm,
            'estados de ubicaci&oacute;n'=>$estados_ubi,
            'ciudad de ubicaci&oacute;n'=>$ciudad_ubi,             
        );
        //reglas para validar cada variable
        $rules2 = array(
            'n&uacute;mero de sobre' => 'required|in:100,101',
            'primer y segundo nombre' => 'required',
            'primer y segundo apellido'=> 'required',
            'sexo'=> 'required',
            'estado civil' => 'required',
            'tel&eacute;fono'=> 'required',
            'correo electr&oacute;nico'=>'required|email',
            'twitter' => 'required|space',
            'facebook' => 'required|space',
            'pais de nacimiento'=>'required',
            'direcci&oacute;n de residencia'=>'required',
            'tel&eacute;fono de residencia'=>'required|digits_between:12,50',
            'direcci&oacute;n permanente'=>'required',
            'estados de ubicaci&oacute;n'=>'required',
            'ciudad de ubicaci&oacute;n'=>'required',      
        );

        if (array_key_exists('pais_nac',Input::all())==true) {
            $variables2['estados de nacimiento']=$estados_nac;
            $variables2['ciudad de nacimiento']=$ciudad_nac;
            $rules2['estados de nacimiento']='required';
            $rules2['ciudad de nacimiento']='required';
        }

        $validation2 = Validator::make($variables2, $rules2);
        if ($validation2->fails()){
            $error_personales=true;
            $message_personales = $validation2->messages();
        }else{
            $error_personales=false;
        }

        $variables3 = array(
            'especialidad'=>$g_especialidad,
           // 'fecha de ingreso' =>$fecha_ing,

           // 'n rusnies'=>$rusnies,
            'carrera que desea estudiar' =>$carrera_uba,
           // 'cursa otra instituci&oacute;n'=>$otros_estudio,
        );
        $rules3 = array(
            'especialidad' =>'required',
           // 'fecha de ingreso'=>'required',
           // 'n rusnies'=>'required',   
            'carrera que desea estudiar'=>'required',     
        );
      /*  if ((array_key_exists('g_especialidad',Input::all())==true) && Input::get('g_especialidad')==3){

            $variables3['exp especialidad']=$ex_especialidad;
            $rules3['exp especialidad']='required';
        }*/
        if ((array_key_exists('otros_estudio',Input::all())==true) && Input::get('otros_estudio')==1){
            $variables3['otra institucio']=$nom_institucion;
            //$variables3['otra carreras']=$otra_carreras;
           // $variables3['fecha de ingreso;']=$fecha_ing;
           // $variables3['nivel de estudio;']=$nivel_estudio;
           // $rules3['otra institucio']='required';
           // $rules3['otra carreras']='required';
            //$rules3['fecha de ingreso']='required';
            $rules3['nivel de estudio']='required';
        }
       /* if ((array_key_exists('se_graduo',Input::all())==true) && Input::get('se_graduo')==1){
            $variables3['nivel de estudio']=$nivel_estudio;

            $rules3['nivel de estudio']='required';
        }*/
        $validation3 = Validator::make($variables3, $rules3);
        if ($validation3->fails()){
            $error_datossecundaria=true;
            $message_datossecundaria = $validation3->messages();
        }/*else{
            $error_datossecundaria=false;
        }*/

        $message_error='Por favor revise los campos obligatorios';

        if ($error_periodo==true || $error_personales==true  ) {           
            $vista= Redirect::action('PreinscripcionlineaController@getIndex')
                ->withInput();
                if ($error_periodo==true) {
                    $vista->with('message_periodo',$message_periodo);
                    $vista->with('message_error_periodo',$message_error);
                }
                if ($error_personales==true) {
                    $vista->with('message_personales',$message_personales);
                    $vista->with('message_error_personales',$message_error);
                }
               /* if ($error_datossecundaria==true) {
                    $vista->with('message_datossecundaria',$message_datossecundaria);
                    $vista->with('message_error_datossecundaria',$message_error);
                }*/
                
                $vista->with('estados_nac_selected',$estados_nac);
                $vista->with('ciudad_nac_selected',$ciudad_nac);
                $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                return $vista;
        }else{

            $preinscripcion_config_activo= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                ->where('estado', '=', 1)
                ->get();

            if(count($preinscripcion_config_activo)==2){
                if ($tsolicitud==2){
                    $cod_reg=0;
                }else{
                    $cod_reg=2;
                }

                $preinscripcion_config= DB::table('config_web')
                    ->where('cod_modulo', '=', 'PREINSC')
                    ->where('cod_reg', '=', $cod_reg)
                    ->get();

                foreach($preinscripcion_config as $key ){
                    $lapso=$key->lapso;
                    // $nucleo_activo=$key->{'estado_'.$nucleo_uba}; 
                } 
            }elseif(count($preinscripcion_config_activo)==1){
                $preinscripcion_config= DB::table('config_web')
                    ->where('cod_modulo', '=', 'PREINSC')
                    ->where('estado', '=', 1)
                    ->get();

                foreach($preinscripcion_config as $key ){
                    $lapso=$key->lapso;
                    //$nucleo_activo=$key->{'estado_'.$nucleo_uba};
                    $cod_reg=$key->cod_reg;
                } 
            }

            $cont = DB::table('preinscritos')->where('lapso','=',20183)->where('numsobre','like',$nsobre.'-10'.$carrera_uba.'%')->count()+1;
            $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
            $celular = explode("-", $telefono);
            $nsobre = $nsobre.'-10'.$carrera_uba.'-'.$cont;
            $sobre_nucleo = explode("-", $nsobre);  

               //return  $sobre_nucleo[0];
            $validasobre=substr($nsobre, 3,3);

            if ($sobre_nucleo[0]==100 || $sobre_nucleo[0]==101){
               if ($sobre_nucleo[1]==101 || $sobre_nucleo[1]==102 || $sobre_nucleo[1]==103 || $sobre_nucleo[1]==104 || $sobre_nucleo[1]==105 || $sobre_nucleo[1]==106 || $sobre_nucleo[1]==107 || $sobre_nucleo[1]==110 || $sobre_nucleo[1]==220){

                    if($tsolicitud==1 and $validasobre==110 || $validasobre==220){
                        if ($validasobre==110){
                            $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Equivalencia';
                        }elseif ($validasobre==220){
                            $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Reincorporado';
                        }
                        
                        $vista= Redirect::action('PreinscripcionlineaController@getIndex')
                            ->withInput();
                            $vista->with('message_error_periodo',$message_error);
                            $vista->with('estados_nac_selected',$estados_nac);
                            $vista->with('ciudad_nac_selected',$ciudad_nac);
                            $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                            $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                        return $vista;
                    }elseif($tsolicitud==3 and $validasobre!=110){
                        if ($validasobre==110){
                            $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Equivalencia';
                        }elseif ($validasobre==220){
                            $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Reincorporado';
                        }
                        $vista= Redirect::action('PreinscripcionlineaController@getIndex')
                            ->withInput();
                            $vista->with('message_error_periodo',$message_error);
                            $vista->with('estados_nac_selected',$estados_nac);
                            $vista->with('ciudad_nac_selected',$ciudad_nac);
                            $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                            $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                        return $vista;
                    }elseif($tsolicitud==2 and $validasobre!=220){
                        if ($validasobre==110){
                            $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Equivalencia';
                        }elseif ($validasobre==220){
                            $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Reincorporado';
                        }
                        $vista= Redirect::action('PreinscripcionlineaController@getIndex')
                            ->withInput();
                            $vista->with('message_error_periodo',$message_error);
                            $vista->with('estados_nac_selected',$estados_nac);
                            $vista->with('ciudad_nac_selected',$ciudad_nac);
                            $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                            $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                        return $vista;
                    }else{
               
                        $verificar_preinsc = DB::table('preinscritos')
                        ->select('cedula')
                        ->where('cedula','=',$cedula)
                        ->where('carrera_cursar','=',$carrera_uba)
                        //->where('lapso','=',$lapso)
                        ->count();

                        if($verificar_preinsc==0){
                            //SQL para insertar los datos de la preinscripcion en la BD
                            $insertar=DB::table('preinscritos')
                            ->insert(array(
                                 'lapso'=> 20183,
                                'tipo_solicitud'=> $tsolicitud,
                                'numsobre'=>  $nsobre,
                                'cedula' => $cedula,
                                'apellidos' => $apellidos,
                                'nombres' => $nombres,
                                'nacionalidad' =>$tcedula,
                                'fecha_nac' => date_format(date_create($fecha),'Y-m-d'),
                                'sexo' => $tsexo,
                                'edo_civil' => $ecivil,
                                'email' => $email,
                                'pais_nac' =>$pais_nac,
                                'estado_nac_inter' =>$estados_nac,
                                'ciudad_nac_inter' =>$ciudad_nac,
                                'direccion_res' =>$direcc_r,
                                'direccion_per' =>$direcc_perm,
                                'estado_direccion_per' =>$estados_ubi,
                                'ciudad_direccion_per' =>$ciudad_ubi,
                                'nombre_institucion_bach' => $nom_institucion,
                                'especialidad_bach' =>$g_especialidad,
                                'otra_especialidad_bach' =>$ex_especialidad,
                                'rusnies' =>$rusnies,
                                'cod_celular' => $celular[0],
                                'celular' => $celular[1],
                                'carrera_cursar' =>$carrera_uba,
                                'twitter' => $twitter,
                                'facebook' => $facebook,
                                'periodo_ubic_inter' => $nivel_estudio,
                                'docum_ident_inter' => $doc1,
                                'foto_inter' => $doc2,
                                'titulo_bach_inter' => $doc3,
                                'notas_certi_bach_inter' => $doc5,
                                'rusnieu_internacional' =>$doc4,
                                'const_buena_conducta_inter' =>$doc6,
                                'notas_certi_otros_est_inter' =>$doc7,
                                'pensum_otros_estu_inter' => $doc8,
                                'programas_otros_estu_inter' =>$doc9,
                                'modalidad_carrera_cursar' => 3,
                                'turno_carrera_cursar' => 3,
                                'cod_reg' => 2,
                                'trabaja'=> 2,
                                'empresa_trab'=>2,
                                'direccion_trab'=>2,
                                'codtelf_trab'=>2,
                                'telf_trab'=>2,
                                'departamento_trab'=>2,
                                'cargo_trab'=>2,
                                'vehiculo'=>2,
                                'placa_veh'=>2,
                                'modelo_veh'=>2,
                                'marca_veh'=>2,
                                'cedula_rep'=>2,
                                'apellidos_rep'=>2,
                                'nombres_rep'=>2,
                                'edad_rep'=>2,
                                'direccion_rep'=>2,
                                'parentesco'=>2,
                                'codtelf_rep'=>2,
                                'telf_rep'=>2,
                                'tipo_institucion_bach'=>2,
                                'turno_bach'=>2,
                                'promedio_bach'=>2,
                                'estado_bach'=>2,
                               'ciudad_bach' =>2,
                                'actividad1'=>2,
                                'actividad2'=>2,
                                'actividad3'=>2,
                                'actividad4'=>2,
                                'cargos_bach' =>2,
                                'nombre_cargo_bach'=>2,
                                'orientacion_carrera'=>2,
                                'decidido_carrera'=>2,
                                'graduado_superior'=>2,
                                'estudios_distancia'=>2,
                                'depo_1'=>2,
                                'depo_2'=>2,
                                'depo_3'=>2,
                                'depo_4'=>2,
                                'depo_5'=>2,
                                'depo_6'=>2,
                                'depo_7'=>2,
                                'depo_8'=>2,
                                'depo_9'=>2,
                                'arti_1'=>2,
                                'arti_2'=>2,
                                'arti_3'=>2,
                                'arti_4'=>2,
                               'arti_5'=>2,
                                'arti_6'=>2,
                                'arti_7'=>2,
                                'arti_8'=>2,
                                'otras_1'=>2,
                                'otras_2'=>2,
                                'otras_3'=>2,
                                'otras_4'=>2,
                                'otras_5'=>2,
                                'otras_6'=>2,
                                'otras_7'=>2,
                                'otras_8'=>2,
                                'otras_9' =>2,
                                'donde_vivira'=>2,
                                'otro_donde_vivira' =>2,
                                'costa_estudios' =>2,
                                'otro_coste_estudios' =>2,
                                'ayudas' =>2,
                                'institucion_ayudas'=>2,
                                'ingreso_mensual'=>2,
                                'viven_padres'=>2,
                                'edo_civil_padres' =>2,
                                'persona_convive'=>2,
                                'nombre_persona_convive'=>2,
                                'adultos_hogar'=>2,
                                'ninos_hogar' =>2,
                                'hermanos' =>2,
                                'hermanos_primaria' =>2,
                                'hermanos_secundaria' =>2,
                                'hermanos_superior' =>2,
                                'hijos' =>2,
                                'hijos_primaria'=>2,
                                'hijos_secundaria' =>2,
                                'hijos_superior'=>2,
                                'fecha_cita'=>2,
                                'graduo_uba'=>2,
                                'inform_carrerasuba'=>2,
                                'fuma'=>2,
                                'desc_discapacidad'=>2,
                                'discapacidad7'=>2,
                                'discapacidad6'=>2,
                                'discapacidad5'=>2,
                                'discapacidad4'=>2,
                                'discapacidad3'=>2,
                                'discapacidad2'=>2,
                                'discapacidad1'=>2,
                                'edo_civil_padres_otro'=>2,
                                'ciudad'=>2,
                                'estado'=>2,
                                'pais'=>2,
                                'sueldo_trab'=>2,
                                'fecha_solicitud'=>date('Y-m-d'),
                                'nombre_institucion_bach'=>2,
                                'ciudad_nac'=>2,
                                'estado_nac'=>2
                               // 'fecha_ing_otrosestu_inter' =>date_format(date_create($fecha_g),'Y-m-d')
                            ));

                        }else{
                            //aqui
                           
                            $title = 'Imprimir Preinscripcion';
                            return View::make('preinscripcion.imprimirpreinterna', array(
                                'title' => $title, 
                                'cedula'=>$cedula
                                //'lapso'=>$lapso
                            ));
                        }
                       
                        $title = 'Imprimir Preinscripcion';
                        return View::make('preinscripcion.imprimirpreinterna', array(
                            'title' => $title, 
                            'cedula'=>$cedula,
                            'lapso'=> 20183                          
                        ));
                    }
                
                }else{
                       //aqu1
                      //return $sobre_nucleo[0];
                    $message_error='Por favor revise n&uacute;mero de sobre';
                    $vista= Redirect::action('PreinscripcionlineaController@getIndex')
                        ->withInput();
                        $vista->with('message_error_personales',$message_error);
                        $vista->with('estados_nac_selected',$estados_nac);
                        $vista->with('ciudad_nac_selected',$ciudad_nac);
                        $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                        $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                   return $vista;
                }
            }

        }
        
    }

    public function postProcesarmodificacion()
    {
        $tsolicitud=Input::get('tsolicitud');
        $fecha=Input::get('fecha');
        $tcedula=Input::get('tcedula');
        $cedula=Input::get('cedula');
        $nsobre=Input::get('nsobre');
        $nombres=Input::get('nombres');
        $apellidos=Input::get('apellidos');
        $tsexo=Input::get('tsexo');
        $ecivil=Input::get('ecivil');
        $telefono=Input::get('telefono');
        $email=Input::get('email');
        $twitter=Input::get('twitter');
        if ($twitter == '') {
            $twitter = 'NO';
        }
        $facebook=Input::get('facebook');
        if ($facebook == '') {
            $facebook = 'NO';
        }
       
        $pais_nac=Input::get('pais_nac');
        if ($pais_nac==''){
            $estados_nac=null;
            $ciudad_nac=null;
        }else{
            $estados_nac=Input::get('estados_nac');
            if ($estados_nac==''){
                $estados_nac=null;
                $ciudad_nac=null;
            }else{        
                $ciudad_nac=Input::get('ciudad_nac');
                if ($ciudad_nac==''){
                    $ciudad_nac=null;
                }else{        
                    $ciudad_nac=Input::get('ciudad_nac');
                }
            }
        }


        //$direcc_r=Input::get('direcc_r');
        $direcc_perm=Input::get('direcc_perm');
        $estados_ubi=Input::get('estados_ubi');
        $ciudad_ubi=Input::get('ciudad_ubi');
        $edad=Input::get('edad');
        //$telefono_r=Input::get('telefono_r');
       // $tipo_institucion=Input::get('tipo_institucion');
        $nom_institucion=Input::get('nom_institucion');
        //$turno_institucion=Input::get('turno_institucion');
        $g_especialidad=Input::get('g_especialidad');

        if ($g_especialidad==3){
            $ex_especialidad=Input::get('ex_especialidad');
        }else{
            $ex_especialidad=null;
        }
       
       // $fecha_g=Input::get('fecha_g');
        $estados_estu_secu=Input::get('estados_estu_secu');
        $ciudad_estu_secu=Input::get('ciudad_estu_secu');

        $rusnies=Input::get('rusnies');
        $carreras=Input::get('carreras');
        $carrera_uba=Input::get('carrera_uba');
        $otros_estudio=Input::get('otros_estudio');

      $otros_estudio=Input::get('otros_estudio');
        if ($otros_estudio==1){
            $nom_institucion=Input::get('nom_institucion');
            $otra_carreras=Input::get('otra_carreras');
            $fecha_ing=Input::get('fecha_ing');
            $nivel_estudio=Input::get('nivel_estudio'); 
        }else{
            $nom_institucion=null;
            $otra_carreras=null;
            $fecha_ing=null;
            $nivel_estudio = null;
        }

        $variables1 = array(
            'tipo de solicitud' =>$tsolicitud,
            'fecha de nacimiento'=>$fecha,
            'nacionalidad'=>$tcedula,
            'c&eacute;dula'=>$cedula
        );
        //reglas para validar cada variable
        $rules1 = array(
            'tipo de solicitud' => 'required',
            'fecha de nacimiento'=>'required',
            'nacionalidad'=>'required',
            'c&eacute;dula' => 'required|numeric|digits_between:6,11',
        );
         
        //en el caso que alguna vadilacion falle retornaremos al formulario.
        $validation1 = Validator::make($variables1, $rules1);
        if ($validation1->fails()){
            $error_periodo=true;
            $message_periodo = $validation1->messages();
        }else{
            $error_periodo=false;
        }

        $variables2 = array(
            'n&uacute;mero de sobre' => $nsobre,
            'primer y segundo nombre'=> $nombres,
            'primer y segundo apellido'=> $apellidos,
            'sexo'=>$tsexo,
            'estado civil'=>$ecivil,
            'tel&eacute;fono'=>$telefono,
            'correo electr&oacute;nico'=>$email,
            'twitter' => $twitter,
            'facebook' => $facebook,
            'pais de nacimiento'=>$pais_nac,
           // 'direcci&oacute;n de residencia'=>$direcc_r,
            'tel&eacute;fono de residencia'=>$telefono_r,
            'direcci&oacute;n permanente'=>$direcc_perm,
            'estados de ubicaci&oacute;n'=>$estados_ubi,
            'ciudad de ubicaci&oacute;n'=>$ciudad_ubi,             
        );

        //reglas para validar cada variable
        $rules2 = array(
            'n&uacute;mero de sobre' => 'required|digits_between:8,13',
            'primer y segundo nombre' => 'required',
            'primer y segundo apellido'=> 'required',
            'sexo'=> 'required',
            'estado civil' => 'required',
            'tel&eacute;fono'=> 'required',
            'correo electr&oacute;nico'=>'required|email',
            'twitter'=> 'required|space',
            'facebook'=> 'required|space',
            'pais de nacimiento'=>'required',
            //'direcci&oacute;n de residencia'=>'required',
            'tel&eacute;fono de residencia'=>'required|digits_between:12,12',
            'direcci&oacute;n permanente'=>'required',
            'estados de ubicaci&oacute;n'=>'required',
            'ciudad de ubicaci&oacute;n'=>'required',      
        );

        if (array_key_exists('pais_nac',Input::all())==true) {
            $variables2['estados de nacimiento']=$estados_nac;
            $variables2['ciudad de nacimiento']=$ciudad_nac;
            $rules2['estados de nacimiento']='required';
            $rules2['ciudad de nacimiento']='required';
        }


        $validation2 = Validator::make($variables2, $rules2);
        if ($validation2->fails()){
            $error_personales=true;
            $message_personales = $validation2->messages();
        }else{
            $error_personales=false;
        }

        $variables3 = array(
            //'tipo de instituci&oacute;n' =>$tipo_institucion,
            'nombre de la instituci&oacute;n'=>$nom_institucion,
            //'turno que estudi&oacute;'=>$turno_institucion,
            'especialidad'=>$g_especialidad,
            'fecha de graduaci&oacute;n' =>$fecha_g,
            'estados de ubicaci&oacute;n'=>$estados_ubi,
            'ciudad de ubicaci&oacute;n'=>$ciudad_ubi,
            'n rusnies'=>$rusnies,
            'carrera que desea estudiar' =>$carrera_uba,
          
          
            'cursa otra instituci&oacute;n'=>$otros_estudio,
        );
        $rules3 = array(
            //'tipo de instituci&oacute;n' =>'required',
            'nombre de la instituci&oacute;n'=>'required',
           // 'turno que estudi&oacute;'=>'required',
            'especialidad' =>'required',
            'fecha de graduaci&oacute;n'=>'required',
            'estados de ubicaci&oacute;n'=>'required',
            'ciudad de ubicaci&oacute;n'=>'required',
            'n rusnies'=>'required',
            'carrera que desea estudiar'=>'required',
           // 'turno carrera'=>'required',
            //'modalidad carrera' =>'required',
           //'nucleo carrera'=>'required',  
            'cursa otra instituci&oacute;n'=>'required',          
        );

        if ((array_key_exists('g_especialidad',Input::all())==true) && Input::get('g_especialidad')==3){

            $variables3['exp especialidad']=$ex_especialidad;
            $rules3['exp especialidad']='required';
        }
        if ((array_key_exists('otros_estudio',Input::all())==true) && Input::get('otros_estudio')==1){
            $variables3['otra institucio']=$otra_institucion;
             $variables3['fecha de ingreso']=$fecha_ing;
            $variables3['otra carreras']=$tipo_institucion;
            $variables3['se gradu&oacute;']=$se_graduo;

            $rules3['otra institucio']='required';
            $rules3['otra carreras']='required';
            $rules3['se gradu&oacute;']='required';
        }

        if ((array_key_exists('se_graduo',Input::all())==true) && Input::get('se_graduo')==1){
            $variables3['nivel de estudio']=$nivel_estudio;

            $rules3['nivel de estudio']='required';
        }

        $validation3 = Validator::make($variables3, $rules3);
        if ($validation3->fails()){
            $error_datossecundaria=true;
            $message_datossecundaria = $validation3->messages();
        }else{
            $error_datossecundaria=false;
        }

        $message_error='Por favor revise los campos incorrectos';

        if ($error_periodo==true || $error_personales==true || $error_datossecundaria==true ) {           
            $data=new stdClass();
            $data->cedula=$cedula;
            $data->nsobre=$nsobre;
            $vista= Redirect::action('PreinscripcionlineaController@getModificarpre', array('data'=>Crypt::Encrypt($data)))
                ->withInput();
                if ($error_periodo==true) {
                    $vista->with('message_periodo',$message_periodo);
                    $vista->with('message_error_periodo',$message_error);
                }
                if ($error_personales==true) {
                    $vista->with('message_personales',$message_personales);
                    $vista->with('message_error_personales',$message_error);
                }
                if ($error_datossecundaria==true) {
                    $vista->with('message_datossecundaria',$message_datossecundaria);
                    $vista->with('message_error_datossecundaria',$message_error);
                }
                
                $vista->with('estados_nac_selected',$estados_nac);
                $vista->with('ciudad_nac_selected',$ciudad_nac);
                $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                return $vista;
        }else{
            $nsobre = $nsobre.'-10'.$carreras_uba.'-0000';

            //$sobre_nucleo = explode("-", $nsobre);       
            $preinscripcion_config= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                ->get();

               // return  $sobre_nucleo[0];


            foreach($preinscripcion_config as $key ){
                $lapso=$key->lapso;
               
            }  

             if ($sobre_nucleo[0]==100 || $sobre_nucleo[0]==101){

                if ($sobre_nucleo[1]==101 || $sobre_nucleo[1]==102 || $sobre_nucleo[1]==103 || $sobre_nucleo[1]==104 || $sobre_nucleo[1]==105 || $sobre_nucleo[1]==106 || $sobre_nucleo[1]==107 || $sobre_nucleo[1]==110 || $sobre_nucleo[1]==220){

                            $preinscripcion_config= DB::table('config_web')
                                ->select('lapso')
                                ->where('cod_modulo', '=', 'PREINSC')
                                ->where('cod_reg','=',2)
                                ->orderby('lapso','DESC')
                                ->first();

                            $lapso = $preinscripcion_config->lapso;
                        
                            $borrar=DB::table('preinscritos')
                                ->where('cedula','=',$cedula)
                                ->where('numsobre','=',$nsobre)
                                ->where('lapso','=',$lapso)
                                ->delete();
                            //SQL para insertar los datos de la preinscripcion en la BD
                            $insertar=DB::table('preinscritos')
                            ->insert(array(
                                 'lapso'=> 20183,
                                'tipo_solicitud'=> $tsolicitud,
                                'numsobre'=>  $nsobre,
                                'cedula' => $cedula,
                                'apellidos' => $apellidos,
                                'nombres' => $nombres,
                                'nacionalidad' =>$tcedula,
                                'fecha_nac' => date_format(date_create($fecha),'Y-m-d'),
                                'sexo' => $tsexo,
                                'edo_civil' => $ecivil,
                                'email' => $email,
                                'pais_nac' =>$pais_nac,
                                'estado_nac_inter' =>$estados_nac,
                                'ciudad_nac_inter' =>$ciudad_nac,
                                'direccion_res' =>$direcc_r,
                                'direccion_per' =>$direcc_perm,
                                'estado_direccion_per' =>$estados_ubi,
                                'ciudad_direccion_per' =>$ciudad_ubi,
                                'nombre_institucion_bach' => $nom_institucion,
                                'especialidad_bach' =>$g_especialidad,
                                'otra_especialidad_bach' =>$ex_especialidad,
                                'rusnies' =>$rusnies,
                                'cod_celular' => $celular[0],
                                'celular' => $celular[1],
                                'carrera_cursar' =>$carrera_uba,
                                'twitter' => $twitter,
                                'facebook' => $facebook,
                                'periodo_ubic_inter' => $nivel_estudio,
                                'docum_ident_inter' => $doc1,
                                'foto_inter' => $doc2,
                                'titulo_bach_inter' => $doc3,
                                'notas_certi_bach_inter' => $doc5,
                                'rusnieu_internacional' =>$doc4,
                                'const_buena_conducta_inter' =>$doc6,
                                'notas_certi_otros_est_inter' =>$doc7,
                                'pensum_otros_estu_inter' => $doc8,
                                'programas_otros_estu_inter' =>$doc9,
                                'modalidad_carrera_cursar' => 3,
                                'turno_carrera_cursar' => 3,
                                'cod_reg' => 2,
                                'trabaja'=> 2,
                                'empresa_trab'=>2,
                                'direccion_trab'=>2,
                                'codtelf_trab'=>2,
                                'telf_trab'=>2,
                                'departamento_trab'=>2,
                                'cargo_trab'=>2,
                                'vehiculo'=>2,
                                'placa_veh'=>2,
                                'modelo_veh'=>2,
                                'marca_veh'=>2,
                                'cedula_rep'=>2,
                                'apellidos_rep'=>2,
                                'nombres_rep'=>2,
                                'edad_rep'=>2,
                                'direccion_rep'=>2,
                                'parentesco'=>2,
                                'codtelf_rep'=>2,
                                'telf_rep'=>2,
                                'tipo_institucion_bach'=>2,
                                'turno_bach'=>2,
                                'promedio_bach'=>2,
                                'estado_bach'=>2,
                               'ciudad_bach' =>2,
                                'actividad1'=>2,
                                'actividad2'=>2,
                                'actividad3'=>2,
                                'actividad4'=>2,
                                'cargos_bach' =>2,
                                'nombre_cargo_bach'=>2,
                                'orientacion_carrera'=>2,
                                'decidido_carrera'=>2,
                                'graduado_superior'=>2,
                                'estudios_distancia'=>2,
                                'depo_1'=>2,
                                'depo_2'=>2,
                                'depo_3'=>2,
                                'depo_4'=>2,
                                'depo_5'=>2,
                                'depo_6'=>2,
                                'depo_7'=>2,
                                'depo_8'=>2,
                                'depo_9'=>2,
                                'arti_1'=>2,
                                'arti_2'=>2,
                                'arti_3'=>2,
                                'arti_4'=>2,
                               'arti_5'=>2,
                                'arti_6'=>2,
                                'arti_7'=>2,
                                'arti_8'=>2,
                                'otras_1'=>2,
                                'otras_2'=>2,
                                'otras_3'=>2,
                                'otras_4'=>2,
                                'otras_5'=>2,
                                'otras_6'=>2,
                                'otras_7'=>2,
                                'otras_8'=>2,
                                'otras_9' =>2,
                                'donde_vivira'=>2,
                                'otro_donde_vivira' =>2,
                                'costa_estudios' =>2,
                                'otro_coste_estudios' =>2,
                                'ayudas' =>2,
                                'institucion_ayudas'=>2,
                                'ingreso_mensual'=>2,
                                'viven_padres'=>2,
                                'edo_civil_padres' =>2,
                                'persona_convive'=>2,
                                'nombre_persona_convive'=>2,
                                'adultos_hogar'=>2,
                                'ninos_hogar' =>2,
                                'hermanos' =>2,
                                'hermanos_primaria' =>2,
                                'hermanos_secundaria' =>2,
                                'hermanos_superior' =>2,
                                'hijos' =>2,
                                'hijos_primaria'=>2,
                                'hijos_secundaria' =>2,
                                'hijos_superior'=>2,
                                'fecha_cita'=>2,
                                'graduo_uba'=>2,
                                'inform_carrerasuba'=>2,
                                'fuma'=>2,
                                'desc_discapacidad'=>2,
                                'discapacidad7'=>2,
                                'discapacidad6'=>2,
                                'discapacidad5'=>2,
                                'discapacidad4'=>2,
                                'discapacidad3'=>2,
                                'discapacidad2'=>2,
                                'discapacidad1'=>2,
                                'edo_civil_padres_otro'=>2,
                                'ciudad'=>2,
                                'estado'=>2,
                                'pais'=>2,
                                'sueldo_trab'=>2,
                                'fecha_solicitud'=>date('Y-m-d'),
                                'nombre_institucion_bach'=>2,
                                'ciudad_nac'=>2,
                                'estado_nac'=>2
                            ));
                        $title = 'Imprimir Preinscripcion';
                        return View::make('preinscripcion.imprimirpreinterna', array(
                            'title' => $title, 
                            'cedula'=>$cedula,
                            'lapso'=>$lapso                          
                        ));
                    }
                }else{
                $message_error='Verifique el N&uacute;mero de Sobre...';
                $vista= Redirect::action('PreinscripcionlineaController@getModificarpreinscripcion')
                    ->withInput();
                    $vista->with('message_error_personales',$message_error);
                    $vista->with('estados_nac_selected',$estados_nac);
                    $vista->with('ciudad_nac_selected',$ciudad_nac);
                    $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                    $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                return $vista;
                } 
        }   
    }

    public function postImprimir()
    {
        $cedula = Input::get('cedula');

         //SQL para consultar  el lapso y el regimen grabado
        $preinscripcion_config = DB::select('SELECT cod_reg,lapso FROM preinscritos WHERE cedula=? AND EXISTS (SELECT * FROM config_web WHERE lapso = preinscritos.lapso AND estado=1)',array($cedula));

        foreach($preinscripcion_config as $key ){
            $lapso=$key->lapso;
            $cod_reg=$key->cod_reg;
        }  

        $db=DB::table('preinscritos')
            ->where('cedula','=',$cedula)
            //->where('lapso','=',$lapso)
            ->get();

        $title="PLANILLA DE SOLICITUD DE REINCORPORACI&Oacute;N";
        $html = View::make('reportes.preinscripcion.planillainternacional',array(
            'title' => $title,
            'value'=>$db
        ));

        return PDF::load($html, 'letter', 'portrait')->show($filename = 'Planilla_de_Inscripcion');
    }
    public function getReimprimirglobal()
    {

        $title = 'Imprimir Preinscripcion';
        return View::make('preinscripcion.reimprimirglobal', array(
            'title' => $title                        
        ));
    }

    public function postReimprimir()
    {
        $cedula=Input::get('cedula');
        $nsobre=Input::get('nsobre');

        $variables1 = array(
            'c&eacute;dula'=>$cedula,
            'n&uacute;mero de sobre' => $nsobre,
        );
        //reglas para validar cada variable
        $rules1 = array(
            'c&eacute;dula' => 'required|numeric|digits_between:6,11',
            'n&uacute;mero de sobre' => 'required|digits_between:8,13',
        );

        $validation1 = Validator::make($variables1, $rules1);
        if ($validation1->fails()){
            $error_reimprimir=true;
            $message_reimprimir = $validation1->messages();
        }else{
            $error_reimprimir=false;
        }

        $message_error='Por favor revise los campos incorrectos';
        if ($error_reimprimir==true ) {
            $vista= Redirect::action('PreinscripcionlineaController@getReimprimirglobal')
                ->withInput();
                if ($error_reimprimir==true) {
                    $vista->with('message_reimprimir',$message_reimprimir);
                    $vista->with('message_error_reimprimir',$message_error);
                }
            return $vista;
        }

        $preinscripcion_config= DB::table('config_web')
                        ->where('cod_modulo', '=', 'PREINSC')
                        ->where('estado', '=', '1')
                        ->get();

        foreach($preinscripcion_config as $key ){
            $lapso=$key->lapso;                
            $cod_reg=$key->cod_reg;
            if ($cod_reg==0){
                $lapso_reg=$key->lapso;
            }else{
                $lapso_trim=$key->lapso;
            }
        } 

        if(count($preinscripcion_config)==2){
            $db=DB::select('select * from preinscritos where cedula = ? and numsobre = ? and (lapso = ? or lapso = ?) limit 1',array($cedula,$nsobre,$lapso_reg,$lapso_trim));
   
        }elseif(count($preinscripcion_config)==1){
            $preinscripcion_config= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                ->where('estado', '=', 1)
                ->get();

            foreach($preinscripcion_config as $key ){
                $lapso=$key->lapso;
                $cod_reg=$key->cod_reg;
            } 
                $db=DB::table('preinscritos')
                ->where('cedula','=',$cedula)
                ->where('numsobre',$nsobre)
                ->where('lapso','=',$lapso)                
                ->get();
           
        }elseif(count($preinscripcion_config)==0){
           
            $db=DB::select('select * from preinscritos where cedula = ? and numsobre = ? ORDER BY fecha_solicitud DESC limit 1',array($cedula,$nsobre));

        } 

        if(count($db)>0){            
            $title="PLANILLA DE SOLICITUD DE REINCORPORACI&Oacute;N";
            $html = View::make('reportes.preinscripcion.planillainternacional',array(
                'title' => $title,
                'value'=>$db
            ));

            return PDF::load($html, 'letter', 'portrait')
                ->show($filename = 'Planilla_de_Inscripcion');
        }else{
            $message_error='No Coinciden los datos o no ha realizado ninguna Preinscripci&oacute;n';
            $vista= Redirect::action('PreinscripcionlineaController@getReimprimirglobal')
                ->withInput();
                $vista->with('message_error_reimprimir',$message_error);
            return $vista;
        } 
    }

    public function getModificarpreinscripcion()
    {
        $title = 'Imprimir Preinscripcion';
        return View::make('preinscripcion.modificarpreinscripcion', array(
            'title' => $title                        
        ));
    }

    public function postModificarpreinscripcion()
    {
        $cedula=Input::get('cedula');
        $nsobre=Input::get('nsobre');

        $preinscripcion_config= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                ->where('estado', '=', '1')
                ->get();

            foreach($preinscripcion_config as $key ){
                $lapso=$key->lapso;                
                $cod_reg=$key->cod_reg;
                if ($cod_reg==0){
                    $lapso_reg=$key->lapso;
                }else{
                    $lapso_trim=$key->lapso;
                }
            } 

        if(count($preinscripcion_config)==2){
                $db=DB::table('preinscritos')
                ->where('cedula','=',$cedula)
                ->where('numsobre',$nsobre)
                ->where('lapso',$lapso_reg)
                ->orWhere('lapso', $lapso_trim)
                ->first(); 
        }elseif(count($preinscripcion_config)==1){
            $preinscripcion_config= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                ->where('estado', '=', 1)
                ->get();


            foreach($preinscripcion_config as $key ){
                $lapso=$key->lapso;
                $cod_reg=$key->cod_reg;
            } 
                $db=DB::table('preinscritos')
                ->where('cedula','=',$cedula)
                ->where('numsobre',$nsobre)
                ->where('lapso','=',$lapso)                
                ->first();

        }elseif(count($preinscripcion_config)==0){
            $db=DB::table('preinscritos')
            ->where('cedula','=',$cedula)
            ->where('numsobre',$nsobre)
            ->orderby('lapso','DESC')
            ->first();

        } 

        $variables1 = array(
            'c&eacute;dula'=>$cedula,
            'n&uacute;mero de sobre' => $nsobre,
        );
        //reglas para validar cada variable
        $rules1 = array(
            'c&eacute;dula' => 'required|numeric|digits_between:6,11',
            'n&uacute;mero de sobre' => 'required|digits_between:8,13',
        );

        $validation1 = Validator::make($variables1, $rules1);
        if ($validation1->fails()){
            $error_modificarpreinscripcion=true;
            $message_modificarpreinscripcion = $validation1->messages();
        }else{
            $error_modificarpreinscripcion=false;
        }

        $message_error='Por favor revise los campos incorrectos';
        if ($error_modificarpreinscripcion==true ) {
            $vista= Redirect::action('PreinscripcionlineaController@getModificarpreinscripcion')
                ->withInput();
                if ($error_modificarpreinscripcion==true) {
                    $vista->with('message_modificarpreinscripcion',$message_modificarpreinscripcion);
                    $vista->with('message_error_modificarpreinscripcion',$message_error);
                }
            return $vista;
        }

        if(count($db)>0){
            $data=new stdClass();
            $data->cedula=$cedula;
            $data->nsobre=$nsobre;
            return Redirect::action('PreinscripcionlineaController@getModificarpre', array('data'=>Crypt::Encrypt($data)));
                   
        }else{
            $message_error='No Coinciden los datos o no ha realizado ninguna Preinscripci&oacute;n';
            $vista= Redirect::action('PreinscripcionlineaController@getModificarpreinscripcion')
                ->withInput();
                $vista->with('message_error_modificarpreinscripcion',$message_error);
            return $vista;
        }
    }

    public function getModificarpre($data)
    {
        $data=Crypt::decrypt($data);
        $cedula=$data->cedula;
        $nsobre=$data->nsobre;

        if(Session::has('message_error_periodo')){
            $tab_active='periodo';
        }elseif(Session::has('message_error_personales')){
            $tab_active='personales';
        }elseif(Session::has('message_error_datossecundaria')){
            $tab_active='datossecundaria'; 
        }else{
            $tab_active='periodo';
        }

        $tsolicitud=array(
            ''=>'SELECCIONAR',
            '1'=>'POR PRIMERA VEZ O REINGRESO',
            '2'=>'REINCORPORACI&Oacute;N',
            '3'=>'EQUIVALENCIA');

        $tcedula=array(
            ''=>'SELECCIONAR',
            'v'=>'V',
            'e'=>'E');

        $tsexo=array(
            ''=>'SELECCIONAR',
            'M'=>'MASCULINO',
            'F'=>'FEMENINO');
        $ecivil=array(
            ''=>'SELECCIONAR',
            'S'=>'SOLTERO',
            'C'=>'CASADO',
            'D'=>'DIVORCIADO',
            'V'=>'VIUDO');

        $pais=array(
            ''=>'SELECCIONAR',
            '1'=>'VENEZUELA',
            '2'=>'OTROS');

        $pais_nac = array('' => 'SELECCIONAR' , ) + DB::table('db_paises')->lists('des_paises', 'ID');

       // $estados = array('' => 'SELECCIONAR' , ) + DB::table('db_estados')->where('id_pais', '230')->lists('des_estados', 'ID');

       /* $ciudad=array(
            ''=>'SELECCIONAR'
            );*/

       /* $estados_nac=array(
            ''=>'SELECCIONAR'
            );*/

       /* $ciudad_nac=array(
            ''=>'SELECCIONAR'
            );*/

      /*  $ciudad_ubi=array(
            ''=>'SELECCIONAR'
            );*/

      /*  $ciudad_cs=array(
            ''=>'SELECCIONAR'
            );*/

        $especialidad=array(
            ''=>'SELECCIONAR',
            '1'=>'CIENCIAS',
            '2'=>'HUMANIDADES',
            '3'=>'OTRA');

       /* $turno=array(
            ''=>'SELECCIONAR',
            '1'=>'MAÑANA',
            '2'=>'TARDE',
            '3'=>'NOCHE',
            '4'=>'REGIMEN FLEXIBLE');*/

      /*  $modalidad=array(
            ''=>'SELECCIONAR',
            '1'=>'PRESENCIAL',
            '2'=>'SEMI-PRESENCIAL',
            '3' =>'VIRTUAL');*/

        /*$nucleo=array(
            ''=>'SELECCIONAR',
            '0'=>'SAN JOAQUIN',
            '1'=>'SAN ANTONIO DE LOS ALTOS',
            '2'=>'APURE',
            '3'=>'PUERTO ORDAZ');*/

        $datos_carrera=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');

        $carreras = array('' => 'SELECCIONAR' , ) + DB::table('carreras')->lists('carrera', 'id');

        $select=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');

        $nivel_estudio =array(
            ''=>'SELECCIONAR',
            '1'=>'T.S.U',
            '2'=>'Profesional'); 

        $est_pre=DB::table('preinscritos')
            ->where('cedula','=',$cedula)
            ->where('numsobre',$nsobre)
            ->first();

        $carreras_uba = array('' => 'SELECCIONAR') + DB::table('carreras_uba')->where('id','=',$est_pre->carrera_cursar)->lists('carrera', 'id');

       // $turno = array('' => 'SELECCIONAR') + DB::table('turno_carreras')->where('carrera','=',$est_pre->carrera_cursar)->lists('turno', 'id_turno');

       // $modalidad = array('' => 'SELECCIONAR') + DB::table('modalidad_carreras')->where('carrera','=',$est_pre->carrera_cursar)->lists('modalidad','id_modalidad');    
             // return $data;
         $title = 'Imprimir Preinscripcion';
        return View::make('preinscripcion.modificardatos', array(
               'title'=>$title,
               'tab'=>$tab_active,
               'tsolicitud'=>$tsolicitud,
               'tcedula'=>$tcedula,
               'tsexo'=>$tsexo,
               'ecivil'=>$ecivil,
               'pais'=>$pais,
               'pais_nac'=>$pais_nac,
               'estados' => $estados,
               'estados_nac_inter' => $estados_nac,
               'ciudad'=>$ciudad,
               'ciudad_nac_inter'=>$ciudad_nac,
               'ciudad_ubi'=>$ciudad_ubi,
               'especialidad'=>$especialidad,
               'ciudad_cs'=>$ciudad_cs,
               'carreras_uba'=>$carreras_uba,
               'carreras'=>$carreras,
               'datos_carrera'=>$datos_carrera,
               'select'=>$select,
               'nivel_estudio' => $nivel_estudio,
               'preinscrito'=>$est_pre
        ));
    }
}
