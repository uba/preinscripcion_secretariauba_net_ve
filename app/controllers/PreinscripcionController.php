<?php
class PreinscripcionController extends BaseController {

    public function getInicio()
    {
        if(Session::has('message_error_periodo')){
            $tab_active='periodo';
        }elseif(Session::has('message_error_personales')){
            $tab_active='personales';
        }elseif(Session::has('message_error_datossecundaria')){
            $tab_active='datossecundaria'; 
        }elseif(Session::has('message_error_datossocioeconomicos')){
            $tab_active='datossocioeconomicos';
        }else{
            $tab_active='periodo';
        }        

        $tcedula=array(
            ''=>'SELECCIONAR',
            'v'=>'V',
            'e'=>'E');

        $tsexo=array(
            ''=>'SELECCIONAR',
            'M'=>'MASCULINO',
            'F'=>'FEMENINO');
        $ecivil=array(
            ''=>'SELECCIONAR',
            'S'=>'SOLTERO',
            'C'=>'CASADO',
            'D'=>'DIVORCIADO',
            'V'=>'VIUDO');

        $trabaja=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');

        $vehiculo=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');

        $pais=array(
            ''=>'SELECCIONAR',
            '1'=>'VENEZUELA',
            '2'=>'OTROS');

        $convenio=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');
        $nucleo=array(
            ''=>'SELECCIONAR',
            '0'=>'SEDE - SAN JOAQU&Iacute;N DE TURMERO',
            '1'=>'N&Uacute;CLEO - SAN ANTONIO DE LOS ALTOS',
            '2'=>'N&Uacute;CLEO - APURE',
            '3'=>'N&Uacute;CLEO - PUERTO ORDAZ');



        $pais_nac = array('' => 'SELECCIONAR' , ) + DB::table('db_paises')->lists('des_paises', 'ID');

        $estados = array('' => 'SELECCIONAR' , ) + DB::table('db_estados')->where('id_pais', '230')->lists('des_estados', 'ID');

        $ciudad=array(
            ''=>'SELECCIONAR'
            );

        $estados_nac=array(
            ''=>'SELECCIONAR'
            );

        $ciudad_nac=array(
            ''=>'SELECCIONAR'
            );

        $ciudad_ubi=array(
            ''=>'SELECCIONAR'
            );

        $ciudad_cs=array(
            ''=>'SELECCIONAR'
            );

        $t_institucion=array(
            ''=>'SELECCIONAR',
            '1'=>'PRIVADO',
            '2'=>'PUBLICO',
            '3'=>'MISION');

        $turno_institucion=array(
            ''=>'SELECCIONAR',
            '1'=>'MAÑANA',
            '2'=>'TARDE',
            '3'=>'NOCHE');

        $especialidad=array(
            ''=>'SELECCIONAR',
            '1'=>'CIENCIAS',
            '2'=>'HUMANIDADES',
            '3'=>'OTRA');

        $carreras_uba = array('' => 'SELECCIONAR' , );

        $turno=array(''=>'SELECCIONAR');        

        $modalidad=array(''=>'SELECCIONAR');

        $informacion= array(
            '' =>'SELECCIONAR',
            '1' => 'REFERENCIA DE OTRO ESTUDIANTE',
            '2' => 'REDES SOCIALES',
            '3' => 'COACHING INTERNACIONAL',
            '4' => 'CONVENIO INTERNACIONAL',
            '5' => 'CONVENIO CREATEC',
            '6' => 'OTROS');

        $datos_carrera=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');

        $institucion = array('' => 'SELECCIONAR' , ) + DB::table('instituciones')->lists('instituciones', 'id');

        $carreras = array('' => 'SELECCIONAR' , ) + DB::table('carreras')->lists('carrera', 'id');

        $vivira_est=array(
            ''=>'SELECCIONAR',
            '1'=>'CON SUS PADRES',
            '2'=>'OTRO FAMILIAR',
            '3'=>'VIVIENDO ALQUILADA O PROPIA',
            '4'=>'RESIDENCIA O PENSIÓN',
            '5'=>'OTRA VIVIENDA');

        $costea_g=array(
            ''=>'SELECCIONAR',
            '1'=>'PADRE',
            '2'=>'MADRE',
            '3'=>'BECA',
            '4'=>'UD. MISMO',
            '5'=>'OTROS');

        $disfruta_d=array(
            ''=>'SELECCIONAR',
            '1'=>'CRÉDITO EDUCATIVO',
            '2'=>'BECA',
            '3'=>'MEDIA BECA',
            '4'=>'NADA',
            '5'=>'OTRO TIPO DE AYUDA');

        $ingreso_men=array(
            ''=>'SELECCIONAR',
            '1'=>'DESDE 5000 HASTA 8000',
            '2'=>'DESDE 8001 HASTA 11000',
            '3'=>'DESDE 11001 HASTA 14000',
            '4'=>'DESDE 14001 HASTA 17000',
            '5'=>'DESDE 17001 Ó MÁS');

        $viven_padres=array(
            ''=>'SELECCIONAR',
            '1'=>'AMBOS',
            '2'=>'MADRE',
            '3'=>'PADRE',
            '4'=>'NINGUNO');

        $ecivil_p=array(
            ''=>'SELECCIONAR',
            '1'=>'SOLTERO',
            '2'=>'CASADO',
            '3'=>'DIVORCIADO',
            '4'=>'VIUDO');

        $vive_con=array(
            ''=>'SELECCIONAR',
            '1'=>'AMBOS PADRES',
            '2'=>'PADRE',
            '3'=>'MADRE',
            '4'=>'ABUELO(A)',
            '5'=>'TIO(A)',
            '6'=>'HERMANO(A)',
            '7'=>'OTRA PERSONA');

        $parentesco=array(
            ''=>'SELECCIONAR',
            '1'=>'PADRE',
            '2'=>'MADRE',
            '3'=>'TIO(A)',
            '4'=>'HERMANO(A)',
            '5'=>'ABUELO(A)',
            '6'=>'PADRASTRO',
            '7'=>'MADRASTRA',
            '8'=>'AMIGO DE LA FAMILIA',            
            '9'=>'OTRA PERSONA...');

        $escribe=array(
            ''=>'SELECCIONAR',
            '1'=>'ZURDO',
            '2'=>'DERECHO');

        $select=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');
        
        $select_poliza=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '0'=>'NO');
        
        $nivel_estudio =array(
            ''=>'SELECCIONAR',
            '1'=>'T.S.U',
            '2'=>'Profesional');        

        $preinscripcion_config= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                ->where('estado', '=', '1')
                ->get();

            foreach($preinscripcion_config as $key ){
                $lapso=$key->lapso;                
                $cod_reg=$key->cod_reg;
                if ($cod_reg==0){
                    $lapso_reg=$key->lapso;
                }else{
                    $lapso_trim=$key->lapso;
                }
            }
        
        $convenios = array('' => 'SELECCIONAR' , ) + DB::table('modalidad_turno')->where('activo','=','1')->orderby('cod_modalidad_turno')->lists('instituto', 'cod_modalidad_turno');

        if (count($preinscripcion_config)>0){
            if (count($preinscripcion_config)==2){
                $tsolicitud=array(
                ''=>'SELECCIONAR',
                '1'=>'POR PRIMERA VEZ O REINGRESO',
                '2'=>'REINCORPORACI&Oacute;N(Por retiro de la Instituci&oacute;n)',
                '3'=>'EQUIVALENCIA');
                $mensaje='Solicitud de Inscripci&oacute;n Trimestral lapso '.$lapso_trim.' y Semestral para el lapso '.$lapso_reg ;
            }elseif($cod_reg==2){
                $tsolicitud=array(
                ''=>'SELECCIONAR',
                '1'=>'POR PRIMERA VEZ O REINGRESO',
                '2'=>'REINCORPORACI&Oacute;N(Por retiro de la Instituci&oacute;n)',
                '3'=>'EQUIVALENCIA');
                $mensaje='Solicitud de Inscripci&oacute;n Trimestral para el lapso '.$lapso_trim;
            }elseif($cod_reg==0){
                $tsolicitud=array(
                ''=>'SELECCIONAR',
                '2'=>'REINCORPORACI&Oacute;N',
                '3'=>'EQUIVALENCIA(S&oacute;lo Semestre)');
                $mensaje='Solicitud de Inscripci&oacute;n Semestral para el lapso '.$lapso_reg;
            }

            //calendario de citas

           $fechas_no = array();

            for ($i=16; $i <= 31; $i++) { 
                $fechas_no[] = '"'.$i.'-12-2018"';
            }

            for ($i=8; $i <= 18; $i++) { 
                if ($i > 9) {
                    $fechas_no[] = '"'.$i.'-11-2018"';
                } else {
                    $fechas_no[] = '"0'.$i.'-11-2018"';
                }
        
            }

            for ($i=1; $i <= 6; $i++) { 
                if ($i > 9) {
                    $fechas_no[] = '"'.$i.'-01-2019"';
                } else {
                    $fechas_no[] = '"0'.$i.'-01-2019"';
                }
        
            }

            $query_fechas = DB::select('SELECT fecha_cita,COUNT(*) AS n_citas FROM preinscritos WHERE lapso = ? AND (cod_nucleo = 0 AND cod_nucleo = 4) AND numsobre LIKE "00-%" GROUP BY fecha_cita HAVING n_citas = 50',array($lapso_trim));  

            foreach ($query_fechas as $value) {
                $fechas_no[] = '"'.date_format(date_create($value->fecha_cita),'d-m-Y').'"';
            }



            setlocale(LC_TIME, 'es_VE'); # Localiza en español es_Venezuela
            date_default_timezone_set('America/La_Paz');

            // if (date('H:s') > '15:10') {
            //     $fechas_no[] = '"'.date('d-m-Y').'"';
            // }

            // $fechas_no[] = '"'.date('d-m-Y').'"';

            $fechas_no[] = '"05-07-2018"';
            $fechas_no[] = '"24-07-2018"';

            $elementos = implode(",", $fechas_no);

            //termina calendario
            
            // return $data;
            $title='Preinscripcion';
            return View::make('preinscripcion.preinscripcion', array(
                   'title'=>$title,
                   'mensaje'=>$mensaje,
                   'convenios'=>$convenios,
                   'tab'=>$tab_active,
                   'tsolicitud'=>$tsolicitud,
                   'tcedula'=>$tcedula,
                   'tsexo'=>$tsexo,
                   'ecivil'=>$ecivil,
                   'trabaja'=>$trabaja,
                   'convenio'=>$convenio,
                   'convenios' =>$convenios,
                   'vehiculo'=>$vehiculo,
                   'pais'=>$pais,
                   'pais_nac'=>$pais_nac,
                   'estados' => $estados,
                   'estados_nac' => $estados_nac,
                   'ciudad'=>$ciudad,
                   'ciudad_nac'=>$ciudad_nac,
                   'ciudad_ubi'=>$ciudad_ubi,
                   't_institucion'=>$t_institucion,
                   'turno_institucion'=>$turno_institucion,
                   'especialidad'=>$especialidad,
                   'informacion'=>$informacion,
                   'ciudad_cs'=>$ciudad_cs,
                   'carreras_uba'=>$carreras_uba,
                   'carreras'=>$carreras,
                   'turno'=>$turno,
                   'modalidad'=>$modalidad,
                   'nucleo'=>$nucleo,
                   'datos_carrera'=>$datos_carrera,
                   'institucion'=>$institucion,
                   'carreras'=>$carreras,
                   'vivira_est'=>$vivira_est,
                   'costea_g'=>$costea_g,
                   'disfruta_d'=>$disfruta_d,
                   'ingreso_men'=>$ingreso_men,
                   'viven_padres'=>$viven_padres,
                   'ecivil_p'=>$ecivil_p,
                   'vive_con'=>$vive_con,
                   'parentesco'=>$parentesco,
                   'escribe'=>$escribe, 
                   'select'=>$select,
                   'poliza' => $select_poliza,
                   'nivel_estudio' => $nivel_estudio,
                   'lapso'=>$lapso,
                   'fechas_no' => $elementos

            ));
        }else{
            $title='Proceso Inactivo';
            return View::make('preinscripcion.mensajepreinscripcion', array(
                   'title'=>$title,
            ));
        }    

        
    }

    //FUNCION PARA OBTENER LAS ESCUELAS EN UN NUCLEO ESPESIFICO
    public function postEscuelanucleos()
    {
        if (Request::ajax()) {
      $cod_nuc = Input::get('nucleo');

      //SQL para buscar la escuela dependiendo del nucleo
      $sql_cod_esc = DB::select('SELECT id, carrera FROM carreras_uba where cod_nuc=? ORDER BY id',array($cod_nuc));

      $cod_esc = array();
      foreach ($sql_cod_esc as $key => $value) {
        $carrera = new stdClass;

        $carrera->id = $value->id;
        $carrera->carrera = $value->carrera;

        $cod_esc[]= $carrera;
      }
    }
    return Response::json(array(
      'carrera_uba' => $cod_esc
    ));
    }

    //FUNCION QUE PROCESA LA PETICION DE AJAX CIUDADES (VISTA: PREINSCRIPCION)
    public function postAjaxciudades()
    {
        
        if(Request::ajax()){
            //obtengo y renombro variables que vienen por ajax

            $estados=Input::get('estados');
 
            $data=array();

            //SQL para obtener todos los tipos de evaluacion de informatica segun la modalidad
            $tipo_array=DB::table('db_ciudades')
            ->select('id','des_ciudades')
            ->where('id_estados', '=', $estados)
            ->where('id_pais', '=', '230')
            ->get();

            foreach($tipo_array as $value){
                $data_ciu = new stdClass();
                $data_ciu->id_ciudad=$value->id;
                $data_ciu->des_ciudad=$value->des_ciudades;
                $data[]=$data_ciu;
            }
            

            return Response::json(array(
                'ciudad' => $data
            ));
        }
    }

    //FUNCION QUE PROCESA LA PETICION DE AJAX CIUDADES (VISTA: PREINSCRIPCION)
    public function postAjaxturnocarreras()
    {
        
        if(Request::ajax()){
            //obtengo y renombro variables que vienen por ajax

            $sel_carrera=Input::get('sel_carrera');
            $data=array();
            $datam=array();

            //SQL para obtener todos los tipos de evaluacion de informatica segun la modalidad
            $tipo_array=DB::table('turno_carreras')
            ->select('id_turno','turno')
            ->where('carrera', '=', $sel_carrera)
            ->orderBy('id_turno')
            ->get();

            foreach($tipo_array as $value){
                $data_tur = new stdClass();
                $data_tur->id_turno=$value->id_turno;
                $data_tur->des_turno=$value->turno;
                $data[]=$data_tur;
            }

            $tipo_array_m=DB::table('modalidad_carreras')
            ->where('carrera','=', $sel_carrera)
            ->orderBy('id_modalidad')
            ->get();

            foreach($tipo_array_m as $value){
                $data_mod = new stdClass();
                $data_mod->id_modalidad=$value->id_modalidad;
                $data_mod->des_modalidad=$value->modalidad;
                $datam[]=$data_mod;
            }
            

            return Response::json(array(
                'turno' => $data,
                'modalidad' => $datam
            ));
        }
    }    

    //FUNCION QUE PROCESA LA PETICION DE AJAX CIUDADES (VISTA: PREINSCRIPCION)
    public function postAjaxestados_nac()
    {
        
        if(Request::ajax()){
            //obtengo y renombro variables que vienen por ajax

            $pais=Input::get('pais');

            $data=array();

            //SQL para obtener todos los tipos de evaluacion de informatica segun la modalidad
            $tipo_array=DB::table('db_estados')
            ->select('des_estados', 'id')
            ->where('id_pais', '=', $pais)
            ->get();

            foreach($tipo_array as $value){
                $data_estados = new stdClass();
                $data_estados->id_estados=$value->id;
                $data_estados->des_estados=$value->des_estados;
                $data[]=$data_estados;
            }
            
            return Response::json(array(
                'estados' => $data
            ));
        }
    }

    //FUNCION QUE PROCESA LA PETICION DE AJAX CIUDADES (VISTA: PREINSCRIPCION)
    public function postAjaxciudades_nac()
    {
        
        if(Request::ajax()){
            //obtengo y renombro variables que vienen por ajax

            $estado=Input::get('estado');

            $data=array();

            //SQL para obtener todos los tipos de evaluacion de informatica segun la modalidad
            $tipo_array=DB::table('db_ciudades')
            ->select('id','des_ciudades')
            ->where('id_estados', '=', $estado)
            ->get();

            
            foreach($tipo_array as $value){
                $data_ciudad = new stdClass();
                $data_ciudad->id_ciudad=$value->id;
                $data_ciudad->des_ciudad=$value->des_ciudades;
                $data[]=$data_ciudad;
            }

            if (count($tipo_array)==0){
                $data_ciudad = new stdClass();
                $data_ciudad->id_ciudad='48315';
                $data_ciudad->des_ciudad='Sin Ciudad';
                $data[]=$data_ciudad;
            }
            return Response::json(array(
                'ciudad' => $data
            ));
        }
    }


    //FUNCION QUE PROCESA LA PETICION DE AJAX PARA PROCESAR LA SOLICITUD (VISTA: DATOSERVICIO)
    public function postProcesarpreinsc()
    {
        $tsolicitud=Input::get('tsolicitud');
        $fecha=Input::get('fecha');
        $tcedula=Input::get('tcedula');
        $cedula=Input::get('cedula');
        $edad=Input::get('edad');
        $carrera_uba=Input::get('carrera_uba');
        $convenio= Input::get('convenio');
        $convenios=Input::get('convenios');
        $nucleo=Input::get('nucleo');
        $nombres=Input::get('nombres');
        $apellidos=Input::get('apellidos');
        $tsexo=Input::get('tsexo');
        $ecivil=Input::get('ecivil');
        $telefono=Input::get('telefono');
        $email=Input::get('email');
        $lapso= Input::get('lapso');
        $twitter=Input::get('twitter');

        if ($twitter == '') {
            $twitter = 'NO';
        }
        $facebook=Input::get('facebook');
        if ($facebook == '') {
            $facebook = 'NO';
        }
        $trabaja=Input::get('trabaja');
        if ($trabaja==1){
            $empresa=Input::get('empresa');
            $direc_empresa=Input::get('direc_empresa');
            $telefono_empre=Input::get('telefono_empre');
            $depart_empresa=Input::get('depart_empresa');
            $cargo_empresa=Input::get('cargo_empresa');

        }else{
            $empresa=null;
            $direc_empresa=null;
            $telefono_empre=null;
            $depart_empresa=null;
            $cargo_empresa=null;
        }
        $vehiculo=Input::get('vehiculo');
        if ($vehiculo==1){
            $placa=Input::get('placa');
            $marca=Input::get('marca');
            $modelo=Input::get('modelo');
        }else{
            $placa=null;
            $marca=null;
            $modelo=null;
        }

        $pais_nac=Input::get('pais_nac');
        if ($pais_nac==''){
            $estados_nac=null;
            $ciudad_nac=null;
        }else{
            $estados_nac=Input::get('estados_nac');
            if ($estados_nac==''){
                $estados_nac=null;
                $ciudad_nac=null;
            }else{        
                $ciudad_nac=Input::get('ciudad_nac');
                if ($ciudad_nac==''){
                    $ciudad_nac=null;
                }else{        
                    $ciudad_nac=Input::get('ciudad_nac');
                }
            }
        }

        $direcc_r=Input::get('direcc_r');
        $direcc_perm=Input::get('direcc_perm');
        $estados_ubi=Input::get('estados_ubi');
        $ciudad_ubi=Input::get('ciudad_ubi');
        $edad=Input::get('edad');
        $telefono_r=Input::get('telefono_r');


        if ($edad!='' && $edad<=17){
            $cedula_rep=Input::get('cedula_rep');
            $nombres_rep=Input::get('nombres_rep');
            $apellidos_rep=Input::get('apellidos_rep');
            $edad_rep=Input::get('edad_rep');
            $direccion_rep=Input::get('direccion_rep');
            $telefono_rep=Input::get('telefono_rep');
            $parentesco=Input::get('parentesco');
        }else{
            $cedula_rep=null;
            $nombres_rep=null;
            $apellidos_rep=null;
            $edad_rep=null;
            $direccion_rep=null;
            $telefono_rep=null;
            $parentesco=null;
        }  
        
        

        $tipo_institucion=Input::get('tipo_institucion');
        $nom_institucion=Input::get('nom_institucion');
        $turno_institucion=Input::get('turno_institucion');
        $g_especialidad=Input::get('g_especialidad');

        if ($g_especialidad==3){
            $ex_especialidad=Input::get('ex_especialidad');
        }else{
            $ex_especialidad=null;
        }
        
        $promedio_b=Input::get('promedio_b');
        $fecha_g=Input::get('fecha_g');
        $estados_estu_secu=Input::get('estados_estu_secu');
        $ciudad_estu_secu=Input::get('ciudad_estu_secu');

        $rusnies=Input::get('rusnies');
        $actividad_1=Input::get('actividad_1');
        $actividad_2=Input::get('actividad_2');
        $actividad_3=Input::get('actividad_3');
        $actividad_4=Input::get('actividad_4');
        $carreras=Input::get('carreras');
        $turno=Input::get('turno');
        $modalidad=Input::get('modalidad');
        $nucleo=Input::get('nucleo');
        $orientacion=Input::get('orientacion');
        $decision_carrera=Input::get('decision_carrera');
        $curso_uba=Input::get('curso_uba');
        if ($curso_uba == 1) {
            $graduo_uba = Input::get('graduo_uba');
        } else {
            $graduo_uba = null;
        }
        
        $carrera_uba=Input::get('carrera_uba');
        $turno_uba=Input::get('turno_uba');
        $modalidad_uba=Input::get('modalidad_uba');
        $infor_uba=Input::get('infor_uba');
        $cargo_d_bachiderato=Input::get('cargo_d_bachiderato');
        $otros_estudio=Input::get('otros_estudio');
        $cargo_d_bachiderato=Input::get('cargo_d_bachiderato');

        if ($cargo_d_bachiderato==1){
            $e_cargo_d_bachiderato=Input::get('e_cargo_d_bachiderato');
        }else{
            $e_cargo_d_bachiderato=null;
        }

        $otros_estudio=Input::get('otros_estudio');
        if ($otros_estudio==1){
            $otra_institucion=Input::get('otra_institucion');
            $otra_carreras=Input::get('otra_carreras');
            $se_graduo=Input::get('se_graduo');
            if ($se_graduo == 1) {
                $nivel_estudio = Input::get('nivel_estudio');
            } else {
                $nivel_estudio = 0;
            }
        }else{
            $otra_institucion=null;
            $otra_carreras=null;
            $se_graduo=null;
            $nivel_estudio = 0;
        }

        $beisbol=Input::get('beisbol');
        $voleibol=Input::get('voleibol');
        $tenis=Input::get('tenis');
        $futbol=Input::get('futbol');
        $baloncesto=Input::get('baloncesto');
        $artes_marciales=Input::get('artes_marciales');
        $futbolito=Input::get('futbolito');
        $ajedrez=Input::get('ajedrez');
        $otros_a_d=Input::get('otros_a_d');
        $teatro=Input::get('teatro');
        $rondalla=Input::get('rondalla');
        $orfeon=Input::get('orfeon');
        $danzas=Input::get('danzas');
        $pintura=Input::get('pintura');
        $estudiantina=Input::get('estudiantina');
        $futbolito=Input::get('futbolito');
        $otras_act_artistica=Input::get('otras_act_artistica');
        $seguridad_indus=Input::get('seguridad_indus');
        $relaciones_h=Input::get('relaciones_h');
        $gru_protocolo=Input::get('gru_protocolo');
        $bombero_u=Input::get('bombero_u');
        $paramedico=Input::get('paramedico');
        $accion_comunitaria=Input::get('accion_comunitaria');
        $proteccion_indus=Input::get('proteccion_indus');
        $mejor_aprendizaje=Input::get('mejor_aprendizaje');
        $otros_grupos=Input::get('otros_grupos');

        

        $costea_gast=Input::get('costea_gast');
        if ($costea_gast==5){
            $nombre_cost_gast=Input::get('nombre_cost_gast');
        }else{
            $nombre_cost_gast=null;
        }

        $disfruta_d=Input::get('disfruta_d');
        if ($disfruta_d==5){
            $nombre_inst=Input::get('nombre_inst');
        }else{
            $nombre_inst=null;
        }
        
        $ingreso_men=Input::get('ingreso_men');
        $viven_padres=Input::get('viven_padres');
        
        $ecivil_padre=Input::get('ecivil_padre');


        $vive_con=Input::get('vive_con');

        if ($vive_con==7){
            $vive_con_ex=Input::get('vive_con_ex');
        }else{
            $vive_con_ex=null;
        }

        $posee_poliza = Input::get('posee_poliza');
        $entidad_poliza = Input::get('entidad_poliza');
        $numero_poliza = Input::get('numero_poliza');
        $entidad_aseguradora = Input::get('entidad_aseguradora');
        $beneficiario = Input::get('beneficiario');

        $adultos=Input::get('adultos');
        $ninos=Input::get('ninos');
        $cuantos_herm=Input::get('cuantos_herm');
        $cuantos_hi=Input::get('cuantos_hi');

        $ceguera=Input::get('ceguera');
        $sordera=Input::get('sordera');
        $retardo_m=Input::get('retardo_m');
        $disc_ext_s=Input::get('disc_ext_s');
        $disc_ext_i=Input::get('disc_ext_i');
        $disc_ninguna=Input::get('disc_ninguna');
        $otra_disc=Input::get('otra_disc');

        if ($otra_disc==true){
            $ex_otra_disc=Input::get('ex_otra_disc');
        }else{
            $ex_otra_disc=null;
        }

        $beisbol=Input::get('beisbol');
        $voleibol=Input::get('voleibol');
        $tenis=Input::get('tenis');
        $futbol=Input::get('futbol');
        $baloncesto=Input::get('baloncesto');
        $ates_marciales=Input::get('ates_marciales');
        $futbolito=Input::get('futbolito');
        $ajedrez=Input::get('ajedrez');
        $otros_a_d=Input::get('otros_a_d');
        $teatro=Input::get('teatro');
        $rondalla=Input::get('rondalla');
        $orfeon=Input::get('orfeon');
        $danzas=Input::get('danzas');
        $pintura=Input::get('pintura');
        $estudiantina=Input::get('estudiantina');
        $musicafolklorica=input::get('musicafolklorica');
        $futbolito=Input::get('futbolito');
        $otras_act_artistica=Input::get('otras_act_artistica');
        $seguridad_indus=Input::get('seguridad_indus');
        $relaciones_h=Input::get('relaciones_h');
        $gru_protocolo=Input::get('gru_protocolo');
        $bombero_u=Input::get('bombero_u');
        $paramedico=Input::get('paramedico');
        $accion_comunitaria=Input::get('accion_comunitaria');
        $proteccion_indus=Input::get('proteccion_indus');
        $mejor_aprendizaje=Input::get('mejor_aprendizaje');
        $otros_grupos=Input::get('otros_grupos');
        $viven_padres=Input::get('viven_padres');

        $vivira_est=Input::get('vivira_est');
        if ($vivira_est==5){
            $otra_vivienda=Input::get('otra_vivienda');
        }else{
            $otra_vivienda=null;
        }

        $costea_g=Input::get('costea_gast');

        if ($costea_g==5){
            $nombre_cost_gast=Input::get('nombre_cost_gast');
        }else{
            $nombre_cost_gast=null;
        }

        $escribe=Input::get('escribe');
        $etnia=Input::get('etnia');
        $fuma=Input::get('fuma');

        $variables1 = array(
            'tipo de solicitud' =>$tsolicitud,
            'fecha de nacimiento'=>$fecha,
            'nacionalidad'=>$tcedula,
            'c&eacute;dula'=>$cedula
        );
        //reglas para validar cada variable
        $rules1 = array(
            'tipo de solicitud' => 'required',
            'fecha de nacimiento'=>'required',
            'nacionalidad'=>'required',
            'c&eacute;dula' => 'required|numeric|digits_between:6,11',
        );
         
        //en el caso que alguna vadilacion falle retornaremos al formulario.
        $validation1 = Validator::make($variables1, $rules1);
        if ($validation1->fails()){
            $error_periodo=true;
            $message_periodo = $validation1->messages();
        }else{
           //return $validation1->failed();    
            $error_periodo=false;
        }
        $variables2 = array(
            
            'primer y segundo nombre'=> $nombres,
            'primer y segundo apellido'=> $apellidos,
            'sexo'=>$tsexo,
            'estado civil'=>$ecivil,
            'tel&eacute;fono'=>$telefono,
            'correo electr&oacute;nico'=>$email,
            'twitter' => $twitter,
            'facebook' => $facebook,
            'trabaja'=>$trabaja,
            'posee veh&iacute;culo'=>$vehiculo,
            'pais de nacimiento'=>$pais_nac,
            'direcci&oacute;n de residencia'=>$direcc_r,
            'tel&eacute;fono de residencia'=>$telefono_r,
            'direcci&oacute;n permanente'=>$direcc_perm,
            'estados de ubicaci&oacute;n'=>$estados_ubi,
            'ciudad de ubicaci&oacute;n'=>$ciudad_ubi,             
        );

        //reglas para validar cada variable
        $rules2 = array(
            
            'primer y segundo nombre' => 'required',
            'primer y segundo apellido'=> 'required',
            'sexo'=> 'required',
            'estado civil' => 'required',
            'tel&eacute;fono'=> 'required',
            'correo electr&oacute;nico'=>'required|email',
            'twitter' => 'required|space',
            'facebook' => 'required|space',
            'trabaja'=> 'required',
            'posee veh&iacute;culo'=>'required',
            'pais de nacimiento'=>'required',
            'direcci&oacute;n de residencia'=>'required',
            'tel&eacute;fono de residencia'=>'required|digits_between:12,12',
            'direcci&oacute;n permanente'=>'required',     
        );

        if ((array_key_exists('edad',Input::all())==true) && (Input::get('edad')<18) && (Input::get('edad')!='')) {
            $variables2['c&eacute;dula representante']=$cedula_rep;
            $variables2['nombre representante']=$nombres_rep;
            $variables2['apellido representante']=$apellidos_rep;
            $variables2['edad representante']=$edad_rep;
            $variables2['dir representante']=$direccion_rep;
            $variables2['tel representante']=$telefono_rep;
            $variables2['parentesco']=$parentesco;

            $rules2['c&eacute;dula representante']='required';
            $rules2['nombre representante']='required';
            $rules2['apellido representante']='required';
            $rules2['edad representante']='required';
            $rules2['dir representante']='required';
            $rules2['tel representante']='required|digits_between:12,12';
            $rules2['parentesco']='required';
        }

        if ((array_key_exists('trabaja',Input::all())==true) && Input::get('trabaja')==1) {
            $variables2['empresa']=$empresa;
            $variables2['direcci&oacute;n de la empresa']=$direc_empresa;
            $variables2['tel&eacute;fono de empresa']=$telefono_empre;
            $variables2['departamento en que labora']=$depart_empresa;
            $variables2['cargo en la empresa']=$cargo_empresa;
            $rules2['empresa']='required';
            $rules2['direcci&oacute;n de la empresa']='required';
            $rules2['tel&eacute;fono de empresa']='required|digits_between:12,12';
            $rules2['departamento en que labora']='required';
            $rules2['cargo en la empresa']='required';
        }

        if ((array_key_exists('vehiculo',Input::all())==true) && Input::get('vehiculo')==1) {
            $variables2['placa']=$placa;
            $variables2['marca']=$marca;
            $variables2['modelo']=$modelo;
            $rules2['placa']='required';
            $rules2['marca']='required';
            $rules2['modelo']='required';

        }

        if (array_key_exists('pais_nac',Input::all())==true) {
            $variables2['estados de nacimiento']=$estados_nac;
            $variables2['ciudad de nacimiento']=$ciudad_nac;
            $rules2['estados de nacimiento']='required';
            $rules2['ciudad de nacimiento']='required';
        }


        $validation2 = Validator::make($variables2, $rules2);
        if ($validation2->fails()){
           //return $validation2->failed();
            $error_personales=true;
            $message_personales = $validation2->messages();
        }else{
            $error_personales=false;
        }



        $variables3 = array(
            'tipo de instituci&oacute;n' =>$tipo_institucion,
            'nombre de la instituci&oacute;n'=>$nom_institucion,
            'turno que estudi&oacute;'=>$turno_institucion,
            'especialidad'=>$g_especialidad,
            'promedio de bachillerato'=>$promedio_b,
            'fecha de graduaci&oacute;n' =>$fecha_g,
            'estados de ubicaci&oacute;n'=>$estados_ubi,
            'ciudad de ubicaci&oacute;n'=>$ciudad_ubi,
            'n rusnies'=>$rusnies,
            'actividad 1'=>$actividad_1,
            'carrera que desea estudiar' =>$carrera_uba,
            'curso uba' =>$curso_uba,
            'turno carrera'=>$turno_uba,
            'modalidad carrera' =>$modalidad_uba,
            'informacion de carrera' =>$infor_uba,
            'recibi&oacute; orientaci&oacute;n vocacional'=>$orientacion,
            'decidido escogencia carrera'=>$decision_carrera,
            'representaci&oacute;n estudiantil'=>$cargo_d_bachiderato,            
            'cursa otra instituci&oacute;n'=>$otros_estudio,
        );
        $rules3 = array(
            'tipo de instituci&oacute;n' =>'required',
            'nombre de la instituci&oacute;n'=>'required',
            'turno que estudi&oacute;'=>'required',
            'especialidad' =>'required',
            'promedio de bachillerato'=>'required',
            'fecha de graduaci&oacute;n'=>'required',
            'n rusnies'=>'required',
            'actividad 1'=>'required',
            'carrera que desea estudiar'=>'required',
            'curso uba' =>'required',
            'turno carrera'=>'required',
            'modalidad carrera' => 'required',
            'informacion de carrera' =>'required',
            'recibi&oacute; orientaci&oacute;n vocacional'=>'required',
            'decidido escogencia carrera'=>'required',
            'representaci&oacute;n estudiantil'=>'required',            
            'cursa otra instituci&oacute;n'=>'required',          
        );


        if ((array_key_exists('g_especialidad',Input::all())==true) && Input::get('g_especialidad')==3){

            $variables3['exp especialidad']=$ex_especialidad;
            $rules3['exp especialidad']='required';
        }
        if ((array_key_exists('otros_estudio',Input::all())==true) && Input::get('otros_estudio')==1){
            $variables3['otra institucio']=$otra_institucion;
            $variables3['otra carreras']=$tipo_institucion;
            $variables3['se gradu&oacute;']=$se_graduo;

            $rules3['otra institucio']='required';
            $rules3['otra carreras']='required';
            $rules3['se gradu&oacute;']='required';
        }

        if ((array_key_exists('se_graduo',Input::all())==true) && Input::get('se_graduo')==1){
            $variables3['nivel de estudio']=$nivel_estudio;

            $rules3['nivel de estudio']='required';
        }

        if ((array_key_exists('curso_uba',Input::all())==true) && Input::get('curso_uba')==1){
            $variables3['graduo uba']=$graduo_uba;

            $rules3['graduo uba']='required';
        }

        if ((array_key_exists('cargo_d_bachiderato',Input::all())==true) && Input::get('cargo_d_bachiderato')==1){            
            $variables3['expl representaci&oacute;n estudiantil']=$e_cargo_d_bachiderato;
            $rules3['expl representaci&oacute;n estudiantil']='required';
        }


        $validation3 = Validator::make($variables3, $rules3);
        if ($validation3->fails()){
            $error_datossecundaria=true;
            $message_datossecundaria = $validation3->messages();
        }else{
            $error_datossecundaria=false;
        }


        $variables4 = array(
            'd&oacute;nde vivir&aacute; estudie' =>$vivira_est,
            'qui&eacute;n costea sus gastos' =>$costea_gast,
            'disfruta d' =>$disfruta_d,
            'ingreso mensual'=>$ingreso_men,
            'viven sus padres'=>$viven_padres,
            'estado civil padres'=>$ecivil_padre,
            'con quien vive ud'=>$vive_con,
            'tiene poliza' => $posee_poliza,
            'cuantos adultos'=>$adultos,
            'cuantos ni&ntilde;os'=>$ninos,
            'cuantos hermanos'=>$cuantos_herm,
            'cuantos hijos'=>$cuantos_hi,
            'zurdo o derecho'=>$escribe,
            'pertenece etnia'=>$etnia,
            'fuma'=>$fuma,              
        );
        $rules4 = array(
            'd&oacute;nde vivir&aacute; estudie' =>'required',
            'qui&eacute;n costea sus gastos'=>'required', 
            'disfruta d'=>'required',  
            'ingreso mensual' =>'required', 
            'viven sus padres' =>'required',   
            'estado civil padres'=>'required',  
            'con quien vive ud'=>'required',
            'tiene poliza' => 'required',
            'cuantos adultos'=>'required',
            'cuantos ni&ntilde;os'=>'required',
            'cuantos hermanos' =>'required',
            'cuantos hijos'=>'required',
            'zurdo o derecho'=>'required',
            'pertenece etnia'=>'required',
            'fuma'=>'required',
        );

        if ((array_key_exists('vivira_est',Input::all())==true) && Input::get('vivira_est')==5){            
            $variables4['expl d&oacute;nde vivir&aacute; estudie']=$otra_vivienda;
            $rules4['expl d&oacute;nde vivir&aacute; estudie']='required';
        }

        if ((array_key_exists('costea_gast',Input::all())==true) && Input::get('costea_gast')==5){                    
            $variables4['otro costea sus gastos']=$nombre_cost_gast;
            $rules4['otro costea sus gastos']='required';
        }

        if ((array_key_exists('disfruta_d',Input::all())==true) && Input::get('disfruta_d')==5){            
            $variables4['nombre de otra instituci&oacute;n ']=$nombre_inst;
            $rules4['nombre de otra instituci&oacute;n ']='required';
        }

        if ((array_key_exists('posee_poliza',Input::all())==true) && Input::get('posee_poliza')==1){            
            $variables4['entidad poliza']=$entidad_poliza;
            $variables4['numero poliza']=$numero_poliza;

            $rules4['entidad poliza']='required';
            $rules4['numero poliza']='required|alpha_dash';
        }else {
            $variables4['beneficiario']=$beneficiario;
            $variables4['entidad_aseguradora']=$entidad_aseguradora;
            $rules4['beneficiario']='required|min:10';
            $rules4['entidad_aseguradora']='required';
        }

        if ((array_key_exists('vive_con',Input::all())==true) && Input::get('vive_con')==7){            
            $variables4['vive otra persona']=$vive_con_ex;
            $rules4['vive otra persona']='required';
        }

        if (array_key_exists('otra_disc',Input::all())==true){            
            $variables4['otra discapacidad']=$ex_otra_disc;
            $rules4['otra discapacidad']='required';
        }

        $validation4 = Validator::make($variables4, $rules4);
        if ($validation4->fails()){
            $error_datossocioeconomicos=true;
            $message_datossocioeconomicos = $validation4->messages();
        }else{
            $error_datossocioeconomicos=false;
        }
        $message_error='Por favor revise los campos obligatorios';

        if ($error_periodo==true || $error_personales==true || $error_datossecundaria==true || $error_datossocioeconomicos==true) {
            $vista= Redirect::action('PreinscripcionController@getInicio')
                ->withInput();
                if ($error_periodo==true) {
                    $vista->with('message_periodo',$message_periodo);
                    $vista->with('message_error_periodo',$message_error);
                }
                if ($error_personales==true) {
                    $vista->with('message_personales',$message_personales);
                    $vista->with('message_error_personales',$message_error);
                }
                if ($error_datossecundaria==true) {
                    $vista->with('message_datossecundaria',$message_datossecundaria);
                    $vista->with('message_error_datossecundaria',$message_error);
                }
                if ($error_datossocioeconomicos==true) {
                    $vista->with('message_datossocioeconomicos',$message_datossocioeconomicos);
                    $vista->with('message_error_datossocioeconomicos',$message_error);
                }
                $vista->with('estados_nac_selected',$estados_nac);
                $vista->with('ciudad_nac_selected',$ciudad_nac);
                $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                return $vista;
        }else{

                $celular = explode("-", $telefono);
                if ($telefono_empre==null){
                    $telefono_empresa[0]=null;
                    $telefono_empresa[1]=null;
                }else{
                    $telefono_empresa = explode("-", $telefono_empre);
                }
                   
                $telefono_residencia = explode("-", $telefono_r);

                if ($telefono_rep==null){
                    $telefono_representante[0]=null;
                    $telefono_representante[1]=null;
                }else{
                    $telefono_representante = explode("-", $telefono_rep); 
                }
            
                $preinscripcion_config_activo= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                ->where('estado', '=', 1)
                ->get();

                if(count($preinscripcion_config_activo)==2){
                    if ($tsolicitud==2){
                    $cod_reg=0;
                    }else{
                    $cod_reg=2;
                    }

                    $preinscripcion_config= DB::table('config_web')
                    ->where('cod_modulo', '=', 'PREINSC')
                    ->where('cod_reg', '=', $cod_reg)
                    ->get();


                    foreach($preinscripcion_config as $key ){
                    $lapso=$key->lapso;
                    $nucleo_activo=$key->{'estado_'.$nucleo};
                    } 
                }elseif(count($preinscripcion_config_activo)==1){
                     $preinscripcion_config= DB::table('config_web')
                    ->where('cod_modulo', '=', 'PREINSC')
                    ->where('estado', '=', 1)
                    ->get();


                    foreach($preinscripcion_config as $key ){
                    $lapso=$key->lapso;
                    $nucleo_activo=$key->{'estado_'.$nucleo};
                    $cod_reg=$key->cod_reg;
                    } 
                } 
            
                if($tsolicitud==1){

                    if ($convenio ==0) {

                    if ($carrera_uba < 9) {
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-10'.$carrera_uba.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre='0'.$nucleo.'-10'.$carrera_uba.'-'.$cont;
                    }else{
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-1'.$carrera_uba.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre='0'.$nucleo.'-1'.$carrera_uba.'-'.$cont;
                    }

                    } else if ($convenio ==2) {

                        if ($carrera_uba < 9) {
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-10'.$carrera_uba.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre='0'.$nucleo.'-10'.$carrera_uba.'-'.$cont;
                        }else{
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-1'.$carrera_uba.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre='0'.$nucleo.'-1'.$carrera_uba.'-'.$cont;
                        }
                    } else if ($convenio==1){

                        if ($carrera_uba < 9) {
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like',$convenios.'-10'.$carrera_uba.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre=$convenios.'-10'.$carrera_uba.'-'.$cont;
                        }else{
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like',$convenios.'-1'.$carrera_uba.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre=$convenios.'-1'.$carrera_uba.'-'.$cont;    
                        }
                    }
                } 
                else if($tsolicitud==2){

                    if ($convenio ==0) {    

                    if ($carrera_uba < 9) {
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-220'.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre='0'.$nucleo.'-220-'.$cont;
                    }else{
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-220'.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre='0'.$nucleo.'-220-'.$cont;
                    }

                    } else if ($convenio==2) {

                        if ($carrera_uba < 9) {
                            $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-220'.'%')->count()+1;
                            $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                            $nsobre='0'.$nucleo.'-220-'.$cont;
                        }else{
                            $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-220'.'%')->count()+1;
                            $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                            $nsobre='0'.$nucleo.'-220-'.$cont;
                        }
                    } else if($convenio==1){

                        if ($carrera_uba < 9) {
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like',$convenios.'-220'.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre=$convenios.'-220-'.$cont;
                        }else{
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like',$convenios.'-220'.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre=$convenios.'-220-'.$cont;    
                        }
                    }

                } else if ($tsolicitud==3){

                    if ($convenio ==0) {

                        if ($carrera_uba < 9) {
                            $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-110'.'%')->count()+1;
                            $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                            $nsobre='0'.$nucleo.'-110-'.$cont;
                        }else{
                            $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-110'.'%')->count()+1;
                            $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                            $nsobre='0'.$nucleo.'-110-'.$cont;
                        }

                    } else if ($convenio==2) {

                        if ($carrera_uba < 9) {
                            $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-110'.'%')->count()+1;
                            $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                            $nsobre='0'.$nucleo.'-110-'.$cont;
                        }else{
                            $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like','0'.$nucleo.'-110'.'%')->count()+1;
                            $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                            $nsobre='0'.$nucleo.'-110-'.$cont;
                        }
                    }else if($convenio==1){

                        if ($carrera_uba < 9) {
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like',$convenios.'-110'.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre=$convenios.'-110-'.$cont;
                        }else{
                        $cont = DB::table('preinscritos')->where('lapso','=',$lapso)->where('numsobre','like',$convenios.'-110'.'%')->count()+1;
                        $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                        $nsobre=$convenios.'-110-'.$cont;    
                        }
                    }
                }

                $sobre_nucleo = explode("-", $nsobre);
                $validasobre=$sobre_nucleo[1];

                if ($sobre_nucleo[0]==00 || $sobre_nucleo[0]==01 || $sobre_nucleo[0]==02 || $sobre_nucleo[0]==03 || $sobre_nucleo[0]==50 || $sobre_nucleo[0]==51 || $sobre_nucleo[0]==52 || $sobre_nucleo[0]==53 || $sobre_nucleo[0]==54 || $sobre_nucleo[0]==56 || $sobre_nucleo[0]==57 || $sobre_nucleo[0]==58 || $sobre_nucleo[0]==59 ||  $sobre_nucleo[0]==60 || $sobre_nucleo[0]==61 || $sobre_nucleo[0]==63 || $sobre_nucleo[0]==64 || $sobre_nucleo[0]==65 || $sobre_nucleo[0]==68 || $sobre_nucleo[0]==69 || $sobre_nucleo[0]==71 || $sobre_nucleo[0]==74 || $sobre_nucleo[0]==72 || $sobre_nucleo[0]==79 || $sobre_nucleo[0]== 91 || $sobre_nucleo[0]== 92 || $sobre_nucleo[0]== 93 || $sobre_nucleo[0]== 95 || $sobre_nucleo[0]== 100 || $sobre_nucleo[0]== 101 || $sobre_nucleo[0]== 102 || $sobre_nucleo[0]== 103 || $sobre_nucleo[0]== 104 || $sobre_nucleo[0]== 105){
                    if ($sobre_nucleo[1]==101 || $sobre_nucleo[1]==102 || $sobre_nucleo[1]==103 || $sobre_nucleo[1]==104 || $sobre_nucleo[1]==105 || $sobre_nucleo[1]==106 || $sobre_nucleo[1]==107 || $sobre_nucleo[1]==108 || $sobre_nucleo[1]==109  || $sobre_nucleo[1]==110 || $sobre_nucleo[1]==220 || $sobre_nucleo[1]==111 || $sobre_nucleo[1]==130 || $sobre_nucleo[1] == 131 || $sobre_nucleo[1] == 132 || $sobre_nucleo[1] == 134 || $sobre_nucleo[1]== 135){

                        if ($nucleo_activo==0){
                            $message_error='Preinscripciones No Disponible para el nucleo '.(nombreNucleo($nucleo));
                            $vista= Redirect::action('PreinscripcionController@getInicio')
                            ->withInput();
                            $vista->with('message_error_periodo',$message_error);
                            $vista->with('estados_nac_selected',$estados_nac);
                            $vista->with('ciudad_nac_selected',$ciudad_nac);
                            $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                            $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                            return $vista;
                        }elseif($edad<=14){
                            $message_error='Fecha de Nacimiento errada...Verifique';
                            $vista= Redirect::action('PreinscripcionController@getInicio')
                            ->withInput();
                            $vista->with('message_error_periodo',$message_error);
                            $vista->with('estados_nac_selected',$estados_nac);
                            $vista->with('ciudad_nac_selected',$ciudad_nac);
                            $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                            $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                            return $vista;

                        }elseif($tsolicitud==1 and $validasobre==110 || $validasobre==220){

                            if ($validasobre==110){
                                $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Equivalencia';
                            }elseif ($validasobre==220){
                                $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Reincorporado';
                            }
                            $vista= Redirect::action('PreinscripcionController@getInicio')
                            ->withInput();
                            $vista->with('message_error_periodo',$message_error);
                            $vista->with('estados_nac_selected',$estados_nac);
                            $vista->with('ciudad_nac_selected',$ciudad_nac);
                            $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                            $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                            return $vista;
                        }elseif($tsolicitud==3 and $validasobre!=110){

                            if ($validasobre==110){
                                $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Equivalencia';
                            }elseif ($validasobre==220){
                                $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Reincorporado';
                            }
                            $vista= Redirect::action('PreinscripcionController@getInicio')
                            ->withInput();
                            $vista->with('message_error_periodo',$message_error);
                            $vista->with('estados_nac_selected',$estados_nac);
                            $vista->with('ciudad_nac_selected',$ciudad_nac);
                            $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                            $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                            return $vista;
                        }elseif($tsolicitud==2 and $validasobre!=220){

                            if ($validasobre==110){
                                $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Equivalencia';
                            }elseif ($validasobre==220){
                                $message_error='Su n&uacute;mero de Sobre es solo para tipo de solicitud Reincorporado';
                            }
                            $vista= Redirect::action('PreinscripcionController@getInicio')
                            ->withInput();
                            $vista->with('message_error_periodo',$message_error);
                            $vista->with('estados_nac_selected',$estados_nac);
                            $vista->with('ciudad_nac_selected',$ciudad_nac);
                            $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                            $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                            return $vista;
                        }elseif($sobre_nucleo[0]==00 and Input::get('fecha_cita') == ''){

                            $message_error='Por favor indiquenos el dia de atenci&oacute;n';
                            $vista= Redirect::action('PreinscripcionController@getInicio')
                            ->withInput();
                            $vista->with('message_cita',$message_error);
                            return $vista;

                        }else{

                            if ($sobre_nucleo[0]==00) {
                                $fecha_cita = date_format(date_create(Input::get('fecha_cita')),'Y-m-d');
                            } else {
                                $fecha_cita = null;
                            }

                            $verificar_preinsc = DB::table('preinscritos')
                            ->select('cedula')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->count();
                            //return dd($verificar_preinsc);
                            if($verificar_preinsc==0){
                            //SQL para insertar los datos de la preinscripcion en la BD

                                $insertar=DB::table('preinscritos')
                                ->insert(array(
                                    'lapso'=> $lapso,
                                    'cod_nucleo' => $nucleo,
                                    'tipo_solicitud'=> $tsolicitud,
                                    'numsobre'=>  $nsobre,
                                    'cedula' => $cedula,
                                    'apellidos' => $apellidos,
                                    'nombres' => $nombres,
                                    'nacionalidad' =>$tcedula,
                                    'fecha_nac' => date_format(date_create($fecha),'Y-m-d'),
                                    'sexo' => $tsexo,
                                    'edo_civil' => $ecivil,
                                    'cod_celular' => $celular[0],
                                    'celular' => $celular[1],
                                    'email' => $email,
                                    'trabaja' => $trabaja,
                                    'empresa_trab' => $empresa,
                                    'direccion_trab' => $direc_empresa,
                                    'codtelf_trab' => $telefono_empresa[0],
                                    'telf_trab' => $telefono_empresa[1],
                                    'departamento_trab' => $depart_empresa,
                                    'cargo_trab' =>  $cargo_empresa,
                                    'vehiculo' =>$vehiculo,
                                    'placa_veh' =>$placa,
                                    'modelo_veh' =>$modelo,
                                    'marca_veh' =>$marca,
                                    'pais_nac' =>$pais_nac,
                                    'estado_nac' =>$estados_nac,
                                    'ciudad_nac' =>$ciudad_nac,
                                    'direccion_res' =>$direcc_r,
                                    'codtelf_res' => $telefono_residencia[0],
                                    'telf_res' => $telefono_residencia[1],
                                    'direccion_per' =>$direcc_perm,
                                    'codtelf_per' =>  $telefono_residencia[0],
                                    'telf_per' =>  $telefono_residencia[1],
                                    'estado_direccion_per' =>$estados_ubi,
                                    'ciudad_direccion_per' =>$ciudad_ubi,
                                    'cedula_rep' =>$cedula_rep,
                                    'apellidos_rep' =>$apellidos_rep,
                                    'nombres_rep' =>$nombres_rep,
                                    'edad_rep' =>$edad_rep,
                                    'direccion_rep' =>$direccion_rep,
                                    'parentesco' =>$parentesco,
                                    'codtelf_rep' =>$telefono_representante[0],
                                    'telf_rep' =>$telefono_representante[1],
                                    'tipo_institucion_bach' =>$tipo_institucion,
                                    'nombre_institucion_bach' => $nom_institucion,
                                    'turno_bach' =>$turno_institucion,
                                    'especialidad_bach' =>$g_especialidad,
                                    'otra_especialidad_bach' =>$ex_especialidad,
                                    'promedio_bach' =>$promedio_b,
                                    'fecha_graduacion_bach' =>date_format(date_create($fecha_g),'Y-m-d'),
                                    'estado_bach' =>$estados_estu_secu,
                                    'ciudad_bach' => $ciudad_estu_secu,
                                    'rusnies' =>$rusnies,
                                    'actividad1' => $actividad_1,
                                    'actividad2' => $actividad_2,
                                    'actividad3' => $actividad_3,
                                    'actividad4' => $actividad_4,
                                    'cargos_bach' =>$cargo_d_bachiderato,
                                    'nombre_cargo_bach' =>$e_cargo_d_bachiderato,
                                    'orientacion_carrera' =>$orientacion,
                                    'decidido_carrera' =>$decision_carrera,
                                    'estudios_superior' =>$otros_estudio,
                                    'instituto_superior' =>$otra_institucion,
                                    'carrera_superior' =>$otra_carreras,
                                    'nivel_superior' => $nivel_estudio,
                                    'graduado_superior' =>$se_graduo,
                                    'carrera_cursar' =>$carrera_uba,
                                    'turno_carrera_cursar' =>$turno_uba,
                                    'modalidad_carrera_cursar' =>$modalidad_uba,
                                    'inform_carrerasuba' =>$infor_uba,
                                    'depo_1' =>$beisbol,
                                    'depo_2' =>$voleibol,
                                    'depo_3' =>$tenis,
                                    'depo_4' =>$futbol,
                                    'depo_5' =>$baloncesto,
                                    'depo_6' =>$artes_marciales,
                                    'depo_7' =>$futbolito,
                                    'depo_8' =>$ajedrez,
                                    'depo_9' =>$otros_a_d,
                                    'arti_1' =>$teatro,
                                    'arti_2' =>$rondalla,
                                    'arti_3' =>$orfeon,
                                    'arti_4' =>$danzas,
                                    'arti_5' =>$pintura,
                                    'arti_6' =>$estudiantina,
                                    'arti_7' =>$musicafolklorica,
                                    'arti_8' =>$otras_act_artistica,
                                    'otras_1' =>$seguridad_indus,
                                    'otras_2' =>$relaciones_h,
                                    'otras_3' =>$gru_protocolo,
                                    'otras_4' => $bombero_u,
                                    'otras_5' =>$paramedico,
                                    'otras_6' =>$accion_comunitaria,
                                    'otras_7' =>$proteccion_indus,
                                    'otras_8' =>$mejor_aprendizaje,
                                    'otras_9' =>$otros_grupos,
                                    'donde_vivira' => $vivira_est,
                                    'otro_donde_vivira' => $otra_vivienda,
                                    'costa_estudios' =>$costea_gast,
                                    'otro_coste_estudios' =>$nombre_cost_gast,
                                    'ayudas' => $disfruta_d,
                                    'institucion_ayudas' =>$nombre_inst,
                                    'ingreso_mensual' =>$ingreso_men,
                                    'viven_padres' =>$viven_padres,
                                    'edo_civil_padres' =>$ecivil_padre,
                                    'persona_convive' =>$vive_con,
                                    'nombre_persona_convive' =>$vive_con_ex,
                                    'adultos_hogar' =>$adultos,
                                    'ninos_hogar' =>$ninos,
                                    'hermanos' =>$cuantos_herm,
                                    'hijos' =>$cuantos_hi,
                                    'sueldo_trab' => $ingreso_men,
                                    'fecha_solicitud' => date('Y-m-d'),
                                    'discapacidad1' =>$ceguera,
                                    'discapacidad2' =>$sordera,
                                    'discapacidad3' =>$retardo_m,
                                    'discapacidad4' =>$disc_ext_s,
                                    'discapacidad5' =>$disc_ext_i,
                                    'discapacidad6' =>$disc_ninguna,
                                    'discapacidad7' =>$otra_disc,
                                    'desc_discapacidad'=>$ex_otra_disc,
                                    'fuma' => $fuma,
                                    'escribe' =>$escribe,
                                    'etnia' =>$etnia,
                                    'estatus' => 0,
                                    'cod_reg'=>$cod_reg,
                                    'poliza' => $posee_poliza,
                                    'twitter' => $twitter,
                                    'facebook' => $facebook,
                                    'curso_uba' => $curso_uba,
                                    'graduo_uba' => $graduo_uba,
                                    'fecha_cita' => $fecha_cita

                                ));

                                if ($posee_poliza == 1) {
                                    $insertar_seguros_aseg=DB::table('seguros_pre_aseg')
                                    ->insert(array(
                                        'numsobre'=>  $nsobre,
                                        'cedula' => $cedula,
                                        'entidad' => mayuscula($entidad_poliza),
                                        'num_poliza' => mayuscula($numero_poliza),
                                        'lapso'=> $lapso
                                    ));
                                }else{
                                    $insertar_seguros_pre=DB::table('seguros_pre')
                                    ->insert(array(
                                        'numsobre'=>  $nsobre,
                                        'cedula' => $cedula,
                                        'beneficiario' => mayuscula($beneficiario),
                                        'lapso'=> $lapso
                                    ));
                                }

                            } else{
                                $validacion_ce=DB::table('preinscritos')->select('cedula','carrera_cursar')->get();

                                $validacion_lap=DB::table('config_web')->select('lapso')->get();

                                foreach ($validacion_ce as $value) {
                                    $ced=$value->cedula;
                                    $carreras_vali=$value->carrera_cursar;

                                    foreach ($validacion_lap as $valu) {
                                        $lap=$valu->lapso;

                                        $message_error='Error Usted Ya Ha Realizado Una Solicitud';
                                        if ($cedula == $ced || $carrera_uba==$carreras_vali || $lapso==$lap ) {
                                            $vista= Redirect::action('PreinscripcionController@getInicio')
                                            ->withInput();
                                            $vista->with('message_error_periodo',$message_error);

                                            return $vista;
                                        }
                                    }
                                }

                            }
                                try {
                                    Mail::send('preinscripcion.message', array('nsobre' => $nsobre,'nombres' =>$nombres, 'apellidos' => $apellidos, 'lapso' =>$lapso), function($message) use($email){

                                        $message->to($email)->subject('Clave de Acceso || Preinscripción UBA');
                                    });
                                } catch (Exception $e) {
                                    //nothing
                                }

                                $title = 'Imprimir Preinscripcion';
                                return View::make('preinscripcion.imprimirpreinscripcion', array(
                                    'title' => $title, 
                                    'cedula'=>$cedula,
                                    'lapso'=>$lapso,
                                    'nsobre'=>$nsobre                          
                                ));
                        }
                    } else{
                        $message_error='Por favor revise n&uacute;mero de sobre';
                        //return $sobre_nucleo[0];
                        $vista= Redirect::action('PreinscripcionController@getInicio')
                        ->withInput();
                        $vista->with('message_error_personales',$message_error);
                        $vista->with('estados_nac_selected',$estados_nac);
                        $vista->with('ciudad_nac_selected',$ciudad_nac);
                        $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                        $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                        return $vista;
                    } 
                }else{
                    $message_error='Por favor revise n&uacute;mero de sobre';
                    $vista= Redirect::action('PreinscripcionController@getInicio')
                    ->withInput();
                    $vista->with('message_error_personales',$message_error);
                    $vista->with('estados_nac_selected',$estados_nac);
                    $vista->with('ciudad_nac_selected',$ciudad_nac);
                    $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                    $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                    return $vista;
                }   
        }
    }

    public function postProcesarmodificacion()
    {
        $tsolicitud=Input::get('tsolicitud');
        $fecha=Input::get('fecha');
        $tcedula=Input::get('tcedula');
        $cedula=Input::get('cedula');
        $convenio=Input::get('convenio');
        $carrera_uba=Input::get('carrera_uba');
        $carreras_uba=Input::get('carreras_uba');
        $lapso=Input::get('lapso');
        $nucleo=Input::get('nucleo');
        $convenios=Input::get('convenios');
        $nombres=Input::get('nombres');
        $apellidos=Input::get('apellidos');
        $tsexo=Input::get('tsexo');
        $ecivil=Input::get('ecivil');
        $telefono=Input::get('telefono');
        $email=Input::get('email');
        $twitter=Input::get('twitter');


        if ($twitter == '') {
            $twitter = 'NO';
        }
        $facebook=Input::get('facebook');
        if ($facebook == '') {
            $facebook = 'NO';
        }
        $trabaja=Input::get('trabaja');

         $preins = DB::table('preinscritos')->select('numsobre')->where('cedula',$cedula)->get();
         foreach ($preins as $value) {
             $nsobre = $value->numsobre;
         }

        $ciudad_nac = DB::table('preinscritos')->select('ciudad_nac', 'numsobre', 'ciudad_direccion_per','estado_nac')->where('cedula',$cedula)->get();
        foreach ($ciudad_nac as $value) {
            $ciudad_ubi=$value->ciudad_direccion_per;
            $estados_nac=$value->estado_nac;
            $convenios = explode("-",$nsobre); 
        }
        if ($trabaja==1){
            $empresa=Input::get('empresa');
            $direc_empresa=Input::get('direc_empresa');
            $telefono_empre=Input::get('telefono_empre');
            $depart_empresa=Input::get('depart_empresa');
            $cargo_empresa=Input::get('cargo_empresa');

        }else{
            $empresa=null;
            $direc_empresa=null;
            $telefono_empre=null;
            $depart_empresa=null;
            $cargo_empresa=null;
        }
        $vehiculo=Input::get('vehiculo');
        if ($vehiculo==1){
            $placa=Input::get('placa');
            $marca=Input::get('marca');
            $modelo=Input::get('modelo');
        }else{
            $placa=null;
            $marca=null;
            $modelo=null;
        }

        $pais_nac=Input::get('pais_nac');
        if ($pais_nac==''){
            $estados_nac=null;
            $ciudad_nac=null;
        }else{
            $estados_nac=Input::get('estados_nac');
            if ($estados_nac==''){
                $estados_nac=null;
                $ciudad_nac=null;
            }else{        
                $ciudad_nac=Input::get('ciudad_nac');
                if ($ciudad_nac==''){
                    $ciudad_nac=null;
                }else{        
                    $ciudad_nac=Input::get('ciudad_nac');
                }
            }
        }


        $direcc_r=Input::get('direcc_r');
        $direcc_perm=Input::get('direcc_perm');
        $estados_ubi=Input::get('estados_ubi');
        $edad=Input::get('edad');
        $telefono_r=Input::get('telefono_r');


        if ($edad!='' && $edad<=17){
            $cedula_rep=Input::get('cedula_rep');
            $nombres_rep=Input::get('nombres_rep');
            $apellidos_rep=Input::get('apellidos_rep');
            $edad_rep=Input::get('edad_rep');
            $direccion_rep=Input::get('direccion_rep');
            $telefono_rep=Input::get('telefono_rep');
            $parentesco=Input::get('parentesco');
        }else{
            $cedula_rep=null;
            $nombres_rep=null;
            $apellidos_rep=null;
            $edad_rep=null;
            $direccion_rep=null;
            $telefono_rep=null;
            $parentesco=null;
        }  
        
        

        $tipo_institucion=Input::get('tipo_institucion');
        $nom_institucion=Input::get('nom_institucion');
        $turno_institucion=Input::get('turno_institucion');
        $g_especialidad=Input::get('g_especialidad');

        if ($g_especialidad==3){
            $ex_especialidad=Input::get('ex_especialidad');
        }else{
            $ex_especialidad=null;
        }
        
        $promedio_b=Input::get('promedio_b');
        $fecha_g=Input::get('fecha_g');
        $estados_estu_secu=Input::get('estados_estu_secu');
        $ciudad_estu_secu=Input::get('ciudad_estu_secu');

        $rusnies=Input::get('rusnies');
        $actividad_1=Input::get('actividad_1');
        $actividad_2=Input::get('actividad_2');
        $actividad_3=Input::get('actividad_3');
        $actividad_4=Input::get('actividad_4');
        $carreras=Input::get('carreras');
        $turno=Input::get('turno');
        $modalidad=Input::get('modalidad');
        $infor_uba=Input::get('infor_uba');
        $nucleo=Input::get('nucleo');
        $orientacion=Input::get('orientacion');
        $decision_carrera=Input::get('decision_carrera');
        $carrera_uba=Input::get('carrera_uba');
        $turno_uba=Input::get('turno_uba');
        $modalidad_uba=Input::get('modalidad_uba');
        $nucleo=Input::get('nucleo');
        $curso_uba=Input::get('curso_uba');
        if ($curso_uba == 1) {
            $graduo_uba = Input::get('graduo_uba');
        } else {
            $graduo_uba = null;
        }
        
        $cargo_d_bachiderato=Input::get('cargo_d_bachiderato');
        $otros_estudio=Input::get('otros_estudio');
        $cargo_d_bachiderato=Input::get('cargo_d_bachiderato');

        if ($cargo_d_bachiderato==1){
            $e_cargo_d_bachiderato=Input::get('e_cargo_d_bachiderato');
        }else{
            $e_cargo_d_bachiderato=null;
        }

        $otros_estudio=Input::get('otros_estudio');
        if ($otros_estudio==1){
            $otra_institucion=Input::get('otra_institucion');
            $otra_carreras=Input::get('otra_carreras');
            $se_graduo=Input::get('se_graduo');
            if ($se_graduo == 1) {
                $nivel_estudio = Input::get('nivel_estudio');
            } else {
                $nivel_estudio = 0;
            }
        }else{
            $otra_institucion=null;
            $otra_carreras=null;
            $se_graduo=null;
            $nivel_estudio = 0;
        }

        $beisbol=Input::get('beisbol');
        $voleibol=Input::get('voleibol');
        $tenis=Input::get('tenis');
        $futbol=Input::get('futbol');
        $baloncesto=Input::get('baloncesto');
        $artes_marciales=Input::get('artes_marciales');
        $futbolito=Input::get('futbolito');
        $ajedrez=Input::get('ajedrez');
        $otros_a_d=Input::get('otros_a_d');
        $teatro=Input::get('teatro');
        $rondalla=Input::get('rondalla');
        $orfeon=Input::get('orfeon');
        $danzas=Input::get('danzas');
        $pintura=Input::get('pintura');
        $estudiantina=Input::get('estudiantina');
        $futbolito=Input::get('futbolito');
        $otras_act_artistica=Input::get('otras_act_artistica');
        $seguridad_indus=Input::get('seguridad_indus');
        $relaciones_h=Input::get('relaciones_h');
        $gru_protocolo=Input::get('gru_protocolo');
        $bombero_u=Input::get('bombero_u');
        $paramedico=Input::get('paramedico');
        $accion_comunitaria=Input::get('accion_comunitaria');
        $proteccion_indus=Input::get('proteccion_indus');
        $mejor_aprendizaje=Input::get('mejor_aprendizaje');
        $otros_grupos=Input::get('otros_grupos');

        

        $costea_gast=Input::get('costea_gast');
        if ($costea_gast==5){
            $nombre_cost_gast=Input::get('nombre_cost_gast');
        }else{
            $nombre_cost_gast=null;
        }

        $disfruta_d=Input::get('disfruta_d');
        if ($disfruta_d==5){
            $nombre_inst=Input::get('nombre_inst');
        }else{
            $nombre_inst=null;
        }
        
        $ingreso_men=Input::get('ingreso_men');
        $viven_padres=Input::get('viven_padres');
        
        $ecivil_padre=Input::get('ecivil_padre');


        $vive_con=Input::get('vive_con');
        if ($vive_con==7){
            $vive_con_ex=Input::get('vive_con_ex');
        }else{
            $vive_con_ex=null;
        }

        $posee_poliza = Input::get('posee_poliza');
        $entidad_poliza = Input::get('entidad_poliza');
        $numero_poliza = Input::get('numero_poliza');
        $entidad_aseguradora = Input::get('entidad_aseguradora');
        $beneficiario = Input::get('beneficiario');

        $adultos=Input::get('adultos');
        $ninos=Input::get('ninos');
        $cuantos_herm=Input::get('cuantos_herm');
        $cuantos_hi=Input::get('cuantos_hi');

        $ceguera=Input::get('ceguera');
        $sordera=Input::get('sordera');
        $retardo_m=Input::get('retardo_m');
        $disc_ext_s=Input::get('disc_ext_s');
        $disc_ext_i=Input::get('disc_ext_i');
        $disc_ninguna=Input::get('disc_ninguna');
        $otra_disc=Input::get('otra_disc');

        if ($otra_disc==true){
            $ex_otra_disc=Input::get('ex_otra_disc');
        }else{
            $ex_otra_disc=null;
        }

        $beisbol=Input::get('beisbol');
        $voleibol=Input::get('voleibol');
        $tenis=Input::get('tenis');
        $futbol=Input::get('futbol');
        $baloncesto=Input::get('baloncesto');
        $ates_marciales=Input::get('ates_marciales');
        $futbolito=Input::get('futbolito');
        $ajedrez=Input::get('ajedrez');
        $otros_a_d=Input::get('otros_a_d');
        $teatro=Input::get('teatro');
        $rondalla=Input::get('rondalla');
        $orfeon=Input::get('orfeon');
        $danzas=Input::get('danzas');
        $pintura=Input::get('pintura');
        $estudiantina=Input::get('estudiantina');
        $musicafolklorica=input::get('musicafolklorica');
        $futbolito=Input::get('futbolito');
        $otras_act_artistica=Input::get('otras_act_artistica');
        $seguridad_indus=Input::get('seguridad_indus');
        $relaciones_h=Input::get('relaciones_h');
        $gru_protocolo=Input::get('gru_protocolo');
        $bombero_u=Input::get('bombero_u');
        $paramedico=Input::get('paramedico');
        $accion_comunitaria=Input::get('accion_comunitaria');
        $proteccion_indus=Input::get('proteccion_indus');
        $mejor_aprendizaje=Input::get('mejor_aprendizaje');
        $otros_grupos=Input::get('otros_grupos');
        $viven_padres=Input::get('viven_padres');

        $vivira_est=Input::get('vivira_est');
        if ($vivira_est==5){
            $otra_vivienda=Input::get('otra_vivienda');
        }else{
            $otra_vivienda=null;
        }

        $costea_g=Input::get('costea_gast');

        if ($costea_g==5){
            $nombre_cost_gast=Input::get('nombre_cost_gast');
        }else{
            $nombre_cost_gast=null;
        }

        $escribe=Input::get('escribe');
        $etnia=Input::get('etnia');
        $fuma=Input::get('fuma');

        $variables1 = array(
            'tipo de solicitud' =>$tsolicitud,
            'fecha de nacimiento'=>$fecha,
            'nacionalidad'=>$tcedula,
            'c&eacute;dula'=>$cedula
        );
        //reglas para validar cada variable
        $rules1 = array(
            'tipo de solicitud' => 'required',
            'fecha de nacimiento'=>'required',
            'nacionalidad'=>'required',
            'c&eacute;dula' => 'required|numeric|digits_between:6,11',
        );
         
        //en el caso que alguna vadilacion falle retornaremos al formulario.
        $validation1 = Validator::make($variables1, $rules1);
        if ($validation1->fails()){
            $error_periodo=true;
            $message_periodo = $validation1->messages();
        }else{
            $error_periodo=false;
        }

        $variables2 = array(
            'n&uacute;mero de sobre' => $nsobre,
            'nucleo' => $nucleo,
            'primer y segundo nombre'=> $nombres,
            'primer y segundo apellido'=> $apellidos,
            'convenios' => $convenios[0],
            'sexo'=>$tsexo,
            'estado civil'=>$ecivil,
            'tel&eacute;fono'=>$telefono,
            'correo electr&oacute;nico'=>$email,
            'twitter' => $twitter,
            'facebook' => $facebook,
            'trabaja'=>$trabaja,
            'posee veh&iacute;culo'=>$vehiculo,
            'pais de nacimiento'=>$pais_nac,
            'direcci&oacute;n de residencia'=>$direcc_r,
            'tel&eacute;fono de residencia'=>$telefono_r,
            'direcci&oacute;n permanente'=>$direcc_perm,
            'estados de ubicaci&oacute;n'=>$estados_ubi,
            'ciudad de ubicaci&oacute;n'=>$ciudad_ubi,             
        );

        //reglas para validar cada variable
        $rules2 = array(
            'n&uacute;mero de sobre' => 'required|digits_between:8,13',
            'primer y segundo nombre' => 'required',
            'primer y segundo apellido'=> 'required',
            'convenios' => 'required',
            'sexo'=> 'required',
            'estado civil' => 'required',
            'tel&eacute;fono'=> 'required',
            'correo electr&oacute;nico'=>'required|email',
            'twitter'=> 'required|space',
            'facebook'=> 'required|space',
            'trabaja'=> 'required',
            'posee veh&iacute;culo'=>'required',
            'pais de nacimiento'=>'required',
            'direcci&oacute;n de residencia'=>'required',
            'tel&eacute;fono de residencia'=>'required|digits_between:12,12',
            'direcci&oacute;n permanente'=>'required',     
        );

        if ((array_key_exists('edad',Input::all())==true) && (Input::get('edad')<18) && (Input::get('edad')!='')) {
            $variables2['c&eacute;dula representante']=$cedula_rep;
            $variables2['nombre representante']=$nombres_rep;
            $variables2['apellido representante']=$apellidos_rep;
            $variables2['edad representante']=$edad_rep;
            $variables2['dir representante']=$direccion_rep;
            $variables2['tel representante']=$telefono_rep;
            $variables2['parentesco']=$parentesco;

            $rules2['c&eacute;dula representante']='required';
            $rules2['nombre representante']='required';
            $rules2['apellido representante']='required';
            $rules2['edad representante']='required';
            $rules2['dir representante']='required';
            $rules2['tel representante']='required|digits_between:12,12';
            $rules2['parentesco']='required';
        }

        if ((array_key_exists('trabaja',Input::all())==true) && Input::get('trabaja')==1) {
            $variables2['empresa']=$empresa;
            $variables2['direcci&oacute;n de la empresa']=$direc_empresa;
            $variables2['tel&eacute;fono de empresa']=$telefono_empre;
            $variables2['departamento en que labora']=$depart_empresa;
            $variables2['cargo en la empresa']=$cargo_empresa;
            $rules2['empresa']='required';
            $rules2['direcci&oacute;n de la empresa']='required';
            $rules2['tel&eacute;fono de empresa']='required|digits_between:12,12';
            $rules2['departamento en que labora']='required';
            $rules2['cargo en la empresa']='required';
        }

        if ((array_key_exists('vehiculo',Input::all())==true) && Input::get('vehiculo')==1) {
            $variables2['placa']=$placa;
            $variables2['marca']=$marca;
            $variables2['modelo']=$modelo;
            $rules2['placa']='required';
            $rules2['marca']='required';
            $rules2['modelo']='required';

        }

        if (array_key_exists('pais_nac',Input::all())==true) {
            $variables2['estados de nacimiento']=$estados_nac;
            $variables2['ciudad de nacimiento']=$ciudad_nac;
            $rules2['estados de nacimiento']='required';
            $rules2['ciudad de nacimiento']='required';
        }


        $validation2 = Validator::make($variables2, $rules2);
        if ($validation2->fails()){
            return $validation2->failed();
            $error_personales=true;
            $message_personales = $validation2->messages();
        }else{
            $error_personales=false;
        }



        $variables3 = array(
            'tipo de instituci&oacute;n' =>$tipo_institucion,
            'nombre de la instituci&oacute;n'=>$nom_institucion,
            'turno que estudi&oacute;'=>$turno_institucion,
            'especialidad'=>$g_especialidad,
            'promedio de bachillerato'=>$promedio_b,
            'fecha de graduaci&oacute;n' =>$fecha_g,
            'estados de ubicaci&oacute;n'=>$estados_ubi,
            'ciudad de ubicaci&oacute;n'=>$ciudad_ubi,
            'n rusnies'=>$rusnies,
            'actividad 1'=>$actividad_1,
            'carrera que desea estudiar' =>$carrera_uba,
            'turno carrera'=>$turno_uba,
            'modalidad carrera' =>$modalidad_uba,
            'informacion de carrera' =>$infor_uba,
            'curso uba' => $curso_uba,
            'recibi&oacute; orientaci&oacute;n vocacional'=>$orientacion,
            'decidido escogencia carrera'=>$decision_carrera,
            'representaci&oacute;n estudiantil'=>$cargo_d_bachiderato,            
            'cursa otra instituci&oacute;n'=>$otros_estudio,
        );
        $rules3 = array(
            'tipo de instituci&oacute;n' =>'required',
            'nombre de la instituci&oacute;n'=>'required',
            'turno que estudi&oacute;'=>'required',
            'especialidad' =>'required',
            'promedio de bachillerato'=>'required',
            'fecha de graduaci&oacute;n'=>'required',
            'n rusnies'=>'required',
            'actividad 1'=>'required',
            'carrera que desea estudiar'=>'required',
            'turno carrera'=>'required',
            'modalidad carrera' =>'required',
            'informacion de carrera' =>'required',
            'curso uba' => 'required',
            'recibi&oacute; orientaci&oacute;n vocacional'=>'required',
            'decidido escogencia carrera'=>'required',
            'representaci&oacute;n estudiantil'=>'required',            
            'cursa otra instituci&oacute;n'=>'required',          
        );

        if ((array_key_exists('g_especialidad',Input::all())==true) && Input::get('g_especialidad')==3){

            $variables3['exp especialidad']=$ex_especialidad;
            $rules3['exp especialidad']='required';
        }
        if ((array_key_exists('otros_estudio',Input::all())==true) && Input::get('otros_estudio')==1){
            $variables3['otra institucio']=$otra_institucion;
            $variables3['otra carreras']=$tipo_institucion;
            $variables3['se gradu&oacute;']=$se_graduo;

            $rules3['otra institucio']='required';
            $rules3['otra carreras']='required';
            $rules3['se gradu&oacute;']='required';
        }

        if ((array_key_exists('se_graduo',Input::all())==true) && Input::get('se_graduo')==1){
            $variables3['nivel de estudio']=$nivel_estudio;

            $rules3['nivel de estudio']='required';
        }

        if ((array_key_exists('curso_uba',Input::all())==true) && Input::get('curso_uba')==1){
            $variables3['graduo uba']=$graduo_uba;

            $rules3['graduo uba']='required';
        }

        if ((array_key_exists('cargo_d_bachiderato',Input::all())==true) && Input::get('cargo_d_bachiderato')==1){            
            $variables3['expl representaci&oacute;n estudiantil']=$e_cargo_d_bachiderato;
            $rules3['expl representaci&oacute;n estudiantil']='required';
        }


        $validation3 = Validator::make($variables3, $rules3);
        if ($validation3->fails()){
            $error_datossecundaria=true;
            $message_datossecundaria = $validation3->messages();
        }else{
            $error_datossecundaria=false;
        }


        $variables4 = array(
            'd&oacute;nde vivir&aacute; estudie' =>$vivira_est,
            'qui&eacute;n costea sus gastos' =>$costea_gast,
            'disfruta d' =>$disfruta_d,
            'ingreso mensual'=>$ingreso_men,
            'viven sus padres'=>$viven_padres,
            'estado civil padres'=>$ecivil_padre,
            'con quien vive ud'=>$vive_con,
            'tiene poliza' => $posee_poliza,
            'cuantos adultos'=>$adultos,
            'cuantos ni&ntilde;os'=>$ninos,
            'cuantos hermanos'=>$cuantos_herm,
            'cuantos hijos'=>$cuantos_hi,
            'zurdo o derecho'=>$escribe,
            'pertenece etnia'=>$etnia,
            'fuma'=>$fuma,              
        );
        $rules4 = array(
            'd&oacute;nde vivir&aacute; estudie' =>'required',
            'qui&eacute;n costea sus gastos'=>'required', 
            'disfruta d'=>'required',  
            'ingreso mensual' =>'required', 
            'viven sus padres' =>'required',   
            'estado civil padres'=>'required',  
            'con quien vive ud'=>'required',
            'tiene poliza' => 'required',
            'cuantos adultos'=>'required',
            'cuantos ni&ntilde;os'=>'required',
            'cuantos hermanos' =>'required',
            'cuantos hijos'=>'required',
            'zurdo o derecho'=>'required',
            'pertenece etnia'=>'required',
            'fuma'=>'required',
        );

        if ((array_key_exists('vivira_est',Input::all())==true) && Input::get('vivira_est')==5){            
            $variables4['expl d&oacute;nde vivir&aacute; estudie']=$otra_vivienda;
            $rules4['expl d&oacute;nde vivir&aacute; estudie']='required';
        }

        if ((array_key_exists('costea_gast',Input::all())==true) && Input::get('costea_gast')==5){            
            $variables4['otro costea sus gastos']=$nombre_cost_gast;
            $rules4['otro costea sus gastos']='required';
        }

        if ((array_key_exists('disfruta_d',Input::all())==true) && Input::get('disfruta_d')==5){            
            $variables4['nombre de otra instituci&oacute;n ']=$nombre_inst;
            $rules4['nombre de otra instituci&oacute;n ']='required';
        }

        if ((array_key_exists('vive_con',Input::all())==true) && Input::get('vive_con')==7){            
            $variables4['vive otra persona']=$vive_con_ex;
            $rules4['vive otra persona']='required';
        }

        if ((array_key_exists('posee_poliza',Input::all())==true) && Input::get('posee_poliza')==1){            
            $variables4['entidad poliza']=$entidad_poliza;
            $variables4['numero poliza']=$numero_poliza;

            $rules4['entidad poliza']='required';
            $rules4['numero poliza']='required|alpha_dash';
        }else{
            $variables4['entidad_aseguradora']=$entidad_aseguradora;
            $variables4['beneficiario']=$beneficiario;
            $rules4['beneficiario']='required|min:10';
            $rules4['entidad_aseguradora']='required';
        }

        if (array_key_exists('otra_disc',Input::all())==true){            
            $variables4['otra discapacidad']=$ex_otra_disc;
            $rules4['otra discapacidad']='required';
        }


        $validation4 = Validator::make($variables4, $rules4);
        if ($validation4->fails()){
            $error_datossocioeconomicos=true;
            $message_datossocioeconomicos = $validation4->messages();
        }else{
            $error_datossocioeconomicos=false;
        }


        $message_error='Por favor revise los campos incorrectos';

        if ($error_periodo==true || $error_personales==true || $error_datossecundaria==true || $error_datossocioeconomicos==true) { 
            $data=new stdClass();
            $data->cedula=$cedula;
            $data->nsobre=$nsobre;
            $vista= Redirect::action('PreinscripcionController@getModificarpre', array('data'=>Crypt::Encrypt($data)))
                ->withInput();
                if ($error_periodo==true) {
                    $vista->with('message_periodo',$message_periodo);
                    $vista->with('message_error_periodo',$message_error);
                }
                if ($error_personales==true) {
                    $vista->with('message_personales',$message_personales);
                    $vista->with('message_error_personales',$message_error);
                }
                if ($error_datossecundaria==true) {
                    $vista->with('message_datossecundaria',$message_datossecundaria);
                    $vista->with('message_error_datossecundaria',$message_error);
                }
                if ($error_datossocioeconomicos==true) {
                    $vista->with('message_datossocioeconomicos',$message_datossocioeconomicos);
                    $vista->with('message_error_datossocioeconomicos',$message_error);
                }
                $vista->with('estados_nac_selected',$estados_nac);
                $vista->with('ciudad_nac_selected',$ciudad_nac);
                $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                return $vista;
        }else{

            $celular = explode("-", $telefono);
            if ($telefono_empre==null){
                $telefono_empresa[0]=null;
                $telefono_empresa[1]=null;
            }else{
                $telefono_empresa = explode("-", $telefono_empre);
            }
               
            $telefono_residencia = explode("-", $telefono_r);

            if ($telefono_rep==null){
                $telefono_representante[0]=null;
                $telefono_representante[1]=null;
            }else{
                $telefono_representante = explode("-", $telefono_rep); 
            }
            

               //return  $sobre_nucleo[0];
            $sobre_nucleo = explode("-", $nsobre);       
            $validasobre=$sobre_nucleo[1];

            $preinscripcion_config= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                ->get();


            foreach($preinscripcion_config as $key ){
                $lapso=$key->lapso;
                $nucleo_activo=$key->{'estado_'.$nucleo};
            }  

           


            if ($sobre_nucleo[0]==00 || $sobre_nucleo[0]==01 || $sobre_nucleo[0]==02 || $sobre_nucleo[0]==03 || $sobre_nucleo[0]==50 || $sobre_nucleo[0]==51 || $sobre_nucleo[0]==52 || $sobre_nucleo[0]==53 || $sobre_nucleo[0]==54 || $sobre_nucleo[0]==56 || $sobre_nucleo[0]==57 || $sobre_nucleo[0]==58 || $sobre_nucleo[0]==59 || $sobre_nucleo[0]==60 || $sobre_nucleo[0]==61 || $sobre_nucleo[0]==63 || $sobre_nucleo[0]==64 || $sobre_nucleo[0]==65 || $sobre_nucleo[0]==68 || $sobre_nucleo[0]==69 || $sobre_nucleo[0]==71 || $sobre_nucleo[0]==74 || $sobre_nucleo[0]==72 || $sobre_nucleo[0]==79 || $sobre_nucleo[0]==91 || $sobre_nucleo[0]== 92 || $sobre_nucleo[0]== 93 || $sobre_nucleo[0]== 95 || $sobre_nucleo[0]== 100 || $sobre_nucleo[0]== 101 || $sobre_nucleo[0]== 102 || $sobre_nucleo[0]== 103 || $sobre_nucleo[0]== 104 || $sobre_nucleo[0]== 105){

                if ($sobre_nucleo[1]==101 || $sobre_nucleo[1]==102 || $sobre_nucleo[1]==103 || $sobre_nucleo[1]==104 || $sobre_nucleo[1]==105 || $sobre_nucleo[1]==106 || $sobre_nucleo[1]==107 || $sobre_nucleo[1]==108 || $sobre_nucleo[1]==109 || $sobre_nucleo[1]==110 || $sobre_nucleo[1]==220 || $sobre_nucleo[1]==111 || $sobre_nucleo[1]==130 || $sobre_nucleo[1] == 131 || $sobre_nucleo[1] == 132 || $sobre_nucleo[1] == 134 || $sobre_nucleo[1]== 135){
                   
                    if ($nucleo_activo==0){
                        $message_error='Preinscripciones No Disponible para el nucleo '.$nucleo;
                        $vista= Redirect::action('PreinscripcionController@getModificarpreinscripcion')
                            ->withInput();
                            $vista->with('message_error_periodo',$message_error);
                            $vista->with('estados_nac_selected',$estados_nac);
                            $vista->with('ciudad_nac_selected',$ciudad_nac);
                            $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                            $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                        return $vista;
                    }else{  

                            //Se elimino ya que las inscripciones es solo para regimen trimestral

                            // if ($sobre_nucleo[1]==110 || $sobre_nucleo[1]==220){
                            //     $cod_reg=0;
                            // }else{
                            //     $cod_reg=2;
                            // }

                            $preinscripcion_config= DB::table('config_web')
                                ->select('lapso')
                                ->where('cod_modulo', '=', 'PREINSC')
                                ->where('cod_reg','=',2)
                                ->orderby('lapso','DESC')
                                ->first();

                            $lapso = $preinscripcion_config->lapso;

                            $fecha_c=DB::table('preinscritos')
                                ->where('cedula','=',$cedula)
                                //->where('numsobre','=',$nsobre)
                                ->where('lapso','=',$lapso)
                                ->first();

                            $fecha_cita = $fecha_c->fecha_cita;

                            $borrar=DB::table('preinscritos')
                                ->where('cedula','=',$cedula)
                                ->where('numsobre','=',$nsobre)
                                ->where('lapso','=',$lapso)
                                ->delete();

                            $borrar_seguro_pre=DB::table('seguros_pre')
                                ->where('cedula','=',$cedula)
                                ->where('numsobre','=',$nsobre)
                                ->where('lapso','=',$lapso)
                                ->delete();
                            
                            $borrar_seguro_aseg=DB::table('seguros_pre_aseg')
                                ->where('cedula','=',$cedula)
                                ->where('lapso','=',$lapso)
                                ->where('numsobre','=',$nsobre)
                                ->delete();
                            //SQL para insertar los datos de la preinscripcion en la BD
                            $insertar=DB::table('preinscritos')
                            ->insert(array(
                                'lapso'=> $lapso,
                                'cod_nucleo' => $nucleo,
                                'tipo_solicitud'=> $tsolicitud,
                                'numsobre'=>  $nsobre,
                                'cedula' => $cedula,
                                'apellidos' => $apellidos,
                                'nombres' => $nombres,
                                'nacionalidad' =>$tcedula,
                                'fecha_nac' => date_format(date_create($fecha),'Y-m-d'),
                                'sexo' => $tsexo,
                                'edo_civil' => $ecivil,
                                'cod_celular' => $celular[0],
                                'celular' => $celular[1],
                                'email' => $email,
                                'trabaja' => $trabaja,
                                'empresa_trab' => $empresa,
                                'direccion_trab' => $direc_empresa,
                                'codtelf_trab' => $telefono_empresa[0],
                                'telf_trab' => $telefono_empresa[1],
                                'departamento_trab' => $depart_empresa,
                                'cargo_trab' =>  $cargo_empresa,
                                'vehiculo' =>$vehiculo,
                                'placa_veh' =>$placa,
                                'modelo_veh' =>$modelo,
                                'marca_veh' =>$marca,
                                'pais_nac' =>$pais_nac,
                                'estado_nac' =>$estados_nac,
                                'ciudad_nac' =>$ciudad_nac,
                                'direccion_res' =>$direcc_r,
                                'codtelf_res' => $telefono_residencia[0],
                                'telf_res' => $telefono_residencia[1],
                                'direccion_per' =>$direcc_perm,
                                'codtelf_per' =>  $telefono_residencia[0],
                                'telf_per' =>  $telefono_residencia[1],
                                'estado_direccion_per' =>$estados_ubi,
                                'ciudad_direccion_per' =>$ciudad_ubi,
                                'cedula_rep' =>$cedula_rep,
                                'apellidos_rep' =>$apellidos_rep,
                                'nombres_rep' =>$nombres_rep,
                                'edad_rep' =>$edad_rep,
                                'direccion_rep' =>$direccion_rep,
                                'parentesco' =>$parentesco,
                                'codtelf_rep' =>$telefono_representante[0],
                                'telf_rep' =>$telefono_representante[1],
                                'tipo_institucion_bach' =>$tipo_institucion,
                                'nombre_institucion_bach' => $nom_institucion,
                                'turno_bach' =>$turno_institucion,
                                'especialidad_bach' =>$g_especialidad,
                                'otra_especialidad_bach' =>$ex_especialidad,
                                'promedio_bach' =>$promedio_b,
                                'fecha_graduacion_bach' =>date_format(date_create($fecha_g),'Y-m-d'),
                                'estado_bach' =>$estados_estu_secu,
                                'ciudad_bach' => $ciudad_estu_secu,
                                'rusnies' =>$rusnies,
                                'actividad1' => $actividad_1,
                                'actividad2' => $actividad_2,
                                'actividad3' => $actividad_3,
                                'actividad4' => $actividad_4,
                                'cargos_bach' =>$cargo_d_bachiderato,
                                'nombre_cargo_bach' =>$e_cargo_d_bachiderato,
                                'orientacion_carrera' =>$orientacion,
                                'decidido_carrera' =>$decision_carrera,
                                'estudios_superior' =>$otros_estudio,
                                'instituto_superior' =>$otra_institucion,
                                'carrera_superior' =>$otra_carreras,
                                'nivel_superior' => $nivel_estudio,
                                'graduado_superior' =>$se_graduo,
                                'carrera_cursar' =>$carrera_uba,
                                'turno_carrera_cursar' =>$turno_uba,
                                'modalidad_carrera_cursar' =>$modalidad_uba,
                                'inform_carrerasuba' =>$infor_uba,
                                'depo_1' =>$beisbol,
                                'depo_2' =>$voleibol,
                                'depo_3' =>$tenis,
                                'depo_4' =>$futbol,
                                'depo_5' =>$baloncesto,
                                'depo_6' =>$artes_marciales,
                                'depo_7' =>$futbolito,
                                'depo_8' =>$ajedrez,
                                'depo_9' =>$otros_a_d,
                                'arti_1' =>$teatro,
                                'arti_2' =>$rondalla,
                                'arti_3' =>$orfeon,
                                'arti_4' =>$danzas,
                                'arti_5' =>$pintura,
                                'arti_6' =>$estudiantina,
                                'arti_7' =>$musicafolklorica,
                                'arti_8' =>$otras_act_artistica,
                                'otras_1' =>$seguridad_indus,
                                'otras_2' =>$relaciones_h,
                                'otras_3' =>$gru_protocolo,
                                'otras_4' => $bombero_u,
                                'otras_5' =>$paramedico,
                                'otras_6' =>$accion_comunitaria,
                                'otras_7' =>$proteccion_indus,
                                'otras_8' =>$mejor_aprendizaje,
                                'otras_9' =>$otros_grupos,
                                'donde_vivira' => $vivira_est,
                                'otro_donde_vivira' => $otra_vivienda,
                                'costa_estudios' =>$costea_gast,
                                'otro_coste_estudios' =>$nombre_cost_gast,
                                'ayudas' => $disfruta_d,
                                'institucion_ayudas' =>$nombre_inst,
                                'ingreso_mensual' =>$ingreso_men,
                                'viven_padres' =>$viven_padres,
                                'edo_civil_padres' =>$ecivil_padre,
                                'persona_convive' =>$vive_con,
                                'nombre_persona_convive' =>$vive_con_ex,
                                'adultos_hogar' =>$adultos,
                                'ninos_hogar' =>$ninos,
                                'hermanos' =>$cuantos_herm,
                                'hijos' =>$cuantos_hi,
                                'sueldo_trab' => $ingreso_men,
                                'fecha_solicitud' => date('Y-m-d'),
                                'discapacidad1' =>$ceguera,
                                'discapacidad2' =>$sordera,
                                'discapacidad3' =>$retardo_m,
                                'discapacidad4' =>$disc_ext_s,
                                'discapacidad5' =>$disc_ext_i,
                                'discapacidad6' =>$disc_ninguna,
                                'discapacidad7' =>$otra_disc,
                                'desc_discapacidad'=>$ex_otra_disc,
                                'fuma' => $fuma,
                                'escribe' =>$escribe,
                                'etnia' =>$etnia,
                                'estatus' => 0,
                                'cod_reg'=>2,
                                'poliza' => $posee_poliza,
                                'twitter' => $twitter,
                                'facebook' => $facebook,
                                'curso_uba' => $curso_uba,
                                'graduo_uba' => $graduo_uba,
                                'fecha_cita' => $fecha_cita
                            ));

                            if ($posee_poliza == 1) {
                                $insertar_seguros_aseg=DB::table('seguros_pre_aseg')
                                ->insert(array(
                                    'numsobre'=>  $nsobre,
                                    'cedula' => $cedula,
                                    'entidad' => mayuscula($entidad_poliza),
                                    'num_poliza' => mayuscula($numero_poliza),
                                    'lapso'=> $lapso
                                ));
                            } else{
                                $insertar_seguros_pre=DB::table('seguros_pre')
                                ->insert(array(
                                    'numsobre'=>  $nsobre,
                                    'cedula' => $cedula,
                                    'beneficiario' => mayuscula($beneficiario),
                                    'lapso'=> $lapso
                                ));
                            }
                        

                        $title = 'Imprimir Preinscripcion';
                        return View::make('preinscripcion.imprimirpreinscripcion', array(
                            'title' => $title, 
                            'cedula'=>$cedula,
                            'nsobre'=>$nsobre,
                            'lapso'=>$lapso                          
                        ));
                    }
                }else{
                $message_error='Verifique el N&uacute;mero de Sobre...';
                $vista= Redirect::action('PreinscripcionController@getModificarpreinscripcion')
                    ->withInput();
                    $vista->with('message_error_personales',$message_error);
                    $vista->with('estados_nac_selected',$estados_nac);
                    $vista->with('ciudad_nac_selected',$ciudad_nac);
                    $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                    $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                return $vista;
                } 

            }else{
                $message_error='Por favor revise n&uacute;mero de sobre';
                $vista= Redirect::action('PreinscripcionController@getModificarpreinscripcion')
                    ->withInput();
                    $vista->with('message_error_personales',$message_error);
                    $vista->with('estados_nac_selected',$estados_nac);
                    $vista->with('ciudad_nac_selected',$ciudad_nac);
                    $vista->with('ciudad_ubi_selected',$ciudad_ubi);
                    $vista->with('ciudad_estu_secu_selected',$ciudad_estu_secu);
                return $vista;
            }   

        }
        
    }

    public function postImprimir()
    {
        $cedula = Input::get('cedula');
       /* $sql_estudiante = DB::table('preinscritosx')
            ->select('cod_reg')
            ->where('cedula','=',$cedula)
            ->first();*/

         //SQL para consultar  el lapso y el regimen grabado
		$preinscripcion_config = DB::select('SELECT cod_reg,lapso FROM preinscritos WHERE cedula=? AND EXISTS (SELECT * FROM config_web WHERE lapso = preinscritos.lapso AND estado=1)',array($cedula));

        /*$cod_reg = $sql_estudiante->cod_reg;

        $preinscripcion_config= DB::table('config_web')
            ->where('cod_modulo', '=', 'PREINSC')
            ->where('cod_reg','=',$cod_reg)
            ->get();*/
        foreach($preinscripcion_config as $key ){
            $lapso=$key->lapso;
            $cod_reg=$key->cod_reg;
        }  

        $db=DB::table('preinscritos')
            ->where('cedula','=',$cedula)
            ->where('lapso','=',$lapso)
            ->get();
        $asegurado=DB::table('seguros_pre_aseg')
            ->where('cedula','=',$cedula)
            ->where('lapso','=',$lapso)
            ->first();
        $no_asegurado=DB::table('seguros_pre')
            ->where('cedula','=',$cedula)
            ->where('lapso','=',$lapso)
            ->first();
        $title="PLANILLA DE SOLICITUD DE REINCORPORACI&Oacute;N";
        $html = View::make('reportes.preinscripcion.planilla',array(
            'title' => $title,
            'value'=>$db,
            'asegurado' => $asegurado,
            'no_asegurado' => $no_asegurado
        ));

        return PDF::load($html, 'letter', 'portrait')->show($filename = 'Planilla_de_Inscripcion');
    }
    public function getReimprimir()
    {

        $title = 'Imprimir Preinscripcion';
        return View::make('preinscripcion.reimprimir', array(
            'title' => $title                      
        ));
    }

    public function postReimprimir()
    {
        $cedula=Input::get('cedula');
        $nsobre=Input::get('nsobre');

        $variables1 = array(
            'c&eacute;dula'=>$cedula,
            'n&uacute;mero de sobre' => $nsobre,
        );
        //reglas para validar cada variable
        $rules1 = array(
            'c&eacute;dula' => 'required|numeric|digits_between:6,11',
            'n&uacute;mero de sobre' => 'required|digits_between:8,13',
        );

        $validation1 = Validator::make($variables1, $rules1);
        if ($validation1->fails()){
            $error_reimprimir=true;
            $message_reimprimir = $validation1->messages();
        }else{
            $error_reimprimir=false;
        }

        $message_error='Por favor revise los campos incorrectos';
        if ($error_reimprimir==true ) {
            $vista= Redirect::action('PreinscripcionController@getReimprimir')
                ->withInput();
                if ($error_reimprimir==true) {
                    $vista->with('message_reimprimir',$message_reimprimir);
                    $vista->with('message_error_reimprimir',$message_error);
                }
            return $vista;
        }

        $preinscripcion_config= DB::table('config_web')
                        ->where('cod_modulo', '=', 'PREINSC')
                        ->where('estado', '=', '1')
                        ->get();

        foreach($preinscripcion_config as $key ){
            $lapso=$key->lapso;                
            $cod_reg=$key->cod_reg;
            if ($cod_reg==0){
                $lapso_reg=$key->lapso;
            }else{
                $lapso_trim=$key->lapso;
            }
        } 

        if(count($preinscripcion_config)==2){
            $db=DB::select('select * from preinscritos where cedula = ? and numsobre = ? and (lapso = ? or lapso = ?) limit 1',array($cedula,$nsobre,$lapso_reg,$lapso_trim));
   
        }elseif(count($preinscripcion_config)==1){
            $preinscripcion_config= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                ->where('estado', '=', 1)
                ->get();

            foreach($preinscripcion_config as $key ){
                $lapso=$key->lapso;
                $cod_reg=$key->cod_reg;
            } 
                $db=DB::table('preinscritos')
                ->where('cedula','=',$cedula)
                ->where('numsobre',$nsobre)
                ->where('lapso','=',$lapso)                
                ->get();
           
        }elseif(count($preinscripcion_config)==0){
           
            $db=DB::select('select * from preinscritos where cedula = ? and numsobre = ? ORDER BY fecha_solicitud DESC limit 1',array($cedula,$nsobre));

        } 

        $asegurado=DB::table('seguros_pre_aseg')
                    ->where('cedula','=',$cedula)
                    ->where('numsobre','=',$nsobre)
                    ->first();
        $no_asegurado=DB::table('seguros_pre')
            ->where('cedula','=',$cedula)
            ->where('numsobre','=',$nsobre)
            ->first();


        if(count($db)>0){            
            $title="PLANILLA DE SOLICITUD DE REINCORPORACI&Oacute;N";
            $html = View::make('reportes.preinscripcion.planilla',array(
                'title' => $title,
                'value'=>$db,
                'nsobre'=>$nsobre,
                'asegurado' => $asegurado,
                'no_asegurado' => $no_asegurado
            ));

            return PDF::load($html, 'letter', 'portrait')
                ->show($filename = $cedula.' Planilla_de_Inscripcion');
        }else{
            $message_error='No Coinciden los datos o no ha realizado ninguna Preinscripci&oacute;n';
            $vista= Redirect::action('PreinscripcionController@getReimprimir')
                ->withInput();
                $vista->with('message_error_reimprimir',$message_error);
            return $vista;

        }
        
    }

    public function getModificarpreinscripcion()
    {

        $title = 'Imprimir Preinscripcion';
        return View::make('preinscripcion.modificarpreinscripcion', array(
            'title' => $title                        
        ));
    }



    public function postModificarpreinscripcion()
    {
        $cedula=Input::get('cedula');
        $nsobre=Input::get('nsobre');
        $preinscripcion_config= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                ->where('estado', '=', '1')
                ->get();


            foreach($preinscripcion_config as $key ){
                $lapso=$key->lapso;                
                $cod_reg=$key->cod_reg;
                if ($cod_reg==0){
                    $lapso_reg=$key->lapso;
                }else{
                    $lapso_trim=$key->lapso;
                }
            } 


        if(count($preinscripcion_config)==2){
                $db=DB::table('preinscritos')
                ->where('cedula','=',$cedula)
                ->where('numsobre',$nsobre)
                ->where('lapso',$lapso_reg)
                ->orWhere('lapso', $lapso_trim)
                ->first(); 
        }elseif(count($preinscripcion_config)==1){
            $preinscripcion_config= DB::table('config_web')
                ->where('cod_modulo', '=', 'PREINSC')
                ->where('estado', '=', 1)
                ->get();


            foreach($preinscripcion_config as $key ){
                $lapso=$key->lapso;
                $cod_reg=$key->cod_reg;
            } 
                $db=DB::table('preinscritos')
                ->where('cedula','=',$cedula)
                ->where('numsobre',$nsobre)
                ->where('lapso','=',$lapso)                
                ->first();

        }elseif(count($preinscripcion_config)==0){
            $db=DB::table('preinscritos')
            ->where('cedula','=',$cedula)
            ->where('numsobre',$nsobre)
            ->orderby('lapso','DESC')
            ->first();

        } 


        $variables1 = array(
            'c&eacute;dula'=>$cedula,
            'n&uacute;mero de sobre' => $nsobre,
        );
        //reglas para validar cada variable
        $rules1 = array(
            'c&eacute;dula' => 'required|numeric|digits_between:6,11',
            'n&uacute;mero de sobre' => 'required|digits_between:8,13',
        );

        $validation1 = Validator::make($variables1, $rules1);
        if ($validation1->fails()){
            return $validation1->failed();
            $error_modificarpreinscripcion=true;
            $message_modificarpreinscripcion = $validation1->messages();
        }else{
            $error_modificarpreinscripcion=false;
        }

        $message_error='Por favor revise los campos incorrectos';
        if ($error_modificarpreinscripcion==true ) {
            
            $vista= Redirect::action('PreinscripcionController@getModificarpreinscripcion')
                ->withInput();
                if ($error_modificarpreinscripcion==true) {
                    $vista->with('message_modificarpreinscripcion',$message_modificarpreinscripcion);
                    $vista->with('message_error_modificarpreinscripcion',$message_error);
                }
            return $vista;
        }

        if(count($db)>0){
            $data=new stdClass();
            $data->cedula=$cedula;
            $data->nsobre=$nsobre;
            return Redirect::action('PreinscripcionController@getModificarpre', array('data'=>Crypt::Encrypt($data)));
                   
        }else{
            $message_error='No Coinciden los datos o no ha realizado ninguna Preinscripci&oacute;n';
            $vista= Redirect::action('PreinscripcionController@getModificarpreinscripcion')
                ->withInput();
                $vista->with('message_error_modificarpreinscripcion',$message_error);
            return $vista;

        }

    }

    public function getModificarpre($data)
    {
       $data=Crypt::decrypt($data);
        $cedula=$data->cedula;
        $nsobre=$data->nsobre;

        if(Session::has('message_error_periodo')){
            $tab_active='periodo';
        }elseif(Session::has('message_error_personales')){
            $tab_active='personales';
        }elseif(Session::has('message_error_datossecundaria')){
            $tab_active='datossecundaria'; 
        }elseif(Session::has('message_error_datossocioeconomicos')){
            $tab_active='datossocioeconomicos';
        }else{
            $tab_active='periodo';
        }

        $tsolicitud=array(
            ''=>'SELECCIONAR',
            '1'=>'POR PRIMERA VEZ O REINGRESO',
            '2'=>'REINCORPORACI&Oacute;N',
            '3'=>'EQUIVALENCIA');

        $tcedula=array(
            ''=>'SELECCIONAR',
            'v'=>'V',
            'e'=>'E');

        $tsexo=array(
            ''=>'SELECCIONAR',
            'M'=>'MASCULINO',
            'F'=>'FEMENINO');
        $ecivil=array(
            ''=>'SELECCIONAR',
            'S'=>'SOLTERO',
            'C'=>'CASADO',
            'D'=>'DIVORCIADO',
            'V'=>'VIUDO');

        $trabaja=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');

        $vehiculo=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');

        $pais=array(
            ''=>'SELECCIONAR',
            '1'=>'VENEZUELA',
            '2'=>'OTROS');
         $convenio=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');

        $pais_nac = array('' => 'SELECCIONAR' , ) + DB::table('db_paises')->lists('des_paises', 'ID');

        $estados = array('' => 'SELECCIONAR' , ) + DB::table('db_estados')->where('id_pais', '230')->lists('des_estados', 'ID');

        $ciudad=array(
            ''=>'SELECCIONAR'
            );

        $estados_nac=array(
            ''=>'SELECCIONAR'
            );

        $ciudad_nac=array(
            ''=>'SELECCIONAR'
            );

        $ciudad_ubi=array(
            ''=>'SELECCIONAR'
            );

        $ciudad_cs=array(
            ''=>'SELECCIONAR'
            );

        $t_institucion=array(
            ''=>'SELECCIONAR',
            '1'=>'PRIVADO',
            '2'=>'PUBLICO',
            '3'=>'MISION');

        $turno_institucion=array(
            ''=>'SELECCIONAR',
            '1'=>'MAÑANA',
            '2'=>'TARDE',
            '3'=>'NOCHE');

        $especialidad=array(
            ''=>'SELECCIONAR',
            '1'=>'CIENCIAS',
            '2'=>'HUMANIDADES',
            '3'=>'OTRA');

        $turno=array(
            ''=>'SELECCIONAR',
            '1'=>'MAÑANA',
            '2'=>'TARDE',
            '3'=>'NOCHE',
            '4'=>'REGIMEN FLEXIBLE');

        $informacion= array(
            '' =>'SELECCIONAR',
            '1' => 'REFERENCIA DE OTRO ESTUDIANTE',
            '2' => 'REDES SOCIALES',
            '3' => 'COACHING INTERNACIONAL',
            '4' => 'CONVENIO INTERNACIONAL',
            '5' => 'CONVENIO CREATEC',
            '6' => 'OTROS');


        $modalidad=array(
            ''=>'SELECCIONAR',
            '1'=>'PRESENCIAL',
            '2'=>'SEMI-PRESENCIAL',
            '3' =>'VIRTUAL');

        $nucleo=array(
            ''=>'SELECCIONAR',
            '0'=>'SEDE - SAN JOAQU&Iacute;N DE TURMERO',
            '1'=>'N&Uacute;CLEO - SAN ANTONIO DE LOS ALTOS',
            '2'=>'N&Uacute;CLEO - APURE',
            '3'=>'N&Uacute;CLEO - PUERTO ORDAZ');

        $datos_carrera=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');
        $convenios = array('' => 'SELECCIONAR' , ) + DB::table('modalidad_turno')->where('activo','=','1')->orderby('cod_modalidad_turno')->lists('instituto', 'cod_modalidad_turno');

        $institucion = array('' => 'SELECCIONAR' , ) + DB::table('instituciones')->lists('instituciones', 'id');

        $carreras = array('' => 'SELECCIONAR' , ) + DB::table('carreras')->lists('carrera', 'id');

        $vivira_est=array(
            ''=>'SELECCIONAR',
            '1'=>'CON SUS PADRES',
            '2'=>'OTRO FAMILIAR',
            '3'=>'VIVIENDO ALQUILADA O PROPIA',
            '4'=>'RESIDENCIA O PENSIÓN',
            '5'=>'OTRA VIVIENDA');

        $costea_g=array(
            ''=>'SELECCIONAR',
            '1'=>'PADRE',
            '2'=>'MADRE',
            '3'=>'BECA',
            '4'=>'UD. MISMO',
            '5'=>'OTROS');

        $disfruta_d=array(
            ''=>'SELECCIONAR',
            '1'=>'CRÉDITO EDUCATIVO',
            '2'=>'BECA',
            '3'=>'MEDIA BECA',
            '4'=>'NADA',
            '5'=>'OTRO TIPO DE AYUDA');

        $ingreso_men=array(
            ''=>'SELECCIONAR',
            '1'=>'DESDE 5000 HASTA 8000',
            '2'=>'DESDE 8001 HASTA 11000',
            '3'=>'DESDE 11001 HASTA 14000',
            '4'=>'DESDE 14001 HASTA 17000',
            '5'=>'DESDE 17001 Ó MÁS');

        $viven_padres=array(
            ''=>'SELECCIONAR',
            '1'=>'AMBOS',
            '2'=>'MADRE',
            '3'=>'PADRE',
            '4'=>'NINGUNO');


        $ecivil_p=array(
            ''=>'SELECCIONAR',
            '1'=>'SOLTERO',
            '2'=>'CASADO',
            '3'=>'DIVORCIADO',
            '4'=>'VIUDO');

        $vive_con=array(
            ''=>'SELECCIONAR',
            '1'=>'AMBOS PADRES',
            '2'=>'PADRE',
            '3'=>'MADRE',
            '4'=>'ABUELO(A)',
            '5'=>'TIO(A)',
            '6'=>'HERMANO(A)',
            '7'=>'OTRA PERSONA');

        $parentesco=array(
            ''=>'SELECCIONAR',
            '1'=>'PADRE',
            '2'=>'MADRE',
            '3'=>'TIO(A)',
            '4'=>'HERMANO(A)',
            '5'=>'ABUELO(A)',
            '6'=>'PADRASTRO',
            '7'=>'MADRASTRA',
            '8'=>'AMIGO DE LA FAMILIA',            
            '9'=>'OTRA PERSONA...');

        $escribe=array(
            ''=>'SELECCIONAR',
            '1'=>'ZURDO',
            '2'=>'DERECHO');

        $select=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '2'=>'NO');

        $select_poliza=array(
            ''=>'SELECCIONAR',
            '1'=>'SI',
            '0'=>'NO');

        $nivel_estudio =array(
            ''=>'SELECCIONAR',
            '1'=>'T.S.U',
            '2'=>'Profesional'); 

        $est_pre=DB::table('preinscritos')
            ->where('cedula','=',$cedula)
            //->where('numsobre',$nsobre)
            ->first();
        
        $carreras_uba = array('' => 'SELECCIONAR') + DB::table('carreras_uba')->where('id','=',$est_pre->carrera_cursar)->where('cod_nuc','=',$est_pre->cod_nucleo)->lists('carrera', 'id');

        $modalidad = array('' => 'SELECCIONAR') + DB::table('modalidad_carreras')->where('carrera','=',$est_pre->carrera_cursar)->lists('modalidad','id_modalidad');

        $asegurado = DB::table('seguros_pre_aseg')
        ->where('cedula','=',$cedula)
        ->where('numsobre','=',$nsobre)
        ->first();

        $no_asegurado = DB::table('seguros_pre')
        ->where('cedula','=',$cedula)
        ->where('numsobre','=',$nsobre)
        ->first();
             // return $data;
         $title = 'Imprimir Preinscripcion';
        return View::make('preinscripcion.modificardatos', array(
               'title'=>$title,
               'tab'=>$tab_active,
               'tsolicitud'=>$tsolicitud,
               'tcedula'=>$tcedula,
               'convenio'=>$convenio,
               'convenios'=>$convenios,
               'tsexo'=>$tsexo,
               'ecivil'=>$ecivil,
               'trabaja'=>$trabaja,
               'vehiculo'=>$vehiculo,
               'pais'=>$pais,
               'pais_nac'=>$pais_nac,
               'estados' => $estados,
               'estados_nac' => $estados_nac,
               'ciudad'=>$ciudad,
               'ciudad_nac'=>$ciudad_nac,
               'ciudad_ubi'=>$ciudad_ubi,
               't_institucion'=>$t_institucion,
               'turno_institucion'=>$turno_institucion,
               'especialidad'=>$especialidad,
               'informacion' =>$informacion,
               'ciudad_cs'=>$ciudad_cs,
               'carreras_uba'=>$carreras_uba,
               'carreras'=>$carreras,
               'turno'=>$turno,
               'modalidad'=>$modalidad,
               'nucleo'=>$nucleo,
               'datos_carrera'=>$datos_carrera,
               'institucion'=>$institucion,
               'carreras'=>$carreras,
               'vivira_est'=>$vivira_est,
               'costea_g'=>$costea_g,
               'disfruta_d'=>$disfruta_d,
               'ingreso_men'=>$ingreso_men,
               'viven_padres'=>$viven_padres,
               'ecivil_p'=>$ecivil_p,
               'vive_con'=>$vive_con,
               'parentesco'=>$parentesco,
               'escribe'=>$escribe, 
               'select'=>$select,
               'poliza' => $select_poliza,
               'nivel_estudio' => $nivel_estudio,
               'preinscrito'=>$est_pre,
               'asegurado' => $asegurado,
               'no_asegurado' => $no_asegurado

        ));
    }

}
