<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/','PreinscripcionController@getInicio');

Route::get('globall','PreinscripcionlineaController@getIndex');


//Ruta para controlador de los procesos inscripcion en linea
Route::controller('preinscripcion', 'PreinscripcionController');

//Ruta para controlador de los procesos inscripcion en linea internacional
Route::controller('globall', 'PreinscripcionlineaController');

?>