@extends('layouts.master')

@section('css')
{{ HTML::style('css/jquery_ui.css')}}
@stop

@section('content')
<div class="container container-margin-top">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="table-responsive">
				<div id="select" class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12" >
					<label>Donde desea estudiar:</label>
						<select name="estudio" id="estudio" class="form-control" onChange="mostrarInformacion(this);" required>
							<option>Selecionar...</option>
							<option value="V">Venezuela</option>
							<option value="E">Exterior</option>
						</select>					
					<div id="msg_select" class="text-danger">&nbsp;</div> 
				</div>
			</div>  
		</div>    
	</div>
	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-col-sm-12 col-col-xs-12" id="div_informacion" style="display:none;">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title"><strong>Informaci&oacute;n General</strong></h6>
			</div>
			<div class="panel-body text-justify">				
				<p><strong>Notas importantes:</strong></p>
					<ul>
						<li>Ser bachiller.</li>						
						<li>Recopilar y escanear los documentos necesarios para formalizar la preinscripci&oacute;n .</li>
						<li>Documentos a escanear son: el T&iacute;tulo de Bachiller y Notas Certificadas de 1ro a 5to año, original y dos copias, m&aacute;s un fondo negro del t&iacute;tulo.</li>
						<li>Adquirir el sobre informativo en la sede o n&uacute;cleo que desea cursar estudios.</li>
						<li>Llenar la planilla de inscripci&oacute;n web en su totalidad planilla de solicitud</li>
					</ul>
					<strong> Recuerda que:</strong><br/>
						<ul>
							<li>Es importante enviar los documentos, sin estos el proceso de preinscripci&oacute;n no tendr&aacute; validez.</li>							  
						</ul>
				</p>
				<br/>
				<br/>
				<form role="form" method="post" action="{{ URL::to('preinscripcionlinea/inicio') }}">						
					<div class="row centrado">
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 centrado">
								{{ Form::submit('Acceder', array('class' => 'btn btn-primary', 'onclick' => 'this.disabled=true; this.value="Enviando"; this.form.submit()')) }}
						</div>
					</div>						
				</form>	
			</div>
		</div>
	</div>
	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-col-sm-12 col-col-xs-12" id="div_informacion1" style="display:none;">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title"><strong>Informaci&oacute;n General</strong></h6>
			</div>
			<div class="panel-body text-justify">				
				<p><strong>Notas importantes:</strong></p>
					<ul>
						<li>Ser bachiller (Art&iacute;culo No. 119 de la Ley de Universidades).</li>						
						<li>Presentar el T&iacute;tulo de Bachiller y Notas Certificadas de 1ro a 5to año, original y dos copias, m&aacute;s un fondo negro del t&iacute;tulo.</li>						
						<li>Adquirir el sobre informativo en la sede o n&uacute;cleo que desea cursar estudios.</li>
						<li>Llenar la planilla de inscripci&oacute;n web en su totalidad planilla de solicitud</li>
					</ul>
				</p>
				<br/>
				<br/>
				<form role="form" method="post" action="{{ URL::to('preinscripcion/inicio') }}">						
					<div class="row centrado">
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 centrado">
								{{ Form::submit('Acceder', array('class' => 'btn btn-primary', 'onclick' => 'this.disabled=true; this.value="Enviando"; this.form.submit()')) }}
						</div>
					</div>						
				</form>	
			</div>
		</div>
	</div>
</div>
@stop
@section('postscript')
{{ HTML::script('js/solonumeros.js') }}
{{ HTML::script('js/datepicker/bootstrap-datepicker.js') }}
{{ HTML::script('js/datepicker/locales/bootstrap-datepicker.es.js') }}
<!-- Incluyo Javascript de la libreria Datatable -->
{{ HTML::script('js/jquery_ui.js') }}
{{ HTML::script('js/datatable/jquery.dataTables.js') }}
{{ HTML::script('js/datatable/dataTables.bootstrap.js') }}

<script type="text/javascript" language="javascript">
	$(document).ready(function() {

	});

	function mostrarInformacion(sel){
	  if (sel.value == 'E'){	  	
	  	document.getElementById('div_informacion1').style.display='none'; 
	    document.getElementById('div_informacion').style.display='block';      
	  }else {
	  	document.getElementById('div_informacion1').style.display='block'; 
	  	document.getElementById('div_informacion').style.display='none';    
	  }
	}
</script>

@stop