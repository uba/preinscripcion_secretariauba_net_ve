<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<style>
			.contenedor{
				background-color: #f5f5f5;
				padding-top: 50px;
				padding-bottom: 50px;
			}
			span{
    		font-size: 0.9em;
			}
			h2{
				color: #f5f5f5;
			    background: #1e2147;
			}
			a,a:hover,a:visited,a:focus{
				text-decoration: none;
			}
			small{
				color: #aaa;
			}
			p{
				margin-bottom: 50px;
				font-size: 16px;
				color: #696767;
			}
			img{
				margin-right: 100px;
			}
			.container{
				margin: auto;
		    width: 50%;
		    padding: 40px 30px;
  			background: #fff;
  			box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.1);
  			margin-bottom: 20px;
  			text-align: center;
			}
			.btn{
				color: #fff;
  			line-height: 1.25;
  			border: 1px solid transparent;
  			padding: 0.5rem 1rem;
  			font-size: 1rem;
  			border-radius: 0.25rem;
  			transition: all 0.2s ease-in-out;
			}
			.btn-info{
				background-color: #0275d8;
		    border-color: #0275d8;
			}
			.text-primary{
				color: #2b90d9 !important;
			}
			.text-uppercase {
  			text-transform: uppercase!important;
			}
			.logo{
 				font-size: 2em;
  			font-weight: 650;
			}
			.logo span{
  			color: #ccc;
			}
			.w-100{
				width: 100%;
			}
			.h-100{
				height: 100%;
			}
		</style>
	</head>
	<body>
		<div class="w-100 h-100 contenedor">
			<div class="container">
			<div class="logo text-uppercase mb-2">
			<img src="{{$message->embed(public_path().'/images/uba1.png')}}" alt="" height="150">				
      		</div>
      		<br>
      			<h2>Solicitud de Preinscripción en linea</h2>
      			<br />
      			<p>
    		Estimado(a) {{$nombres.' '.$apellidos}} su solicitud de inscripción de nuevo ingreso registrada con éxito en el lapso {{$lapso}}, debe imprimir un ejemplar de la planilla generada por el sistema, la cual deberá consignar<strong> en la institución con los recaudos exigidos en un sobre amarillo tamaño oficio, para formalizar su inscripción.</strong><br><br> Para modificar la planilla ingrese al siguiente link <a href="http://preinscripcion.secretariauba.net.ve">http://preinscripcion.secretariauba.net.ve</a><br><br>Su clave de acceso generada para modificación de datos e re-impresión de la planilla es: {{$nsobre}}</p>
			</div>
		</div>
	</body>
</html>