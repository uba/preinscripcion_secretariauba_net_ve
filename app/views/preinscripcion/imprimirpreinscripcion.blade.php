@extends('layouts.masterpreinscripcion')

@section('css')
{{ HTML::style('css/jquery_ui.css')}}
{{ HTML::style('css/datepicker.css')}}
@stop
@section('content')

<div class="container container-margin-top">

		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title"><strong>Preinscripci&oacute;n</strong></h6>
			</div>
			<div class="panel-body text-justify">

				<p><strong>Nota importante:</strong></p>
				<ul>
                    <li>Solicitud de inscripción de nuevo ingreso registrada, imprimir un ejemplar de la planilla generada por el sistema, la cual deberá consignar <span class="text-danger">en la institución con los recaudos exigidos en un sobre amarillo tamaño oficio, para formalizar su inscripción.</spam></li><br>
                    <li>Su Clave de Acceso generada para Modificación De Datos e Re-impresión De la Planilla es: {{$nsobre}}.</spam></li>	
				<br/>
				</ul>
				<form role="form" target="_blank" method="post" action="{{ URL::to('preinscripcion/imprimir') }}">
					<div class="row centrado">
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 centrado">
							{{ Form::hidden('cedula',  $cedula , $attributes = array('class' => 'form-control', 'id'=>'cedula')) }}
							{{ Form::submit('Imprimir', array('class' => 'btn btn-success', 'style' => 'text-transform: initial;')) }}
						</div>
					</div>
				</form>	
				
			</div>
		</div>
</div>
@stop
@section('postscript')
{{ HTML::script('js/jquery_ui.js') }}
{{ HTML::script('js/datepicker/bootstrap-datepicker.js') }}
{{ HTML::script('js/datepicker/locales/bootstrap-datepicker.es.js') }}
{{ HTML::script('js/mask-plugins/src/jquery.mask.js') }}
{{ HTML::script('js/sololetras.js') }}
{{ HTML::script('js/solonumeros.js') }}
@stop