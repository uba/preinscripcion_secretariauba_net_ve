@extends('layouts.preinscripcioninternacional')

@section('css')
{{ HTML::style('css/jquery_ui.css')}}
{{ HTML::style('css/datepicker.css')}}
@stop
@section('content')

<div class="container container-margin-top">

		<div class="panel panel-default">
			<div class="panel-heading">
				<h6 class="panel-title"><strong>Preinscripci&oacute;n</strong></h6>
			</div>
			
			<div class="panel-body text-justify">
				@if(Session::has('message_error_reimprimir'))
					<div>
						<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
							<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_reimprimir') }}</strong></div>
						</div>
					</div>
					<br><br><br><br>
				@endif
				<p><strong>Nota importante:</strong></p>
				<ul>
                    <li>Imprimir un ejemplar de la planilla generada por el sistema, la cual deberá consignar <span class="text-danger">en la institución con los recaudos exigidos, para formalizar su inscripción.</spam></li>
				<br/>
				</ul>
				<form role="form" method="post" target="_blank" action="{{ URL::to('global/reimprimir') }}">
					<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12   @if(Session::has('message_reimprimir') && Session::get('message_reimprimir')->has('c&eacute;dula')){{'has-error'}} @endif">
						<label for="cedula">C&eacute;dula <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
						{{ Form::text('cedula',  NULL , $attributes = array('class' => 'form-control', 'id'=>'cedula', 'onkeypress'=>'return soloNumeros(event)','maxlength'=>'11')) }}
						<div class="text-danger" id="text_uc">
							@if(Session::has('message_reimprimir') && Session::get('message_reimprimir')->has('c&eacute;dula'))	{{ Session::get('message_reimprimir')->first('c&eacute;dula') }}	@else {{ "&nbsp;" }} @endif
						</div>
					</div>
					<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_reimprimir') && Session::get('message_reimprimir')->has('n&uacute;mero de sobre')){{'has-error'}} @endif">
						<label for="nom_ape">N&uacute;mero de sobre <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
						{{ Form::text('nsobre',  NULL , $attributes = array('class' => 'form-control', 'id'=>'nsobre','data-inputmask'=>'data-inputmask="&quot;mask&quot;: &quot;99-999-00999&quot;', 'onkeypress'=>'return soloNumeros(event)')) }}
						<div class="text-danger" id="text_uc">
							@if(Session::has('message_reimprimir') && Session::get('message_reimprimir')->has('n&uacute;mero de sobre'))	{{ Session::get('message_reimprimir')->first('n&uacute;mero de sobre') }}	@else {{ "&nbsp;" }} @endif
						</div>
					</div>
				
					<div class="row centrado">
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 centrado">
							{{ Form::submit('Re-Imprimir', array('class' => 'btn btn-success', 'onclick' => 'this.disabled=true; this.value="Enviando"; this.form.submit()', 'style' => 'text-transform: initial;')) }}
						</div>
					</div>
				</form>	
			</div>
		</div>
</div>
@stop
@section('postscript')
<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		$('#nsobre').mask('999-999-000999', {placeholder: "___-___-_____"});
	});
 </script>
{{ HTML::script('js/jquery_ui.js') }}
{{ HTML::script('js/datepicker/bootstrap-datepicker.js') }}
{{ HTML::script('js/datepicker/locales/bootstrap-datepicker.es.js') }}
{{ HTML::script('js/mask-plugins/src/jquery.mask.js') }}
{{ HTML::script('js/sololetras.js') }}
{{ HTML::script('js/solonumeros.js') }}
@stop