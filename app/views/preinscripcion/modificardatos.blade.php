@extends('layouts.masterpreinscripcion')

@section('css')
{{ HTML::style('css/jquery_ui.css')}}

@stop
<A id="subir"></A>
@section('content')

<div class="container container-margin-top">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs nav-justified" role="tablist">
	  <li id="periodo1" class="negrita @if($tab=='periodo'){{'active'}}@endif"><a href="#periodo2" role="tab" data-toggle="tab">Per&iacute;odo Acad&eacute;mico</a></li>
	  <li id="personales1" class="negrita @if($tab=='personales'){{'active'}}@endif"><a href="#personales2" role="tab" data-toggle="tab">Datos Personales</a></li>
	  <li id="datossecundaria1" class="negrita @if($tab=='datossecundaria'){{'active'}}@endif"><a href="#datossecundaria2" role="tab" data-toggle="tab">Datos de Educaci&oacute;n</a></li>
      <li id="datossocioeconomicos1" class="negrita @if($tab=='datossocioeconomicos'){{'active'}}@endif"><a href="#datossocioeconomicos2" role="tab" data-toggle="tab">Datos Socioeconomicos</a></li>

    </ul>
    	<form name="parametros" role="form" method="POST" action="{{ URL::to('preinscripcion/procesarmodificacion') }}">
			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane @if($tab=='periodo'){{'active'}}@endif" id="periodo2"><br>
					<div class="panel panel-primary" >
						<div class="panel-heading"><h3 class="panel-title">Solicitud de Inscripci&oacute;n Nuevo ingreso (Regulares o Equivalencia)</h3></div>
						<div class="panel-body">
							@if(Session::has('message_error_periodo'))
								<div>
									<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
										<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_periodo') }}</strong></div>
									</div>
								</div>
							@endif
							<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_periodo') && Session::get('message_periodo')->has('tipo de solicitud')){{'has-error'}} @endif">
								<label for="nom_ape">Tipo de Solicitud <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
								{{ Form::select('tsolicitud', $tsolicitud, $preinscrito->tipo_solicitud , $attributes = array('class' => 'form-control','readonly'=>'readonly', 'id'=>'tsolicitud')) }}  
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_periodo') && Session::get('message_periodo')->has('tipo de solicitud'))	{{ Session::get('message_periodo')->first('tipo de solicitud') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_periodo') && Session::get('message_periodo')->has('fecha de nacimiento')){{'has-error'}} @endif">
								<label for="nom_ape">Fecha de Nacimiento <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
								<div class="input-group date">
					             	{{ Form::text('fecha', date_format(date_create($preinscrito->fecha_nac),'d-m-Y'), $attributes = array('class' => 'form-control', 'id'=>'fecha', 'placeholder'=>'Seleccione fecha de nacimiento', 'readonly'=>'readonly', 'required'=>'required')) }}<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
					            	{{ Form::hidden('edad',  NULL , $attributes = array('class' => 'form-control', 'id'=>'edad')) }}
					            </div>
					            <div class="text-danger" id="text_uc">
					            	@if(Session::has('message_periodo') && Session::get('message_periodo')->has('fecha de nacimiento'))	{{ Session::get('message_periodo')->first('fecha de nacimiento') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_periodo') && Session::get('message_periodo')->has('nacionalidad')){{'has-error'}} @endif">
								<label for="nom_ape">Nacionalidad <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
								{{ Form::select('tcedula', $tcedula, $preinscrito->nacionalidad , $attributes = array('class' => 'form-control', 'id'=>'tcedula', 'readonly'=>'readonly')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_periodo') && Session::get('message_periodo')->has('nacionalidad'))	{{ Session::get('message_periodo')->first('nacionalidad') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_periodo') && Session::get('message_periodo')->has('c&eacute;dula')){{'has-error'}} @endif">
								<label for="cedula">C&eacute;dula <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
								{{ Form::text('cedula',  $preinscrito->cedula , $attributes = array('class' => 'form-control', 'readonly'=>'readonly', 'id'=>'cedula', 'onkeypress'=>'return soloNumeros(event)','maxlength'=>'11')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_periodo') && Session::get('message_periodo')->has('c&eacute;dula'))	{{ Session::get('message_periodo')->first('c&eacute;dula') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 derecha">
								<a href="#subir" id="sig_1" class="btn btn-primary">Siguiente <span class="icon-arrow-right-2" style="font-size:9px;"></span></a>
							</div>
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12  @if($errors->has('documento')){{'has-error'}} @endif">
								<p><strong>Notas importantes:</strong></p>
								<ul>
				                    <li>La Universidad Bicentenaria de Aragua (UBA), en su deber de mejorar los servicios a la comunidad estudiantil, ha suscrito un convenio con "Seguros Universitas C.A" de una p&oacute;liza personal de accidentes para los estudiantes con una cobertura 7/24, durante un a&ntilde;o de aplicaci&oacute;n y uso inmediato a ser firmada. La inversi&oacute;n es muy econ&oacute;mica en comparaci&oacute;n con cualquier p&oacute;liza individual que se asuma para cubrir iguales riesgos, para lo cual ha sido estimada la prima en Bs. 1000 anual.</li>
				                    <li>Los pasos para afiliarse a este innovador servicio y asumir los beneficios de cobertura e indemnizaci&oacute;n de manera inmediata, son:</li>
				                    <li>Completar la informaci&oacute;n que se exige en la planilla de solicitud del servicio anexa a esta p&aacute;gina. (Descargarla).</li>                            
				                    <li>Consignar Copia de la c&eacute;dula de identidad del solicitante.</li>
				                    <li>Consignar planilla del pago de Bs. 1000 de la prima, que hizo a trav&eacute;s de un cheque o transferencia a nombre de Seguros Universitas C.A (RIF: J-00148811-1), en los bancos:</li>
									<li>Banesco Cuenta: 0134-1099-2700-0300-0960)</li>
									<li>Bancaribe Cuenta: 0114-0165-1016-5006-1393)</li>
									<li>Banco Nacional de Cr&eacute;dito (BNC) cuenta: 0191-0098-7821-9809-1964)</li>
									<li>BANPLUS, cuenta: 0174-0126-2712-6400-52710 &oacute; en las taquillas 03 del m&oacute;dulo "A" de CIAUBA.</li>
									<li>Dirigirse a la Oficina de Seguro Universitas, aquiacute; en la CIAUBA, al lado de carnetizaci&oacute;n, donde le entregar&aacute;n de inmediato, la poliza firmada por la aseguradora. Para mayor informaci&oacute;n, comun&iacute;quese por el tel&eacute;fono: 0414-3434365, con la Lic. Daisy Ramos, representante de Seguros Universitas; o con el Departamento de Admisi&oacute;n de la UBA a trav&eacute;s del n&uacute;mero: 0243-2650011, extensi&oacute;n 018/037.</li>
									<li>Descarga la Planilla de Seguro <a href="{{ URL::to('documents/seguro/Solicitud de Seguro Accidentes Personales.pdf') }}"  target="_blank"><strong>Aqui</strong></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane @if($tab=='personales'){{'active'}}@endif" id="personales2"><br>
					<div class="panel panel-primary">	
						<div class="panel-heading"><h3 class="panel-title">Solicitud de Inscripci&oacute;n Nuevo ingreso (Regulares o Equivalencia)</h3></div>
						<div class="panel-body">
							@if(Session::has('message_error_personales'))
								<div>
									<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
										<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_personales') }}</strong></div>
									</div>
								</div>
							@endif
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
							<p><strong>¿En que Sede Desea Cursar Estudios?</strong></p>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{'has-error'}} @endif">
							<label for="nom_ape">Nucleos <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::select('nucleo', $nucleo ,$preinscrito->cod_nucleo, $turno =array('class' => 'form-control', 'id'=>'nucleo',)) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{ Session::get('message_personales')->first('nucleos') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{'has-error'}} @endif">
							<label for="nom_ape">¿Ingresa Mediante un Convenio?<span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::select('convenio', $convenio, NULL , $attributes = array('class' => 'form-control', 'id'=>'convenio',)) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{ Session::get('message_personales')->first('nucleos') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{'has-error'}} @endif">
							<label for="nom_ape">Convenios<span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::select('convenios', $convenios, NULL , $attributes = array('class' => 'form-control', 'id'=>'convenios',)) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{ Session::get('message_personales')->first('nucleos') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo nombre')){{'has-error'}} @endif">
								<label for="nom_ape">Primer y segundo nombre <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
								{{ Form::text('nombres',  $preinscrito->nombres , $attributes = array('class' => 'form-control', 'id'=>'nombres', 'readonly'=>'readonly',)) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo nombre'))	{{ Session::get('message_personales')->first('primer y segundo nombre') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo apellido')){{'has-error'}} @endif">
								<label for="nom_ape">Primer y segundo apellido <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('apellidos',  $preinscrito->apellidos , $attributes = array('class' => 'form-control', 'id'=>'apellidos', 'readonly'=>'readonly',)) }}
								<div class="text-danger" id="text_uc">					
									@if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo apellido'))	{{ Session::get('message_personales')->first('primer y segundo apellido') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('sexo')){{'has-error'}} @endif">
								<label for="nom_ape">Sexo <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::select('tsexo', $tsexo, $preinscrito->sexo , $attributes = array('class' => 'form-control', 'id'=>'tsexo', 'readonly'=>'readonly',)) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('sexo'))	{{ Session::get('message_personales')->first('sexo') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('estado civil')){{'has-error'}} @endif">
								<label for="nom_ape">Estado civil <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::select('ecivil', $ecivil, $preinscrito->edo_civil , $attributes = array('class' => 'form-control', 'id'=>'ecivil')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('estado civil'))	{{ Session::get('message_personales')->first('estado civil') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono')){{'has-error'}} @endif">
								<label for="nom_ape">Tel&eacute;fono <span class="glyphicon glyphicon-asterisk text-danger"></span></label> 
									{{ Form::text('telefono', $preinscrito->cod_celular.''.$preinscrito->celular, $attributes = array('class' => 'form-control', 'id'=>'telefono','placeholder'=>'(____)________', 'onkeypress'=>'return soloNumeros(event)')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono'))	{{ Session::get('message_personales')->first('tel&eacute;fono') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('correo electr&oacute;nico')){{'has-error'}} @endif">
								<label for="">Correo electr&oacute;nico <span class="glyphicon glyphicon-asterisk text-danger"></span></label> 
								{{ Form::email('email', $preinscrito->email, $attributes = array('class' => 'form-control', 'id'=>'email','placeholder'=>'uba@example.com')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('correo electr&oacute;nico'))	{{ Session::get('message_personales')->first('correo electr&oacute;nico') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('twitter')){{'has-error'}} @endif">
								<label for="">Twitter </label> 
								<div class="input-group date" >
									<span class="input-group-addon">@</span>{{ Form::text('twitter', $preinscrito->twitter, $attributes = array('class' => 'form-control', 'id'=>'twitter','placeholder'=>'EjemploUba')) }}
								</div>
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('twitter'))	{{ Session::get('message_personales')->first('twitter') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('facebook')){{'has-error'}} @endif">
								<label for="">Facebook </label>
								<div class="input-group date" >
								<span class="input-group-addon">www.facebook.com/</span>{{ Form::text('facebook', $preinscrito->facebook, $attributes = array('class' => 'form-control', 'id'=>'facebook','placeholder'=>'ejemplo.uba30')) }}
								</div>
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('facebook'))	{{ Session::get('message_personales')->first('facebook') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>Datos del Trabajo</strong></legend>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_personales') && Session::get('message_personales')->has('trabaja')){{'has-error'}} @endif">
								<label for="nom_ape">¿Trabaja? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('trabaja', $trabaja, $preinscrito->trabaja , $attributes = array('class' => 'form-control', 'id'=>'trabaja')) }} 
								<div class="text-danger" id="text_uc" >
									@if(Session::has('message_personales') && Session::get('message_personales')->has('trabaja'))	{{ Session::get('message_personales')->first('trabaja') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>	
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('empresa')){{'has-error'}} @endif">
								<label for="nom_ape" id="Empresal">Empresa <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('empresa', $preinscrito->empresa_trab, $attributes = array('class' => 'form-control', 'id'=>'empresa')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('empresa'))	{{ Session::get('message_personales')->first('empresa') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n de la empresa')){{'has-error'}} @endif">
								<label for="nom_ape" id="direc_empresal">Direcci&oacute;n de la empresa <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('direc_empresa', $preinscrito->direccion_trab, $attributes = array('class' => 'form-control', 'id'=>'direc_empresa')) }}
								<div class="text-danger" id="text_uc">
									@if($errors->has('direcci&oacute;n de la empresa'))	{{ $errors->first('direcci&oacute;n de la empresa') }}	@else {{ "&nbsp;" }} @endif
									@if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n de la empresa'))	{{ Session::get('message_personales')->first('direcci&oacute;n de la empresa') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono de empresa')){{'has-error'}} @endif">
								<label for="nom_ape" id="telefono_emprel">Tel&eacute;fono de Empresa <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
									{{ Form::text('telefono_empre', $preinscrito->codtelf_trab.''.$preinscrito->telf_trab, $attributes = array('class' => 'form-control', 'id'=>'telefono_empre','placeholder'=>'(____)________', 'onkeypress'=>'return soloNumeros(event)')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono de empresa'))	{{ Session::get('message_personales')->first('tel&eacute;fono de empresa') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('departamento en que labora')){{'has-error'}} @endif">
								<label for="nom_ape" id="depart_empresal">Departamento en que labora <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('depart_empresa', $preinscrito->departamento_trab, $attributes = array('class' => 'form-control', 'id'=>'depart_empresa')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('departamento en que labora'))	{{ Session::get('message_personales')->first('departamento en que labora') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('cargo en la empresa')){{'has-error'}} @endif">
								<label for="nom_ape" id="cargo_empresal">Cargo en la empresa <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('cargo_empresa', $preinscrito->cargo_trab, $attributes = array('class' => 'form-control', 'id'=>'cargo_empresa')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('cargo en la empresa'))	{{ Session::get('message_personales')->first('cargo en la empresa') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>Datos del Vehiculo</strong></legend>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('posee veh&iacute;culo')){{'has-error'}} @endif">
								<label for="nom_ape">¿Posee veh&iacute;culo? <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
									{{ Form::select('vehiculo', $vehiculo, $preinscrito->vehiculo , $attributes = array('class' => 'form-control', 'id'=>'vehiculo')) }}
								
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('posee veh&iacute;culo'))	{{ Session::get('message_personales')->first('posee veh&iacute;culo') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('placa')){{'has-error'}} @endif"  id="placad">
								<label for="placa">Placa <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('placa', $preinscrito->placa_veh, $attributes = array('class' => 'form-control', 'id'=>'placa')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('placa'))	{{ Session::get('message_personales')->first('placa') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('marca')){{'has-error'}} @endif" id="marcad">
								<label for="nom_ape">Marca <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
									{{ Form::text('marca', $preinscrito->marca_veh, $attributes = array('class' => 'form-control', 'id'=>'marca')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('marca'))	{{ Session::get('message_personales')->first('marca') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('modelo')){{'has-error'}} @endif" id="modelod">
								<label for="nom_ape">Modelo <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('modelo', $preinscrito->modelo_veh, $attributes = array('class' => 'form-control', 'id'=>'modelo')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('modelo'))	{{ Session::get('message_personales')->first('modelo') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>		
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>Datos de Pais de Nacimiento</strong></legend>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if(Session::has('message_personales') && Session::get('message_personales')->has('pais de nacimiento')){{'has-error'}} @endif">
								<label for="nom_ape">Pais de Nacimiento <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::select('pais_nac', $pais_nac, $preinscrito->pais_nac , $attributes = array('class' => 'form-control', 'id'=>'pais_nac')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('pais de nacimiento'))	{{ Session::get('message_personales')->first('pais de nacimiento') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('estados de nacimiento')){{'has-error'}} @endif">
								<label for="estados">Estado de Nacimiento <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::select('estados_nac', $estados_nac, $preinscrito->estado_nac , $attributes = array('class' => 'form-control', 'id'=>'estados_nac')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('estados de nacimiento'))	{{ Session::get('message_personales')->first('estados de nacimiento') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>

							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_personales') && Session::get('message_personales')->has('ciudad de nacimiento')){{'has-error'}} @endif">
								<label for="ciudad">Ciudad de Nacimiento </label>
								{{ Form::select('ciudad_nac', $ciudad_nac , $preinscrito->ciudad_nac, $attributes =array('class' => 'form-control', 'id'=>'ciudad_nac')) }}
								<div class="text-danger">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('ciudad de nacimiento'))	{{ Session::get('message_personales')->first('ciudad de nacimiento') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n de residencia')){{'has-error'}} @endif" >
								<label for="nom_ape" id="">Direcci&oacute;n de residencia mientras estudie <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
									{{ Form::text('direcc_r', $preinscrito->direccion_res, $attributes = array('class' => 'form-control', 'id'=>'direcc_r')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n de residencia'))	{{ Session::get('message_personales')->first('direcci&oacute;n de residencia') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono de residencia')){{'has-error'}} @endif">
								<label for="nom_ape">Tel&eacute;fono de Residencia <span class="glyphicon glyphicon-asterisk text-danger"></span></label> 
									{{ Form::text('telefono_r', $preinscrito->codtelf_res.''.$preinscrito->telf_res, $attributes = array('class' => 'form-control', 'id'=>'telefono_r','placeholder'=>'(____)________', 'onkeypress'=>'return soloNumeros(event)')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono de residencia'))	{{ Session::get('message_personales')->first('tel&eacute;fono de residencia') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n permanente')){{'has-error'}} @endif">
								<label for="nom_ape" id="">Direcci&oacute;n permanente (trabajo o habitaci&oacute;n) <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
									{{ Form::text('direcc_perm', $preinscrito->direccion_per, $attributes = array('class' => 'form-control', 'id'=>'direcc_t_h')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n permanente'))	{{ Session::get('message_personales')->first('direcci&oacute;n permanente') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('estados de ubicaci&oacute;n')){{'has-error'}} @endif">
								<label for="estados">Estados de Ubicaci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::select('estados_ubi', $estados, $preinscrito->estado_direccion_per , $attributes = array('class' => 'form-control', 'id'=>'estados_ubi')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('estados de ubicaci&oacute;n'))	{{ Session::get('message_personales')->first('estados de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_personales') && Session::get('message_personales')->has('ciudad de ubicaci&oacute;n')){{'has-error'}} @endif">
								<label for="ciudad">Ciudad de Ubicaci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('ciudad_ubi', $ciudad ,null, $attributes =array('class' => 'form-control', 'id'=>'ciudad_ubi')) }}
								<div class="text-danger">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('ciudad de ubicaci&oacute;n'))	{{ Session::get('message_personales')->first('ciudad de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>Datos de su Representante Legal</strong></legend>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('c&eacute;dula representante')){{'has-error'}} @endif">
								<label for="nom_ape">C&eacute;dula de su Representante <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
								{{ Form::text('cedula_rep',  $preinscrito->cedula_rep , $attributes = array('class' => 'form-control', 'id'=>'cedula_rep', 'onkeypress'=>'return soloNumeros(event)')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('c&eacute;dula representante'))	{{ Session::get('message_personales')->first('c&eacute;dula representante') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('nombre representante')){{'has-error'}} @endif">
								<label for="nom_ape">Nombre de su Representante <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
								{{ Form::text('nombres_rep',  $preinscrito->nombres_rep , $attributes = array('class' => 'form-control', 'id'=>'nombres_rep')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('nombre representante'))	{{ Session::get('message_personales')->first('nombre representante') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('apellido representante')){{'has-error'}} @endif">
								<label for="nom_ape">Apellido  de su Representante <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('apellidos_rep',  $preinscrito->apellidos_rep , $attributes = array('class' => 'form-control', 'id'=>'apellidos_rep')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('apellido representante'))	{{ Session::get('message_personales')->first('apellido representante') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('edad representante')){{'has-error'}} @endif">
								<label for="nom_ape">Edad de su Representante <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
								{{ Form::text('edad_rep',  $preinscrito->edad_rep , $attributes = array('class' => 'form-control', 'id'=>'edad_rep', 'onkeypress'=>'return soloNumeros(event)')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('edad representante'))	{{ Session::get('message_personales')->first('edad representante') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('dir representante')){{'has-error'}} @endif">
								<label for="nom_ape">Direcci&oacute;n de su Representante <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
								{{ Form::text('direccion_rep',  $preinscrito->direccion_rep , $attributes = array('class' => 'form-control', 'id'=>'direccion_rep')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('dir representante'))	{{ Session::get('message_personales')->first('dir representante') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('tel representante')){{'has-error'}} @endif" id="telefono_empred">
								<label for="nom_ape" id="telefono_emprel">Tel&eacute;fono de su Representante <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
									{{ Form::text('telefono_rep', $preinscrito->codtelf_rep.''.$preinscrito->telf_rep, $attributes = array('class' => 'form-control', 'id'=>'telefono_rep','placeholder'=>'(____)________', 'onkeypress'=>'return soloNumeros(event)')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('tel representante'))	{{ Session::get('message_personales')->first('tel representante') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('parentesco')){{'has-error'}} @endif">
								<label for="estados">Parentesco <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::select('parentesco', $parentesco, $preinscrito->parentesco , $attributes = array('class' => 'form-control', 'id'=>'parentesco')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_personales') && Session::get('message_personales')->has('parentesco'))	{{ Session::get('message_personales')->first('parentesco') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 izquierda">
									<a href="#subir" id="ant_1" class="btn btn-primary"><span class="icon-arrow-left-2" style="font-size:9px;"></span> Anterior</a>
								</div>
								<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 derecha">
									<a href="#subir" id="sig_2" class="btn btn-primary">Siguiente <span class="icon-arrow-right-2" style="font-size:9px;"></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane @if($tab=='datossecundaria'){{'active'}}@endif" id="datossecundaria2"><br>
					<div class="panel panel-primary">
						<div class="panel-heading"><h3 class="panel-title">Datos de Educaci&oacute;n Secundaria</h3></div>
						<div class="panel-body">
							@if(Session::has('message_error_datossecundaria'))
								<div>
									<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
										<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_datossecundaria') }}</strong></div>
									</div>
								</div>
							@endif
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('tipo de instituci&oacute;n')){{'has-error'}} @endif">
								<label for="ciudad">Tipo de Instituci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('tipo_institucion', $t_institucion ,$preinscrito->tipo_institucion_bach, $attributes =array('class' => 'form-control', 'id'=>'t_institucion')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('tipo de instituci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('tipo de instituci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('nombre de la instituci&oacute;n')){{'has-error'}} @endif">
								<label for="nom_ape" id="">Nombre de la Instituci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
									{{ Form::text('nom_institucion', $preinscrito->nombre_institucion_bach, $attributes = array('class' => 'form-control', 'id'=>'n_institucion')) }}
								<div class="text-danger" id="">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('nombre de la instituci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('nombre de la instituci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('turno que estudi&oacute;')){{'has-error'}} @endif">
								<label for="ciudad">Turno que estudi&oacute; <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('turno_institucion', $turno_institucion ,$preinscrito->turno_bach, $attributes =array('class' => 'form-control', 'id'=>'t_institucion')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('turno que estudi&oacute;'))	{{ Session::get('message_datossecundaria')->first('turno que estudi&oacute;') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('especialidad')){{'has-error'}} @endif">
								<label for="ciudad">Especialidad <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('g_especialidad', $especialidad ,$preinscrito->especialidad_bach, $attributes =array('class' => 'form-control', 'id'=>'g_especialidad')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('especialidad'))	{{ Session::get('message_datossecundaria')->first('especialidad') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('exp especialidad')){{'has-error'}} @endif">
								<label for="nom_ape" id="">Explique <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
									{{ Form::text('ex_especialidad', $preinscrito->otra_especialidad_bach, $attributes = array('class' => 'form-control', 'id'=>'ex_especialidad')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('exp especialidad'))	{{ Session::get('message_datossecundaria')->first('exp especialidad') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('promedio de bachillerato')){{'has-error'}} @endif">
								<label for="nom_ape" >Promedio de bachillerato <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
									{{ Form::text('promedio_b', $preinscrito->promedio_bach, $attributes = array('class' => 'form-control', 'id'=>'promedio_b','placeholder'=>'__.__', 'onkeypress'=>'return soloNumeros(event)')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('promedio de bachillerato'))	{{ Session::get('message_datossecundaria')->first('promedio de bachillerato') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('fecha de graduaci&oacute;n')){{'has-error'}} @endif">
								<label for="nom_ape">Fecha de graduaci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
								<div class="input-group date" >
								   {{ Form::text('fecha_g', date_format(date_create($preinscrito->fecha_graduacion_bach),'d-m-Y'), $attributes = array('class' => 'form-control', 'id'=>'fecha_g', 'placeholder'=>'Seleccione fecha de graduaci&oacute;n', 'readonly'=>'readonly', 'required'=>'required')) }}<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>					           
					            </div>
					            <div class="text-danger" id="text_uc">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('fecha de graduaci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('fecha de graduaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>¿D&oacute;nde curs&oacute; el &uacute;ltimo a&ntilde;o de Educaci&oacute;n Secundaria?</strong></legend>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('estados de ubicaci&oacute;n')){{'has-error'}} @endif">
								<label for="estados">Estados de Ubicaci&oacute;n Educaci&oacute;n Secundaria <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::select('estados_estu_secu', $estados, $preinscrito->estado_bach , $attributes = array('class' => 'form-control', 'id'=>'estados_estu_secu')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('estados de ubicaci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('estados de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
								</div>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('ciudad de ubicaci&oacute;n')){{'has-error'}} @endif">
								<label for="ciudad">Ciudad de Ubicaci&oacute;n Educaci&oacute;n Secundaria <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('ciudad_estu_secu', $ciudad_cs ,$preinscrito->ciudad_bach, $attributes =array('class' => 'form-control', 'id'=>'ciudad_estu_secu')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('ciudad de ubicaci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('ciudad de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>

							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('n rusnies')){{'has-error'}} @endif">
								<label for="nom_ape" >N. de R.U.S.N.I.E.S <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
									{{ Form::text('rusnies', $preinscrito->rusnies, $attributes = array('class' => 'form-control', 'id'=>'rusnies')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('n rusnies'))	{{ Session::get('message_datossecundaria')->first('n rusnies') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>Desde que se gradu&oacute; en a&ntilde;os anteriores por favor indique que actividades ha realizado desde entonces .  (Al menos una actividad)</strong></legend>
							</div>	 	
						 	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 1')){{'has-error'}} @endif">
								<label for="nom_ape" >Actividad 1: <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
									{{ Form::text('actividad_1', $preinscrito->actividad1, $attributes = array('class' => 'form-control', 'id'=>'actividad_1')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 1'))	{{ Session::get('message_datossecundaria')->first('actividad 1') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 2')){{'has-error'}} @endif">
								<label for="nom_ape" >Actividad 2:</span></label> 
									{{ Form::text('actividad_2', $preinscrito->actividad2, $attributes = array('class' => 'form-control', 'id'=>'actividad_2')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 2'))	{{ Session::get('message_datossecundaria')->first('actividad 2') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 3')){{'has-error'}} @endif">
								<label for="nom_ape" >Actividad 3: </span></label> 
									{{ Form::text('actividad_3', $preinscrito->actividad3, $attributes = array('class' => 'form-control', 'id'=>'actividad_3')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 3'))	{{ Session::get('message_datossecundaria')->first('actividad 3') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 4')){{'has-error'}} @endif">
								<label for="nom_ape" >Actividad 4: </span></label> 
									{{ Form::text('actividad_4', $preinscrito->actividad4, $attributes = array('class' => 'form-control', 'id'=>'actividad_4')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 4'))	{{ Session::get('message_datossecundaria')->first('actividad 4') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>Datos de la Carrera a Estudiar</strong></legend>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('carrera que desea estudiar')){{'has-error'}} @endif">
								<label for="ciudad">Carrera que desea estudiar <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('carrera_uba', $carreras_uba ,$preinscrito->carrera_cursar, $attributes =array('class' => 'form-control', 'id'=>'carrera_uba')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('carrera que desea estudiar'))	{{ Session::get('message_datossecundaria')->first('carrera que desea estudiar') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('turno carrera')){{'has-error'}} @endif">
								<label for="ciudad">Turno <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('turno_uba',$turno_institucion  ,$preinscrito->turno_carrera_cursar, $turno=array('class' => 'form-control', 'id'=>'turno_uba')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('turno carrera'))	{{ Session::get('message_datossecundaria')->first('turno carrera') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-3 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('modalidad carrera')){{'has-error'}} @endif">
								<label for="ciudad">Modalidad de Estudios <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('modalidad_uba', $modalidad, $preinscrito->modalidad_carrera_cursar, $modalidad =array('class' => 'form-control', 'id' => 'modalidad_uba')) }}
									<div class="text-danger">
										@if(Session::has('message_datossencundaria') && Session::get('message_datossecundaria')->has('modalidad carrera')) {{
										Session::get('message_datossecundaria')->first('modalidad carrera') }} @else {{ "&nbsp;" }} @endif							
									</div>
							</div>
							
							<div class="form-group col-lg-4 col-md-3 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('recibi&oacute; orientaci&oacute;n vocacional')){{'has-error'}} @endif">
								<label for="ciudad">¿Recibi&oacute; orientaci&oacute;n de su carrera? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('orientacion', $datos_carrera ,$preinscrito->orientacion_carrera, $datos_carrera =array('class' => 'form-control', 'id'=>'orientacion')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('recibi&oacute; orientaci&oacute;n vocacional'))	{{ Session::get('message_datossecundaria')->first('recibi&oacute; orientaci&oacute;n vocacional') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('decidido escogencia carrera')){{'has-error'}} @endif">
								<label for="ciudad">¿Esta decidido con su carrera? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('decision_carrera', $convenio ,$preinscrito->decidido_carrera, $turno =array('class' => 'form-control', 'id'=>'decision_carrera')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('decidido escogencia carrera'))	{{ Session::get('message_datossecundaria')->first('decidido escogencia carrera') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('informacion de carrera')){{'has-error'}} @endif">
								<label>¿A través de que medio obtuvo información de nuestras carreras?<span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('infor_uba', $informacion, 'NULL', $attributes =array('class' => 'form-control', 'id' => 'infor_uba')) }}
							</div>
							<div class="form-group col-lg-8 col-md-8 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('curso uba')){{'has-error'}} @endif">
								<label for="ciudad">¿Usted curs&oacute; estudios anteriormente en la Universidad Bicentenaria de Aragua? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('curso_uba', $poliza ,$preinscrito->curso_uba, $attributes =array('class' => 'form-control', 'id'=>'curso_uba')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('curso uba'))	{{ Session::get('message_datossecundaria')->first('curso uba') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('graduo uba')){{'has-error'}} @endif">
								<label for="ciudad">¿Se gradu&oacute;? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								@if($preinscrito->curso_uba == 1)
									{{ Form::select('graduo_uba', $poliza ,$preinscrito->graduo_uba, $attributes =array('class' => 'form-control', 'id'=>'graduo_uba')) }}
								@else
									{{ Form::select('graduo_uba', $poliza ,NULL, $attributes =array('class' => 'form-control', 'id'=>'graduo_uba','disabled')) }}
								@endif
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('graduo uba'))	{{ Session::get('message_datossecundaria')->first('graduo uba') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
		
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>Cargos durante el bachillerato con representaci&oacute;n estudiantil, deportes</strong></legend>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('representaci&oacute;n estudiantil')){{'has-error'}} @endif">
								<label for="ciudad">¿Ha cursado ud. cargos? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('cargo_d_bachiderato', $convenio ,$preinscrito->cargos_bach, $turno =array('class' => 'form-control', 'id'=>'cargo_d_bachiderato')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('representaci&oacute;n estudiantil'))	{{ Session::get('message_datossecundaria')->first('representaci&oacute;n estudiantil') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('expl representaci&oacute;n estudiantil')){{'has-error'}} @endif">
								<label for="nom_ape" >Explique </span></label> 
									{{ Form::text('e_cargo_d_bachiderato', $preinscrito->nombre_cargo_bach, $attributes = array('class' => 'form-control', 'id'=>'e_cargo_d_bachiderato')) }}
								<div class="text-danger" id="text_uc">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('expl representaci&oacute;n estudiantil'))	{{ Session::get('message_datossecundaria')->first('expl representaci&oacute;n estudiantil') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
	
							
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>Datos de otra Instituci&oacute;n</strong></legend>
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('cursa otra instituci&oacute;n')){{'has-error'}} @endif">
								<label for="ciudad">¿Cursa en otra Instituci&oacute;n de Educ Superior? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('otros_estudio', $convenio ,$preinscrito->estudios_superior, $turno =array('class' => 'form-control', 'id'=>'otros_estudio')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('cursa otra instituci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('cursa otra instituci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('otra institucio')){{'has-error'}} @endif">
								<label for="ciudad">Instituci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('otra_institucion', $institucion ,$preinscrito->instituto_superior, $turno =array('class' => 'form-control', 'id'=>'otra_institucion')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('otra institucio'))	{{ Session::get('message_datossecundaria')->first('otra institucio') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>	
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('otra carreras')){{'has-error'}} @endif">
								<label for="ciudad">Carreras <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('otra_carreras', $carreras ,$preinscrito->carrera_superior, $turno =array('class' => 'form-control', 'id'=>'otra_carreras')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('otra carreras'))	{{ Session::get('message_datossecundaria')->first('otra carreras') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('se gradu&oacute;')){{'has-error'}} @endif">
								<label for="ciudad">¿Se gradu&oacute;? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								{{ Form::select('se_graduo', $convenio ,$preinscrito->graduado_superior, $turno =array('class' => 'form-control', 'id'=>'se_graduo')) }}
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('se gradu&oacute;'))	{{ Session::get('message_datossecundaria')->first('se gradu&oacute;') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('nivel de estudio')){{'has-error'}} @endif">
								<label for="ciudad">Nivel de Estudio <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
								@if ($preinscrito->nivel_superior > 0)
									{{ Form::select('nivel_estudio', $nivel_estudio ,$preinscrito->nivel_superior, $turno =array('class' => 'form-control', 'id'=>'nivel_estudio')) }}
								@else
									{{ Form::select('nivel_estudio', $nivel_estudio ,NULL, $turno =array('class' => 'form-control', 'id'=>'nivel_estudio', 'disabled')) }}
								@endif
								<div class="text-danger">
									@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('nivel de estudio'))	{{ Session::get('message_datossecundaria')->first('nivel de estudio') }}	@else {{ "&nbsp;" }} @endif
								</div> 
							</div>
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>Actividades Deportivas</strong></legend>
							</div>
							<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->depo_1==1)
									{{ Form::checkbox('beisbol', 1, false,array('class' => 'form-group', 'id'=>'beisbol','checked'=>'true')); }}
								@else
									{{ Form::checkbox('beisbol', 1, false,array('class' => 'form-group', 'id'=>'beisbol')); }}
								@endif
								<label for="ciudad">Beisbol </label>
							</div>
							<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->depo_2==1)
									{{ Form::checkbox('voleibol', 1, false,array('class' => 'form-group', 'id'=>'voleibol','checked'=>'true')); }}
								@else
									{{ Form::checkbox('voleibol', 1, false,array('class' => 'form-group', 'id'=>'voleibol')); }}
								@endif
								<label for="ciudad">Voleibol </label>
							</div>
							<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->depo_3==1)
									{{ Form::checkbox('tenis', 1, false,array('class' => 'form-group', 'id'=>'tenis','checked'=>'true')); }}
								@else
									{{ Form::checkbox('tenis', 1, false,array('class' => 'form-group', 'id'=>'tenis')); }}
								@endif
								<label for="ciudad" >Tenis de Mesa </label>
							</div>
							<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->depo_4==1)
									{{ Form::checkbox('futbol', 1, false,array('class' => 'form-group', 'id'=>'futbol','checked'=>'true')); }}
								@else
									{{ Form::checkbox('futbol', 1, false,array('class' => 'form-group', 'id'=>'futbol')); }}
								@endif
								<label for="ciudad">F&uacute;tbol </label>
							</div>
							<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->depo_5==1)
									{{ Form::checkbox('baloncesto', 1, false,array('class' => 'form-group', 'id'=>'baloncesto','checked'=>'true')); }} 
								@else
									{{ Form::checkbox('baloncesto', 1, false,array('class' => 'form-group', 'id'=>'baloncesto')); }} 
								@endif
								<label for="ciudad">Baloncesto </label>
							</div>
							<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->depo_6==1)
									{{ Form::checkbox('artes_marciales', 1, false,array('class' => 'form-group', 'id'=>'artes_marciales','checked'=>'true')); }}
								@else
									{{ Form::checkbox('artes_marciales', 1, false,array('class' => 'form-group', 'id'=>'artes_marciales')); }}
								@endif
								
								<label for="ciudad">Artes Marciales </label>
							</div>
							<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->depo_7==1)
									{{ Form::checkbox('futbolito', 1, false,array('class' => 'form-group', 'id'=>'futbolito','checked'=>'true')); }}
								@else
									{{ Form::checkbox('futbolito', 1, false,array('class' => 'form-group', 'id'=>'futbolito')); }}
								@endif								
								<label for="ciudad">Futbolito </label>
							</div>
							<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->depo_8==1)
									{{ Form::checkbox('ajedrez', 1, false,array('class' => 'form-group', 'id'=>'ajedrez','checked'=>'true')); }}
								@else
									{{ Form::checkbox('ajedrez', 1, false,array('class' => 'form-group', 'id'=>'ajedrez')); }}
								@endif								
								<label for="ciudad">Ajedrez </label>
							</div>
							<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->depo_9==1)
									{{ Form::checkbox('otros_a_d', 1, false,array('class' => 'form-group', 'id'=>'otros_a_d','checked'=>'true')); }}
								@else
									{{ Form::checkbox('otros_a_d', 1, false,array('class' => 'form-group', 'id'=>'otros_a_d')); }}
								@endif

								<label for="ciudad">Otros </label>
							</div>
							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>Actividades Art&iacute;sticas</strong></legend>
							</div>
							<div style="text-align: leftleft;" class=" col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->arti_1==1)
									{{ Form::checkbox('teatro', 1, false,array('class' => 'form-group', 'id'=>'teatro','checked'=>'true')); }}
								@else
									{{ Form::checkbox('teatro', 1, false,array('class' => 'form-group', 'id'=>'teatro')); }}
								@endif	
								
								<label for="ciudad">Teatro </label>								
							</div>							
							<div style="text-align: leftleft;" class=" col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->arti_2==1)
									{{ Form::checkbox('rondalla', 1, false,array('class' => 'form-group', 'id'=>'rondalla','checked'=>'true')); }}
								@else
									{{ Form::checkbox('rondalla', 1, false,array('class' => 'form-group', 'id'=>'rondalla')); }}
								@endif								
								<label for="ciudad">Rondalla </label>
							</div>							
							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->arti_3==1)
									{{ Form::checkbox('orfeon', 1, false,array('class' => 'form-group', 'id'=>'orfeon')); }}
								@else
									{{ Form::checkbox('orfeon', 1, false,array('class' => 'form-group', 'id'=>'orfeon')); }}
								@endif
								<label for="ciudad" >Orfe&oacute;n Universitario </label>
							</div>	
							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->arti_4==1)
									{{ Form::checkbox('danzas', 1, false,array('class' => 'form-group', 'id'=>'danzas','checked'=>'true')); }}
								@else
									{{ Form::checkbox('danzas', 1, false,array('class' => 'form-group', 'id'=>'danzas')); }}
								@endif

								<label for="ciudad">Danzas </label>
							</div>				
							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->arti_5==1)
									{{ Form::checkbox('pintura', 1, false,array('class' => 'form-group', 'id'=>'pintura','checked'=>'true')); }}
								@else
									{{ Form::checkbox('pintura', 1, false,array('class' => 'form-group', 'id'=>'pintura')); }}
								@endif

								<label for="ciudad">Pintura </label>
							</div>							
							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->arti_6==1)
									{{ Form::checkbox('estudiantina', 1, false,array('class' => 'form-group', 'id'=>'estudiantina','checked'=>'true')); }}
								@else
									{{ Form::checkbox('estudiantina', 1, false,array('class' => 'form-group', 'id'=>'estudiantina')); }}
								@endif

								<label for="ciudad">Estudiantina </label>
							</div>
							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->arti_7==1)
									{{ Form::checkbox('musicafolklorica', 1, false,array('class' => 'form-group', 'id'=>'musicafolklorica','checked'=>'true')); }}
								@else
									{{ Form::checkbox('musicafolklorica', 1, false,array('class' => 'form-group', 'id'=>'musicafolklorica')); }}
								@endif

								<label for="ciudad">M&uacute;sica Folkl&oacute;rica </label>
							</div>
							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->arti_7==1)
									{{ Form::checkbox('otras_act_artistica', 1, false,array('class' => 'form-group', 'id'=>'otras_act_artistica','checked'=>'true')); }}
								@else
									{{ Form::checkbox('otras_act_artistica', 1, false,array('class' => 'form-group', 'id'=>'otras_act_artistica')); }}
								@endif
								<label for="ciudad">Otras </label>
							</div>




							<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
								<legend><strong>Otras Actividades</strong></legend>
							</div>
							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
									@if ($preinscrito->otras_1==1)
										{{ Form::checkbox('seguridad_indus', 1, false,array('class' => 'form-group', 'id'=>'seguridad_indus','checked'=>'true')); }}
									@else
										{{ Form::checkbox('seguridad_indus', 1, false,array('class' => 'form-group', 'id'=>'seguridad_indus')); }}
									@endif
									<label for="ciudad">Seguridad Industrial</label>
							</div>

							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->otras_2==1)
									{{ Form::checkbox('relaciones_h', 1, false,array('class' => 'form-group', 'id'=>'relaciones_h','checked'=>'true')); }}
								@else
									{{ Form::checkbox('relaciones_h', 1, false,array('class' => 'form-group', 'id'=>'relaciones_h')); }}
								@endif
								<label for="ciudad">Relaciones Humanas </label>	
							</div>

							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->otras_3==1)
									{{ Form::checkbox('gru_protocolo', 1, false,array('class' => 'form-group', 'id'=>'gru_protocolo','checked'=>'true')); }}
								@else
									{{ Form::checkbox('gru_protocolo', 1, false,array('class' => 'form-group', 'id'=>'gru_protocolo')); }}
								@endif
								<label for="ciudad">Grupo de Protocolo </label>
							</div>

							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->otras_4==1)
									{{ Form::checkbox('bombero_u', 1, false,array('class' => 'form-group', 'id'=>'bombero_u','checked'=>'true')); }}
								@else
									{{ Form::checkbox('bombero_u', 1, false,array('class' => 'form-group', 'id'=>'bombero_u')); }}
								@endif
								<label for="ciudad">Bombero Universitario </label>
							</div>

							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->otras_5==1)
									{{ Form::checkbox('paramedico', 1, false,array('class' => 'form-group', 'id'=>'paramedico','checked'=>'true')); }}
								@else
									{{ Form::checkbox('paramedico', 1, false,array('class' => 'form-group', 'id'=>'paramedico')); }}
								@endif
								<label for="ciudad">Param&eacute;dico </label>
							</div>

							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->otras_6==1)
									{{ Form::checkbox('accion_comunitaria', 1, false,array('class' => 'form-group', 'id'=>'accion_comunitaria','checked'=>'true')); }}
								@else
									{{ Form::checkbox('accion_comunitaria', 1, false,array('class' => 'form-group', 'id'=>'accion_comunitaria')); }}
								@endif
								<label for="ciudad">Acci&oacute;n Comunitaria </label>
							</div>

							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->otras_7==1)
									{{ Form::checkbox('proteccion_indus', 1, false,array('class' => 'form-group', 'id'=>'proteccion_indus','checked'=>'true')); }}
								@else
									{{ Form::checkbox('proteccion_indus', 1, false,array('class' => 'form-group', 'id'=>'proteccion_indus')); }}
								@endif								
								<label for="ciudad">Protecci&oacute;n Industrial </label>
							</div>

							<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->otras_8==1)
									{{ Form::checkbox('mejor_aprendizaje', 1, false,array('class' => 'form-group', 'id'=>'mejor_aprendizaje','checked'=>'true')); }}
								@else
									{{ Form::checkbox('mejor_aprendizaje', 1, false,array('class' => 'form-group', 'id'=>'mejor_aprendizaje')); }}
								@endif
								<label for="ciudad">Mejoramiento del Aprendizaje </label>
							</div>

							<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
								@if ($preinscrito->otras_9==1)
									{{ Form::checkbox('otros_grupos', 1, false,array('class' => 'form-group', 'id'=>'otros_grupos','checked'=>'true')); }}
								@else
									{{ Form::checkbox('otros_grupos', 1, false,array('class' => 'form-group', 'id'=>'otros_grupos')); }}
								@endif

								<label for="ciudad">Otros Grupos </label>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 izquierda">
									<a href="#subir" id="ant_2" class="btn btn-primary"><span class="icon-arrow-left-2" style="font-size:9px;"></span> Anterior</a>
								</div>
								<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 derecha">
									<a href="#subir" id="sig_3" class="btn btn-primary">Siguiente <span class="icon-arrow-right-2" style="font-size:9px;"></span></a>
								</div>
							</div>
								
						</div>
					</div>
				</div>

				<div class="tab-pane @if($tab=='datossocioeconomicos'){{'active'}}@endif" id="datossocioeconomicos2">
					<br>
					<div class="panel panel-primary">

						<div class="panel-heading">
						    <h3 class="panel-title">Datos Socioeconomicos</h3>
						</div>
						
							<div class="panel-body">

									@if(Session::has('message_error_datossocioeconomicos'))
										<div>
											<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
												<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_datossocioeconomicos') }}</strong></div>
											</div>
										</div>
									@endif	

									<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('d&oacute;nde vivir&aacute; estudie')){{'has-error'}} @endif">
										<label for="ciudad">¿D&oacute;nde vivir&aacute; Ud. mientras estudie? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
										{{ Form::select('vivira_est', $vivira_est ,$preinscrito->donde_vivira, $turno =array('class' => 'form-control', 'id'=>'vivira_est')) }}
										<div class="text-danger">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('d&oacute;nde vivir&aacute; estudie'))	{{ Session::get('message_datossocioeconomicos')->first('d&oacute;nde vivir&aacute; estudie') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>


									<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('expl d&oacute;nde vivir&aacute; estudie')){{'has-error'}} @endif">
										<label for="nom_ape" id="">¿Explique d&oacute;nde vivir&aacute; Ud. mientras estudie?<span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
											{{ Form::text('otra_vivienda', $preinscrito->otro_donde_vivira, $attributes = array('class' => 'form-control', 'id'=>'otra_vivienda')) }}
										<div class="text-danger" id="text_uc">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('expl d&oacute;nde vivir&aacute; estudie'))	{{ Session::get('message_datossocioeconomicos')->first('expl d&oacute;nde vivir&aacute; estudie') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>


									<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('qui&eacute;n costea sus gastos')){{'has-error'}} @endif">
										<label for="ciudad">¿Qui&eacute;n costea sus gastos de estudios? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
										{{ Form::select('costea_gast', $costea_g ,$preinscrito->costa_estudios, $turno =array('class' => 'form-control', 'id'=>'costea_gast')) }}
										<div class="text-danger">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('qui&eacute;n costea sus gastos'))	{{ Session::get('message_datossocioeconomicos')->first('qui&eacute;n costea sus gastos') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>

									<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('otro costea sus gastos')){{'has-error'}} @endif">
										<label for="nom_ape" id="">¿Nombre que otro costea sus gastos? </label> 
											{{ Form::text('nombre_cost_gast', $preinscrito->otro_coste_estudios, $attributes = array('class' => 'form-control', 'id'=>'nombre_cost_gast')) }}
										<div class="text-danger" id="text_uc">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('otro costea sus gastos'))	{{ Session::get('message_datossocioeconomicos')->first('otro costea sus gastos') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>

									<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('disfruta d')){{'has-error'}} @endif">
										<label for="ciudad">¿Disfruta Ud. de? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
										{{ Form::select('disfruta_d', $disfruta_d ,$preinscrito->ayudas, $turno =array('class' => 'form-control', 'id'=>'disfruta_d')) }}
										<div class="text-danger">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('disfruta d'))	{{ Session::get('message_datossocioeconomicos')->first('disfruta d') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>


									<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('nombre de otra instituci&oacute;n ')){{'has-error'}} @endif">
										<label for="nom_ape" id="">Nombre de otra instituci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
											{{ Form::text('nombre_inst', $preinscrito->institucion_ayudas, $attributes = array('class' => 'form-control', 'id'=>'nombre_inst')) }}
										<div class="text-danger" id="text_uc">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('nombre de otra instituci&oacute;n '))	{{ Session::get('message_datossocioeconomicos')->first('nombre de otra instituci&oacute;n ') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>
								

									<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('ingreso mensual')){{'has-error'}} @endif">
										<label for="ciudad">Indique el ingreso mensual familiar <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
										{{ Form::select('ingreso_men', $ingreso_men ,$preinscrito->ingreso_mensual, $turno =array('class' => 'form-control', 'id'=>'ingreso_men')) }}
										<div class="text-danger">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('ingreso mensual'))	{{ Session::get('message_datossocioeconomicos')->first('ingreso mensual') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>

									<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('viven sus padres')){{'has-error'}} @endif">
										<label for="ciudad">¿Viven sus padres? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
										{{ Form::select('viven_padres', $viven_padres ,$preinscrito->viven_padres, $turno =array('class' => 'form-control', 'id'=>'viven_padres')) }}
										<div class="text-danger">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('viven sus padres'))	{{ Session::get('message_datossocioeconomicos')->first('viven sus padres') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>

									<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('estado civil padres')){{'has-error'}} @endif">
										<label for="ciudad">¿Estado civil de sus padres? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
										{{ Form::select('ecivil_padre', $ecivil_p ,$preinscrito->edo_civil_padres, $turno =array('class' => 'form-control', 'id'=>'ecivil_padre')) }}
										<div class="text-danger">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('estado civil padres'))	{{ Session::get('message_datossocioeconomicos')->first('estado civil padres') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>


									<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('con quien vive ud')){{'has-error'}} @endif">
										<label for="ciudad">¿Con quien vive Ud.? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
										{{ Form::select('vive_con', $vive_con ,$preinscrito->persona_convive, $turno =array('class' => 'form-control', 'id'=>'vive_con')) }}
										<div class="text-danger">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('con quien vive ud'))	{{ Session::get('message_datossocioeconomicos')->first('con quien vive ud') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>
									<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('vive otra persona')){{'has-error'}} @endif">
										<label for="nom_ape" id="">Indique el Nombre <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
											{{ Form::text('vive_con_ex', $preinscrito->nombre_persona_convive, $attributes = array('class' => 'form-control', 'id'=>'vive_con_ex')) }}
										<div class="text-danger" id="text_uc">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('vive otra persona'))	{{ Session::get('message_datossocioeconomicos')->first('vive otra persona') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>
									<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('tiene poliza')){{'has-error'}} @endif">
							<label for="ciudad">¿Usted posee una p&oacute;liza de seguros? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('posee_poliza', $poliza ,$preinscrito->poliza, $attributes =array('class' => 'form-control', 'id'=>'posee_poliza')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('tiene poliza'))	{{ Session::get('message_datossocioeconomicos')->first('tiene poliza') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('entidad poliza')){{'has-error'}} @endif">
							<label for="ciudad">¿Entidad de donde proviene su p&oacute;liza? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							@if($preinscrito->poliza == 1 && count($asegurado)>0)
							{{ Form::text('entidad_poliza',$asegurado->entidad, $attributes =array('class' => 'form-control', 'id'=>'entidad_poliza')) }}
							@else
							{{ Form::text('entidad_poliza',NULL, $attributes =array('class' => 'form-control', 'id'=>'entidad_poliza', 'readOnly' => 'readOnly')) }}
							@endif
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('entidad poliza'))	{{ Session::get('message_datossocioeconomicos')->first('entidad poliza') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('numero poliza')){{'has-error'}} @endif">
							<label for="ciudad">Nº de p&oacute;liza <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							@if($preinscrito->poliza == 1 && count($asegurado)>0)
								{{ Form::text('numero_poliza',$asegurado->num_poliza, $attributes =array('class' => 'form-control mayuscula', 'id'=>'numero_poliza')) }}
							@else
								{{ Form::text('numero_poliza',NULL, $attributes =array('class' => 'form-control mayuscula', 'id'=>'numero_poliza', 'readOnly' => 'readOnly')) }}
							@endif
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('numero poliza'))	{{ Session::get('message_datossocioeconomicos')->first('numero poliza') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<p><strong>Nota:</strong> Si usted posee una poliza de accidentes personales debe anexar al sobre la copia de la misma.</p>
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('entidad_aseguradora')){{'has-error'}} @endif">
							<label for="ciudad">Entidad aseguradora <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							@if($preinscrito->poliza == 0 && count($no_asegurado)>0)
							{{ Form::text('entidad_aseguradora','C.A DE SEGUROS LA OCCIDENTAL', $attributes =array('class' => 'form-control', 'id'=>'entidad_aseguradora', 'readOnly' => 'readOnly')) }}
							@else
							{{ Form::text('entidad_aseguradora',NULL, $attributes =array('class' => 'form-control', 'id'=>'entidad_aseguradora', 'readOnly' => 'readOnly')) }}
							@endif
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('entidad_aseguradora'))	{{ Session::get('message_datossocioeconomicos')->first('entidad_aseguradora') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('beneficiario')){{'has-error'}} @endif">
							<label for="ciudad">Indique el nombre del beneficiario <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							@if($preinscrito->poliza == 0 && count($no_asegurado)>0)
							{{ Form::text('beneficiario',$no_asegurado->beneficiario, $attributes =array('class' => 'form-control', 'placeholder' => 'NOMBRE DEL BENEFICIARIO','id'=>'beneficiario')) }}
							@else
							{{ Form::text('beneficiario',NULL, $attributes =array('class' => 'form-control', 'id'=>'beneficiario', 'readOnly' => 'readOnly')) }}
							@endif
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('beneficiario'))	{{ Session::get('message_datossocioeconomicos')->first('beneficiario') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>

								<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
									<legend><strong>Personas que viven en su hogar</strong></legend>
								</div>

									<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos adultos')){{'has-error'}} @endif">
										<label for="nom_ape" id="l">Adultos <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
											{{ Form::text('adultos', $preinscrito->adultos_hogar, $attributes = array('class' => 'form-control', 'id'=>'adultos', 'onkeypress'=>'return soloNumeros(event)')) }}
										<div class="text-danger" id="text_uc">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos adultos'))	{{ Session::get('message_datossocioeconomicos')->first('cuantos adultos') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>
									<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos ni&ntilde;os')){{'has-error'}} @endif">
										<label for="nom_ape" id="">Ni&ntilde;os <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
											{{ Form::text('ninos', $preinscrito->ninos_hogar, $attributes = array('class' => 'form-control', 'id'=>'ninos', 'onkeypress'=>'return soloNumeros(event)')) }}
										<div class="text-danger" id="text_uc">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos ni&ntilde;os'))	{{ Session::get('message_datossocioeconomicos')->first('cuantos ni&ntilde;os') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>
									<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos hermanos')){{'has-error'}} @endif">
										<label for="nom_ape" id="">¿Cuantos hermanos tiene? <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
											{{ Form::text('cuantos_herm', $preinscrito->hermanos, $attributes = array('class' => 'form-control', 'id'=>'cuantos_herm', 'onkeypress'=>'return soloNumeros(event)')) }}
										<div class="text-danger" id="text_uc">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos hermanos'))	{{ Session::get('message_datossocioeconomicos')->first('cuantos hermanos') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>
									<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos hijos')){{'has-error'}} @endif">
										<label for="nom_ape" id="">¿Cuantos Hijos tiene? <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
											{{ Form::text('cuantos_hi', $preinscrito->hijos, $attributes = array('class' => 'form-control', 'id'=>'cuantos_hi', 'onkeypress'=>'return soloNumeros(event)')) }}
										<div class="text-danger" id="text_uc">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos hijos'))	{{ Session::get('message_datossocioeconomicos')->first('cuantos hijos') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>
								<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
									<legend><strong>Posee alguna discapacidad</strong></legend>
								</div>

									<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
										@if ($preinscrito->discapacidad1==1)
											{{ Form::checkbox('ceguera','1', false,array('class' => 'form-group', 'id'=>'ceguera','checked'=>'true' )); }}
										@else
											{{ Form::checkbox('ceguera','1', false,array('class' => 'form-group', 'id'=>'ceguera' )); }}
										@endif
										<label for="ciudad">Ceguera Total </label>
									</div>
									<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
										@if ($preinscrito->discapacidad2==1)
											{{ Form::checkbox('sordera', '1', false,array('class' => 'form-group', 'id'=>'sordera','checked'=>'true')); }}
										@else
											{{ Form::checkbox('sordera', '1', false,array('class' => 'form-group', 'id'=>'sordera')); }}
										@endif

										<label for="ciudad">Sordera Total </label>
									</div>
									<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
										@if ($preinscrito->discapacidad3==1)
											{{ Form::checkbox('retardo_m', '1', false,array('class' => 'form-group', 'id'=>'retardo_m','checked'=>'true')); }}
										@else
											{{ Form::checkbox('retardo_m', '1', false,array('class' => 'form-group', 'id'=>'retardo_m')); }}
										@endif

										<label for="ciudad">Retardo Mental </label>
									</div>
									<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
										@if ($preinscrito->discapacidad4==1)
											{{ Form::checkbox('disc_ext_s', '1', false,array('class' => 'form-group', 'id'=>'disc_ext_s','checked'=>'true')); }}
										@else
											{{ Form::checkbox('disc_ext_s', '1', false,array('class' => 'form-group', 'id'=>'disc_ext_s')); }}
										@endif

										<label for="ciudad">Disc. de Extremidades Superiores </label>
									</div>

									<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
										@if ($preinscrito->discapacidad5==1)
											{{ Form::checkbox('disc_ext_i', '1', false,array('class' => 'form-group', 'id'=>'disc_ext_i','checked'=>'true')); }}
										@else
											{{ Form::checkbox('disc_ext_i', '1', false,array('class' => 'form-group', 'id'=>'disc_ext_i')); }}
										@endif

										<label for="ciudad">Disc. de Extremidades Inferiores </label>
									</div>
									<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
										@if ($preinscrito->discapacidad6==1)
											{{ Form::checkbox('disc_ninguna', '1', false,array('class' => 'form-group', 'id'=>'disc_ninguna','checked'=>'true')); }}
										@else
											{{ Form::checkbox('disc_ninguna', '1', false,array('class' => 'form-group', 'id'=>'disc_ninguna')); }}
										@endif

										<label for="ciudad">Ninguna </label>
									</div>
									<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
										@if ($preinscrito->discapacidad7==1)
											{{ Form::checkbox('otra_disc', '1', false,array('class' => 'form-group', 'id'=>'otra_disc','checked'=>'true')); }}
										@else
											{{ Form::checkbox('otra_disc', '1', false,array('class' => 'form-group', 'id'=>'otra_disc')); }}
										@endif

										<label for="ciudad">Otra </label>
									</div>
									<div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('otra discapacidad')){{'has-error'}} @endif">
										<label for="nom_ape" id="">Explique la otra discapacidad: <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
											{{ Form::text('ex_otra_disc', $preinscrito->desc_discapacidad, $attributes = array('class' => 'form-control', 'id'=>'ex_otra_disc')) }}
										<div class="text-danger" id="text_uc">
											@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('otra discapacidad'))	{{ Session::get('message_datossocioeconomicos')->first('otra discapacidad') }}	@else {{ "&nbsp;" }} @endif
										</div> 
									</div>

								<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('zurdo o derecho')){{'has-error'}} @endif">
									<label for="ciudad">¿Zurdo o Derecho? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
									{{ Form::select('escribe', $escribe ,$preinscrito->escribe, $turno =array('class' => 'form-control', 'id'=>'escribe')) }}
									<div class="text-danger">
										@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('zurdo o derecho'))	{{ Session::get('message_datossocioeconomicos')->first('zurdo o derecho') }}	@else {{ "&nbsp;" }} @endif
									</div> 
								</div>
								<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('pertenece etnia')){{'has-error'}} @endif">
									<label for="ciudad">¿Pertenece Ud. a una Etnia? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
									{{ Form::select('etnia', $select ,$preinscrito->etnia, $turno =array('class' => 'form-control', 'id'=>'etnia')) }}
									<div class="text-danger">
										@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('pertenece etnia'))	{{ Session::get('message_datossocioeconomicos')->first('pertenece etnia') }}	@else {{ "&nbsp;" }} @endif
									</div> 
								</div>
								<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('fuma')){{'has-error'}} @endif">
									<label for="ciudad">¿Ud. Fuma? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
									{{ Form::select('fuma', $select ,$preinscrito->fuma, $turno =array('class' => 'form-control', 'id'=>'fuma')) }}
									<div class="text-danger">
										@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('fuma'))	{{ Session::get('message_datossocioeconomicos')->first('fuma') }}	@else {{ "&nbsp;" }} @endif
									</div> 
								</div>								

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 izquierda">
										<a href="#subir" id="ant_3" class="btn btn-primary"><span class="icon-arrow-left-2" style="font-size:9px;"></span> Anterior</a>
									</div>
									<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 derecha">
										{{ Form::submit('Modificar Pre-Inscripci&oacute;n', array('class' => 'btn btn-success', 'onclick' => 'this.disabled=true; this.value="Enviando"; this.form.submit()', 'style' => 'text-transform: initial;')) }}
									</div>
								</div>

							</div>
					</div>
				</div>
			</div>
		</form>
</div>
@stop
@section('postscript')

<script type="text/javascript" language="javascript"> 

    $(document).ready(function() {
	    $('#fecha,#fecha_g').datepicker({
	        language: "es",
	        autoclose: true,
	        dateFormat: "dd-mm-yy",
	        changeMonth:true,
	        changeYear:true,
	        yearRange: "c-90:c+0",
	        monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"], // Names of months for drop-down and formatting
		    monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"], // For formatting
		    dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"], // For formatting
		    dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"], // For formatting
		    dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"], // Column headings for days starting at Sunday   
	    });

		//$('#nsobre').mask('999-999-000999', {placeholder: "__-__-_____"});
		$('#telefono').mask('9999-9999999', {placeholder: "____-________"});
		$('#telefono_empre').mask('9999-9999999', {placeholder: "____-________"});
		$('#telefono_r').mask('9999-9999999', {placeholder: "____-________"});
		$('#telefono_rep').mask('9999-9999999', {placeholder: "____-________"});
		$('#promedio_b').mask('99.00', {placeholder: "__.__"});
		$('#cedula').mask('99999999999');
		$('#cedula_rep').mask('99999990');
		$('#edad_rep').mask('90');

		var fecha_nac = $("#fecha").val();
		if (fecha_nac=='') {
			document.getElementById('cedula_rep').disabled = true;
			document.getElementById('nombres_rep').disabled = true;
			document.getElementById('apellidos_rep').disabled = true;
			document.getElementById('edad_rep').disabled = true;
			document.getElementById('direccion_rep').disabled = true;
			document.getElementById('telefono_rep').disabled = true;
			document.getElementById('parentesco').disabled = true;	
		}else{
			var div = fecha_nac.split("-");
			var fecha_nacimiento = div[2]+'/'+div[1]+'/'+div[0];
			var dia = div[0];
			var mes = div[1];
			var ano = div[2];
			fecha_hoy = new Date();
			ahora_ano = fecha_hoy.getYear();
			ahora_mes = fecha_hoy.getMonth();
			ahora_dia = fecha_hoy.getDate();
			edad = (ahora_ano + 1900) - ano;			    
		    if ( ahora_mes < (mes - 1)){ 
		      edad--;
		    }
		    if (((mes - 1) == ahora_mes) && (ahora_dia < dia)){ 
		      edad--;
		    }
		    if (edad > 1900){
		        edad -= 1900;
		    }
			$("#edad").val(edad);
			if (edad>17){
				document.getElementById('cedula_rep').disabled = true;
				document.getElementById('nombres_rep').disabled = true;
				document.getElementById('apellidos_rep').disabled = true;
				document.getElementById('edad_rep').disabled = true;
				document.getElementById('direccion_rep').disabled = true;
				document.getElementById('telefono_rep').disabled = true;
				document.getElementById('parentesco').disabled = true;													
			}else{
				document.getElementById('cedula_rep').disabled = false;
				document.getElementById('nombres_rep').disabled = false;
				document.getElementById('apellidos_rep').disabled = false;
				document.getElementById('edad_rep').disabled = false;
				document.getElementById('direccion_rep').disabled = false;
				document.getElementById('telefono_rep').disabled = false;
				document.getElementById('parentesco').disabled = false;	
			}
		}
			

		var selec_e = document.getElementById('trabaja').value;
    	if (selec_e==1){
    		document.getElementById('empresa').disabled = false;
    		document.getElementById('direc_empresa').disabled = false;
    		document.getElementById('telefono_empre').disabled = false;
    		document.getElementById('depart_empresa').disabled = false;
    		document.getElementById('cargo_empresa').disabled = false;
    	}else{
    		document.getElementById('empresa').disabled = true;
    		document.getElementById('direc_empresa').disabled = true;
    		document.getElementById('telefono_empre').disabled = true;
    		document.getElementById('depart_empresa').disabled = true;
    		document.getElementById('cargo_empresa').disabled = true;
    	}

    	var selec_v = document.getElementById('vehiculo').value;
    	if (selec_v==1){
    		document.getElementById('placa').disabled = false;
    		document.getElementById('marca').disabled = false;
    		document.getElementById('modelo').disabled = false;
		}else{
			document.getElementById('placa').disabled = true;
			document.getElementById('marca').disabled = true;
			document.getElementById('modelo').disabled = true;
		}

		var nucleo =document.getElementById('nucleo').value;
		if (nucleo==0){
			document.getElementById('convenio').disabled = false;
		}

		$('#nucleo').change(function(){
			var nucleo = $(this).val();
			if (nucleo==0) {
				$('#convenio').prop('disabled', false);
			}else{
				$('#convenio').prop('disabled', true);
				$('#convenio').val('');
			}
		});


		var convenio = document.getElementById('convenio').value;
    	if (convenio==1){
			document.getElementById('convenios').disabled = false;
			
		}
		else if(convenio==0){
			document.getElementById('convenios').disabled = true;
		}
		else{
			document.getElementById('convenios').disabled = true;
		}

		$('#convenio').change(function(){ 
	    	var convenio = $(this).val();
	    	if (convenio==1){
	    		$('#convenios').prop('disabled',false);
			}else{
				$('#convenios').prop('disabled',true);
				$('#convenios').val('');
			}
		});

		var pais_nac= $('#pais_nac').val();
		if (pais_nac==230) {
			$('#ciudad_estu_secu').prop('disabled', false);
			$('#estados_estu_secu').prop('disabled', false);

		}
		else{
			$('#ciudad_estu_secu').prop('disabled', true);
			$('#estados_estu_secu').prop('disabled', true);
		}

		$('#pais_nac').change(function(){

			var pais_nac = $(this).val();
			if (pais_nac==230) {
				$('#estados_estu_secu').prop('disabled', false);
				$('#ciudad_estu_secu').prop('disabled', false);
			}
			else {
				$('#estados_estu_secu').prop('disabled', true);
				$('#ciudad_estu_secu').prop('disabled', true);
			}
		})



		var pais = $("#pais_nac").val();
		if (pais!=''){ 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxestados_nac')}}", 
				dataType: "json",
				data: {
					pais: pais,
				},
				cache: false,
				success: function(data)
				{
					var estados_select = '<option value="">Seleccionar...</option>';
					for(datos in data.estados){
						estados_select+='<option value="'+data.estados[datos].id_estados+'">'+data.estados[datos].des_estados+'</option>';
					}
					$("#estados_nac").html(estados_select);
					<?php if ($preinscrito->estado_nac) { ?>
						var estados_nac_selected= {{$preinscrito->estado_nac}};
						if (estados_nac_selected != '') {
							$('#estados_nac option[value="'+estados_nac_selected+'"]').attr('selected','selected');
						};
					<?php } ?>
					var estado = $("#estados_nac").val();
				
				} 
			});
		};

		<?php if ($preinscrito->estado_nac) { ?>
			var estado = {{$preinscrito->estado_nac}};
		
			if (estado!=''){		 
				$.ajax({
					type: "POST",
					url: "{{URL::action('PreinscripcionController@postAjaxciudades_nac')}}", 
					dataType: "json",
					data: {
						estado: estado,
					},
					cache: false,
					success: function(data)
					{
						var ciudad_select = '<option value="">Seleccionar...</option>';
						for(datos in data.ciudad){
							ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
						}
						$("#ciudad_nac").html(ciudad_select);

						<?php if ($preinscrito->ciudad_nac) { ?>
							var ciudad_nac_selected= {{$preinscrito->ciudad_nac}};
							if (ciudad_nac_selected != '') {
								$('#ciudad_nac option[value="'+ciudad_nac_selected+'"]').attr('selected','selected');
							};
						<?php } ?>
					} 
				});
			}
		<?php } ?>	

		@if ($preinscrito->pais_nac)

			var pais = {{ $preinscrito->pais_nac }};
			if (pais!=''){ 
				$.ajax({
					type: "POST",
					url: "{{URL::action('PreinscripcionController@postAjaxestados_nac')}}", 
					dataType: "json",
					data: {
						pais: pais,
					},
					cache: false,
					success: function(data)
					{
						var estados_select = '<option value="">Seleccionar...</option>';
						for(datos in data.estados){
							estados_select+='<option value="'+data.estados[datos].id_estados+'">'+data.estados[datos].des_estados+'</option>';
						}
						$("#estados_nac").html(estados_select);

						<?php if ($preinscrito->estado_nac) { ?>
							var estados_nac_selected= {{ $preinscrito->estado_nac }};
							if (estados_nac_selected != '') {
								$('#estados_nac option[value="'+estados_nac_selected+'"]').attr('selected','selected');
							};
						<?php } ?>
						var estado = $("#estados_nac").val();
					
					} 
				});
			};
		@endif
	
		@if ($preinscrito->estado_nac) 
		
			var estado = {{ $preinscrito->estado_nac }};
		
			if (estado!=''){		 
				$.ajax({
					type: "POST",
					url: "{{URL::action('PreinscripcionController@postAjaxciudades_nac')}}", 
					dataType: "json",
					data: {
						estado: estado,
					},
					cache: false,
					success: function(data)
					{
						var ciudad_select = '<option value="">Seleccionar...</option>';
						for(datos in data.ciudad){
							ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
						}
						$("#ciudad_nac").html(ciudad_select);

						<?php if  ($preinscrito->ciudad_nac)  { ?>
							var ciudad_nac_selected= {{ $preinscrito->ciudad_nac }};
							if (ciudad_nac_selected != '') {
								$('#ciudad_nac option[value="'+ciudad_nac_selected+'"]').attr('selected','selected');
							};
						<?php } ?>
					} 
				});
			}
		@endif

		<?php if ($preinscrito->estado_direccion_per){?>

			var estados_ubi = {{ $preinscrito->estado_direccion_per }};
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_ubi,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_ubi").html(ciudad_select);

					<?php if ($preinscrito->ciudad_direccion_per) { ?>
							var ciudad_ubi_selected = {{ $preinscrito->ciudad_direccion_per }};
							if (ciudad_ubi_selected != 0) {				
								$('#ciudad_ubi option[value="'+ciudad_ubi_selected+'"]').attr('selected','selected');
							};
					<?php } ?>
				} 
			});
		<?php } ?> 

		@if ($preinscrito->estado_bach)
			var estados_ubi = {{ $preinscrito->estado_bach }};
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_ubi,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_estu_secu").html(ciudad_select);

					<?php if ($preinscrito->ciudad_bach) { ?>
							var ciudad_bach_selected = {{ $preinscrito->ciudad_bach }};

							if (ciudad_bach_selected != 0) {
								$('#ciudad_estu_secu option[value="'+ciudad_bach_selected+'"]').attr('selected','selected');
							};
					<?php } ?>
				} 
			});
		@endif


		
			

	    $('#trabaja').change(function(){ 
	    	var selec_e = document.getElementById('trabaja').value;
	    	if (selec_e==1){
	    		document.getElementById('empresa').disabled = false;
	    		document.getElementById('direc_empresa').disabled = false;
	    		document.getElementById('telefono_empre').disabled = false;
	    		document.getElementById('depart_empresa').disabled = false;
	    		document.getElementById('cargo_empresa').disabled = false;
	    	}else{
	    		document.getElementById('empresa').disabled = true;
	    		document.getElementById('direc_empresa').disabled = true;
	    		document.getElementById('telefono_empre').disabled = true;
	    		document.getElementById('depart_empresa').disabled = true;
	    		document.getElementById('cargo_empresa').disabled = true;
	    	}

		});

		

		$('#vehiculo').change(function(){ 
	    	var selec_v = document.getElementById('vehiculo').value;
	    	if (selec_v==1){
	    		document.getElementById('placa').disabled = false;
	    		document.getElementById('marca').disabled = false;
	    		document.getElementById('modelo').disabled = false;
			}else{
				document.getElementById('placa').disabled = true;
				document.getElementById('marca').disabled = true;
				document.getElementById('modelo').disabled = true;
			}

		});

		var remember = document.getElementById('otra_disc');
		    if (remember.checked){
		        document.getElementById('ex_otra_disc').disabled = false;
		    }else{
		        document.getElementById('ex_otra_disc').disabled = true;
		    }

		$('#otra_disc').click(function(){ 
	    	var selectp = document.getElementById('otra_disc').checked;
	    	if (selectp==true){
	    		document.getElementById('ex_otra_disc').disabled = false;
			}else{
				document.getElementById('ex_otra_disc').disabled = true;
			}
		});
		


		var selectp = document.getElementById('g_especialidad').value;	    	
	    	if (selectp==3){
	    		document.getElementById('ex_especialidad').disabled = false;
			}else{
				document.getElementById('ex_especialidad').disabled = true;
			}

		$('#g_especialidad').change(function(){ 
	    	var selectp = document.getElementById('g_especialidad').value;
	    	if (selectp==3){
	    		document.getElementById('ex_especialidad').disabled = false;
			}else{
				document.getElementById('ex_especialidad').disabled = true;
			}
		});

		$('#pais_nac').change(function(){

			var pais = $("#pais_nac").val();
			 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxestados_nac')}}", 
				dataType: "json",
				data: {
					pais: pais,
				},
				cache: false,
				success: function(data)
				{
					var estados_select = '<option value="">Seleccionar...</option>';
					for(datos in data.estados){
						estados_select+='<option value="'+data.estados[datos].id_estados+'">'+data.estados[datos].des_estados+'</option>';
					}
					$("#estados_nac").html(estados_select);
				} 
			});
		});

		$('#estados_nac').change(function(){
			var estado = $("#estados_nac").val();
				 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades_nac')}}", 
				dataType: "json",
				data: {
					estado: estado,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_nac").html(ciudad_select);
				} 
			});
		});

		$('#estados').change(function(){
			var estados = $("#estados").val();
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad").html(ciudad_select);
				} 
			});
		});

		var estados_estu_secu = $("#estados_estu_secu").val();
		if (estados_estu_secu!=''){ 	 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_estu_secu,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_estu_secu").html(ciudad_select);
					<?php if ($preinscrito->ciudad_bach && $preinscrito->ciudad_bach!="") { ?>
							var ciudad_estu_secu = {{$preinscrito->ciudad_bach}};
							if (ciudad_estu_secu != '') {
								$('#ciudad_estu_secu option[value="'+ciudad_estu_secu+'"]').attr('selected','selected');
							};

					<?php } ?>
				} 
			});
		}


		$('#estados_estu_secu').change(function(){
			var estados_estu_secu = $("#estados_estu_secu").val();	 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_estu_secu,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_estu_secu").html(ciudad_select);
				} 
			});
		
		});

		var estados_ubi = $("#estados_ubi").val();
		if (estados_ubi!=''){ 	 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_ubi,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_ubi").html(ciudad_select);
					<?php if ($preinscrito->ciudad_direccion_per && $preinscrito->ciudad_direccion_per !="") { ?>
							var ciudad_ubi_selected = {{$preinscrito->ciudad_direccion_per}};

							if (ciudad_ubi_selected != '') {
								$('#ciudad_ubi option[value="'+ciudad_ubi_selected+'"]').attr('selected','selected');
							};

					<?php } ?>
				} 
			});
		}

		$('#estados_ubi').change(function(){
			var estados_ubi = $("#estados_ubi").val();	 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_ubi,
				},
				cache: false,
				success: function(data)
				{
					
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_ubi").html(ciudad_select);
				} 
			});
		
		});


		$('#estados_cs').change(function(){
			var estados_cs = $("#estados_cs").val();		 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_cs,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_cs").html(ciudad_select);
				} 
			});
		
		});


		var cargo_d_bachiderato = document.getElementById('cargo_d_bachiderato').value;
		if (cargo_d_bachiderato==1){
			document.getElementById('e_cargo_d_bachiderato').disabled = false;
		}else{
			document.getElementById('e_cargo_d_bachiderato').disabled = true;
		}

		$('#cargo_d_bachiderato').change(function(){ 
	    	var cargo_d_bachiderato = document.getElementById('cargo_d_bachiderato').value;
	    	if (cargo_d_bachiderato==1){
				document.getElementById('e_cargo_d_bachiderato').disabled = false;
			}else{
				document.getElementById('e_cargo_d_bachiderato').disabled = true;
			}
		});

		var otros_estudio = document.getElementById('otros_estudio').value;
    	if (otros_estudio==1){
			document.getElementById('otra_institucion').disabled = false;
			document.getElementById('otra_carreras').disabled = false;
			document.getElementById('se_graduo').disabled = false;
		}else{
			document.getElementById('otra_institucion').disabled = true;
			document.getElementById('otra_carreras').disabled = true;
			document.getElementById('se_graduo').disabled = true;
		}

		$('#otros_estudio').change(function(){ 
	    	var selectp = document.getElementById('otros_estudio').value;
	    	if (selectp==1){
				document.getElementById('otra_institucion').disabled = false;
				document.getElementById('otra_carreras').disabled = false;
				document.getElementById('se_graduo').disabled = false;
			}else{
				document.getElementById('otra_institucion').disabled = true;
				document.getElementById('otra_carreras').disabled = true;
				document.getElementById('se_graduo').disabled = true;
			}
		});

		var selectp = document.getElementById('vivira_est').value;
	    	if (selectp==5){
				document.getElementById('otra_vivienda').disabled = false;
			}else{
				document.getElementById('otra_vivienda').disabled = true;
			}

		$('#vivira_est').change(function(){ 
	    	var selectp = document.getElementById('vivira_est').value;
	    	if (selectp==5){
				document.getElementById('otra_vivienda').disabled = false;
			}else{
				document.getElementById('otra_vivienda').disabled = true;
			}	
		});

		$('#vivira_est').change(function(){ 
	    	var selectp = document.getElementById('vivira_est').value;	
		});

		var selectp = document.getElementById('costea_gast').value;
	    	if (selectp==5){
				document.getElementById('nombre_cost_gast').disabled = false;
			}else{
				document.getElementById('nombre_cost_gast').disabled = true;
			}

		$('#costea_gast').change(function(){ 
	    	var selectp = document.getElementById('costea_gast').value;
	    	if (selectp==5){
				document.getElementById('nombre_cost_gast').disabled = false;
			}else{
				document.getElementById('nombre_cost_gast').disabled = true;
			}	
		});

		var selectp = document.getElementById('disfruta_d').value;
	    	if (selectp==5){
				document.getElementById('nombre_inst').disabled = false;
			}else{
				document.getElementById('nombre_inst').disabled = true;
			}

		$('#disfruta_d').change(function(){ 
	    	var selectp = document.getElementById('disfruta_d').value;
	    	if (selectp==5){
				document.getElementById('nombre_inst').disabled = false;
			}else{
				document.getElementById('nombre_inst').disabled = true;
			}	
		});

		

		var selectp = document.getElementById('vive_con').value;
	    	if (selectp==7){
				document.getElementById('vive_con_ex').disabled = false;
			}else{
				document.getElementById('vive_con_ex').disabled = true;
			}

		$('#vive_con').change(function(){ 
	    	var selectp = document.getElementById('vive_con').value;
	    	if (selectp==7){
				document.getElementById('vive_con_ex').disabled = false;
			}else{
				document.getElementById('vive_con_ex').disabled = true;
			}	
		});

		

		$('#sig_1').click(function(){	 
			var sig_a = document.getElementById("personales1");
			var sig_b = document.getElementById("personales2");
			$('#periodo1').removeClass('active');
			$('#periodo2').removeClass('active');
			$('#personales1').addClass('active');
			$('#personales2').addClass('active');
		});

		$('#ant_1').click(function(){
			var sig_a = document.getElementById("periodo1");
			var sig_b = document.getElementById("periodo2");
			$('#personales1').removeClass('active');
			$('#personales2').removeClass('active');
			$('#periodo1').addClass('active');
			$('#periodo2').addClass('active');
		});

		$('#sig_2').click(function(){
			var sig_a = document.getElementById("datossecundaria1");
			var sig_b = document.getElementById("datossecundaria2");
			$('#personales1').removeClass('active');
			$('#personales2').removeClass('active');
			$('#datossecundaria1').addClass('active');
			$('#datossecundaria2').addClass('active');
		});

		$('#ant_2').click(function(){
			var sig_a = document.getElementById("personales1");
			var sig_b = document.getElementById("personales2");
			$('#datossecundaria1').removeClass('active');
			$('#datossecundaria2').removeClass('active');
			$('#personales1').addClass('active');
			$('#personales2').addClass('active');
		});

		$('#sig_3').click(function(){
			var sig_a = document.getElementById("datossocioeconomicos1");
			var sig_b = document.getElementById("datossocioeconomicos2");
			$('#datossecundaria1').removeClass('active');
			$('#datossecundaria2').removeClass('active');
			$('#datossocioeconomicos1').addClass('active');
			$('#datossocioeconomicos2').addClass('active');
		});

		$('#ant_3').click(function(){
			var sig_a = document.getElementById("datossecundaria1");
			var sig_b = document.getElementById("datossecundaria2");
			$('#datossocioeconomicos1').removeClass('active');
			$('#datossocioeconomicos2').removeClass('active');
			$('#datossecundaria1').addClass('active');
			$('#datossecundaria2').addClass('active');
		});

		$('#fecha').change(function(){
			var fecha_nac = $("#fecha").val();
			var div = fecha_nac.split("-")
			var fecha_nacimiento = div[2]+'/'+div[1]+'/'+div[0];
			var dia = div[0];
			var mes = div[1];
			var ano = div[2];
			fecha_hoy = new Date();
			ahora_ano = fecha_hoy.getYear();
			ahora_mes = fecha_hoy.getMonth();
			ahora_dia = fecha_hoy.getDate();
			edad = (ahora_ano + 1900) - ano;			    
		    if ( ahora_mes < (mes - 1)){ 
		      edad--;
		    }
		    if (((mes - 1) == ahora_mes) && (ahora_dia < dia)){ 
		      edad--;
		    }
		    if (edad > 1900){
		        edad -= 1900;
		    }
			$("#edad").val(edad);
			if (edad>17){				
				document.getElementById('cedula_rep').disabled = true;
				document.getElementById('nombres_rep').disabled = true;
				document.getElementById('apellidos_rep').disabled = true;
				document.getElementById('edad_rep').disabled = true;
				document.getElementById('direccion_rep').disabled = true;
				document.getElementById('telefono_rep').disabled = true;
				document.getElementById('parentesco').disabled = true;								
			}else{
				document.getElementById('cedula_rep').disabled = false;
				document.getElementById('nombres_rep').disabled = false;
				document.getElementById('apellidos_rep').disabled = false;
				document.getElementById('edad_rep').disabled = false;
				document.getElementById('direccion_rep').disabled = false;
				document.getElementById('telefono_rep').disabled = false;
				document.getElementById('parentesco').disabled = false;
			}
		});
		$('#posee_poliza').change(function(){
			var opcion = $('#posee_poliza').val();
			if (opcion == 1) {
				document.getElementById('entidad_poliza').readOnly = false;
				document.getElementById('numero_poliza').readOnly = false;
				document.getElementById('beneficiario').readOnly = true;
				$('#entidad_poliza').val('');
				$('#numero_poliza').val('');
				$('#entidad_aseguradora').val('');
				$('#beneficiario').val('');
			}else if (opcion == 0){
				document.getElementById('entidad_poliza').readOnly = true;
				document.getElementById('numero_poliza').readOnly = true;
				document.getElementById('beneficiario').readOnly = false;
				$('#entidad_poliza').val('');
				$('#numero_poliza').val('');
				$('#entidad_aseguradora').val('C.A DE SEGUROS LA OCCIDENTAL');
				$('#beneficiario').val('');
			} else {
				document.getElementById('entidad_poliza').readOnly = true;
				document.getElementById('numero_poliza').readOnly = true;
				document.getElementById('beneficiario').readOnly = true;
				$('#entidad_poliza').val('');
				$('#numero_poliza').val('');
				$('#entidad_aseguradora').val('');
				$('#beneficiario').val('');
			}
		});
		$('#curso_uba').change(function(){
			var opcion = $('#curso_uba').val();
			if (opcion == 1) {
				document.getElementById('graduo_uba').disabled= false;
				$('#graduo_uba').val('');
			}else if (opcion == 0){
				document.getElementById('graduo_uba').disabled= true;
				$('#graduo_uba').val('');
			} else {
				document.getElementById('graduo_uba').disabled= true;
				$('#graduo_uba').val('');
			}
		});

		var nivel_estudio = $('#nivel_estudio').val();

		if (nivel_estudio != '') {
			document.getElementById('nivel_estudio').disabled= false;
		}
		
		$('#se_graduo').change(function(){
			var opcion = $('#se_graduo').val();
			if (opcion == 1) {
				document.getElementById('nivel_estudio').disabled= false;
				$('#nivel_estudio').val('');
			}else if (opcion == 0){
				document.getElementById('nivel_estudio').disabled= true;
				$('#nivel_estudio').val('');
			} else {
				document.getElementById('nivel_estudio').disabled= true;
				$('#nivel_estudio').val('');
			}
		});
    });
 </script>
{{ HTML::script('js/jquery_ui.js') }}
{{ HTML::script('js/mask-plugins/src/jquery.mask.js') }}
{{ HTML::script('js/sololetras.js') }}
{{ HTML::script('js/solonumeros.js') }}
@stop