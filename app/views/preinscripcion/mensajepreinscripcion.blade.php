@extends('layouts.masterpreinscripcion')
@section('css')
{{ HTML::style('css/jquery_ui.css')}}
@stop
@section('content')
<div class="container container-margin-top">
	<div>
		<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
			<div class="alert alert-danger error-msg centrado"><strong>{{$title}}</strong></div>
		</div>
	</div>

</div>
@stop
@section('postscript')
<script type="text/javascript" language="javascript">    
    $(document).ready(function() {

    });
 </script>
{{ HTML::script('js/jquery_ui.js') }}
{{ HTML::script('js/mask-plugins/src/jquery.mask.js') }}
{{ HTML::script('js/sololetras.js') }}
{{ HTML::script('js/solonumeros.js') }}
@stop