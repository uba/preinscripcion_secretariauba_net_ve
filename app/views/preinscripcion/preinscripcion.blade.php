@extends('layouts.masterpreinscripcion')

@section('css')
{{ HTML::style('css/jquery_ui.css')}}
@stop
<A id="subir"></A>
@section('content')
<div class="container container-margin-top">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs nav-justified" role="tablist">
	  <li id="periodo1" class="negrita @if($tab=='periodo'){{'active'}}@endif"><a href="#periodo2" role="tab" data-toggle="tab">Per&iacute;odo Acad&eacute;mico</a></li>
	  <li id="personales1" class="negrita @if($tab=='personales'){{'active'}}@endif"><a href="#personales2" role="tab" data-toggle="tab">Datos Personales</a></li>
	  <li id="datossecundaria1" class="negrita @if($tab=='datossecundaria'){{'active'}}@endif"><a href="#datossecundaria2" role="tab" data-toggle="tab">Datos de Educaci&oacute;n</a></li>
      <li id="datossocioeconomicos1" class="negrita @if($tab=='datossocioeconomicos'){{'active'}}@endif"><a href="#datossocioeconomicos2" role="tab" data-toggle="tab">Datos Socioec&oacute;nomicos</a></li>
    </ul>
	<form name="parametros" role="form" method="POST" action="{{ URL::to('preinscripcion/procesarpreinsc') }}">
		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane @if($tab=='periodo'){{'active'}}@endif" id="periodo2"><br>
				<div class="panel panel-primary" >
					<div class="panel-heading"><h3 class="panel-title">{{ $mensaje }}</h3></div>
					<div class="panel-body">
						@if(Session::has('message_error_periodo'))
							<div>
								<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
									<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_periodo') }}</strong></div>
								</div>
							</div>
						@endif
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_periodo') && Session::get('message_periodo')->has('tipo de solicitud')){{'has-error'}} @endif">
							<label for="nom_ape">Tipo de Solicitud <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::select('tsolicitud', $tsolicitud, NULL , $attributes = array('class' => 'form-control', 'id'=>'tsolicitud')) }}							  
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_periodo') && Session::get('message_periodo')->has('tipo de solicitud'))	{{ Session::get('message_periodo')->first('tipo de solicitud') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_periodo') && Session::get('message_periodo')->has('fecha de nacimiento')){{'has-error'}} @endif">
							<label for="nom_ape">Fecha de Nacimiento <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							<div class="input-group date" id="fecha_n">
				             	{{ Form::text('fecha', NULL, $attributes = array('class' => 'form-control', 'id'=>'fecha', 'placeholder'=>'Seleccione fecha de nacimiento', 'readonly'=>'readonly', 'required'=>'required')) }}<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				            	{{ Form::hidden('edad',  NULL , $attributes = array('class' => 'form-control', 'id'=>'edad')) }}
				            </div>
				            <div class="text-danger" id="text_uc">
				            	@if(Session::has('message_periodo') && Session::get('message_periodo')->has('fecha de nacimiento'))	{{ Session::get('message_periodo')->first('fecha de nacimiento') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_periodo') && Session::get('message_periodo')->has('nacionalidad')){{'has-error'}} @endif">
							<label for="nom_ape">Nacionalidad <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::select('tcedula', $tcedula, NULL , $attributes = array('class' => 'form-control', 'id'=>'tcedula')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_periodo') && Session::get('message_periodo')->has('nacionalidad'))	{{ Session::get('message_periodo')->first('nacionalidad') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_periodo') && Session::get('message_periodo')->has('c&eacute;dula')){{'has-error'}} @endif">
							<label for="cedula">C&eacute;dula <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::text('cedula',  NULL , $attributes = array('class' => 'form-control', 'id'=>'cedula', 'onkeypress'=>'return soloNumeros(event)','maxlength'=>'11')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_periodo') && Session::get('message_periodo')->has('c&eacute;dula'))	{{ Session::get('message_periodo')->first('c&eacute;dula') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 derecha">
							<a href="#subir" id="sig_1" class="btn btn-primary">Siguiente <span class="icon-arrow-right-2" style="font-size:9px;"></span></a>
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12  @if($errors->has('documento')){{'has-error'}} @endif">
							<p><strong>Notas importantes:</strong></p>
							<ul>
			                    <li>La Universidad Bicentenaria de Aragua (UBA), en su deber de mejorar los servicios a la comunidad estudiantil, ha suscrito un convenio con "Seguros La Occidental C.A" de una p&oacute;liza personal de accidentes para los estudiantes con una cobertura 7/24, durante un a&ntilde;o de aplicaci&oacute;n y uso inmediato a ser firmada. La inversi&oacute;n es muy econ&oacute;mica en comparaci&oacute;n con cualquier p&oacute;liza individual que se asuma para cubrir iguales riesgos, para lo cual ha sido estimada la prima en Bs. 1000 anual.</li>
			                    <li>Los pasos para afiliarse a este innovador servicio y asumir los beneficios de cobertura e indemnizaci&oacute;n de manera inmediata, son:</li>
			                    <li>Consignar Copia de la c&eacute;dula de identidad del solicitante.</li>
			                    <li>Consignar planilla del pago de Bs. 1.500,00 de la prima a nombre de C.A. de Seguros La Occidental C.A (RIF: J-070011300), en el banco:</li>
								<li>Banco Occidental del Descuento (BOD), cuenta: 0116-0101-43-2101005332.</li>
								<li>Descarga aqu&iacute; informaci&oacute;n del Seguro&nbsp;<a href="{{ URL::to('http://secretariauba.net.ve/descargas/la_occidental_seguros.pdf') }}"  target="_blank"><strong>Aqui</strong></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane @if($tab=='personales'){{'active'}}@endif" id="personales2"><br>
				<div class="panel panel-primary">	
					<div class="panel-heading"><h3 class="panel-title">Solicitud de Inscripci&oacute;n Nuevo ingreso (Regulares o Equivalencia)</h3></div>
					<div class="panel-body">
						@if(Session::has('message_error_personales'))
							<div>
								<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
									<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_personales') }}</strong></div>
								</div>
							</div>
						@endif
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
							{{-- <p><strong>¿En qu&eacute; sede o n&uacute;cleo desea cursar estudios?</strong></p> --}}
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{'has-error'}} @endif">
							<label for="nom_ape">Sede y N&uacute;cleos <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::select('nucleo', $nucleo ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'nucleo', 'required'=>'required')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{ Session::get('message_personales')->first('nucleos') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{'has-error'}} @endif">
							<label for="nom_ape">¿Ingresa mediante un Convenio o Coach?<span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::select('convenio', $convenio, NULL , $attributes = array('class' => 'form-control', 'id'=>'convenio')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{ Session::get('message_personales')->first('nucleos') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{'has-error'}} @endif">
							<label for="nom_ape">Convenios / Coach<span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::select('convenios', $convenios, NULL , $attributes = array('class' => 'form-control', 'id'=>'convenios')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('nucleos')){{ Session::get('message_personales')->first('nucleos') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo nombre')){{'has-error'}} @endif">
							<label for="nom_ape">Primer y segundo nombre <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::text('nombres',  NULL , $attributes = array('class' => 'form-control', 'id'=>'nombres')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo nombre'))	{{ Session::get('message_personales')->first('primer y segundo nombre') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo apellido')){{'has-error'}} @endif">
							<label for="nom_ape">Primer y segundo apellido <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::text('apellidos',  NULL , $attributes = array('class' => 'form-control', 'id'=>'apellidos')) }}
							<div class="text-danger" id="text_uc">					
								@if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo apellido'))	{{ Session::get('message_personales')->first('primer y segundo apellido') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('sexo')){{'has-error'}} @endif">
							<label for="nom_ape">Sexo <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::select('tsexo', $tsexo, NULL , $attributes = array('class' => 'form-control', 'id'=>'tsexo')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('sexo'))	{{ Session::get('message_personales')->first('sexo') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('estado civil')){{'has-error'}} @endif">
							<label for="nom_ape">Estado civil <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::select('ecivil', $ecivil, NULL , $attributes = array('class' => 'form-control', 'id'=>'ecivil')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('estado civil'))	{{ Session::get('message_personales')->first('estado civil') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono')){{'has-error'}} @endif">
							<label for="nom_ape">Tel&eacute;fono <span class="glyphicon glyphicon-asterisk text-danger"></span></label> 
								{{ Form::text('telefono', NULL, $attributes = array('class' => 'form-control', 'id'=>'telefono','placeholder'=>'(____)________', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono'))	{{ Session::get('message_personales')->first('tel&eacute;fono') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('correo electr&oacute;nico')){{'has-error'}} @endif">
							<label for="">Correo electr&oacute;nico <span class="glyphicon glyphicon-asterisk text-danger"></span></label> 
							{{ Form::email('email', NULL, $attributes = array('class' => 'form-control', 'id'=>'email','placeholder'=>'uba@example.com')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('correo electr&oacute;nico'))	{{ Session::get('message_personales')->first('correo electr&oacute;nico') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('twitter')){{'has-error'}} @endif">
							<label for="">Twitter </label> 
							<div class="input-group date" >
								<span class="input-group-addon">@</span>{{ Form::text('twitter', NULL, $attributes = array('class' => 'form-control', 'id'=>'twitter','placeholder'=>'EjemploUba')) }}
							</div>
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('twitter'))	{{ Session::get('message_personales')->first('twitter') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('facebook')){{'has-error'}} @endif">
							<label for="">Facebook </label>
							<div class="input-group date" >
							<span class="input-group-addon">www.facebook.com/</span>{{ Form::text('facebook', NULL, $attributes = array('class' => 'form-control', 'id'=>'facebook','placeholder'=>'ejemplo.uba30')) }}
							</div>
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('facebook'))	{{ Session::get('message_personales')->first('facebook') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Datos del Trabajo</strong></legend>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_personales') && Session::get('message_personales')->has('trabaja')){{'has-error'}} @endif">
							<label for="nom_ape">¿Trabaja? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('trabaja', $trabaja, NULL , $attributes = array('class' => 'form-control', 'id'=>'trabaja')) }} 
							<div class="text-danger" id="text_uc" >
								@if(Session::has('message_personales') && Session::get('message_personales')->has('trabaja'))	{{ Session::get('message_personales')->first('trabaja') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>	
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('empresa')){{'has-error'}} @endif">
							<label for="nom_ape" id="Empresal">Empresa <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::text('empresa', NULL, $attributes = array('class' => 'form-control', 'id'=>'empresa')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('empresa'))	{{ Session::get('message_personales')->first('empresa') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n de la empresa')){{'has-error'}} @endif">
							<label for="nom_ape" id="direc_empresal">Direcci&oacute;n de la empresa <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::text('direc_empresa', NULL, $attributes = array('class' => 'form-control', 'id'=>'direc_empresa')) }}
							<div class="text-danger" id="text_uc">
								@if($errors->has('direcci&oacute;n de la empresa'))	{{ $errors->first('direcci&oacute;n de la empresa') }}	@else {{ "&nbsp;" }} @endif
								@if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n de la empresa'))	{{ Session::get('message_personales')->first('direcci&oacute;n de la empresa') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono de empresa')){{'has-error'}} @endif">
							<label for="nom_ape" id="telefono_emprel">Tel&eacute;fono de Empresa <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('telefono_empre', NULL, $attributes = array('class' => 'form-control', 'id'=>'telefono_empre','placeholder'=>'(____)________', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono de empresa'))	{{ Session::get('message_personales')->first('tel&eacute;fono de empresa') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('departamento en que labora')){{'has-error'}} @endif">
							<label for="nom_ape" id="depart_empresal">Departamento en que labora <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::text('depart_empresa', NULL, $attributes = array('class' => 'form-control', 'id'=>'depart_empresa')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('departamento en que labora'))	{{ Session::get('message_personales')->first('departamento en que labora') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('cargo en la empresa')){{'has-error'}} @endif">
							<label for="nom_ape" id="cargo_empresal">Cargo en la empresa <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::text('cargo_empresa', NULL, $attributes = array('class' => 'form-control', 'id'=>'cargo_empresa')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('cargo en la empresa'))	{{ Session::get('message_personales')->first('cargo en la empresa') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Datos del Vehiculo</strong></legend>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('posee veh&iacute;culo')){{'has-error'}} @endif">
							<label for="nom_ape">¿Posee veh&iacute;culo? <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::select('vehiculo', $vehiculo, NULL , $attributes = array('class' => 'form-control', 'id'=>'vehiculo')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('posee veh&iacute;culo'))	{{ Session::get('message_personales')->first('posee veh&iacute;culo') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('placa')){{'has-error'}} @endif"  id="placad">
							<label for="placa">Placa <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::text('placa', NULL, $attributes = array('class' => 'form-control', 'id'=>'placa')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('placa'))	{{ Session::get('message_personales')->first('placa') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('marca')){{'has-error'}} @endif" id="marcad">
							<label for="nom_ape">Marca <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('marca', NULL, $attributes = array('class' => 'form-control', 'id'=>'marca')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('marca'))	{{ Session::get('message_personales')->first('marca') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('modelo')){{'has-error'}} @endif" id="modelod">
							<label for="nom_ape">Modelo <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::text('modelo', NULL, $attributes = array('class' => 'form-control', 'id'=>'modelo')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('modelo'))	{{ Session::get('message_personales')->first('modelo') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>		
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Datos de Pais de Nacimiento</strong></legend>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if(Session::has('message_personales') && Session::get('message_personales')->has('pais de nacimiento')){{'has-error'}} @endif">
							<label for="nom_ape">Pais de Nacimiento <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::select('pais_nac', $pais_nac, NULL , $attributes = array('class' => 'form-control', 'id'=>'pais_nac')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('pais de nacimiento'))	{{ Session::get('message_personales')->first('pais de nacimiento') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('estados de nacimiento')){{'has-error'}} @endif">
							<label for="estados">Estado de Nacimiento <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::select('estados_nac', $estados_nac, NULL , $attributes = array('class' => 'form-control', 'id'=>'estados_nac')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('estados de nacimiento'))	{{ Session::get('message_personales')->first('estados de nacimiento') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_personales') && Session::get('message_personales')->has('ciudad de nacimiento')){{'has-error'}} @endif">
							<label for="ciudad">Ciudad de Nacimiento </label>
							{{ Form::select('ciudad_nac', $ciudad_nac ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'ciudad_nac')) }}
							<div class="text-danger">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('ciudad de nacimiento'))	{{ Session::get('message_personales')->first('ciudad de nacimiento') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n de residencia')){{'has-error'}} @endif" >
							<label for="nom_ape" id="">Direcci&oacute;n de residencia mientras estudie <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('direcc_r', NULL, $attributes = array('class' => 'form-control', 'id'=>'direcc_r')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n de residencia'))	{{ Session::get('message_personales')->first('direcci&oacute;n de residencia') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono de residencia')){{'has-error'}} @endif">
							<label for="nom_ape">Tel&eacute;fono de Residencia <span class="glyphicon glyphicon-asterisk text-danger"></span></label> 
								{{ Form::text('telefono_r', NULL, $attributes = array('class' => 'form-control', 'id'=>'telefono_r','placeholder'=>'(____)________', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono de residencia'))	{{ Session::get('message_personales')->first('tel&eacute;fono de residencia') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n permanente')){{'has-error'}} @endif">
							<label for="nom_ape" id="">Direcci&oacute;n permanente (trabajo o habitaci&oacute;n) <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('direcc_perm', NULL, $attributes = array('class' => 'form-control', 'id'=>'direcc_t_h')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n permanente'))	{{ Session::get('message_personales')->first('direcci&oacute;n permanente') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('estados de ubicaci&oacute;n')){{'has-error'}} @endif">
							<label for="estados">Estados de Ubicaci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::select('estados_ubi', $estados, NULL , $attributes = array('class' => 'form-control', 'id'=>'estados_ubi')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('estados de ubicaci&oacute;n'))	{{ Session::get('message_personales')->first('estados de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_personales') && Session::get('message_personales')->has('ciudad de ubicaci&oacute;n')){{'has-error'}} @endif">
							<label for="ciudad">Ciudad de Ubicaci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('ciudad_ubi', $ciudad ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'ciudad_ubi')) }}
							<div class="text-danger">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('ciudad de ubicaci&oacute;n'))	{{ Session::get('message_personales')->first('ciudad de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Datos de su Representante Legal</strong></legend>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('c&eacute;dula representante')){{'has-error'}} @endif">
							<label for="nom_ape">C&eacute;dula de su Representante <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::text('cedula_rep',  NULL , $attributes = array('class' => 'form-control', 'id'=>'cedula_rep', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('c&eacute;dula representante'))	{{ Session::get('message_personales')->first('c&eacute;dula representante') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('nombre representante')){{'has-error'}} @endif">
							<label for="nom_ape">Nombre de su Representante <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::text('nombres_rep',  NULL , $attributes = array('class' => 'form-control', 'id'=>'nombres_rep')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('nombre representante'))	{{ Session::get('message_personales')->first('nombre representante') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('apellido representante')){{'has-error'}} @endif">
							<label for="nom_ape">Apellido  de su Representante <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::text('apellidos_rep',  NULL , $attributes = array('class' => 'form-control', 'id'=>'apellidos_rep')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('apellido representante'))	{{ Session::get('message_personales')->first('apellido representante') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('edad representante')){{'has-error'}} @endif">
							<label for="nom_ape">Edad de su Representante <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::text('edad_rep',  NULL , $attributes = array('class' => 'form-control', 'id'=>'edad_rep', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('edad representante'))	{{ Session::get('message_personales')->first('edad representante') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('dir representante')){{'has-error'}} @endif">
							<label for="nom_ape">Direcci&oacute;n de su Representante <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::text('direccion_rep',  NULL , $attributes = array('class' => 'form-control', 'id'=>'direccion_rep')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('dir representante'))	{{ Session::get('message_personales')->first('dir representante') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('tel representante')){{'has-error'}} @endif" id="telefono_empred">
							<label for="nom_ape" id="telefono_emprel">Tel&eacute;fono de su Representante <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('telefono_rep', NULL, $attributes = array('class' => 'form-control', 'id'=>'telefono_rep','placeholder'=>'(____)________', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('tel representante'))	{{ Session::get('message_personales')->first('tel representante') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('parentesco')){{'has-error'}} @endif">
							<label for="estados">Parentesco <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::select('parentesco', $parentesco, NULL , $attributes = array('class' => 'form-control', 'id'=>'parentesco')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('parentesco'))	{{ Session::get('message_personales')->first('parentesco') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 izquierda">
								<a href="#subir" id="ant_1" class="btn btn-primary"><span class="icon-arrow-left-2" style="font-size:9px;"></span> Anterior</a>
							</div>
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 derecha">
								<a href="#subir" id="sig_2" class="btn btn-primary">Siguiente <span class="icon-arrow-right-2" style="font-size:9px;"></span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane @if($tab=='datossecundaria'){{'active'}}@endif" id="datossecundaria2"><br>
				<div class="panel panel-primary">
					<div class="panel-heading"><h3 class="panel-title">Datos de Educaci&oacute;n Secundaria</h3></div>
					<div class="panel-body">
						@if(Session::has('message_error_datossecundaria'))
							<div>
								<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
									<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_datossecundaria') }}</strong></div>
								</div>
							</div>
						@endif
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('tipo de instituci&oacute;n')){{'has-error'}} @endif">
							<label for="ciudad">Tipo de Instituci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('tipo_institucion', $t_institucion ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'t_institucion')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('tipo de instituci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('tipo de instituci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('nombre de la instituci&oacute;n')){{'has-error'}} @endif">
							<label for="nom_ape" id="">Nombre de la Instituci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('nom_institucion', NULL, $attributes = array('class' => 'form-control', 'id'=>'n_institucion')) }}
							<div class="text-danger" id="">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('nombre de la instituci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('nombre de la instituci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('turno que estudi&oacute;')){{'has-error'}} @endif">
							<label for="ciudad">Turno que estudi&oacute; <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('turno_institucion', $turno_institucion ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'t_institucion')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('turno que estudi&oacute;'))	{{ Session::get('message_datossecundaria')->first('turno que estudi&oacute;') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('especialidad')){{'has-error'}} @endif">
							<label for="ciudad">Especialidad <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('g_especialidad', $especialidad ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'g_especialidad')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('especialidad'))	{{ Session::get('message_datossecundaria')->first('especialidad') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('exp especialidad')){{'has-error'}} @endif">
							<label for="nom_ape" id="">Explique <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('ex_especialidad', NULL, $attributes = array('class' => 'form-control', 'id'=>'ex_especialidad')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('exp especialidad'))	{{ Session::get('message_datossecundaria')->first('exp especialidad') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('promedio de bachillerato')){{'has-error'}} @endif">
							<label for="nom_ape" >Promedio de bachillerato <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('promedio_b', NULL, $attributes = array('class' => 'form-control', 'id'=>'promedio_b','placeholder'=>'__.__', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('promedio de bachillerato'))	{{ Session::get('message_datossecundaria')->first('promedio de bachillerato') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('fecha de graduaci&oacute;n')){{'has-error'}} @endif">
							<label for="nom_ape">Fecha de graduaci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							<div class="input-group date" id="notiene">
							   {{ Form::text('fecha_g', NULL, $attributes = array('class' => 'form-control', 'id'=>'fecha_g', 'placeholder'=>'Seleccione fecha de graduaci&oacute;n', 'readonly'=>'readonly', 'required'=>'required')) }}<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>					           
				            </div>
				            <div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('fecha de graduaci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('fecha de graduaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>¿D&oacute;nde curs&oacute; el &uacute;ltimo a&ntilde;o de Educaci&oacute;n Secundaria?</strong></legend>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('estados de ubicaci&oacute;n')){{'has-error'}} @endif">
							<label for="estados">Estados de Ubicaci&oacute;n Educaci&oacute;n Secundaria <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::select('estados_estu_secu', $estados, NULL , $attributes = array('class' => 'form-control', 'id'=>'estados_estu_secu')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('estados de ubicaci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('estados de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('ciudad de ubicaci&oacute;n')){{'has-error'}} @endif">
							<label for="ciudad">Ciudad de Ubicaci&oacute;n Educaci&oacute;n Secundaria <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('ciudad_estu_secu', $ciudad_cs ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'ciudad_estu_secu')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('ciudad de ubicaci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('ciudad de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('n rusnies')){{'has-error'}} @endif">
							<label for="nom_ape" >N. de R.U.S.N.I.E.S <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('rusnies', NULL, $attributes = array('class' => 'form-control', 'id'=>'rusnies')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('n rusnies'))	{{ Session::get('message_datossecundaria')->first('n rusnies') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Desde que se gradu&oacute; en a&ntilde;os anteriores por favor indique que actividades ha realizado desde entonces .  (Al menos una actividad)</strong></legend>
						</div>	 	
					 	<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 1')){{'has-error'}} @endif">
							<label for="nom_ape" >Actividad 1: <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('actividad_1', NULL, $attributes = array('class' => 'form-control', 'id'=>'actividad_1')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 1'))	{{ Session::get('message_datossecundaria')->first('actividad 1') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 2')){{'has-error'}} @endif">
							<label for="nom_ape" >Actividad 2:</span></label> 
								{{ Form::text('actividad_2', NULL, $attributes = array('class' => 'form-control', 'id'=>'actividad_2')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 2'))	{{ Session::get('message_datossecundaria')->first('actividad 2') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 3')){{'has-error'}} @endif">
							<label for="nom_ape" >Actividad 3: </span></label> 
								{{ Form::text('actividad_3', NULL, $attributes = array('class' => 'form-control', 'id'=>'actividad_3')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 3'))	{{ Session::get('message_datossecundaria')->first('actividad 3') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 4')){{'has-error'}} @endif">
							<label for="nom_ape" >Actividad 4: </span></label> 
								{{ Form::text('actividad_4', NULL, $attributes = array('class' => 'form-control', 'id'=>'actividad_4')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('actividad 4'))	{{ Session::get('message_datossecundaria')->first('actividad 4') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Datos de la Carrera a Estudiar</strong></legend>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('carrera que desea estudiar')){{'has-error'}} @endif">
							<label for="ciudad">Carrera que desea estudiar <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('carrera_uba', $carreras_uba ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'carrera_uba')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('carrera que desea estudiar'))	{{ Session::get('message_datossecundaria')->first('carrera que desea estudiar') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('turno carrera')){{'has-error'}} @endif">
							<label for="ciudad">Turno <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('turno_uba', $turno ,'NULL', $turno =array('class' => 'form-control', 'id'=>'turno_uba')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('turno carrera'))	{{ Session::get('message_datossecundaria')->first('turno carrera') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>

						<div class="form-group col-lg-3 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('modalidad carrera')){{'has-error'}} @endif">
							<label for="ciudad">Modalidad de Estudios <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('modalidad_uba', $modalidad, 'NULL', $modalidad =array('class' => 'form-control', 'id' => 'modalidad_uba')) }}
							<div class="text-danger">
								@if(Session::has('message_datossencundaria') && Session::get('message_datossecundaria')->has('modalidad carrera')) {{
								Session::get('message_datossecundaria')->first('modalidad carrera') }} @else {{ "&nbsp;" }} @endif							
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-3 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('recibi&oacute; orientaci&oacute;n vocacional')){{'has-error'}} @endif">
							<label for="ciudad">¿Recibi&oacute; orientaci&oacute;n de su carrera? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('orientacion', $datos_carrera ,'NULL', $turno =array('class' => 'form-control', 'id'=>'orientacion')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('recibi&oacute; orientaci&oacute;n vocacional'))	{{ Session::get('message_datossecundaria')->first('recibi&oacute; orientaci&oacute;n vocacional') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('decidido escogencia carrera')){{'has-error'}} @endif">
							<label for="ciudad">¿Esta decidido con su carrera? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('decision_carrera', $datos_carrera ,'NULL', $turno =array('class' => 'form-control', 'id'=>'decision_carrera')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('decidido escogencia carrera'))	{{ Session::get('message_datossecundaria')->first('decidido escogencia carrera') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						
						<div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('informacion de carrera')){{'has-error'}} @endif">
							<label>¿A través de que medio obtuvo información de nuestras carreras?<span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('infor_uba', $informacion, 'NULL', $attributes =array('class' => 'form-control', 'id' => 'infor_uba')) }}
						</div>
						
						<div class="form-group col-lg-8 col-md-8 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('curso uba')){{'has-error'}} @endif">
							<label for="ciudad">¿Usted curs&oacute; estudios anteriormente en la Universidad Bicentenaria de Aragua? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('curso_uba', $poliza ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'curso_uba')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('curso uba'))	{{ Session::get('message_datossecundaria')->first('curso uba') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('graduo uba')){{'has-error'}} @endif">
							<label for="ciudad">¿Se gradu&oacute;? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('graduo_uba', $poliza ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'graduo_uba', 'disabled')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('graduo uba'))	{{ Session::get('message_datossecundaria')->first('graduo uba') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Cargos durante el bachillerato con representaci&oacute;n estudiantil, deportes</strong></legend>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('representaci&oacute;n estudiantil')){{'has-error'}} @endif">
							<label for="ciudad">¿Ha cursado ud. cargos? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('cargo_d_bachiderato', $datos_carrera ,'NULL', $turno =array('class' => 'form-control', 'id'=>'cargo_d_bachiderato')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('representaci&oacute;n estudiantil'))	{{ Session::get('message_datossecundaria')->first('representaci&oacute;n estudiantil') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('expl representaci&oacute;n estudiantil')){{'has-error'}} @endif">
							<label for="nom_ape" >Explique </span></label> 
								{{ Form::text('e_cargo_d_bachiderato', NULL, $attributes = array('class' => 'form-control', 'id'=>'e_cargo_d_bachiderato')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('expl representaci&oacute;n estudiantil'))	{{ Session::get('message_datossecundaria')->first('expl representaci&oacute;n estudiantil') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Datos de otra Instituci&oacute;n</strong></legend>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('cursa otra instituci&oacute;n')){{'has-error'}} @endif">
							<label for="ciudad">¿Cursa en otra Instituci&oacute;n de Educ Superior? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('otros_estudio', $datos_carrera ,'NULL', $turno =array('class' => 'form-control', 'id'=>'otros_estudio')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('cursa otra instituci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('cursa otra instituci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('otra institucio')){{'has-error'}} @endif">
							<label for="ciudad">Instituci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('otra_institucion', $institucion ,'NULL', $turno =array('class' => 'form-control', 'id'=>'otra_institucion')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('otra institucio'))	{{ Session::get('message_datossecundaria')->first('otra institucio') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>	
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('otra carreras')){{'has-error'}} @endif">
							<label for="ciudad">Carreras <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('otra_carreras', $carreras ,'NULL', $turno =array('class' => 'form-control', 'id'=>'otra_carreras')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('otra carreras'))	{{ Session::get('message_datossecundaria')->first('otra carreras') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('se gradu&oacute;')){{'has-error'}} @endif">
							<label for="ciudad">¿Se gradu&oacute;? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('se_graduo', $datos_carrera ,'NULL', $turno =array('class' => 'form-control', 'id'=>'se_graduo')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('se gradu&oacute;'))	{{ Session::get('message_datossecundaria')->first('se gradu&oacute;') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('nivel de estudio')){{'has-error'}} @endif">
							<label for="ciudad">Nivel de Estudio <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('nivel_estudio', $nivel_estudio ,'NULL', $turno =array('class' => 'form-control', 'id'=>'nivel_estudio', 'disabled')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('nivel de estudio'))	{{ Session::get('message_datossecundaria')->first('nivel de estudio') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Actividades Deportivas</strong></legend>
						</div>
						<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('beisbol', 1, false,array('class' => 'form-group', 'id'=>'beisbol')); }}
							<label for="ciudad">Beisbol </label>
						</div>
						<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('voleibol', 1, false,array('class' => 'form-group', 'id'=>'voleibol')); }}
							<label for="ciudad">Voleibol </label>
						</div>
						<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('tenis', 1, false,array('class' => 'form-group', 'id'=>'tenis')); }}
							<label for="ciudad" >Tenis de Mesa </label>
						</div>
						<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('futbol', 1, false,array('class' => 'form-group', 'id'=>'futbol')); }}
							<label for="ciudad">F&uacute;tbol </label>
						</div>
						<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('baloncesto', 1, false,array('class' => 'form-group', 'id'=>'baloncesto')); }} 
							<label for="ciudad">Baloncesto </label>
						</div>
						<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('artes_marciales', 1, false,array('class' => 'form-group', 'id'=>'artes_marciales')); }}
							<label for="ciudad">Artes Marciales </label>
						</div>
						<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('futbolito', 1, false,array('class' => 'form-group', 'id'=>'futbolito')); }}
							<label for="ciudad">Futbolito </label>
						</div>
						<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('ajedrez', 1, false,array('class' => 'form-group', 'id'=>'ajedrez')); }}
							<label for="ciudad">Ajedrez </label>
						</div>
						<div style="text-align: leftleftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('otros_a_d', 1, false,array('class' => 'form-group', 'id'=>'otros_a_d')); }}
							<label for="ciudad">Otros </label>
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Actividades Art&iacute;sticas</strong></legend>
						</div>
						<div style="text-align: leftleft;" class=" col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('teatro', 1, false,array('class' => 'form-group', 'id'=>'teatro')); }}
							<label for="ciudad">Teatro </label>								
						</div>							
						<div style="text-align: leftleft;" class=" col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('rondalla', 1, false,array('class' => 'form-group', 'id'=>'rondalla')); }}								
							<label for="ciudad">Rondalla </label>
						</div>							
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('orfeon', 1, false,array('class' => 'form-group', 'id'=>'orfeon')); }}
							<label for="ciudad" >Orfe&oacute;n Universitario </label>
						</div>	
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('danzas', 1, false,array('class' => 'form-group', 'id'=>'danzas')); }}
							<label for="ciudad">Danzas </label>
						</div>				
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('pintura', 1, false,array('class' => 'form-group', 'id'=>'pintura')); }}
							<label for="ciudad">Pintura </label>
						</div>							
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('estudiantina', 1, false,array('class' => 'form-group', 'id'=>'estudiantina')); }}
							<label for="ciudad">Estudiantina </label>
						</div>
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('musicafolklorica', 1, false,array('class' => 'form-group', 'id'=>'musicafolklorica')); }}
							<label for="ciudad">M&uacute;sica Folkl&oacute;rica </label>
						</div>
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('otras_act_artistica', 1, false,array('class' => 'form-group', 'id'=>'otras_act_artistica')); }}
							<label for="ciudad">Otras </label>
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Otras Actividades</strong></legend>
						</div>
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('seguridad_indus', 1, false,array('class' => 'form-group', 'id'=>'seguridad_indus')); }}
							<label for="ciudad">Seguridad Industrial</label>
						</div>
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('relaciones_h', 1, false,array('class' => 'form-group', 'id'=>'relaciones_h')); }}
							<label for="ciudad">Relaciones Humanas </label>	
						</div>
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('gru_protocolo', 1, false,array('class' => 'form-group', 'id'=>'gru_protocolo')); }}
							<label for="ciudad">Grupo de Protocolo </label>
						</div>
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('bombero_u', 1, false,array('class' => 'form-group', 'id'=>'bombero_u')); }}
							<label for="ciudad">Bombero Universitario </label>
						</div>
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('paramedico', 1, false,array('class' => 'form-group', 'id'=>'paramedico')); }}
							<label for="ciudad">Param&eacute;dico </label>
						</div>
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('accion_comunitaria', 1, false,array('class' => 'form-group', 'id'=>'accion_comunitaria')); }}
							<label for="ciudad">Acci&oacute;n Comunitaria </label>
						</div>
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('proteccion_indus', 1, false,array('class' => 'form-group', 'id'=>'proteccion_indus')); }}
							<label for="ciudad">Protecci&oacute;n Industrial </label>
						</div>
						<div style="text-align: leftleft;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('mejor_aprendizaje', 1, false,array('class' => 'form-group', 'id'=>'mejor_aprendizaje')); }}
							<label for="ciudad">Mejoramiento del Aprendizaje </label>
						</div>
						<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('otros_grupos', 1, false,array('class' => 'form-group', 'id'=>'otros_grupos')); }}
							<label for="ciudad">Otros Grupos </label>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 izquierda">
								<a href="#subir" id="ant_2" class="btn btn-primary"><span class="icon-arrow-left-2" style="font-size:9px;"></span> Anterior</a>
							</div>
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 derecha">
								<a href="#subir" id="sig_3" class="btn btn-primary">Siguiente <span class="icon-arrow-right-2" style="font-size:9px;"></span></a>
							</div>
						</div>		
					</div>
				</div>
			</div>
			<div class="tab-pane @if($tab=='datossocioeconomicos'){{'active'}}@endif" id="datossocioeconomicos2">
				<br>
				<div class="panel panel-primary">
					<div class="panel-heading">
					    <h3 class="panel-title">Datos Socioeconomicos</h3>
					</div>
					<div class="panel-body">
						@if(Session::has('message_error_datossocioeconomicos'))
							<div>
								<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
									<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_datossocioeconomicos') }}</strong></div>
								</div>
							</div>
						@endif	
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('d&oacute;nde vivir&aacute; estudie')){{'has-error'}} @endif">
							<label for="ciudad">¿D&oacute;nde vivir&aacute; Ud. mientras estudie? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('vivira_est', $vivira_est ,'NULL', $turno =array('class' => 'form-control', 'id'=>'vivira_est')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('d&oacute;nde vivir&aacute; estudie'))	{{ Session::get('message_datossocioeconomicos')->first('d&oacute;nde vivir&aacute; estudie') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('expl d&oacute;nde vivir&aacute; estudie')){{'has-error'}} @endif">
							<label for="nom_ape" id="">¿Explique d&oacute;nde vivir&aacute; Ud. mientras estudie?<span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('otra_vivienda', NULL, $attributes = array('class' => 'form-control', 'id'=>'otra_vivienda')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('expl d&oacute;nde vivir&aacute; estudie'))	{{ Session::get('message_datossocioeconomicos')->first('expl d&oacute;nde vivir&aacute; estudie') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('qui&eacute;n costea sus gastos')){{'has-error'}} @endif">
							<label for="ciudad">¿Qui&eacute;n costea sus gastos de estudios? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('costea_gast', $costea_g ,'NULL', $turno =array('class' => 'form-control', 'id'=>'costea_gast')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('qui&eacute;n costea sus gastos'))	{{ Session::get('message_datossocioeconomicos')->first('qui&eacute;n costea sus gastos') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('otro costea sus gastos')){{'has-error'}} @endif">
							<label for="nom_ape" id="">¿Nombre que otro costea sus gastos? </label> 
								{{ Form::text('nombre_cost_gast', NULL, $attributes = array('class' => 'form-control', 'id'=>'nombre_cost_gast')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('otro costea sus gastos'))	{{ Session::get('message_datossocioeconomicos')->first('otro costea sus gastos') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('disfruta d')){{'has-error'}} @endif">
							<label for="ciudad">¿Disfruta Ud. de? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('disfruta_d', $disfruta_d ,'NULL', $turno =array('class' => 'form-control', 'id'=>'disfruta_d')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('disfruta d'))	{{ Session::get('message_datossocioeconomicos')->first('disfruta d') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('nombre de otra instituci&oacute;n ')){{'has-error'}} @endif">
							<label for="nom_ape" id="">Nombre de otra instituci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('nombre_inst', NULL, $attributes = array('class' => 'form-control', 'id'=>'nombre_inst')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('nombre de otra instituci&oacute;n '))	{{ Session::get('message_datossocioeconomicos')->first('nombre de otra instituci&oacute;n ') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('viven sus padres')){{'has-error'}} @endif">
							<label for="ciudad">¿Viven sus padres? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('viven_padres', $viven_padres ,'null', $turno =array('class' => 'form-control', 'id'=>'viven_padres')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('viven sus padres'))	{{ Session::get('message_datossocioeconomicos')->first('viven sus padres') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('ingreso mensual')){{'has-error'}} @endif">
							<label for="ciudad">Indique el ingreso mensual familiar <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('ingreso_men', $ingreso_men ,'NULL', $turno =array('class' => 'form-control', 'id'=>'ingreso_men')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('ingreso mensual'))	{{ Session::get('message_datossocioeconomicos')->first('ingreso mensual') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('estado civil padres')){{'has-error'}} @endif">
							<label for="ciudad">¿Estado civil de sus padres? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('ecivil_padre', $ecivil_p ,'NULL', $turno =array('class' => 'form-control', 'id'=>'ecivil_padre')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('estado civil padres'))	{{ Session::get('message_datossocioeconomicos')->first('estado civil padres') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('con quien vive ud')){{'has-error'}} @endif">
							<label for="ciudad">¿Con quien vive Ud.? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('vive_con', $vive_con ,'NULL', $turno =array('class' => 'form-control', 'id'=>'vive_con')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('con quien vive ud'))	{{ Session::get('message_datossocioeconomicos')->first('con quien vive ud') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('vive otra persona')){{'has-error'}} @endif">
							<label for="nom_ape" id="">Indique el Nombre <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('vive_con_ex', NULL, $attributes = array('class' => 'form-control', 'id'=>'vive_con_ex')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('vive otra persona'))	{{ Session::get('message_datossocioeconomicos')->first('vive otra persona') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('tiene poliza')){{'has-error'}} @endif">
							<label for="ciudad">¿Usted posee una p&oacute;liza de seguros? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('posee_poliza', $poliza ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'posee_poliza')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('tiene poliza'))	{{ Session::get('message_datossocioeconomicos')->first('tiene poliza') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('entidad poliza')){{'has-error'}} @endif">
							<label for="ciudad">¿Entidad de donde proviene su p&oacute;liza? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::text('entidad_poliza',NULL, $attributes =array('class' => 'form-control', 'id'=>'entidad_poliza', 'readOnly' => 'readOnly')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('entidad poliza'))	{{ Session::get('message_datossocioeconomicos')->first('entidad poliza') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('numero poliza')){{'has-error'}} @endif">
							<label for="ciudad">Nº de p&oacute;liza <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::text('numero_poliza',NULL, $attributes =array('class' => 'form-control mayuscula', 'id'=>'numero_poliza', 'readOnly' => 'readOnly')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('numero poliza'))	{{ Session::get('message_datossocioeconomicos')->first('numero poliza') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<p><strong>Nota:</strong> Si usted posee una poliza de accidentes personales debe anexar al sobre la copia de la misma.</p>
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('entidad_aseguradora')){{'has-error'}} @endif">
							<label for="ciudad">Entidad aseguradora <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::text('entidad_aseguradora',NULL, $attributes =array('class' => 'form-control', 'id'=>'entidad_aseguradora', 'readOnly' => 'readOnly')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('entidad_aseguradora'))	{{ Session::get('message_datossocioeconomicos')->first('entidad_aseguradora') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('beneficiario')){{'has-error'}} @endif">
							<label for="ciudad">Indique el nombre del beneficiario <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::text('beneficiario',NULL, $attributes =array('class' => 'form-control', 'id'=>'beneficiario', 'placeholder' => 'NOMBRE DEL BENEFICIARIO', 'readOnly' => 'readOnly')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('beneficiario'))	{{ Session::get('message_datossocioeconomicos')->first('beneficiario') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Personas que viven en su hogar</strong></legend>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos adultos')){{'has-error'}} @endif">
							<label for="nom_ape" id="l">Adultos (Indique una cantidad)<span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('adultos', NULL, $attributes = array('class' => 'form-control', 'id'=>'adultos', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos adultos'))	{{ Session::get('message_datossocioeconomicos')->first('cuantos adultos') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos ni&ntilde;os')){{'has-error'}} @endif">
							<label for="nom_ape" id="">Ni&ntilde;os (Indique una cantidad) <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('ninos', NULL, $attributes = array('class' => 'form-control', 'id'=>'ninos', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos ni&ntilde;os'))	{{ Session::get('message_datossocioeconomicos')->first('cuantos ni&ntilde;os') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos hermanos')){{'has-error'}} @endif">
							<label for="nom_ape" id="">¿Cuantos hermanos tiene? (Indique cantidad) <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('cuantos_herm', NULL, $attributes = array('class' => 'form-control', 'id'=>'cuantos_herm', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos hermanos'))	{{ Session::get('message_datossocioeconomicos')->first('cuantos hermanos') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos hijos')){{'has-error'}} @endif">
							<label for="nom_ape" id="">¿Cuantos Hijos tiene? (Indique una cantidad) <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('cuantos_hi', NULL, $attributes = array('class' => 'form-control', 'id'=>'cuantos_hi', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('cuantos hijos'))	{{ Session::get('message_datossocioeconomicos')->first('cuantos hijos') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Posee alguna discapacidad</strong></legend>
						</div>
						<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('ceguera', '1', false,array('class' => 'form-group', 'id'=>'ceguera')); }}
							<label for="ciudad">Ceguera Total </label>
						</div>
						<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('sordera', '1', false,array('class' => 'form-group', 'id'=>'sordera')); }}
							<label for="ciudad">Sordera Total </label>
						</div>
						<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('retardo_m', '1', false,array('class' => 'form-group', 'id'=>'retardo_m')); }}
							<label for="ciudad">Retardo Mental </label>
						</div>
						<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('disc_ext_s', '1', false,array('class' => 'form-group', 'id'=>'disc_ext_s')); }}
							<label for="ciudad">Disc. de Extremidades Superiores </label>
						</div>

						<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('disc_ext_i', '1', false,array('class' => 'form-group', 'id'=>'disc_ext_i')); }}
							<label for="ciudad">Disc. de Extremidades Inferiores </label>
						</div>
						<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('disc_ninguna', '1', false,array('class' => 'form-group', 'id'=>'disc_ninguna')); }}
							<label for="ciudad">Ninguna </label>
						</div>
						<div style="text-align: left;" class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('otra_disc', '1', false,array('class' => 'form-group', 'id'=>'otra_disc')); }}
							<label for="ciudad">Otra </label>
						</div>
						<div class="form-group col-lg-3 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('otra discapacidad')){{'has-error'}} @endif">
							<label for="nom_ape" id="">Explique la otra discapacidad: <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('ex_otra_disc', NULL, $attributes = array('class' => 'form-control', 'id'=>'ex_otra_disc')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('otra discapacidad'))	{{ Session::get('message_datossocioeconomicos')->first('otra discapacidad') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('zurdo o derecho')){{'has-error'}} @endif">
							<label for="ciudad">¿Zurdo o Derecho? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('escribe', $escribe ,'NULL', $turno =array('class' => 'form-control', 'id'=>'escribe')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('zurdo o derecho'))	{{ Session::get('message_datossocioeconomicos')->first('zurdo o derecho') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('pertenece etnia')){{'has-error'}} @endif">
							<label for="ciudad">¿Pertenece Ud. a una Etnia? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('etnia', $select ,'NULL', $turno =array('class' => 'form-control', 'id'=>'etnia')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('pertenece etnia'))	{{ Session::get('message_datossocioeconomicos')->first('pertenece etnia') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('fuma')){{'has-error'}} @endif">
							<label for="ciudad">¿Ud. Fuma? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('fuma', $select ,'NULL', $turno =array('class' => 'form-control', 'id'=>'fuma')) }}
							<div class="text-danger">
								@if(Session::has('message_datossocioeconomicos') && Session::get('message_datossocioeconomicos')->has('fuma'))	{{ Session::get('message_datossocioeconomicos')->first('fuma') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						@if(Session::has('message_cita'))
							<div>
								<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
									<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_cita') }}</strong></div>
								</div>
							</div>
						@endif
						<div class="form-group col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12  @if(Session::has('message_cita')){{'has-error'}} @endif" style="display: none;" id="cita">
							<label for="nom_ape">Seleccione el d&iacute;a que desea ser atendido(a) en nuestra casa de estudios <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							<div class="input-group date" id="fecha_n">
				             	{{ Form::text('fecha_cita', NULL, $attributes = array('class' => 'form-control', 'id'=>'fecha_cita', 'placeholder'=>'Seleccione una fecha','disabled'=>'disabled')) }}<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				            </div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 izquierda">
								<a href="#subir" id="ant_4" class="btn btn-primary"><span class="icon-arrow-left-2" style="font-size:9px;"></span> Anterior</a>
							</div>
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 derecha">
								{{ Form::submit('Procesar Pre-Inscripci&oacute;n', array('class' => 'btn btn-success', 'onclick' => 'this.disabled=true; this.value="Enviando"; this.form.submit()', 'style' => 'text-transform: initial;')) }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@stop
@section('postscript')
<script type="text/javascript" language="javascript">
	$('#carrera_uba').change(function(){ 	
		var sel_carrera = $('#carrera_uba').val();
		$("#turno_uba").html('<option value="">Seleccionar...</option>');
		$("#modalidad_uba").html('<option value="">Seleccionar...</option>');

		if (sel_carrera!=''){		 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxturnocarreras')}}", 
				dataType: "json",
				data: {
					sel_carrera: sel_carrera,
				},
				cache: false,
				success: function(data)
				{
					var sel_turno = '<option value="">Seleccionar...</option>';
					for(datos in data.turno){
						sel_turno+='<option value="'+data.turno[datos].id_turno+'">'+data.turno[datos].des_turno+'</option>';
					}

					var sel_modalidad = '<option value="">Seleccionar...</option>';
					for(datos in data.modalidad){
						sel_modalidad+='<option value="'+data.modalidad[datos].id_modalidad+'">'+data.modalidad[datos].des_modalidad+'</option>';
					}

					if ($('#convenios').val() == '68' && $('#carrera_uba').val() == '1') {
						sel_turno+='<option value="2">Tarde</option>';
					}
					$("#turno_uba").html(sel_turno);
					$("#modalidad_uba").html(sel_modalidad);
				} 
			});
		}
	}); 
	
    $(document).ready(function() {

    	$('#fecha,#fecha_g').datepicker({
	        language: "es",
	        autoclose: true,
	        dateFormat: "dd-mm-yy",
	        changeMonth:true,
	        changeYear:true,
	        yearRange: "c-90:c+0",
	        monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"], // Names of months for drop-down and formatting
		    monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"], // For formatting
		    dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"], // For formatting
		    dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"], // For formatting
		    dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"], // Column headings for days starting at Sunday	          
	    });

	    $('#fecha_cita').datepicker({
	        language: "es",
	        autoclose: true,
	        dateFormat: "dd-mm-yy",
	        changeMonth:true,
	        changeYear:true,
	        beforeShowDay: PrepareDate,
	        minDate: 0,
	        maxDate:"13-05-2019",
	        yearRange: "c-90:c+0",
	        monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"], // Names of months for drop-down and formatting
		    monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"], // For formatting
		    dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"], // For formatting
		    dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"], // For formatting
		    dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"], // Column headings for days starting at Sunday	          
	    });

	    var disabledDates = new Array({{$fechas_no}});

    	function DisableSpecificDates(date) {
		    var string = jQuery.datepicker.formatDate('dd-mm-yy', date);
		    return [disabledDates.indexOf(string) == -1];
		    }
		function PrepareDate(date){
		    if ($.datepicker.noWeekends(date)[0]) {
		    return DisableSpecificDates(date);
		    } else {
		    return $.datepicker.noWeekends(date);
		    }
		}

		//$('#nsobre').mask('99-999-000999', {placeholder: "__-__-_____"});
		$('#telefono').mask('9999-99999999', {placeholder: "____-________"});
		$('#telefono_empre').mask('9999-99999999', {placeholder: "____-________"});
		$('#telefono_r').mask('9999-99999999', {placeholder: "____-________"});
		$('#telefono_rep').mask('9999-99999999', {placeholder: "____-________"});
		$('#promedio_b').mask('99.00', {placeholder: "__.__"});
		$('#cedula').mask('99999999999');
		$('#cedula_rep').mask('99999990');
		$('#edad_rep').mask('90');

		/*$('#nsobre').blur(function(){
			var nsobre = $('#nsobre').val();
			var num = nsobre.split("-");
			var cod_nuc = num[0];
			var cod_esc = num[1];

			if (cod_nuc == '00' || cod_nuc >= 50) {
				
				$('#nucleo_uba').html('<option value="0">SAN JOAQUIN</option>');
			
			}else if (cod_nuc == '01'){
				
				$('#nucleo_uba').html('<option value="1">SAN ANTONIO DE LOS ALTOS</option>');

			} else if (cod_nuc == '02'){

				$('#nucleo_uba').html('<option value="2">APURE</option>');
			
			} else if (cod_nuc == '03'){
				
				$('#nucleo_uba').html('<option value="3">PUERTO ORDAZ</option>');
			
			} else {

				$('#nucleo_uba').html('<option value="">SELECCIONAR</option><option value="0">SAN JOAQUIN</option><option value="1">SAN ANTONIO DE LOS ALTOS</option><option value="2">APURE</option><option value="3">PUERTO ORDAZ</option>');
			}

			if (cod_esc == '101') {
				
				$('#carrera_uba').html('<option value="1">INGENIERIA DE SISTEMAS</option>');
			
			}else if (cod_esc == '102'){
				
				$('#carrera_uba').html('<option value="2">INGENIERIA ELECTRICA</option>');;

			} else if (cod_esc == '103'){

				$('#carrera_uba').html('<option value="3">ADMINISTRACION DE EMPRESAS</option>');
			
			} else if (cod_esc == '104'){
				
				$('#carrera_uba').html('<option value="4">CONTADURIA PUBLICA</option>');

			} else if (cod_esc == '105'){
				
				$('#carrera_uba').html('<option value="5">DERECHO</option>');
			
			} else if (cod_esc == '106'){
				
				$('#carrera_uba').html('<option value="6">COMUNICACION SOCIAL</option>');
			
			} else if (cod_esc == '107'){
				
				$('#carrera_uba').html('<option value="7">PSICOLOGIA</option>');
			
			}  else {

				$('#carrera_uba').html('<option value="">SELECCIONAR</option><option value="1">INGENIERIA DE SISTEMAS</option><option value="2">INGENIERIA ELECTRICA</option><option value="3">ADMINISTRACION DE EMPRESAS</option><option value="4">CONTADURIA PUBLICA</option><option value="5">DERECHO</option><option value="6">COMUNICACION SOCIAL</option><option value="7">PSICOLOGIA</option>');
			}*/
			
			var sel_carrera = $('#carrera_uba').val();
			$("#turno_uba").html('<option value="">Seleccionar...</option>');
			$("#modalidad_uba").html('<option value="">Seleccionar...</option>');

			if (sel_carrera!=''){		 
				$.ajax({
					type: "POST",
					url: "{{URL::action('PreinscripcionController@postAjaxturnocarreras')}}", 
					dataType: "json",
					data: {
						sel_carrera: sel_carrera,
					},
					cache: false,
					success: function(data)
					{
						var sel_turno = '<option value="">Seleccionar...</option>';
						for(datos in data.turno){
							sel_turno+='<option value="'+data.turno[datos].id_turno+'">'+data.turno[datos].des_turno+'</option>';
						}
						var sel_modalidad = '<option value="">Seleccionar...</option>';
						for(datos in data.modalidad){
							sel_modalidad+='<option value="'+data.modalidad[datos].id_modalidad+'">'+data.modalidad[datos].des_modalidad+'</option>';
						}

						if ($('#convenios').val() == '68' && $('#carrera_uba').val() == '1') {
							sel_turno+='<option value="2">Tarde</option>';
						}
						$("#turno_uba").html(sel_turno);
						$("#modalidad_uba").html(sel_modalidad);
					} 
				});
			}
		//});

		var nucleo = $('#nucleo').val();
		var convenio = $('#convenio').val();

		if (convenio == 2) {
			$('#cita').css('display', 'block');
			$('#fecha_cita').removeAttr('disabled');
			$('#fecha_cita').attr('readOnly', true);
			$('#fecha_cita').attr('required', true);
		
		} 
		else if(convenio == 0 ){

			$('#cita').css('display', 'none');
			$('#fecha_cita').removeAttr('readOnly');
			$('#fecha_cita').removeAttr('required');
			$('#fecha_cita').attr('disabled', true);
		}

		/*else{

			$('#cita').css('display', 'none');
			$('#fecha_cita').removeAttr('readOnly');
			$('#fecha_cita').removeAttr('required');
			$('#fecha_cita').attr('disabled', true);
		}*/

		$('#convenio').blur(function(){
			var convenio = $(this).val();
			
			if (convenio == 2 ) {
				$('#cita').css('display', 'block');
				$('#fecha_cita').removeAttr('disabled');
				$('#fecha_cita').attr('readOnly', true);
				$('#fecha_cita').attr('required', true);
			
			} 
			else if(convenio == 0){

				$('#cita').css('display', 'none');
				$('#fecha_cita').removeAttr('readOnly');
				$('#fecha_cita').removeAttr('required');
				$('#fecha_cita').attr('disabled', true);
			}

			/*else {

				$('#cita').css('display', 'none');
				$('#fecha_cita').removeAttr('readOnly');
				$('#fecha_cita').removeAttr('required');
				$('#fecha_cita').attr('disabled', true);
			}*/
		
		});

		$('#nucleo').blur(function(){

			var nucleo = $(this).val();

			if(nucleo != 0){

				$('#cita').css('display', 'none');
				$('#fecha_cita').removeAttr('readOnly');
				$('#fecha_cita').removeAttr('required');
				$('#fecha_cita').attr('disabled', true);
			}
		});

		var fecha_nac = $("#fecha").val();
		if (fecha_nac=='') {
			document.getElementById('cedula_rep').disabled = true;
			document.getElementById('nombres_rep').disabled = true;
			document.getElementById('apellidos_rep').disabled = true;
			document.getElementById('edad_rep').disabled = true;
			document.getElementById('direccion_rep').disabled = true;
			document.getElementById('telefono_rep').disabled = true;
			document.getElementById('parentesco').disabled = true;	
		}else{
			var div = fecha_nac.split("-");
			var fecha_nacimiento = div[2]+'/'+div[1]+'/'+div[0];
			var dia = div[0];
			var mes = div[1];
			var ano = div[2];
			fecha_hoy = new Date();
			ahora_ano = fecha_hoy.getYear();
			ahora_mes = fecha_hoy.getMonth();
			ahora_dia = fecha_hoy.getDate();
			edad = (ahora_ano + 1900) - ano;

		    if ( ahora_mes < (mes - 1)){ 
		      edad--;
		    }
		    if (((mes - 1) == ahora_mes) && (ahora_dia < dia)){ 
		      edad--;
		    }
		    if (edad > 1900){
		        edad -= 1900;
		    }
			$("#edad").val(edad);
			if (edad>17){
				document.getElementById('cedula_rep').disabled = true;
				document.getElementById('nombres_rep').disabled = true;
				document.getElementById('apellidos_rep').disabled = true;
				document.getElementById('edad_rep').disabled = true;
				document.getElementById('direccion_rep').disabled = true;
				document.getElementById('telefono_rep').disabled = true;
				document.getElementById('parentesco').disabled = true;													
			}else{
				document.getElementById('cedula_rep').disabled = false;
				document.getElementById('nombres_rep').disabled = false;
				document.getElementById('apellidos_rep').disabled = false;
				document.getElementById('edad_rep').disabled = false;
				document.getElementById('direccion_rep').disabled = false;
				document.getElementById('telefono_rep').disabled = false;
				document.getElementById('parentesco').disabled = false;	
			}
		}			

		var selec_e = document.getElementById('trabaja').value;
    	if (selec_e==1){
    		document.getElementById('empresa').disabled = false;
    		document.getElementById('direc_empresa').disabled = false;
    		document.getElementById('telefono_empre').disabled = false;
    		document.getElementById('depart_empresa').disabled = false;
    		document.getElementById('cargo_empresa').disabled = false;
    	}else{
    		document.getElementById('empresa').disabled = true;
    		document.getElementById('direc_empresa').disabled = true;
    		document.getElementById('telefono_empre').disabled = true;
    		document.getElementById('depart_empresa').disabled = true;
    		document.getElementById('cargo_empresa').disabled = true;
    	}

    	var selec_v = document.getElementById('vehiculo').value;
    	if (selec_v==1){
    		document.getElementById('placa').disabled = false;
    		document.getElementById('marca').disabled = false;
    		document.getElementById('modelo').disabled = false;
		}else{
			document.getElementById('placa').disabled = true;
			document.getElementById('marca').disabled = true;
			document.getElementById('modelo').disabled = true;
		}

		var nucleo =document.getElementById('nucleo').value;
		if (nucleo==0){
			document.getElementById('convenio').disabled = true;
		}

		$('#nucleo').change(function(){
			var nucleo = $(this).val();
			if (nucleo==0) {
				$('#convenio').prop('disabled', false);
			}else{
				$('#convenio').prop('disabled', true);
				$('#convenio').val('');
				$('#convenios').val('');
				$('#convenios').prop('disabled', true);
			}
		});

		var nucleo = $("#nucleo").val();
			if (nucleo==0) {
				$('#convenio').prop('disabled', false);
			}else{
				$('#convenio').prop('disabled', true);
				$('#convenio').val('');
				$('#convenios').val('');
				$('#convenios').prop('disabled', true);
			}


		var convenio = document.getElementById('convenio').value;
    	if (convenio==1){
			document.getElementById('convenios').disabled = false;
			
		}
		else if(convenio==2){
			document.getElementById('convenios').disabled = true;
		}
		else{
			document.getElementById('convenios').disabled = true;
		}

		$('#convenio').change(function(){ 
	    	var convenio = $(this).val();
	    	if (convenio==1){
	    		$('#convenios').prop('disabled',false);
			}else{
				$('#convenios').prop('disabled',true);
				$('#convenios').val('');
			}
		});

		var nucleo = $("#nucleo").val();
            $.ajax({
                type: "POST",
                url: "{{URL::action('PreinscripcionController@postEscuelanucleos')}}", 
                dataType: "json",
                data: {
                    nucleo: nucleo
                },
                cache: false,
                success: function(data)
                {
                    var carrera_uba = "<option value=''>SELECCIONAR...</option>";
                    for(datos in data.carrera_uba){
                        carrera_uba += '<option value="'+ data.carrera_uba[datos].id +'">'+ data.carrera_uba[datos].carrera +'</option>';
                    }
                    $("#carrera_uba").html(carrera_uba);
                } 
            })

		$("#nucleo").change(function(){
            var nucleo = $("#nucleo").val();
            $.ajax({
                type: "POST",
                url: "{{URL::action('PreinscripcionController@postEscuelanucleos')}}", 
                dataType: "json",
                data: {
                    nucleo: nucleo
                },
                cache: false,
                success: function(data)
                {
                    var carrera_uba = "<option value=''>SELECCIONAR...</option>";
                    for(datos in data.carrera_uba){
                        carrera_uba += '<option value="'+ data.carrera_uba[datos].id +'">'+ data.carrera_uba[datos].carrera +'</option>';
                    }
                    $("#carrera_uba").html(carrera_uba);
                } 
            })
        })

		var pais_nac= $('#pais_nac').val();
		if (pais_nac==230) {
			$('#ciudad_estu_secu').prop('disabled', false);
			$('#estados_estu_secu').prop('disabled', false);
			$('#estados_ubi').prop('disabled', false);
			$('#ciudad_ubi').prop('disabled', false);
		}
		else{
			$('#ciudad_estu_secu').prop('disabled', true);
			$('#estados_estu_secu').prop('disabled', true);
			$('#estados_ubi').prop('disabled', true);
			$('#ciudad_ubi').prop('disabled', true);
		}

		$('#pais_nac').change(function(){

			var pais_nac = $(this).val();
			if (pais_nac==230) {
				$('#estados_estu_secu').prop('disabled', false);
				$('#ciudad_estu_secu').prop('disabled', false);
				$('#estados_ubi').prop('disabled', false);
				$('#ciudad_ubi').prop('disabled', false);
			}
			else {
				$('#estados_estu_secu').prop('disabled', true);
				$('#ciudad_estu_secu').prop('disabled', true);
				$('#estados_ubi').prop('disabled', true);
			    $('#ciudad_ubi').prop('disabled', true);
			}
		})



		var pais = $("#pais_nac").val();
		if (pais!=''){ 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxestados_nac')}}", 
				dataType: "json",
				data: {
					pais: pais,
				},
				cache: false,
				success: function(data)
				{
					var estados_select = '<option value="">Seleccionar...</option>';
					for(datos in data.estados){
						estados_select+='<option value="'+data.estados[datos].id_estados+'">'+data.estados[datos].des_estados+'</option>';
					}
					$("#estados_nac").html(estados_select);

					<?php if (Session::has('estados_nac_selected')) { ?>
						var estados_nac_selected= {{Session::get('estados_nac_selected')}};
						if (estados_nac_selected != '') {
							$('#estados_nac option[value="'+estados_nac_selected+'"]').attr('selected','selected');
						};
					<?php } ?>
					var estado = $("#estados_nac").val();
				
				} 
			});
		};

		<?php if (Session::has('convenio')) { ?>
			
			var nucleo = $("#nucleo").val();
			if (nucleo==0) {
				$('#convenio').prop('disabled', false);
			}else{
				$('#convenio').prop('disabled', true);
				$('#convenio').val('');
				$('#convenios').val('');
				$('#convenios').prop('disabled', true);
			}
		<?php } ?>	

		<?php if (Session::has('estados_nac_selected')) { ?>
			var estado = {{Session::get('estados_nac_selected')}};
		
			if (estado!=''){		 
				$.ajax({
					type: "POST",
					url: "{{URL::action('PreinscripcionController@postAjaxciudades_nac')}}", 
					dataType: "json",
					data: {
						estado: estado,
					},
					cache: false,
					success: function(data)
					{
						var ciudad_select = '<option value="">Seleccionar...</option>';
						for(datos in data.ciudad){
							ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
						}
						$("#ciudad_nac").html(ciudad_select);

						<?php if (Session::has('ciudad_nac_selected')) { ?>
							var ciudad_nac_selected= {{Session::get('ciudad_nac_selected')}};
							if (ciudad_nac_selected != '') {
								$('#ciudad_nac option[value="'+ciudad_nac_selected+'"]').attr('selected','selected');
							};
						<?php } ?>
					} 
				});
			}
		<?php } ?>

	    $('#trabaja').change(function(){ 
	    	var selec_e = document.getElementById('trabaja').value;
	    	if (selec_e==1){
	    		document.getElementById('empresa').disabled = false;
	    		document.getElementById('direc_empresa').disabled = false;
	    		document.getElementById('telefono_empre').disabled = false;
	    		document.getElementById('depart_empresa').disabled = false;
	    		document.getElementById('cargo_empresa').disabled = false;
	    	}else{
	    		document.getElementById('empresa').disabled = true;
	    		document.getElementById('direc_empresa').disabled = true;
	    		document.getElementById('telefono_empre').disabled = true;
	    		document.getElementById('depart_empresa').disabled = true;
	    		document.getElementById('cargo_empresa').disabled = true;
	    	}

		});

		$('#vehiculo').change(function(){ 
	    	var selec_v = document.getElementById('vehiculo').value;
	    	if (selec_v==1){
	    		document.getElementById('placa').disabled = false;
	    		document.getElementById('marca').disabled = false;
	    		document.getElementById('modelo').disabled = false;
			}else{
				document.getElementById('placa').disabled = true;
				document.getElementById('marca').disabled = true;
				document.getElementById('modelo').disabled = true;
			}

		});

		var remember = document.getElementById('otra_disc');
		    if (remember.checked){
		        document.getElementById('ex_otra_disc').disabled = false;
		    }else{
		        document.getElementById('ex_otra_disc').disabled = true;
		    }

		$('#otra_disc').click(function(){ 
	    	var selectp = document.getElementById('otra_disc').checked;
	    	if (selectp==true){
	    		document.getElementById('ex_otra_disc').disabled = false;
			}else{
				document.getElementById('ex_otra_disc').disabled = true;
			}
		});
		
		var selectp = document.getElementById('g_especialidad').value;	    	
	    	if (selectp==3){
	    		document.getElementById('ex_especialidad').disabled = false;
			}else{
				document.getElementById('ex_especialidad').disabled = true;
			}

		$('#g_especialidad').change(function(){ 
	    	var selectp = document.getElementById('g_especialidad').value;
	    	if (selectp==3){
	    		document.getElementById('ex_especialidad').disabled = false;
			}else{
				document.getElementById('ex_especialidad').disabled = true;
			}
		});

		$('#pais_nac').change(function(){

			var pais = $("#pais_nac").val();
			 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxestados_nac')}}", 
				dataType: "json",
				data: {
					pais: pais,
				},
				cache: false,
				success: function(data)
				{
					var estados_select = '<option value="">Seleccionar...</option>';
					for(datos in data.estados){
						estados_select+='<option value="'+data.estados[datos].id_estados+'">'+data.estados[datos].des_estados+'</option>';
					}
					$("#estados_nac").html(estados_select);
				} 
			});
		});

		$('#estados_nac').change(function(){
			var estado = $("#estados_nac").val();
				 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades_nac')}}", 
				dataType: "json",
				data: {
					estado: estado,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_nac").html(ciudad_select);
				} 
			});
		});

		$('#estados').change(function(){
			var estados = $("#estados").val();
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad").html(ciudad_select);
				} 
			});
		});

		var estados_estu_secu = $("#estados_estu_secu").val();
		if (estados_estu_secu!=''){ 	 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_estu_secu,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_estu_secu").html(ciudad_select);
					<?php if (Session::has('ciudad_estu_secu_selected') && Session::get('ciudad_estu_secu_selected') !="") { ?>
							var ciudad_estu_secu = {{Session::get('ciudad_estu_secu_selected')}};

							if (ciudad_estu_secu != '') {
								$('#ciudad_estu_secu option[value="'+ciudad_estu_secu+'"]').attr('selected','selected');
							};

					<?php } ?>
				} 
			});
		}

		$('#estados_estu_secu').change(function(){
			var estados_estu_secu = $("#estados_estu_secu").val();	 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_estu_secu,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_estu_secu").html(ciudad_select);
				} 
			});
		
		});

		var estados_ubi = $("#estados_ubi").val();
		if (estados_ubi!=''){ 	 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_ubi,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_ubi").html(ciudad_select);
					<?php if (Session::has('ciudad_ubi_selected') && Session::get('ciudad_ubi_selected') !="") { ?>
							var ciudad_ubi_selected = {{Session::get('ciudad_ubi_selected')}};

							if (ciudad_ubi_selected != '') {
								$('#ciudad_ubi option[value="'+ciudad_ubi_selected+'"]').attr('selected','selected');
							};

					<?php } ?>
				} 
			});
		}

		$('#estados_ubi').change(function(){
			var estados_ubi = $("#estados_ubi").val();	 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_ubi,
				},
				cache: false,
				success: function(data)
				{
					
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_ubi").html(ciudad_select);
				} 
			});
		});

		$('#estados_cs').change(function(){
			var estados_cs = $("#estados_cs").val();		 
			$.ajax({
				type: "POST",
				url: "{{URL::action('PreinscripcionController@postAjaxciudades')}}", 
				dataType: "json",
				data: {
					estados: estados_cs,
				},
				cache: false,
				success: function(data)
				{
					var ciudad_select = '<option value="">Seleccionar...</option>';
					for(datos in data.ciudad){
						ciudad_select+='<option value="'+data.ciudad[datos].id_ciudad+'">'+data.ciudad[datos].des_ciudad+'</option>';
					}
					$("#ciudad_cs").html(ciudad_select);
				} 
			});
		
		});

		var cargo_d_bachiderato = document.getElementById('cargo_d_bachiderato').value;
		if (cargo_d_bachiderato==1){
			document.getElementById('e_cargo_d_bachiderato').disabled = false;
		}else{
			document.getElementById('e_cargo_d_bachiderato').disabled = true;
		}

		$('#cargo_d_bachiderato').change(function(){ 
	    	var cargo_d_bachiderato = document.getElementById('cargo_d_bachiderato').value;
	    	if (cargo_d_bachiderato==1){
				document.getElementById('e_cargo_d_bachiderato').disabled = false;
			}else{
				document.getElementById('e_cargo_d_bachiderato').disabled = true;
			}
		});

		var otros_estudio = document.getElementById('otros_estudio').value;
    	if (otros_estudio==1){
			document.getElementById('otra_institucion').disabled = false;
			document.getElementById('otra_carreras').disabled = false;
			document.getElementById('se_graduo').disabled = false;
		}else{
			document.getElementById('otra_institucion').disabled = true;
			document.getElementById('otra_carreras').disabled = true;
			document.getElementById('se_graduo').disabled = true;
		}

		$('#otros_estudio').change(function(){ 
	    	var selectp = document.getElementById('otros_estudio').value;
	    	if (selectp==1){
				document.getElementById('otra_institucion').disabled = false;
				document.getElementById('otra_carreras').disabled = false;
				document.getElementById('se_graduo').disabled = false;
			}else{
				document.getElementById('otra_institucion').disabled = true;
				document.getElementById('otra_carreras').disabled = true;
				document.getElementById('se_graduo').disabled = true;
			}
		});

		var selectp = document.getElementById('vivira_est').value;
	    	if (selectp==5){
				document.getElementById('otra_vivienda').disabled = false;
			}else{
				document.getElementById('otra_vivienda').disabled = true;
			}

		$('#vivira_est').change(function(){ 
	    	var selectp = document.getElementById('vivira_est').value;
	    	if (selectp==5){
				document.getElementById('otra_vivienda').disabled = false;
			}else{
				document.getElementById('otra_vivienda').disabled = true;
			}	
		});

		$('#vivira_est').change(function(){ 
	    	var selectp = document.getElementById('vivira_est').value;	
		});

		var selectg = document.getElementById('costea_gast').value;
	    	if (selectg==5){
				document.getElementById('nombre_cost_gast').disabled = false;
			}else{
				document.getElementById('nombre_cost_gast').disabled = true;
			}

		$('#costea_gast').change(function(){ 
	    	var selectg = document.getElementById('costea_gast').value;
	    	if (selectg==5){
				document.getElementById('nombre_cost_gast').disabled = false;
			}else{
				document.getElementById('nombre_cost_gast').disabled = true;
			}	
		});

		var selectp = document.getElementById('disfruta_d').value;
	    	if (selectp==5){
				document.getElementById('nombre_inst').disabled = false;
			}else{
				document.getElementById('nombre_inst').disabled = true;
			}

		$('#disfruta_d').change(function(){ 
	    	var selectp = document.getElementById('disfruta_d').value;
	    	if (selectp==5){
				document.getElementById('nombre_inst').disabled = false;
			}else{
				document.getElementById('nombre_inst').disabled = true;
			}	
		});		

		var selectp = document.getElementById('vive_con').value;
	    	if (selectp==7){
				document.getElementById('vive_con_ex').disabled = false;
			}else{
				document.getElementById('vive_con_ex').disabled = true;
			}

		$('#vive_con').change(function(){ 
	    	var selectp = document.getElementById('vive_con').value;
	    	if (selectp==7){
				document.getElementById('vive_con_ex').disabled = false;
			}else{
				document.getElementById('vive_con_ex').disabled = true;
			}	
		});

		$('#sig_1').click(function(){	 
			var sig_a = document.getElementById("personales1");
			var sig_b = document.getElementById("personales2");
			$('#periodo1').removeClass('active');
			$('#periodo2').removeClass('active');
			$('#personales1').addClass('active');
			$('#personales2').addClass('active');
		});

		$('#ant_1').click(function(){
			var sig_a = document.getElementById("periodo1");
			var sig_b = document.getElementById("periodo2");
			$('#personales1').removeClass('active');
			$('#personales2').removeClass('active');
			$('#periodo1').addClass('active');
			$('#periodo2').addClass('active');
		});

		$('#sig_2').click(function(){
			var sig_a = document.getElementById("datossecundaria1");
			var sig_b = document.getElementById("datossecundaria2");
			$('#personales1').removeClass('active');
			$('#personales2').removeClass('active');
			$('#datossecundaria1').addClass('active');
			$('#datossecundaria2').addClass('active');
		});

		$('#ant_2').click(function(){
			var sig_a = document.getElementById("personales1");
			var sig_b = document.getElementById("personales2");
			$('#datossecundaria1').removeClass('active');
			$('#datossecundaria2').removeClass('active');
			$('#personales1').addClass('active');
			$('#personales2').addClass('active');
		});

		$('#sig_3').click(function(){
			var sig_a = document.getElementById("datossocioeconomicos1");
			var sig_b = document.getElementById("datossocioeconomicos2");
			$('#datossecundaria1').removeClass('active');
			$('#datossecundaria2').removeClass('active');
			$('#datossocioeconomicos1').addClass('active');
			$('#datossocioeconomicos2').addClass('active');
		});

		$('#ant_3').click(function(){
			var sig_a = document.getElementById("datossecundaria1");
			var sig_b = document.getElementById("datossecundaria2");
			$('#datossocioeconomicos1').removeClass('active');
			$('#datossocioeconomicos2').removeClass('active');
			$('#datossecundaria1').addClass('active');
			$('#datossecundaria2').addClass('active');
		});

		$('#fecha').change(function(){
			var fecha_nac = $("#fecha").val();
			var div = fecha_nac.split("-")
			var fecha_nacimiento = div[2]+'/'+div[1]+'/'+div[0];
			var dia = div[0];
			var mes = div[1];
			var ano = div[2];
			fecha_hoy = new Date();
			ahora_ano = fecha_hoy.getYear();
			ahora_mes = fecha_hoy.getMonth();
			ahora_dia = fecha_hoy.getDate();
			edad = (ahora_ano + 1900) - ano;			    
		    if ( ahora_mes < (mes - 1)){ 
		      edad--;
		    }
		    if (((mes - 1) == ahora_mes) && (ahora_dia < dia)){ 
		      edad--;
		    }
		    if (edad > 1900){
		        edad -= 1900;
		    }
			$("#edad").val(edad);
			if (edad>17){				
				document.getElementById('cedula_rep').disabled = true;
				document.getElementById('nombres_rep').disabled = true;
				document.getElementById('apellidos_rep').disabled = true;
				document.getElementById('edad_rep').disabled = true;
				document.getElementById('direccion_rep').disabled = true;
				document.getElementById('telefono_rep').disabled = true;
				document.getElementById('parentesco').disabled = true;								
			}else{
				document.getElementById('cedula_rep').disabled = false;
				document.getElementById('nombres_rep').disabled = false;
				document.getElementById('apellidos_rep').disabled = false;
				document.getElementById('edad_rep').disabled = false;
				document.getElementById('direccion_rep').disabled = false;
				document.getElementById('telefono_rep').disabled = false;
				document.getElementById('parentesco').disabled = false;
			}
		});

		$('#posee_poliza').change(function(){
			var opcion = $('#posee_poliza').val();
			if (opcion == 1) {
				document.getElementById('entidad_poliza').readOnly = false;
				document.getElementById('numero_poliza').readOnly = false;
				document.getElementById('beneficiario').readOnly = true;
				$('#entidad_poliza').val('');
				$('#numero_poliza').val('');
				$('#entidad_aseguradora').val('');
				$('#beneficiario').val('');
			}else if (opcion == 0){
				document.getElementById('entidad_poliza').readOnly = true;
				document.getElementById('numero_poliza').readOnly = true;
				document.getElementById('beneficiario').readOnly = false;
				$('#entidad_poliza').val('');
				$('#numero_poliza').val('');
				$('#entidad_aseguradora').val('C.A DE SEGUROS LA OCCIDENTAL');
				$('#beneficiario').val('');
			} else {
				document.getElementById('entidad_poliza').readOnly = true;
				document.getElementById('numero_poliza').readOnly = true;
				document.getElementById('beneficiario').readOnly = true;
				$('#entidad_poliza').val('');
				$('#numero_poliza').val('');
				$('#entidad_aseguradora').val('');
				$('#beneficiario').val('');
			}
		});

		$('#curso_uba').change(function(){
			var opcion = $('#curso_uba').val();
			if (opcion == 1) {
				document.getElementById('graduo_uba').disabled= false;
				$('#graduo_uba').val('');
			}else if (opcion == 0){
				document.getElementById('graduo_uba').disabled= true;
				$('#graduo_uba').val('');
			} else {
				document.getElementById('graduo_uba').disabled= true;
				$('#graduo_uba').val('');
			}
		});

		var nivel_estudio = $('#nivel_estudio').val();

		if (nivel_estudio != '') {
			document.getElementById('nivel_estudio').disabled= false;
		}

		$('#se_graduo').change(function(){
			var opcion = $('#se_graduo').val();
			if (opcion == 1) {
				document.getElementById('nivel_estudio').disabled= false;
				$('#nivel_estudio').val('');
			}else if (opcion == 0){
				document.getElementById('nivel_estudio').disabled= true;
				$('#nivel_estudio').val('');
			} else {
				document.getElementById('nivel_estudio').disabled= true;
				$('#nivel_estudio').val('');
			}
		});
    });
 </script>
{{ HTML::script('js/jquery_ui.js') }}
{{ HTML::script('js/mask-plugins/src/jquery.mask.js') }}
{{ HTML::script('js/sololetras.js') }}
{{ HTML::script('js/solonumeros.js') }}
@stop