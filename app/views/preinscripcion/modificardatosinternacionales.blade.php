@extends('layouts.preinscripcioninternacional')
@section('css')
{{ HTML::style('css/jquery_ui.css')}}
@stop
<A id="subir"></A>
@section('content')
<div class="container container-margin-top">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs nav-justified" role="tablist">
	  <li id="periodo1" class="negrita @if($tab=='periodo'){{'active'}}@endif"><a href="#periodo2" role="tab" data-toggle="tab">Per&iacute;odo Acad&eacute;mico</a></li>
	  <li id="personales1" class="negrita @if($tab=='personales'){{'active'}}@endif"><a href="#personales2" role="tab" data-toggle="tab">Datos Personales</a></li>
	  <li id="datossecundaria1" class="negrita @if($tab=='datossecundaria'){{'active'}}@endif"><a href="#datossecundaria2" role="tab" data-toggle="tab">Datos de Educaci&oacute;n</a></li>
    </ul>
	<form name="parametros" role="form" method="POST" action="{{ URL::to('global/procesarmodificacion') }}">
		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane @if($tab=='periodo'){{'active'}}@endif" id="periodo2"><br>
				<div class="panel panel-primary" >
					<div class="panel-heading"><h3 class="panel-title">{{ $mensaje }}</h3></div>
					<div class="panel-body">
						@if(Session::has('message_error_periodo'))
							<div>
								<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
									<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_periodo') }}</strong></div>
								</div>
							</div>
						@endif
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_periodo') && Session::get('message_periodo')->has('tipo de solicitud')){{'has-error'}} @endif">
							<label for="nom_ape">Tipo de Solicitud <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::select('tsolicitud', $tsolicitud, NULL , $attributes = array('class' => 'form-control', 'id'=>'tsolicitud')) }}							  
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_periodo') && Session::get('message_periodo')->has('tipo de solicitud'))	{{ Session::get('message_periodo')->first('tipo de solicitud') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_periodo') && Session::get('message_periodo')->has('fecha de nacimiento')){{'has-error'}} @endif">
							<label for="nom_ape">Fecha de Nacimiento <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							<div class="input-group date" id="fecha_n">
				             	{{ Form::text('fecha', NULL, $attributes = array('class' => 'form-control', 'id'=>'fecha', 'placeholder'=>'Seleccione fecha de nacimiento', 'readonly'=>'readonly', 'required'=>'required')) }}<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				            	{{ Form::hidden('edad',  NULL , $attributes = array('class' => 'form-control', 'id'=>'edad')) }}
				            </div>
				            <div class="text-danger" id="text_uc">
				            	@if(Session::has('message_periodo') && Session::get('message_periodo')->has('fecha de nacimiento'))	{{ Session::get('message_periodo')->first('fecha de nacimiento') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_periodo') && Session::get('message_periodo')->has('nacionalidad')){{'has-error'}} @endif">
							<label for="nom_ape">Nacionalidad <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::select('tcedula', $tcedula, NULL , $attributes = array('class' => 'form-control', 'id'=>'tcedula')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_periodo') && Session::get('message_periodo')->has('nacionalidad'))	{{ Session::get('message_periodo')->first('nacionalidad') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_periodo') && Session::get('message_periodo')->has('c&eacute;dula')){{'has-error'}} @endif">
							<label for="cedula">C&eacute;dula <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::text('cedula',  NULL , $attributes = array('class' => 'form-control', 'id'=>'cedula', 'onkeypress'=>'return soloNumeros(event)','maxlength'=>'11')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_periodo') && Session::get('message_periodo')->has('c&eacute;dula'))	{{ Session::get('message_periodo')->first('c&eacute;dula') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 derecha">
							<a href="#subir" id="sig_1" class="btn btn-primary">Siguiente <span class="icon-arrow-right-2" style="font-size:9px;"></span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane @if($tab=='personales'){{'active'}}@endif" id="personales2"><br>
				<div class="panel panel-primary">	
					<div class="panel-heading"><h3 class="panel-title">Solicitud de Inscripci&oacute;n Nuevo ingreso (Regulares o Equivalencia)</h3></div>
					<div class="panel-body">
						@if(Session::has('message_error_personales'))
							<div>
								<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
									<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_personales') }}</strong></div>
								</div>
							</div>
						@endif
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('n&uacute;mero de sobre')){{'has-error'}} @endif">
							<label for="nom_ape">Convenio <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::select('nsobre', $nsobre, NULL , $attributes = array('class' => 'form-control', 'id'=>'nsobre')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('n&uacute;mero de sobre'))	{{ Session::get('message_personales')->first('n&uacute;mero de sobre') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo nombre')){{'has-error'}} @endif">
							<label for="nom_ape">Primer y Segundo Nombre <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							{{ Form::text('nombres',  NULL , $attributes = array('class' => 'form-control', 'id'=>'nombres')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo nombre'))	{{ Session::get('message_personales')->first('primer y segundo nombre') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo apellido')){{'has-error'}} @endif">
							<label for="nom_ape">Primer y Segundo Apellido <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::text('apellidos',  NULL , $attributes = array('class' => 'form-control', 'id'=>'apellidos')) }}
							<div class="text-danger" id="text_uc">					
								@if(Session::has('message_personales') && Session::get('message_personales')->has('primer y segundo apellido'))	{{ Session::get('message_personales')->first('primer y segundo apellido') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('sexo')){{'has-error'}} @endif">
							<label for="nom_ape">Sexo <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::select('tsexo', $tsexo, NULL , $attributes = array('class' => 'form-control', 'id'=>'tsexo')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('sexo'))	{{ Session::get('message_personales')->first('sexo') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('estado civil')){{'has-error'}} @endif">
							<label for="nom_ape">Estado Civil <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::select('ecivil', $ecivil, NULL , $attributes = array('class' => 'form-control', 'id'=>'ecivil')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('estado civil'))	{{ Session::get('message_personales')->first('estado civil') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono')){{'has-error'}} @endif">
							<label for="nom_ape">Tel&eacute;fono <span class="glyphicon glyphicon-asterisk text-danger"></span></label> 
								{{ Form::text('telefono', NULL, $attributes = array('class' => 'form-control', 'id'=>'telefono','placeholder'=>'(____)________', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono'))	{{ Session::get('message_personales')->first('tel&eacute;fono') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('correo electr&oacute;nico')){{'has-error'}} @endif">
							<label for="">Correo electr&oacute;nico <span class="glyphicon glyphicon-asterisk text-danger"></span></label> 
							{{ Form::email('email', NULL, $attributes = array('class' => 'form-control', 'id'=>'email','placeholder'=>'uba@example.com')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('correo electr&oacute;nico'))	{{ Session::get('message_personales')->first('correo electr&oacute;nico') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('twitter')){{'has-error'}} @endif">
							<label for="">Twitter </label> 
							<div class="input-group date" >
								<span class="input-group-addon">@</span>{{ Form::text('twitter', NULL, $attributes = array('class' => 'form-control', 'id'=>'twitter','placeholder'=>'EjemploUba')) }}
							</div>
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('twitter'))	{{ Session::get('message_personales')->first('twitter') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('facebook')){{'has-error'}} @endif">
							<label for="">Facebook </label>
							<div class="input-group date" >
							<span class="input-group-addon">www.facebook.com/</span>{{ Form::text('facebook', NULL, $attributes = array('class' => 'form-control', 'id'=>'facebook','placeholder'=>'ejemplo.uba30')) }}
							</div>
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('facebook'))	{{ Session::get('message_personales')->first('facebook') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if(Session::has('message_personales') && Session::get('message_personales')->has('pais de nacimiento')){{'has-error'}} @endif">
							<label for="nom_ape">Pais de Nacimiento <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::select('pais_nac', $pais_nac, NULL , $attributes = array('class' => 'form-control', 'id'=>'pais_nac')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('pais de nacimiento'))	{{ Session::get('message_personales')->first('pais de nacimiento') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('estados de nacimiento')){{'has-error'}} @endif">
							<label for="estados">Estado de Nacimiento <span class="glyphicon glyphicon-asterisk text-danger" ></span></label>
							{{ Form::text('estados_nac', NULL, $attributes = array('class' => 'form-control', 'id'=>'estados_nac')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('Estado de Nacimiento'))	{{ Session::get('message_personales')->first('cargo en la empresa') }}	@else {{ "&nbsp;" }} @endif
							</div>  
						</div>

						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_personales') && Session::get('message_personales')->has('ciudad de nacimiento')){{'has-error'}} @endif">
							<label for="ciudad">Ciudad de Nacimiento <span class="glyphicon glyphicon-asterisk text-danger" ></span></label>
							{{ Form::text('ciudad_nac', NULL, $attributes = array('class' => 'form-control', 'id'=>'ciudad_nac')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('Ciudad de Nacimiento'))	{{ Session::get('message_personales')->first('cargo en la empresa') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n de residencia')){{'has-error'}} @endif" >
							<label for="nom_ape" id="">Direcci&oacute;n de residencia mientras estudie <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('direcc_r', NULL, $attributes = array('class' => 'form-control', 'id'=>'direcc_r')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n de residencia'))	{{ Session::get('message_personales')->first('direcci&oacute;n de residencia') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono de residencia')){{'has-error'}} @endif">
							<label for="nom_ape">Tel&eacute;fono de Residencia <span class="glyphicon glyphicon-asterisk text-danger"></span></label> 
								{{ Form::text('telefono_r', NULL, $attributes = array('class' => 'form-control', 'id'=>'telefono_r','placeholder'=>'(____)________', 'onkeypress'=>'return soloNumeros(event)')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('tel&eacute;fono de residencia'))	{{ Session::get('message_personales')->first('tel&eacute;fono de residencia') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n permanente')){{'has-error'}} @endif">
							<label for="nom_ape" id="">Direcci&oacute;n permanente (trabajo o habitaci&oacute;n) <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('direcc_perm', NULL, $attributes = array('class' => 'form-control', 'id'=>'direcc_t_h')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('direcci&oacute;n permanente'))	{{ Session::get('message_personales')->first('direcci&oacute;n permanente') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_personales') && Session::get('message_personales')->has('estados de ubicaci&oacute;n')){{'has-error'}} @endif">
							<label for="estados">Estado de Ubicaci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::text('estados_ubi',  NULL , $attributes = array('class' => 'form-control', 'id'=>'estados_ubi')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('estados de ubicaci&oacute;n'))	{{ Session::get('message_personales')->first('estados de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_personales') && Session::get('message_personales')->has('ciudad de ubicaci&oacute;n')){{'has-error'}} @endif">
							<label for="ciudad">Ciudad de Ubicaci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::text('ciudad_ubi', NULL, $attributes =array('class' => 'form-control', 'id'=>'ciudad_ubi')) }}
							<div class="text-danger">
								@if(Session::has('message_personales') && Session::get('message_personales')->has('ciudad de ubicaci&oacute;n'))	{{ Session::get('message_personales')->first('ciudad de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 izquierda">
								<a href="#subir" id="ant_1" class="btn btn-primary"><span class="icon-arrow-left-2" style="font-size:9px;"></span> Anterior</a>
							</div>
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 derecha">
								<a href="#subir" id="sig_2" class="btn btn-primary">Siguiente <span class="icon-arrow-right-2" style="font-size:9px;"></span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane @if($tab=='datossecundaria'){{'active'}}@endif" id="datossecundaria2"><br>
				<div class="panel panel-primary">
					<div class="panel-heading"><h3 class="panel-title">Datos de Educaci&oacute;n Secundaria</h3></div>
					<div class="panel-body">
						@if(Session::has('message_error_datossecundaria'))
							<div>
								<div class="col-lg-8 col-lg-offset-2 col-md-12 col-sm-12 col-xs-12">
									<div class="alert alert-danger error-msg centrado"><strong>{{ Session::get('message_error_datossecundaria') }}</strong></div>
								</div>
							</div>
						@endif
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>¿D&oacute;nde curs&oacute; el &uacute;ltimo a&ntilde;o de Educaci&oacute;n Secundaria?</strong></legend>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('especialidad')){{'has-error'}} @endif">
							<label for="ciudad">Especialidad <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('g_especialidad', $especialidad ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'g_especialidad')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('especialidad'))	{{ Session::get('message_datossecundaria')->first('especialidad') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('exp especialidad')){{'has-error'}} @endif">
							<label for="nom_ape" id="">Explique <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('ex_especialidad', NULL, $attributes = array('class' => 'form-control', 'id'=>'ex_especialidad')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('exp especialidad'))	{{ Session::get('message_datossecundaria')->first('exp especialidad') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('fecha de graduaci&oacute;n')){{'has-error'}} @endif">
							<label for="nom_ape">Fecha de graduaci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							<div class="input-group date" id="notiene">
							   {{ Form::text('fecha_g', NULL, $attributes = array('class' => 'form-control', 'id'=>'fecha_g', 'placeholder'=>'Seleccione fecha de graduaci&oacute;n', 'readonly'=>'readonly', 'required'=>'required')) }}<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>					           
				            </div>
				            <div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('fecha de graduaci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('fecha de graduaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						
						
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('estados de ubicaci&oacute;n')){{'has-error'}} @endif">
							<label for="estados">Estado de Ubicaci&oacute;n Educaci&oacute;n Secundaria <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
							{{ Form::text('estados_estu_secu', NULL , $attributes = array('class' => 'form-control', 'id'=>'estados_estu_secu')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('estados de ubicaci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('estados de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('ciudad de ubicaci&oacute;n')){{'has-error'}} @endif">
							<label for="ciudad">Ciudad de Ubicaci&oacute;n Educaci&oacute;n Secundaria <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::text('ciudad_estu_secu', NULL, $attributes = array('class' => 'form-control', 'id'=>'ciudad_estu_secu')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('ciudad de ubicaci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('ciudad de ubicaci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('n rusnies')){{'has-error'}} @endif">
							<label for="nom_ape" >N. de R.U.S.N.I.E.S <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('rusnies', NULL, $attributes = array('class' => 'form-control', 'id'=>'rusnies')) }}
							<div class="text-danger" id="text_uc">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('n rusnies'))	{{ Session::get('message_datossecundaria')->first('n rusnies') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Datos de otra Instituci&oacute;n</strong></legend>
						</div>
						<div class="form-group col-lg-8 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('cursa otra instituci&oacute;n')){{'has-error'}} @endif">
							<label for="ciudad">¿Realiza o ha realizado estudios en otra Instituci&oacute;n de Educaci&oacute;n Universitaria? <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('otros_estudio', $datos_carrera ,'NULL', $turno =array('class' => 'form-control', 'id'=>'otros_estudio')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('cursa otra instituci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('cursa otra instituci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('nombre de la instituci&oacute;n')){{'has-error'}} @endif">
							<label for="nom_ape" id="">Nombre de la Instituci&oacute;n <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('nom_institucion', NULL, $attributes = array('class' => 'form-control', 'id'=>'nom_institucion' )) }}
							<div class="text-danger" id="">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('nombre de la instituci&oacute;n'))	{{ Session::get('message_datossecundaria')->first('nombre de la instituci&oacute;n') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>	
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('otra carreras')){{'has-error'}} @endif">
							<label for="ciudad">Carrera que curso <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('otra_carreras', $carreras ,'NULL', $turno =array('class' => 'form-control', 'id'=>'otra_carreras')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('otra carreras'))	{{ Session::get('message_datossecundaria')->first('otra carreras') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>

						<div class="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12  @if(Session::has('message_periodo') && Session::get('message_periodo')->has('fecha de ingreso')){{'has-error'}} @endif">
							<label for="nom_ape">Fecha de Ingreso <span class="glyphicon glyphicon-asterisk text-danger" style="font-size:11px;"></span></label> 
							<div class="input-group date" id="fecha_n">
				             	{{ Form::text('fecha_ing', NULL, $attributes = array('class' => 'form-control', 'id'=>'fecha_ing', 'placeholder'=>'Seleccione fecha de ingreso', 'readonly'=>'readonly', 'required'=>'required')) }}<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
				            </div>
				            <div class="text-danger" id="text_uc">
				            	@if(Session::has('message_periodo') && Session::get('message_periodo')->has('fecha de ingreso'))	{{ Session::get('message_periodo')->first('fecha de ingreso') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>

						<div class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12  @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('peridoo de la instituci&oacute;n')){{'has-error'}} @endif">
							<label for="nom_ape" id="">Periodo de Ubicacion en la carrera <span class="glyphicon glyphicon-asterisk text-danger" ></span></label> 
								{{ Form::text('nivel_estudio', NULL, $attributes = array('class' => 'form-control', 'id'=>'nivel_estudio', 'placeholder'=>'Primer Trimestre/Semestre' )) }}
							<div class="text-danger" id="">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('periodo de ubicacion'))	{{ Session::get('message_datossecundaria')->first('periodo de ubicacion') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Datos de la Carrera a Estudiar</strong></legend>
						</div>
						<div class="form-group col-lg-4 col-md-6 col-sm-12 col-xs-12 @if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('carrera que desea estudiar')){{'has-error'}} @endif">
							<label for="ciudad">Carrera que desea estudiar <span class="glyphicon glyphicon-asterisk text-danger"></span></label>
							{{ Form::select('carrera_uba', $carreras_uba ,'NULL', $attributes =array('class' => 'form-control', 'id'=>'carrera_uba')) }}
							<div class="text-danger">
								@if(Session::has('message_datossecundaria') && Session::get('message_datossecundaria')->has('carrera que desea estudiar'))	{{ Session::get('message_datossecundaria')->first('carrera que desea estudiar') }}	@else {{ "&nbsp;" }} @endif
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend><strong>Documentos (Requisitos)</strong></legend>
						</div>
						<div style="text-align: left;" class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('doc1', '1', false,array('class' => 'form-group', 'id'=>'doc1')); }}
							<label for="ciudad">(DI) Documento de Identidad ampliado</label>
						</div>
						<div style="text-align: left;" class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('doc2', '1', false,array('class' => 'form-group', 'id'=>'doc2')); }}
							<label for="ciudad">(FT) Fotografia en .jpg o png</label>
						</div>
						<div style="text-align: left;" class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('doc3', '1', false,array('class' => 'form-group', 'id'=>'doc3')); }}
							<label for="ciudad">(TB) Titulo de Bachiller</label>
						</div>
						<br>
						<div style="text-align: left;" class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('doc4', '1', false,array('class' => 'form-group', 'id'=>'doc4')); }}
							<label for="ciudad">(RU) Constancia RUSNIEU</label>
						</div>

						<div style="text-align: left;" class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('doc5', '1', false,array('class' => 'form-group', 'id'=>'doc5')); }}
							<label for="ciudad">(NC) Notas certificadas de 1ero a 5to año</label>
						</div>
						<div style="text-align: left;" class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('doc6', '1', false,array('class' => 'form-group', 'id'=>'doc6')); }}
							<label for="ciudad">(BC) Constancia de Buena Conducta</label>
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: center;">			
							<legend>Si ha realizado estudios en otra institucion y aplica por Equivalencia:</legend>
						</div>
						
						<div style="text-align: left;" class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('doc7', '1', false,array('class' => 'form-group', 'id'=>'doc7')); }}
							<label for="ciudad">NCE- Notas Certificadas de Estudios realizados</label>
						</div>
						<div style="text-align: left;" class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('doc8', '1', false,array('class' => 'form-group', 'id'=>'doc8')); }}
							<label for="ciudad">PER- Pensum de la Carrera que curso</label>
						</div>
						<div style="text-align: left;" class="form-group col-lg-4 col-md-6 col-sm-6 col-xs-12 @if($errors->has('tipo evaluaci&oacute;n')){{'has-error'}} @endif">
							{{ Form::checkbox('doc9', '1', false,array('class' => 'form-group', 'id'=>'doc9')); }}
							<label for="ciudad">PRG- Programas de las materias cursadas</label>
						</div>
						<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<p><strong>Los documentos deben enviarse en forma digital al correo: admision.internacional@uba.edu.ve</strong></p>
						</div>
		
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 izquierda">
								<a href="#subir" id="ant_2" class="btn btn-primary"><span class="icon-arrow-left-2" style="font-size:9px;"></span> Anterior</a>
							</div>
						<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 derecha">
								{{ Form::submit('Procesar Pre-Inscripci&oacute;n', array('class' => 'btn btn-success', 'onclick' => 'this.disabled=true; this.value="Enviando"; this.form.submit()', 'style' => 'text-transform: initial;')) }}
						</div>
						</div>
					</div>
				</div>
			</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@stop
@section('postscript')
<script type="text/javascript" language="javascript">  
    $(document).ready(function() {

    	$('#fecha,#fecha_g,#fecha_ing').datepicker({
	        language: "es",
	        autoclose: true,
	        dateFormat: "dd-mm-yy",
	        changeMonth:true,
	        changeYear:true,
	        yearRange: "c-90:c+0",
	        monthNames: ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"], // Names of months for drop-down and formatting
		    monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"], // For formatting
		    dayNames: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"], // For formatting
		    dayNamesShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"], // For formatting
		    dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sa"], // Column headings for days starting at Sunday	          
	    });


	    var disabledDates = new Array({{$fechas_no}});

    	function DisableSpecificDates(date) {
		    var string = jQuery.datepicker.formatDate('dd-mm-yy', date);
		    return [disabledDates.indexOf(string) == -1];
		    }
		function PrepareDate(date){
		    if ($.datepicker.noWeekends(date)[0]) {
		    return DisableSpecificDates(date);
		    } else {
		    return $.datepicker.noWeekends(date);
		    }
		}

		
		$('#telefono').mask('9999-9999999', {placeholder: "____-________"});
		$('#telefono_r').mask('9999-9999999', {placeholder: "____-________"});
		
		$('#cedula').mask('99999999999');
		

		$('#nsobre').blur(function(){
			var nsobre = $('#nsobre').val();
			var num = nsobre.split("-");
			var cod_esc = num[1];

			if (cod_esc == '101') {
				
				$('#carrera_uba').html('<option value="1">INGENIERIA DE SISTEMAS</option>');
			
			}else if (cod_esc == '102'){
				
				$('#carrera_uba').html('<option value="2">INGENIERIA ELECTRICA</option>');;

			} else if (cod_esc == '103'){

				$('#carrera_uba').html('<option value="3">ADMINISTRACION DE EMPRESAS</option>');
			
			} else if (cod_esc == '104'){
				
				$('#carrera_uba').html('<option value="4">CONTADURIA PUBLICA</option>');

			} else if (cod_esc == '105'){
				
				$('#carrera_uba').html('<option value="5">DERECHO</option>');
			
			} else if (cod_esc == '106'){
				
				$('#carrera_uba').html('<option value="6">COMUNICACION SOCIAL</option>');
			
			} else if (cod_esc == '107'){
				
				$('#carrera_uba').html('<option value="7">PSICOLOGIA</option>');
			
			}  else {

				$('#carrera_uba').html('<option value="">SELECCIONAR</option><option value="1">INGENIERIA DE SISTEMAS</option><option value="2">INGENIERIA ELECTRICA</option><option value="3">ADMINISTRACION DE EMPRESAS</option><option value="4">CONTADURIA PUBLICA</option><option value="5">DERECHO</option><option value="6">COMUNICACION SOCIAL</option><option value="7">PSICOLOGIA</option>');
			}
			
			var sel_carrera = $('#carrera_uba').val();

		});


		var fecha_nac = $("#fecha").val();
		if (fecha_nac=='') {
			

		}else{
			var div = fecha_nac.split("-");
			var fecha_nacimiento = div[2]+'/'+div[1]+'/'+div[0];
			var dia = div[0];
			var mes = div[1];
			var ano = div[2];
			fecha_hoy = new Date();
			ahora_ano = fecha_hoy.getYear();
			ahora_mes = fecha_hoy.getMonth();
			ahora_dia = fecha_hoy.getDate();
			edad = (ahora_ano + 1900) - ano;

		    if ( ahora_mes < (mes - 1)){ 
		      edad--;
		    }
		    if (((mes - 1) == ahora_mes) && (ahora_dia < dia)){ 
		      edad--;
		    }
		    if (edad > 1900){
		        edad -= 1900;
		    }
			$("#edad").val(edad);
			
		}			

		
		var selectp = document.getElementById('g_especialidad').value;	    	
	    	if (selectp==3){
	    		document.getElementById('ex_especialidad').disabled = false;
			}else{
				document.getElementById('ex_especialidad').disabled = true;
			}

		$('#g_especialidad').change(function(){ 
	    	var selectp = document.getElementById('g_especialidad').value;
	    	if (selectp==3){
	    		document.getElementById('ex_especialidad').disabled = false;
			}else{
				document.getElementById('ex_especialidad').disabled = true;
			}
		});

		var otros_estudio = document.getElementById('otros_estudio').value;
    	if (otros_estudio==1){
			document.getElementById('nom_institucion').disabled = false;
			document.getElementById('otra_carreras').disabled = false;
			document.getElementById('fecha_ing').disabled = false;
			document.getElementById('nivel_estudio').disabled = false;
		}else{
			document.getElementById('otra_carreras').disabled = true;
			document.getElementById('nom_institucion').disabled = true;
			document.getElementById('fecha_ing').disabled = true;
			document.getElementById('nivel_estudio').disabled = true;
		}

		$('#otros_estudio').change(function(){ 
	    	var selectp = $(this).val();
	    	if (selectp==1){
	    		$('#nom_institucion').prop('disabled',false);
	    		$('#otra_carreras').prop('disabled',false);
	    		$('#fecha_ing').prop('disabled',false);
	    		$('#nivel_estudio').prop('disabled',false);
			}else{
				$('#nom_institucion').prop('disabled',true);
	    		$('#otra_carreras').prop('disabled',true);
	    		$('#fecha_ing').prop('disabled',true);
	    		$('#nivel_estudio').prop('disabled',true);
			}
		});

		$('#sig_1').click(function(){	 
			var sig_a = document.getElementById("personales1");
			var sig_b = document.getElementById("personales2");
			$('#periodo1').removeClass('active');
			$('#periodo2').removeClass('active');
			$('#personales1').addClass('active');
			$('#personales2').addClass('active');
		});


		$('#ant_1').click(function(){
			var sig_a = document.getElementById("periodo1");
			var sig_b = document.getElementById("periodo2");
			$('#personales1').removeClass('active');
			$('#personales2').removeClass('active');
			$('#periodo1').addClass('active');
			$('#periodo2').addClass('active');
		});

		$('#sig_2').click(function(){
			var sig_a = document.getElementById("datossecundaria1");
			var sig_b = document.getElementById("datossecundaria2");
			$('#personales1').removeClass('active');
			$('#personales2').removeClass('active');
			$('#datossecundaria1').addClass('active');
			$('#datossecundaria2').addClass('active');
		});

		$('#ant_2').click(function(){
			var sig_a = document.getElementById("personales1");
			var sig_b = document.getElementById("personales2");
			$('#datossecundaria1').removeClass('active');
			$('#datossecundaria2').removeClass('active');
			$('#personales1').addClass('active');
			$('#personales2').addClass('active');
		});


		$('#fecha').change(function(){
			var fecha_nac = $("#fecha").val();
			var div = fecha_nac.split("-")
			var fecha_nacimiento = div[2]+'/'+div[1]+'/'+div[0];
			var dia = div[0];
			var mes = div[1];
			var ano = div[2];
			fecha_hoy = new Date();
			ahora_ano = fecha_hoy.getYear();
			ahora_mes = fecha_hoy.getMonth();
			ahora_dia = fecha_hoy.getDate();
			edad = (ahora_ano + 1900) - ano;			    
		    if ( ahora_mes < (mes - 1)){ 
		      edad--;
		    }
		    if (((mes - 1) == ahora_mes) && (ahora_dia < dia)){ 
		      edad--;
		    }
		    if (edad > 1900){
		        edad -= 1900;
		    }
			$("#edad").val(edad);
			
		});

		var nivel_estudio = $('#nivel_estudio').val();

		if (nivel_estudio != '') {
			document.getElementById('nivel_estudio').disabled= false;
		}

		$('#se_graduo').change(function(){
			var opcion = $('#se_graduo').val();
			if (opcion == 1) {
				document.getElementById('nivel_estudio').disabled= false;
				$('#nivel_estudio').val('');
			}else if (opcion == 0){
				document.getElementById('nivel_estudio').disabled= true;
				$('#nivel_estudio').val('');
			} else {
				document.getElementById('nivel_estudio').disabled= true;
				$('#nivel_estudio').val('');
			}
		});
    });
 </script>
{{ HTML::script('js/jquery_ui.js') }}
{{ HTML::script('js/mask-plugins/src/jquery.mask.js') }}
{{ HTML::script('js/sololetras.js') }}
{{ HTML::script('js/solonumeros.js') }}
@stop