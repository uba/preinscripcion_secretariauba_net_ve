<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<style>
			.contenedor{
				background-color: #f5f5f5;
				padding-top: 50px;
				padding-bottom: 50px;
			}
			span{
    		font-size: 0.9em;
			}
			h2{
				color: #888;
			}
			a,a:hover,a:visited,a:focus{
				text-decoration: none;
			}
			small{
				color: #aaa;
			}
			p{
				margin-bottom: 50px;
				font-size: 16px;
				color: #696767;
			}
			.container{
				margin: auto;
		    width: 50%;
		    padding: 40px 30px;
  			background: #fff;
  			box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.1);
  			margin-bottom: 20px;
  			text-align: center;
			}
			.btn{
				color: #fff;
  			line-height: 1.25;
  			border: 1px solid transparent;
  			padding: 0.5rem 1rem;
  			font-size: 1rem;
  			border-radius: 0.25rem;
  			transition: all 0.2s ease-in-out;
			}
			.btn-info{
				background-color: #0275d8;
		    border-color: #0275d8;
			}
			.text-primary{
				color: #2b90d9 !important;
			}
			.text-uppercase {
  			text-transform: uppercase!important;
			}
			.logo{
 				font-size: 2em;
  			font-weight: 650;
			}
			.logo span{
  			color: #ccc;
			}
			.w-100{
				width: 100%;
			}
			.h-100{
				height: 100%;
			}
		</style>
	</head>
	<body>
		<div class="w-100 h-100 contenedor">
			<div class="container">
			<div class="logo text-uppercase mb-2">
      	<span>Preinscripcion</span><strong class="text-primary">UBA</strong>
      		</div>
      			<br />
      			<h2>Clave de Acceso</h2>
      			<br />
      			<p>
    		Su Clave de Acceso en <a href="http://preinscripcion.secretariauba.net.ve">http://preinscripcion.secretariauba.net.ve</a> es: .
      			</p>
			</div>
		</div>
	</body>
</html>