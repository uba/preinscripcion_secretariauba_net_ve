<style type="text/css">
<!--
body{
	width: 100%;
	height: 100%;
	font-size: 12pt;
	font-family: times-roman;
}
span{
	display: inline-block;
}
.header,
.footer {
	position: fixed;
	left: 0;
	right: 0;
	color: black;
}
.header {
	top: 0px;
}
.footer {
	bottom: 10px;
}
hr {
	page-break-before: always;
	border: 0;
}
div.encabezado{
	text-align:center;
	font-style: italic;
	font-size: 11pt;
}
.centrado{
	text-align: center;
}
.fecha{
	text-align: right;
	margin-right: 1cm;
}
.titulo{
	padding-top: 110px;
	font-size: 16pt;
}
.texto_footer{
	font-style: italic;
	font-size: 9pt;
	line-height: 1px;
}
.linea_abajo{
	border-bottom: 2px solid black;
}
a{
	color: blue;
}
.negrita{
	font-weight: bold;
}
.firma{
	text-decoration: overline;
}
.datos{
	font-size: 12pt;
	line-height: 18pt;
}
.validacion{
	font-size: 9pt;
}
.contenido{
	margin-top: -10px;
	line-height: 15pt;
	text-align: justify;
    text-indent: 60px;
    margin-left: 1cm;
    margin-right: 1cm;
    font-size: 12pt;
}
.institucion{
    margin-left: 1cm;
    font-size: 13pt;
    margin-bottom: 0px;
}
.dirigido{
	margin-top: 0px;
	text-align: right;
	margin-right: 1cm;
    font-size: 13pt;
    margin-bottom: 5px;
}
.lista{
	font-size: 12pt;
	margin-left: 1cm;
}
.atentamente{
	margin-top: 40px;
}

-->
</style>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="{{ URL::to('images/favicon.ico')}}">
</head>
<body>
	<div class="header">
		<table width="100%;">
			<tr>
				<td style="width: 15%; padding-left:5%;">
					<img src="{{URL::to('images/logo_uba.png')}}" alt='UBA' height="120px" />
				</td>
				<td style="width: 60%;">
					<div class="encabezado">REP&Uacute;BLICA BOLIVARIANA DE VENEZUELA</div>
					<div class="encabezado">UNIVERSIDAD BICENTENARIA DE ARAGUA</div>
					<div class="encabezado">VICERRECTORADO ACAD&Eacute;MICO</div>
					<div class="encabezado">{{$facultad}}</div>
					<div class="encabezado">ESCUELA DE {{ nombreEscuelalargo($datos_estudiante->COD_ESC)}}</div>
					<div class="encabezado">COORDINACION DE PASANT&Iacute;AS</div>
					<div class="encabezado">{{ mayuscula($ubi_nucleo)}}</div>
				</td>
				<td style="width: 15%; padding-left:10%;">
					<img src="{{URL::to('images/logos/'.$logo.'.jpg')}}" width="100px" />
				</td>
			</tr>
		</table>
	</div>
	<h3 class="titulo centrado">IDENTIFICACI&Oacute;N DEL PASANTE</h3>
	<div class="datos" style="width:100%;">
      <p class="negrita">I. Datos Personales</p>
      <div>APELLIDOS: <b>{{ $apellidos }}</b></div>
      <div>NOMBRES: <b>{{ $nombres }}</b></div>
      <div>CEDULA DE IDENTIDAD: <b>{{ $datos_estudiante->CEDULA }}</b> </div>
      <div>DIRECCION DE HABITACION: <b>{{ mayuscula($datos_estudiante->DIR_HAB) }}</b> </div>
      <div>TEL&Eacute;FONO: <b>{{ mayuscula($datos_estudiante->TEL_MOV) }}</b></div>
      <div>CIUDAD Y FECHA NACIMIENTO: <b>{{ mayuscula($datos_estudiante->CIU) }} - </b> <b>{{ date_format(date_create($datos_estudiante->FEC_NAC),'d-m-Y') }}</b> </div>
      <div>CORREO ELETR&Oacute;NICO: <b>{{ mayuscula($datos_estudiante->EMAIL) }}</b></div>
    </div>
    <div class="datos">
      <p><strong>II. Datos Acad&eacute;micos</strong></p>
      <div>ESCUELA: <b>{{ mayuscula(nombreEscuelalargo($datos_estudiante->COD_ESC)) }}</b></div>
      <div>SEMESTRE QUE CURSA: <b>{{ $ubi_semestre->ubi_sem}}</b></div>
      <div>LAPSO DE SOLICITUD: <b>{{ $ubi_semestre->lapso}}</b> </div>
    </div>
    <div class="datos">
      <p><strong>III. Datos de la pasant&iacute;a</strong></p>
      <div>INSTITUCI&Oacute;N:<span style='display:inline; white-space:pre;'> ____________________________________________________________________________</span></div>
      <div>GERENCIA / DEPARTAMENTO / UNIDAD: <span style='display:inline; white-space:pre;'> ___________________________________________________</span></div>
      <div>DIRECCI&Oacute;N: <span style='display:inline; white-space:pre;'>  _____________________________________________________________________________</span></div>
      <div>TEL&Eacute;FONOS / FAX: <span style='display:inline; white-space:pre;'>  _______________________________________________________________________</span> </div>
      <div>FECHA DE INICIO: <span style='display:inline; white-space:pre;'>   ______________________ </span>FECHA DE CULMINACI&Oacute;N: _______________________ </div>
      <div>TUTOR INDUSTRIAL: <span style='display:inline; white-space:pre;'>   ____________________________________________________________________</span></div>
      <div>TELEFONO DEL TUTOR: <span style='display:inline; white-space:pre;'>   _________________________________________________________________</span></div>
      <div>CORREO ELECTR&Oacute;NICO: <span style='display:inline; white-space:pre;'>   ________________________________________________________________</span></div>
      <div>OBSERVACIONES: <span style='display:inline; white-space:pre;'>   ______________________________________________________________________</span></div>
    </div>
    <div class="validacion"><strong>SE HACE CONSTAR QUE LOS DATOS PRESENTADOS SON CIERTOS EN SU TOTALIDAD EN CASO DE QUE EXISTA ALGUNA MODIFICACI&Oacute;N EN LOS MISMOS, DEBE SER NOTIFICADO INMEDIATAMENTE A LA COORDINACION DE PASANT&Iacute;AS </strong></div>
    <br>
    <div class="firma"> FIRMA DEL ALUMNO </div>
    <div class="footer">
		<p class="texto_footer linea_abajo">&quot;Una Universidad para la Creatividad&quot;</p>
  		<p class="texto_footer centrado">Av. Intercomunal Santiago Mari&ntilde;o c&#47;c Av. Universidad, Sector La Providencia,  San Joaqu&iacute;n de Turmero. Estado Aragua. Venezuela.</p>
  		<p class="texto_footer centrado ">Tel&eacute;fono: M&aacute;ster  (0243) 2650011 &#45; 265.00.52 &#45; 265.00.57 Fax: 265.00.62</p>
  		<p class="texto_footer centrado">web <a>http://www.uba.edu.ve</a>  &#47; e-mail: <a>ubaweb@uba.edu.ve</a></p>
	</div>
	<hr>
	<h3 class="titulo centrado">&nbsp;</h3>

	<p class="fecha">{{ $ubi_nucleo.', '.minuscula(utf8_encode($fecha_cabecera)) }}</p>
	<p class="institucion">Señores <br> <span class="negrita" >{{ $institucion }}</span> <br> Presentes.-</p>
	<p class="dirigido">Dirigido A: <br> <span class="negrita" >{{ $encargado }}</span></p>
	<p class="contenido">
		Me dirijo a Usted, en la ocasi&oacute;n de presentarle @if($datos_estudiante->SEXO == "F") {{"a la"}} @elseif($datos_estudiante->SEXO=="M") {{"al"}} @endif Bachiller <b>{{ minuscula($nombres).' '.minuscula($apellidos) }}</b>,
		titular de la C&eacute;dula de Identidad <b>{{ $datos_estudiante->CEDULA}}</b>, alumno(a) regular de la carrera de <b>{{ minuscula(nombreEscuelalargo($datos_estudiante->COD_ESC))}}</b>
		@if($mencion!= '') en la especialidad de <b>{{ $mencion }}</b>@endif
		de esta casa de estudios, est&aacute; gestionando en tan prestigiosa instituci&oacute;n la realizaci&oacute;n de su pasant&iacute;a,
		a fin de cumplir con los requisitos establecidos por la Universidad Bicentenaria de Aragua, en cuanto a la asignatura <b>{{$materia}}</b>
		@if($datos_estudiante->COD_ESC==7)
        con una duraci&oacute;n m&iacute;nima de 12 semanas  Tiempo Completo, equivalente a 480 horas, ajust&aacute;ndose a la necesidad y al horario institucional, ya que es requisito indispensable para la aprobaci&oacute;n de la asignatura, para optar al t&iacute;tulo de Licenciado en Psicolog&iacute;a.
      	@elseif($datos_estudiante->COD_ESC==6)
        <p class="contenido">Es importante destacar que el alumno, según el Consejo Universitario, debe cumplir con una etapa de pasant&iacute;as la cual tiene una duraci&oacute;n de (480) horas correspondiente a ocho (7) horas diarias de lunes a viernes. Cumpliendo  Doce (12) semanas. En el lapso de tres meses y una semana</p>
        @elseif($datos_estudiante->COD_ESC==5)
        <p class="contenido">Es importante destacar que el alumno, según el Consejo Universitario, debe cumplir con una etapa de pasant&iacute;as la cual tiene una duraci&oacute;n de (120) horas correspondiente</p>
      	@else
        con una duraci&oacute;n m&iacute;nima de 12 semanas.
        @endif
	</p>
	<p class="contenido">Agradecemos toda la colaboraci&oacute;n que pueda prestarnos en la formacion profesional
		@if($datos_estudiante->SEXO == "F") {{ " de la bachiller arriba mencionada"}} @elseif($datos_estudiante->SEXO=="M") {{ " del bachiller arriba mencionado"}} @endif y en caso de aceptación del pasante antes mencionado, se agradece enviar la correspondiente notificación indicando:
	</p>
	<ul class="lista" >
	      <li>Datos del Tutor industrial (C&eacute;dula, Nombres y Apellidos, Tel&eacute;fono , Correo)</li>
	      <li>Carta de Aceptaci&oacute;n.</li>
	      <li>Departamento donde realiza la pasant&iacute;a.</li>
	      <li>Fecha de inicio y culminación de las mismas.</li>
	    </ul>
	<p class="contenido">Por lo antes expuesto, agradezco la atenci&oacute;n que tengan a bien dispensarle a la  presente, la cual permitir&aacute; mantener un estrecho contacto entre ambas organizaciones se despide de usted</p>
	<p class="atentamente centrado">
		Atentamente
	</p>
	<p class="firma centrado" style="margin-bottom:0px">{{ '&nbsp;&nbsp;&nbsp;'.minuscula($firma->nombres).' '.minuscula($firma->apellidos).'&nbsp;&nbsp;&nbsp;' }}</p>
	<p class="centrado negrita" style="margin-top:0;">{{ $cargo }}</p>
	 <div class="footer">
	 	<p style="margin-top:-20px">Esta carta no tiene validez sin sello humedo y firma.</p>
	</div>
</body>
</html>