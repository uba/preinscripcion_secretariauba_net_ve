<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<style type="text/css">
	body{
		width: 100%;
		height: 100%;
		font-size: 14px;
		font-family: sans-serif; 
	}
	hr{
		page-break-after: always;
		border: 0;
	}
	p.grande { line-height: 150%; }
	.tabla{
		width: 100%;
	}
	.datos{
		/*border-collapse: collapse;*/
		width: 40%;
		margin-top: 50px;
		vertical-align: middle;
		font-size: 12px;
		/*border: 1px solid black;*/
	}
	.contenido{
		width: 100%;
		margin-top: 40px;
		vertical-align: middle;
		font-size: 12px;
	}
	.celda{
		border: 1.5px solid black;
	}
	.centro td{
		text-align: center;
	}
	.justificado td{
		text-align: justify;
	}

	.izquierda{
		text-align: left !important;
	}

	.bottom { 
		position: absolute; 
		text-align: center;
		width: 100%;
		bottom: 15px;
	}
	.header,
	.footer {
		position: fixed;
		left: 0;
		right: 0;
	}
	.header {
		top: 0;
	}
	.footer {
		bottom: 0;
		margin-bottom: 200px;
	}
	.page-number {
		text-align: center;
	}
	.page-number:before {
		content: counter(page);
	}
	.separacion{
		margin-top: 160px;
	}
	</style>
</head>
<body>
	<div class="header">
		<table class="tabla">
			<tr>
				<td width="10%" align="center">
					<img src='images/logo_uba.png' alt='UBA' width="70px" height="70px" />
				</td>
				<td width="60%" align="center">
					<strong><span>UNIVERSIDAD BICENTENARIA DE ARAGUA</span></strong><br>
					<strong><span>DIRECCI&Oacute;N DE ADMISI&Oacute;N Y CONTROL DE ESTUDIOS</span></strong><br>
					<strong><span>SECRETAR&Iacute;A</span></strong><br>
					<strong><span>MARACAY - VENEZUELA</span></strong><br>
					<strong><span>PER&Iacute;ODO ACAD&Eacute;MICO {{$lapso_letra}}</span></strong><br><br>
				</td>
			</tr>
		</table>
		<table width="100%">
			<tr>
				
				<td colspan="2" align="right" style="font-size:11px;"> {{$fecha}} Serial: {{$numero}} - {{$serial}}</td>
			</tr>
			<tr>
				<td colspan="2" style="font-size:11px;">
					<h3 style="text-align:center;">HORARIO DE CLASES (Inscripci&oacute;n Web)</h3>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="font-size:12px;">
					<p>
					<strong>C&Eacute;DULA DE IDENTIDAD:</strong> {{$cedula}} &nbsp;&nbsp;&nbsp;&nbsp; <strong>CARRERA: </strong> {{$escuela}}<br/>
					<strong>APELLIDOS Y NOMBRES:</strong> {{$nombre}}<br/> </p>
				</td>
			</tr>
		</table>
		<table width="100%">
			<tr style="font-weight:bold;font-size:11px;">
				<td>C&Oacute;DIGO</td>
				<td>DESCRIPCI&Oacute;N DE LA ASIGNATURA</td>
				<td>SECCI&Oacute;N</td>
				<td>HORARIO</td>
				<td>(EDIFICIO) AULA(S)</td>
			</tr>
				@foreach($materias as $key => $value)
					<tr style="font-size:12px;">
						<td>{{$value->cod_mat}}</td>
						<td>{{$value->des_mat}}</td>
						<td align="center">{{$value->sec_mat}}</td>
						<td> {{$value->DIA1}}-{{$value->DIA2}}</td>
						<td>({{$value->EDI1}}) {{$value->AULA1}} | ({{$value->EDI2}}) {{$value->AULA2}}</td>
					</tr>
				@endforeach
		</table>
			<span style="font-size:11px;">Tus Materias en proceso de Cambio en el periodo: {{$lapso}}, son las siguientes:/span><br/></span>
			<table class="table table-bordered">
			  <thead>
			    <tr class="info">
			    	<th class="text-center">C&oacute;digo</th>
			    	<th class="text-center">Descripci&oacute;n</th>
			    	<th class="text-center">Secci&oacute;n solicitada</th>
			    	<th class="text-center">U.C.</th>
			    	<th class="text-center">Semestre</th>
			    	<th class="text-center">Horario</th>
			    	<th class="text-center">Lapso</th>
			    	<th class="text-center">Respuesta</th>
			    </tr>
			  </thead>
			  @foreach($HorarioListoResp as $key => $value)
			  <tr>
			  	<td class="text-center">{{$value->cod_mat}}</td>
			  	<td class="text-center">{{$value->des_mat}}</td>
			  	<td class="text-center">{{$value->sec_mat}}</td>
			  	<td class="text-center">{{$value->uni_cre}}</td>
			  	<td class="text-center">{{$value->sem_mat}}</td>
			  	@if(($value->DIA1!="") and ($value->DIA2==""))
			  		<td class="text-center">{{$value->DIA1}}</td>
			  	@endif
			  	@if(($value->DIA1!="") and ($value->DIA2!=""))
			  		<td class="text-center">{{$value->DIA1." <br> ".$value->DIA2}}</td>
			  	@endif
			  		<td class="text-center">{{$lapso}}</td>
			  		@if ($value->aprobado == 0)
			  			<td class="text-center text-danger"><strong>{{$value->aprobadoC}}</strong></td>
			  		@elseif($value->aprobado == 1)
			  			<td class="text-center text-success"><strong>{{$value->aprobadoC}}</strong></td>
			  		@elseif($value->aprobado == 2)
			  			<td class="text-center text-danger"><strong>{{$value->aprobadoC}}</strong></td>
			  		@endif
			  </tr>
			  @endforeach
			</table>
			<table class="table table-bordered">
			  <thead>
			    <tr class="info">
			    	<th class="text-center">C&oacute;digo</th>
			    	<th class="text-center">Descripci&oacute;n</th>
			    	<th class="text-center">Costo Unitario Bs.</th>
			    </tr>
			  </thead>
			  @foreach($Costos as $key => $value)
			  <tr>
			  	<td class="text-center">{{$value->cod_mat}}</td>
			  	<td class="text-center">{{$value->des_mat}}</td>
			  	<td class="text-center">{{$value->monto}}</td>
			  </tr>
			  <tr>
			  	<td class="text-right" colspan="2"><strong>Monto Total a Pagar:</strong></td>
			  	<td class="text-center text-danger"><strong>{{$total}}</strong></td>
			  </tr>
			  @endforeach
			</table>
			<br>
			<center>
			<table border="0" width="100%" >
				<tr>
					<td align="center">________________________</td>
					<td></td>
					<td align="center"><img src='images/firma_autorizada.jpg'></td>
				</tr>
				<tr>
					<td align="center">Firma del Estudiante</td>
					<td></td>
					<td align="center">Firma del Funcionario</td>
				</tr>
			</table>
		</center><br>
		<div style="text-align: right;">Fecha de Impresi&oacute;n: {{$fecha}}</div>

	</div>
</body>
</html>