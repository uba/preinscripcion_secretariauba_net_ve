<style type="text/css">
body{
	margin-top: -25px;
	width: 100%;
	height: 100%;
	font-size: 12pt;
	font-family: times-roman; 
}
span{
	font-weight: bold;
}
.header,
.footer {
	position: fixed;
	left: 0;
	right: 0;
	color: black;
}
.header {
	top: 0px;
}	
.footer {
	bottom: 10px;
}
.encabezado{
	text-align:center;
	font-size: 10pt;
	font-weight: bold
}
.codseg{
	text-align:right;
	font-size: 10pt;
	font-weight: bold

}
.condiciones, .datos{
	font-size: 9.5pt;
	text-align: justify;
}

.condiciones > p{
	margin-top: 0;
	text-align: justify;
}
.cuadro{
	font-size: 10pt;
	border: 1px solid black;
	text-align: justify;
	margin:1px;
	padding:1px;
}
.centrado{
	text-align: center;
}
.derecho{
	text-align: right;
}
.texto_footer{
	font-style: italic;
	font-size: 9pt;
	line-height: 1px;
}
.linea_abajo{
	border-bottom: 2px solid black;
}
a{
	color: blue;
}
.negrita{
	font-weight: bold;
}
.firma{
	text-decoration: overline;
}

.validacion{
	font-size: 9pt;
}


.lista{
	font-size: 12pt;
	margin-left: 1cm;
}
.atentamente{
	margin-top: 40px;
}
</style>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="{{ URL::to('images/favicon.ico')}}">
</head>
<body>
	<table width="100%" class="cuadro">
		<tr>
			<td style="width: 5%;">
				<img src='images/logo_uba.png' alt='UBA' height="75px" />
			</td>
			<td width="38%" >
				<p class="encabezado">INSTRUCCIONES GENERALES PARA <br>
					PROCESAR LA SOLICITUD DE INSCRIPCIÓN <br>
					{{ $lapso_letra}} | {{ $datos_solicitud->id }}<br>
					N&uacute;clo: {{ nombreNucleo(Session::get('cod_nuc'))}}
				</p>
				<p class="codseg">C&oacute;digo de Seguridad: {{ $datos_solicitud->codseg }}</p>
			</td>
			<td style="width: 5%;">
				<img src='images/logo_uba.png' alt='UBA' height="75px" />
			</td>
			<td width="38%"  style="padding-left:-130px ">
				<p class="encabezado">UNIVERSIDAD BICENTENARIA DE ARAGUA SECRETAR&Iacute;A <br>
					DIRECCI&Oacute;N GENERAL DE ADMISI&Oacute;N Y CONTROL DE ESTUDIOS<br>
					PLANILLA SOLICITUD DE INSCRIPCI&Oacute;N <br>
					{{ $lapso_letra}} | {{ $datos_solicitud->id }}
				</p>		
				<p id="codseg">C&oacute;digo de Seguridad: {{ $datos_solicitud->codseg }}</p>
</td>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="condiciones">
				<p><strong>Condiciones:</strong></p>
				<p>Haber cursado el semestre regular anterior.</p>
				<p><strong>Estar solvente administrativamente</strong> con el semestre regular anterior.</p>
				<p><strong>Estar solvente con la Biblioteca</strong></p>
				<p><strong>S&oacute;lo se admite</strong> la solicitud de inscripci&oacute;n de <strong>DOS (2)</strong> asignaturas en turnos diferentes.</p>
				<p>Debe incluir las materias a solicitar en una misma operaci&oacute;n. <strong>En caso de requerir alg&uacute;n cambio debe eliminar la solicitud y realizar una nueva. <br>ESTO SER&Aacute; POSIBLE HASTA TANTO NO CONSIGNE LOS RECAUDOS EN LA FACULTAD RESPECTIVA LOS DIAS {{ date_format(date_create($dia1), 'd-m-Y')}} y {{ date_format(date_create($dia2), 'd-m-Y')}}</strong>.</p>
				<p><strong>NO HABR&Aacute; INSCRIPCI&Oacute;N DE REZAGADOS PARA EL VERANO.</strong></p>
				<p>La P&aacute;gina Web s&oacute;lo estar&aacute; habilitada del <strong> {{ date_format(date_create($fecha_ini), 'd-m-Y')}} al {{ date_format(date_create($dia1), 'd-m-Y')}}.</strong></p>
				<p>Deber&aacute; imprimir dos(2) ejemplares de la planilla generada por el sistema.</p>
				<p>La consignaci&oacute;n de la planilla se llevar&aacute; a cabo en cada Facultad respetando el orden de atenci&oacute;n establedico, los dias {{ date_format(date_create($dia1), 'd-m-Y')}} y {{ date_format(date_create($dia2), 'd-m-Y')}}.</p>
				<p>Inicio de clase es el: <strong>{{date_format(date_create($fecha_ini_clases), 'd-m-Y')}}</strong></p>
				<p><strong>Para Finalizar la solicitud de inscripci&oacute;n debe:</strong></p>
				<p>1. Consignar los recaudos exigidos (planilla de solicitud de inscripci&oacute;n y d&eacute;posito bancario) en la Facultad respectiva atendiendo el orden establecido de acuerdo a la terminaci&oacute;n de su n&uacute;mero de c&eacute;dula.</p>
				<div class="cuadro">
					<strong>RECUERDA:</strong> esta es una <strong>solicitud</strong> sujeta al cupo disponible y, una vez recibidas y procesadas todas las solicitudes, estas son organizadas en orden decreciente atendiendo al <strong>&Iacute;ndice Acad&eacute;mico Acumulado</strong> hasta el semestre regular anterior al que est&eacute; finalizando, para su procesamiento. Para mas informaci&oacute;n ingrese dentro del men&uacute; "Procesos Acad&eacute;micos" en la opcion "Intensivo",luego "Solicitud de Verano", el {{ date_format(date_create($dia_atencion_listado), 'd-m-Y')}} debe revisar los listados provisionales de las materias solicitadas.
				</div>
				<div>
					V.B Func. Administrativo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					V.B Func. D.A.C.E.
				</div>
			</td>
			<td colspan="2">
				<table>
					<tr>
						<td><strong>Fecha Solicitud:</strong></td><td>{{date_format(date_create($datos_solicitud->fecha), 'd-m-Y')}}</td></tr>
					<tr>
						<td><strong>C&eacute;dula de Identidad:</strong></td>
						<td>{{ Session::get('cedula') }}</td>
					</tr>
					<tr>
						<td><strong>Apellidos y Nombres:</strong></td>
						<td>{{ Session::get('nom_ape_completo')}}</td>
					</tr>
					<tr>
						<td><strong>Escuela</strong></td>
						<td>{{ nombreEscuela(Session::get('cod_esc')) }}</td>
					</tr>	
					<tr>
						<td><strong>Tel&eacute;fono:</strong></td>
						<td>{{ $datos_solicitud->cod_cel.'-'.$datos_solicitud->tel_cel }}</td>
					</tr>
					<tr>
						<td><strong>Correo Elect&oacute;nico:</strong></td>
						<td>{{ $datos_solicitud->email }}</td>
					</tr>	
					<tr>
						<td colspan="2">
							<h4>Asignaturas a Inscribir</h4>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table >
								<tr style="border-collapse: collapse;border:1px solid black;">
									<td style="border-collapse: collapse;border:1px solid black;">C&oacute;digo</td>
									<td style="border-collapse: collapse;border:1px solid black; width=50%;">Materia</td>
									<td style="border-collapse: collapse;border:1px solid black;">Turno</td>
								</tr>
								@if($datos_solicitud->materia1 !="")
									<tr>
										<td>{{ $datos_solicitud->materia1}}</td>
										<td>{{ nombreMateria($datos_solicitud->materia1,Session::get('cod_nuc')) }}</td>
										<td>{{$turno1}}</td>
									</tr>
								@endif
								@if($datos_solicitud->materia2 !="")
								<tr>
									<td>{{ $datos_solicitud->materia2}}</td>
									<td>{{nombreMateria($datos_solicitud->materia2,Session::get('cod_nuc'))}}</td>
									<td>{{$turno2}}</td>
								</tr>
								@endif
								<tr>
									<td colspan="3"> <strong>Total Unidades de Cr&eacute;dito: {{$uni_cre}}</strong></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3"><h4>Datos del Pago</h4></td>
					</tr>
					<tr>
						<td>Tipo de Pago</td>
						<td class="centrado"># Referencia</td>
						<td>Monto Bs.</td>
					</tr>
					@if($datos_solicitud->tipo_pago1!="")
		                <tr>
		                	@if ($datos_solicitud->tipo_pago1=='1-AP')
		                		<td>Recibo de caja</td>
		                	@else
		                		<td>D&eacute;posito Bancario</td>
		                	@endif
                            <td class="centrado">{{$datos_solicitud->num_ref1}}</td>
                            <td class="centrado">{{$datos_solicitud->monto1}}</td>     
                        </tr>
                    @endif
                    @if($datos_solicitud->tipo_pago2 !="")
		                <tr>
		                	@if ($datos_solicitud->tipo_pago2=='1-AP')
		                		<td>Recibo de caja</td>
		                	@else
		                		<td>D&eacute;posito Bancario</td>
		                	@endif
                            <td class="centrado">{{$datos_solicitud->num_ref2}}</td>
                            <td class="centrado">{{$datos_solicitud->monto2}}</td>     
                        </tr>
                    @endif
                    @if($datos_solicitud->tipo_pago3 !="")
		                <tr>
		                	@if ($datos_solicitud->tipo_pago3=='1-AP')
		                		<td>Recibo de caja</td>
		                	@else
		                		<td>D&eacute;posito Bancario</td>
		                	@endif
                            <td class="centrado">{{$datos_solicitud->num_ref3}}</td>
                            <td class="centrado">{{$datos_solicitud->monto3}}</td>     
                        </tr>
                    @endif
                	<tr>
	                	<td class="centrado" colspan="3">Total Cancelado Bs.: {{$total_p}}</td>
                    </tr>
                    <tr>
                    	<table width="100%" align="center" style="padding-top:50px">
                    		<tr>
                    			<td width="40%" align="center" style="padding-left:30px">______________________________<br>Firma del Alumno</td>
                    			<td width="5%">&nbsp;</td>
                    			<td width="40%" align="center">______________________________<br>Funcionario Receptor (Sello)</td>
                    		</tr>
                    		<tr>
                    			<td colspan="3"><strong>Nota:</strong> <br>A) Si el arancel cancelado no corresponde al número de unidades de cr&eacute;dito a inscribir, esta solicitud no ser&aacute; procesada. </td>
                    		</tr>
                    		<tr>
                    			<td colspan="3">B) &Eacute;sta planilla debe acompa&ntilde;arse con los dep&oacute;sitos bancarios correspondientes.</td>
                    		</tr>
                    		<tr>
                    			<td colspan="3"><strong> {{ $fec_consignacion }} </strong></td>
                    		</tr>
                    		
                    	</table>
					</tr>		
				</table>
			</td>
		</tr>
	</table>
</body>
</html>