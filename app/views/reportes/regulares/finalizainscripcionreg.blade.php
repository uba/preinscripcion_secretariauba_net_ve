<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<style type="text/css">
	body{
		width: 100%;
		height: 100%;
		font-size: 14px;
		font-family: sans-serif; 
	}
	hr{
		page-break-after: always;
		border: 0;
	}
	p.grande { line-height: 150%; }
	.tabla{
		width: 100%;
	}
	.datos{
		/*border-collapse: collapse;*/
		width: 40%;
		margin-top: 50px;
		vertical-align: middle;
		font-size: 12px;
		/*border: 1px solid black;*/
	}
	.contenido{
		width: 100%;
		margin-top: 40px;
		vertical-align: middle;
		font-size: 12px;
	}
	.celda{
		border: 1.5px solid black;
	}
	.centro td{
		text-align: center;
	}
	.justificado td{
		text-align: justify;
	}

	.izquierda{
		text-align: left !important;
	}

	.bottom { 
		position: absolute; 
		text-align: center;
		width: 100%;
		bottom: 15px;
	}
	.header,
	.footer {
		position: fixed;
		left: 0;
		right: 0;
	}
	.header {
		top: 0;
	}
	.footer {
		bottom: 0;
		margin-bottom: 200px;
	}
	.page-number {
		text-align: center;
	}
	.page-number:before {
		content: counter(page);
	}
	.separacion{
		margin-top: 160px;
	}
	</style>
</head>
<body>
	<div class="header">
		<table class="tabla">
			<tr>
				<td width="10%" align="center">
					<img src='images/logo_uba.png' alt='UBA' width="70px" height="70px" />
				</td>
				<td width="60%" align="center">
					<strong><span>UNIVERSIDAD BICENTENARIA DE ARAGUA</span></strong><br>
					<strong><span>DIRECCI&Oacute;N DE ADMISI&Oacute;N Y CONTROL DE ESTUDIOS</span></strong><br>
					<strong><span>SECRETAR&Iacute;A</span></strong><br>
					<strong><span>MARACAY - VENEZUELA</span></strong><br>
					<strong><span>PER&Iacute;ODO ACAD&Eacute;MICO {{$lapso_letra}}</span></strong>
				</td>
			</tr>
		</table>
		<table width="100%">
			<tr>				
				<td colspan="2" align="right" style="font-size:11px;"> {{$fecha}} Serial: {{$numero}} - {{$serial}}</td>
			</tr>
			<tr>
				<td colspan="2" style="font-size:11px;">
					<h3 style="text-align:center;">HORARIO DE CLASES (Inscripci&oacute;n Web)</h3>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="font-size:11px;">
					<p>
					<strong>C&Eacute;DULA DE IDENTIDAD:</strong> {{$cedula}} &nbsp;&nbsp;&nbsp;&nbsp; <strong>CARRERA: </strong> {{$escuela}}<br/>
					<strong>APELLIDOS Y NOMBRES:</strong> {{$nombre}}<br/> </p>
				</td>
			</tr>
		</table>
		<table width="100%">
			<tr style="font-weight:bold;font-size:11px;">
				<td>C&Oacute;DIGO</td>
				<td>DESCRIPCI&Oacute;N DE LA ASIGNATURA</td>
				<td>SECCI&Oacute;N</td>
				<td>HORARIO</td>
				<td>(EDIFICIO) AULA(S)</td>
			</tr>
				@foreach($materias as $key => $value)
					<tr style="font-size:12px;">
						<td>{{$value->cod_mat}}</td>
						<td>{{$value->des_mat}}</td>
						<td align="center">{{$value->sec_mat}}</td>
						@if($value->BAN_CE == 1)
							<td>{{$value->DIA1}}-{{$value->DIA2}}</td>
							<td align="center">{{$value->OBS_MAT}}</td>
						@elseif($value->BAN_CE == 4)
							<td>{{$value->DIA1}}-{{$value->DIA2}}</td>
							<td align="center">{{$value->OBS_MAT}}</td>
						@elseif($value->BAN_CE == 3)
							<td>{{$value->DIA1}}-{{$value->DIA2}}</td>
							<td>({{$value->EDI1}}) {{$value->AULA1}} | ({{$value->EDI2}}) {{$value->AULA2}}</td>
						@elseif($value->BAN_CE > 50)
							<td>{{$value->DIA1}}-{{$value->DIA2}}</td>
							<td align="center">{{$value->OBS_MAT}}</td>
						@else
							<td> {{$value->DIA1}}-{{$value->DIA2}}</td>
							<td>({{$value->EDI1}}) {{$value->AULA1}} | ({{$value->EDI2}}) {{$value->AULA2}}</td>
						@endif
					</tr>
				@endforeach
		</table>
			<p style="text-align:justify;font-size: 11px">
				Se hace de su conocimiento el cumplimiento de las Resoluciones del Consejo Universitario <strong>Nros.012-12, 014-12 y 015-12</strong> las cuales establecen el semestre en el cual los estudiantes de las diferentes Facultades y Escuelas de la Universidad Bicentenaria de Aragua, deben haber aprobado los requisitos de Grado establecidos en la Institución.
			<p>
			<p style="text-align:justify;font-size: 11px">
				<strong>OBSERVACIONES:</strong><br/>
					- El uso del Carnet es Obligatorio. A&uacute;n estando solvente, el carnet es el &uacute;nico medio para accesar a los edificios. ES INTRANSFERIBLE.<br/>
					- Los alumnos inscritos en asignaturas bajo r&eacute;gimen semipresencial, deber&aacute;n regirse con car&aacute;cter obligatorio por el <strong>Art. 9</strong>, Par&aacute;grafo &Uacute;nico del Reglamento Interno de Evaluai&oacute;n Estudiantil Vigente, aprobado el 22-01-2013, seg&uacute;n <strong>Resoluci&oacute;n No. 013-13</strong>, el cual cita: &quot;...en la modalidad de estudios semipresencial al menos el cincuenta (50%) de la calificaci&oacute;n total de cada corte, deber&aacute; ser aplicado obligatoriamente de manera presencial.&quot;<br/>
					@if (Session::get('cod_reg')==0)
						- Fecha Inicio Adici&oacute;n de Materias y Cambios de Secci&oacute;n (Proceso WEB): {{$fechaiac}}<br/>
						- Fecha Fin Adici&oacute;n de Materias y Cambios de Secci&oacute;n (Proceso WEB): {{$fechafac}} <br/>
					@endif
					- Fecha de Retiro de Materias:  {{$fechaira}} al {{$fechafra}}
			</p>
			<p style="text-align:justify;font-size: 11px">
				<strong><span style="font-size:11px;">LEY DE SERVICIO COMUNITARIO DEL ESTUDIANTE DE EDUCACI&Oacute;N SUPERIOR</span></strong><br/>
					- <strong>ART&Iacute;CULO 16.</strong> Los prestadores del servicio comunitario son los estudiantes de Educaci&oacute;n Superior que hayan cumplido al menos, con el (50%) del total de la carga acad&eacute;mica de la carrera.
			</p>
			<center>
			<table border="0" width="100%" >
				<tr>
					<td align="center">________________________</td>
					<td></td>
					<td align="center"><img src='images/firma_autorizada.jpg'></td>
				</tr>
				<tr style="font-size:11px;">
					<td align="center">Firma del Estudiante</td>
					<td></td>
					<td align="center">Firma del Funcionario</td>
				</tr>
			</table>
		</center><br>
		<p>
			<span style="font-size:11px;">NO LLEVA ENMIENDAS.<br/>
				HORARIO SUJETO A MODIFICACIONES POR EL DECANATO DE LA FACULTAD Y/O DIRECCI&Oacute;N DE CONTROL DE ESTUDIOS (D.A.C.E.).</span>
		</p><br>
		<div style="text-align: right;">Fecha de Impresi&oacute;n: {{$fechas}}</div>

	</div>
</body>
</html>