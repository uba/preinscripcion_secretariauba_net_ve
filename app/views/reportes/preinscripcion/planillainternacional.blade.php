<style type="text/css">
    <!--
    .footer {
        position: fixed;
        left: 0;
        right: 0;
        color: black;
    }
    .footer {
        bottom: 10px;
    }
    body{
        width: 100%;
        height: 100%;
        font-size: 12pt;
        font-family: times-roman; 
    }
    span{
        display: inline-block;
    }
    hr {
        page-break-before: always;
        border: 0;
    }
    div.encabezado{
        margin-left: 0.5cm;
        font-size: 11pt;
    }
    .centrado{
        text-align: center;
    }
    .fecha{
        text-align: right;
        margin-right: 1cm;
    }
    .titulo{
        font-size: 16pt;
    }
    .texto_footer{
        font-style: italic;
        font-size: 9pt;
        line-height: 1px;
    }
    .linea_abajo{
        border-bottom: 2px solid black;
    }
    a{
        color: blue;
    }
    .negrita{
        font-weight: bold;
    }
    .raya_abajo{
        border-bottom: 2px solid black;
    }
    .firma{
        text-decoration: overline;
    }
    .principal{
        font-size: 10pt;
        line-height: 5pt;
    }

    .validacion{
        font-size: 9pt;
    }
    .contenido{
        margin-top: -10px;
        line-height: 15pt;
        text-align: justify;
        text-indent: 60px;
        margin-left: 1cm;
        margin-right: 1cm;
        font-size: 12pt;
    }
    .institucion{
        margin-left: 1cm;
        font-size: 13pt;
        margin-bottom: 0px;
    }
    .dirigido{
        margin-top: 0px;
        text-align: right;
        margin-right: 1cm;
        font-size: 13pt;
        margin-bottom: 5px;
    }
    .lista{
        font-size: 12pt;
        margin-left: 1cm;
    }
    .atentamente{
        margin-top: 40px;
    }

-->
</style>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="{{ URL::to('images/favicon.ico')}}">
</head>
<body>
<div id="header">
        <table width="100%;">
            <tr>
                <td style="width: 15%; padding-left:5%;">
                    <img src="images/logo_uba.png" height="120px">
                </td>
                <td style="width: 60%;">
                    <div class="encabezado">Rep&uacute;blica Bolivariana de Venezuela</div>
                    <div class="encabezado">Universidad Bicentenaria de Aragua</div>
                    <div class="encabezado">Secretar&iacute;a</div>
                    @foreach($value as $value)
                    <div class="encabezado"></div>
                </td>
                <td style="width: 15%; padding-left:10%;">
                    <img src="images/foto-rec.jpg" height="120px">
                    
                </td>
            </tr>
        </table>
</div>   
 <div class="footer">
            <p class="texto_footer centrado">Universidad Bicentenaria de Aragua - Una Universidad para la Creatividad</p>
</div> 
@if($value->tipo_solicitud==1)
    <h4 class="titulo centrado"><strong>PLANILLA DE SOLICITUD DE INSCRIPCI&Oacute;N</strong></h4>
@endif
 @if($value->tipo_solicitud==2)
    <h4 class="titulo centrado"><strong>PLANILLA DE SOLICITUD DE REINCORPORACI&Oacute;N</strong></h4>
@endif
 @if($value->tipo_solicitud==3)
    <h4 class="titulo centrado"><strong>PLANILLA DE SOLICITUD DE INSCRIPCI&Oacute;N <br/> Seg&uacute;n Art. 182, Ley de Universidades Vigente</strong></h4>
@endif
<div class="principal" style="width:100%;"> 
        <p><strong>Tipo de Solicitud:</strong>{{ nombre_solicitud($value->tipo_solicitud)}} </p>
      
        <p><strong>N&uacute;mero de Sobre:</strong> {{ $value->numsobre}} </p>
        <p><strong>Lapso Acad&eacute;mico:</strong> {{ $value->lapso}} </p>
        <br>
        <p class="negrita raya_abajo">DATOS PERSONALES</p>  
        <p><strong>C&eacute;dula de Identidad:</strong> {{ $value->cedula}} </p>
        <p><strong>Apellidos y Nombres:</strong> {{ minuscula(trimEstudiante($value->apellidos.','.$value->nombres)) }}</p>
        <p><strong>Fecha de Nacimiento:</strong> {{ $value->fecha_nac }}</p>
        <p><strong>Sexo:</strong> {{ tipo_sexo($value->sexo) }}</p>
        <p><strong>Estado Civil:</strong> {{ estado_civil($value->edo_civil) }} </p>
        <p><strong>Celular:</strong> {{ $value->cod_celular.'-'.$value->celular }}</p>
        <p><strong>Email:</strong> {{ $value->email }}</p>

        <br>
        <p><strong>Datos de su Lugar de Nacimiento</strong> </p>
        <p><strong>Pa&iacute;s:</strong> {{ pais_nacimiento($value->pais_nac) }}</p>
        <!--<p><strong>Estado:</strong> {{ estado_nacimiento($value->estado_nac_inter) }}</p>
        <p><strong>Ciudad:</strong> {{ ciudad_nacimiento($value->ciudad_nac_inter) }}</p>-->
        <br> 
       
        <p class="negrita raya_abajo">DATOS DE LA CARRERA QUE DESEA CURSAR EN ESTA INSTITUCI&oacute;N</p>
        <p><strong>Carrera:</strong> {{ minuscula(nombreEscuelalargo($value->carrera_cursar)) }}</p>
        
        <p class="negrita raya_abajo">Documentos Requisitos</p> 
        @if($value->docum_ident_inter!="")
        <p><strong>(DI) Documento de Identidad ampliado: </strong> {{($value->docum_ident_inter)}}</p>  
        @endif
         @if($value->rusnieu_internacional!="")
        <p><strong>(RU) Constancia RUSNIEU: </strong> {{($value->rusnieu_internacional)}}</p> 
        @endif
         @if($value->foto_inter!="")
        <p><strong>(FT) Fotografia en .jpg o png: </strong> {{($value->foto_inter)}}</p> 
        @endif
         @if($value->notas_certi_bach_inter!="")
        <p><strong>(NC) Notas certificadas de 1ero a 5to año: </strong> {{($value->notas_certi_bach_inter)}}</p> 
        @endif
         @if($value->titulo_bach_inter!="")
        <p><strong>(TB) Titulo de Bachiller: </strong> {{($value->titulo_bach_inter)}}</p> 
        @endif
         @if($value->const_buena_conducta_inter!="")
        <p><strong>(BC) Constancia de Buena Conducta: </strong> {{($value->const_buena_conducta_inter)}}</p> 
        @endif
        <br>
        <p class="negrita raya_abajo">Documentos si ha realizado estudios en otra instituci&oacute;n y aplica por Equivalencia: </p> 
         @if($value->notas_certi_otros_est_inter!="")
        <p><strong>NCE- Notas Certificadas de Estudios realizados: </strong> {{($value->notas_certi_otros_est_inter)}}</p> 
        @endif
         @if($value->pensum_otros_estu_inter!="")
        <p><strong>PER- Pensum de la Carrera que curso: </strong> {{($value->pensum_otros_estu_inter)}}</p>
        @endif 
         @if($value->programas_otros_estu_inter!="")
        <p><strong>PRG- Programas de las materias cursadas: </strong> {{($value->programas_otros_estu_inter)}}</p> 
        @endif
    @endforeach
</body> 
</html>