<style type="text/css">
    <!--
    .footer {
        position: fixed;
        left: 0;
        right: 0;
        color: black;
    }
    .footer {
        bottom: 10px;
    }
    body{
        width: 100%;
        height: 100%;
        font-size: 12pt;
        font-family: times-roman; 
    }
    span{
        display: inline-block;
    }
    hr {
        page-break-before: always;
        border: 0;
    }
    div.encabezado{
        margin-left: 0.5cm;
        font-size: 11pt;
    }
    .centrado{
        text-align: center;
    }
    .fecha{
        text-align: right;
        margin-right: 1cm;
    }
    .titulo{
        font-size: 16pt;
    }
    .texto_footer{
        font-style: italic;
        font-size: 9pt;
        line-height: 1px;
    }
    .linea_abajo{
        border-bottom: 2px solid black;
    }
    a{
        color: blue;
    }
    .negrita{
        font-weight: bold;
    }
    .raya_abajo{
        border-bottom: 2px solid black;
    }
    .firma{
        text-decoration: overline;
    }
    .principal{
        font-size: 10pt;
        line-height: 5pt;
    }

    .validacion{
        font-size: 9pt;
    }
    .contenido{
        margin-top: -10px;
        line-height: 15pt;
        text-align: justify;
        text-indent: 60px;
        margin-left: 1cm;
        margin-right: 1cm;
        font-size: 12pt;
    }
    .institucion{
        margin-left: 1cm;
        font-size: 13pt;
        margin-bottom: 0px;
    }
    .dirigido{
        margin-top: 0px;
        text-align: right;
        margin-right: 1cm;
        font-size: 13pt;
        margin-bottom: 5px;
    }
    .lista{
        font-size: 12pt;
        margin-left: 1cm;
    }
    .atentamente{
        margin-top: 40px;
    }
    .tabla{
	border-collapse: collapse;
	width: 100%;
	vertical-align: middle;
	line-height: 20pt;
	}
	.tabla td{
		border: 2px solid black;
		font-size: 9pt;
	}

-->
</style>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link rel="shortcut icon" href="{{ URL::to('images/favicon.ico')}}">
</head>
<body>
   
    @foreach($value as $value)
    <div id="header">
        <table width="100%;">
            <tr>
                <td style="width: 15%; padding-left:5%;">
                    <img src="images/logo_uba.png" height="120px">
                </td>
                <td style="width: 60%;">
                    <div class="encabezado">Rep&uacute;blica Bolivariana de Venezuela</div>
                    <div class="encabezado">Universidad Bicentenaria de Aragua</div>
                    <div class="encabezado">Secretar&iacute;a</div>
                    <div class="encabezado">{{ nombreNucleo($value->cod_nucleo)}}</div>
                    <div class="encabezado">Estado {{estadoNucleo($value->cod_nucleo)}}</div>
                    <div class="encabezado"></div>
                </td>

                <td style="width: 15%; padding-left:10%;">
                    <img src="images/foto-rec.jpg" height="120px">
                    @if($value->tipo_solicitud==1)
                        <div><h5><strong>F-41-PSINI-01-09</strong></h5></div>
                    @endif
                    @if($value->tipo_solicitud==2)
                        <div><h5><strong>F-41-PSINI-01-09</strong></h5></div>
                    @endif
                    @if($value->tipo_solicitud==3)
                        <div><h5><strong>F-40-PSINI-01-09</strong></h5></div>
                    @endif
                </td>
            </tr>
        </table>
    </div>
    <div class="footer">
            <p class="texto_footer centrado">Universidad Bicentenaria de Aragua - Una Universidad para la Creatividad</p>
    </div> 
    @if($value->tipo_solicitud==1)
        <h4 class="titulo centrado"><strong>PLANILLA DE SOLICITUD DE INSCRIPCI&Oacute;N</strong></h4>
    @endif
    @if($value->tipo_solicitud==2)
        <h4 class="titulo centrado"><strong>PLANILLA DE SOLICITUD DE REINCORPORACI&Oacute;N</strong></h4>
    @endif
    @if($value->tipo_solicitud==3)
        <h4 class="titulo centrado"><strong>PLANILLA DE SOLICITUD DE INSCRIPCI&Oacute;N <br/> Seg&uacute;n Art. 182, Ley de Universidades Vigente</strong></h4>
    @endif
    @if($value->cod_nucleo == 0 && $value->fecha_cita != '')
    <h5>Fecha de Atenci&oacute;n: {{date_format(date_create($value->fecha_cita),'d/m/Y')}}
    <br>Horario: (8:00am - 12:00pm) / (1:00pm - 4:00pm)</h5>
    @endif
    <div class="principal" style="width:100%;"> 
        <p><strong>Tipo de Solicitud:</strong> {{ nombre_solicitud($value->tipo_solicitud)}}</p>
        <p><strong>N&uacute;mero de Expediente:</strong> {{ $value->numsobre}}</p>
        <p><strong>Lapso Acad&eacute;mico:</strong> {{ $value->lapso}}</p>
        <br>
        <p class="negrita raya_abajo">DATOS PERSONALES</p>  
        <p><strong>C&eacute;dula de Identidad:</strong> {{ $value->cedula}}</p>
        <p><strong>Apellidos y Nombres:</strong> {{ minuscula(trimEstudiante($value->apellidos.','.$value->nombres)) }}</p>
        <p><strong>Nacionalidad:</strong> {{ nacionalidad($value->nacionalidad) }}</p>
        <p><strong>Fecha de Nacimiento:</strong> {{ $value->fecha_nac }}</p>
        <p><strong>Sexo:</strong> {{ tipo_sexo($value->sexo) }}</p>
        <p><strong>Estado Civil:</strong> {{ estado_civil($value->edo_civil) }}</p>
        <p><strong>Celular:</strong> {{ $value->cod_celular.'-'.$value->celular }}</p>
        <p><strong>Email:</strong> {{ $value->email }}</p>
        <p><strong>Twitter:</strong> @if($value->twitter != '' && $value->twitter != 'NO') {{ '@'.$value->twitter }} @else NO POSEE @endif</p>
        <p><strong>Facebook:</strong> {{ $value->facebook }}</p>
        <br>
        @if($value->trabaja==1)
        <p><strong>Datos de su Trabajo Actual</strong> </p>
        <p><strong>Nombre de la empresa:</strong> {{ minuscula($value->empresa_trab) }}</p>
        <p><strong>Direcci&oacute;n de la empresa:</strong> {{ minuscula($value->direccion_trab) }}</p>
        <p><strong>Tel&eacute;fono de la empresa:</strong> {{ $value->codtelf_trab.'-'. $value->telf_trab }}</p>
        <p><strong>Departamento en que labora:</strong> {{ minuscula($value->departamento_trab) }}</p>
        <p><strong>Cargo en la empresa:</strong> {{ $value->cargo_trab }}</p>
        <br>
        @endif
        @if($value->vehiculo==1)
        <p><strong>Datos de su Veh&iacute;culo</strong> </p>
        <p><strong>Placa:</strong> {{ $value->placa_veh }}</p>
        <p><strong>Marca:</strong> {{ $value->marca_veh }}</p>
        <p><strong>Modelo:</strong> {{ $value->modelo_veh }}</p>
        <br>
        @endif
        <p><strong>Datos de su Lugar de Nacimiento</strong> </p>
        <p><strong>Pa&iacute;s:</strong> {{ pais_nacimiento($value->pais_nac) }}</p>
        <p><strong>Estado:</strong> {{ estado_nacimiento($value->estado_nac) }}</p>
        <p><strong>Ciudad:</strong> {{ ciudad_nacimiento($value->ciudad_nac) }}</p>  
        <br>      
        @if($value->cedula_rep!="")
        <p class="negrita raya_abajo">DATOS DEL REPRESENTANTE</p> 
        <p><strong>Cedula del Representante:</strong> {{ $value->cedula_rep }}</p>
        <p><strong>Apellidos y Nombres:</strong> {{ minuscula(trimEstudiante($value->apellidos_rep.','.$value->nombres_rep)) }}</p>
        <p><strong>Edad:</strong> {{ $value->edad_rep }}</p> 
        <p><strong>Direcci&oacute;n:</strong> {{ minuscula($value->direccion_rep) }}</p>
        @endif
        <hr>
        <p class="negrita raya_abajo" >DATOS ACAD&Eacute;MICOS</p>
        <p><strong>Tipo Instituci&oacute;n:</strong> {{ tipo_institucion($value->tipo_institucion_bach) }}</p>
        <p><strong>Nombre de la Instituci&oacute;n:</strong> {{ $value->nombre_institucion_bach }}</p>
        <p><strong>Turno que estudi&oacute;:</strong> {{ turno($value->turno_bach) }}</p>
        <p><strong>Fecha de Graduaci&oacute;n:</strong> {{ $value->fecha_graduacion_bach }}</p>
        <p><strong>Estado(&uacute;ltimo año de bachillerato):</strong> {{ estados_bach($value->estado_bach) }}</p>
        <p><strong>Ciudad(&uacute;ltimo año de bachillerato):</strong> {{ ciudades($value->ciudad_bach) }}</p>
        <br>
        <p><strong>Actividades realizadas en el tiempo transcurrido desde su graduaci&oacute;n</strong> </p>
        <p><strong>Actividad 1:</strong> {{ $value->actividad1 }}</p>
        @if($value->actividad2!="")
        <p><strong>Actividad 2:</strong> {{ $value->actividad2 }}</p>
        @endif
        @if($value->actividad3!="")
        <p><strong>Actividad 3:</strong> {{ $value->actividad3 }}</p>
        @endif
        @if($value->actividad4!="")
        <p><strong>Actividad 4:</strong> {{ $value->actividad4 }}</p>
        @endif
        <p><strong>R.U.S.N.I.E.S:</strong> {{ $value->rusnies }}</p>
        <p><strong>Recibi&oacute; orientaci&oacute;n para la escogencia de la carrera:</strong> {{ orientacion_carrera($value->orientacion_carrera) }}</p>
        <p><strong>Est&aacute; decidido con la escogencia de la carrera:</strong> {{ decidido_carrera($value->decidido_carrera) }}</p>
        <p><strong>Ha ocupado Ud. cargo durante el bachillerato relacionados con representaci&oacute;n estudiantil,deporte.etc:</strong> {{ sino_preinsc($value->cargos_bach) }}</p>
        <p><strong>Ha cursado Ud. Estudios en otra instituci&oacute;n de educaci&oacute;n superior:</strong> {{ sino_preinsc($value->estudios_superior) }}</p>
        
        @if($value->instituto_superior!="")
        <p><strong>Instituto:</strong> {{ minuscula(nombreInstitutosuperior($value->instituto_superior)) }}</p>
        @endif
        @if($value->carrera_superior!="")
        <p><strong>Carrera:</strong> {{ minuscula(nombreCarrerosuperior($value->carrera_superior)) }}</p>
        @endif
        @if($value->graduado_superior!="")
        <p><strong>Se gradu&oacute;:</strong> {{ sino_preinsc($value->graduado_superior) }}</p>
        @endif
        @if($value->nivel_superior>0 && $value->nivel_superior!="")
        <p><strong>Nivel de estudio:</strong> @if($value->nivel_superior == 1) T.S.U @else Profesional @endif </p>
        @endif
        <br>
        <p><strong>Datos de la carrera que desea cursar en esta instituc&oacute;n</strong> </p>
        <p><strong>Carrera:</strong> {{ minuscula(nombreEscuelalargo($value->carrera_cursar)) }}</p>
        <p><strong>Turno en que desea estudiar:</strong> {{ turno($value->turno_carrera_cursar) }}</p>
        <p><strong>Modalidad de estudios en que desea estudiar:</strong> {{ modalidad($value->modalidad_carrera_cursar)}} </p>
        <p><strong>N&uacute;cleo donde cursar&aacute; sus estudios:</strong> {{ nombreNucleo($value->cod_nucleo) }}</p>
        <p><strong>Curs&oacute; estudios anteriormente en esta instituci&oacute;n:</strong> @if($value->curso_uba ==1 ) Si @else No @endif</p>
        @if($value->curso_uba ==1 )
        <p><strong>¿Se gradu&oacute;?</strong> @if($value->graduo_uba ==1) Si @else No @endif </p>
        @endif
        <br>
        <p class="negrita raya_abajo">Actividades Deportivas</p> 
        @if($value->depo_1==1) <p><strong>Beisbol</strong> </p> @endif   
        @if($value->depo_2==1) <p><strong>voleibol</strong> </p> @endif
        @if($value->depo_3==1) <p><strong>tenis</strong> </p> @endif
        @if($value->depo_4==1) <p><strong>futbol</strong> </p> @endif
        @if($value->depo_5==1) <p><strong>baloncesto</strong> </p> @endif
        @if($value->depo_6==1) <p><strong>artes marciales</strong> </p> @endif
        @if($value->depo_7==1) <p><strong>futbolito</strong> </p> @endif
        @if($value->depo_8==1) <p><strong>ajedrez</strong> </p> @endif
        <p class="negrita raya_abajo">Actividades Artisticas</p> 
        @if($value->arti_1==1) <p><strong>teatro</strong> </p> @endif
        @if($value->arti_2==1) <p><strong>rondalla</strong> </p> @endif
        @if($value->arti_3==1) <p><strong>orfeon</strong> </p> @endif
        @if($value->arti_4==1) <p><strong>danzas</strong> </p> @endif
        @if($value->arti_5==1) <p><strong>pintura</strong> </p> @endif
        @if($value->arti_6==1) <p><strong>estudiantina</strong> </p> @endif
        <p class="negrita raya_abajo">Otras Actividades</p> 
        @if($value->otras_1==1) <p><strong>Seguridad Industrial</strong> </p> @endif
        @if($value->otras_2==1) <p><strong>Relaciones Humanas</strong> </p> @endif
        @if($value->otras_3==1) <p><strong>Grupo de Protocolo</strong> </p> @endif
        @if($value->otras_4==1) <p><strong>Bombero Universitario</strong> </p> @endif
        @if($value->otras_5==1) <p><strong>Param&eacute;dico</strong> </p> @endif
        @if($value->otras_6==1) <p><strong>Acci&oacute;n Comunitaria</strong> </p> @endif
        @if($value->otras_7==1) <p><strong>Protecci&oacute;n Industrial</strong> </p> @endif
        @if($value->otras_8==1) <p><strong>Mejoramiento del Aprendizaje</strong> </p> @endif
        <hr>        
        <p class="negrita raya_abajo">DATOS SOCIECON&Oacute;MICOS</p>            
        <p><strong>D&oacute;nde vivir&aacute; Ud. Mientras estudie:</strong> {{ vivira_con($value->donde_vivira) }}</p>
        <p><strong>Qui&eacute;n costea sus estudios:</strong> {{ costea_gastos($value->costa_estudios) }}</p>
        <p><strong>Disfruta Ud. de:</strong> {{ disfruta_de($value->ayudas) }}</p>
        <p><strong>Sueldo actual en su grupo familiar:</strong> {{ ingreso_mensual($value->ingreso_mensual) }}</p>
        <p><strong>Viven sus padres:</strong> {{ viven_padres($value->viven_padres) }}</p>
        <p><strong>Estado civil de sus padres:</strong> {{ estado_civil_padres($value->edo_civil_padres) }}</p>
        <p><strong>Vive Ud. Con:</strong> {{ vive_con($value->persona_convive) }} @if ($value->persona_convive==7)<strong>Nombre:&nbsp; {{$value->nombre_persona_convive}}</strong> @endif</p>
        <p><strong>Adultos y ni&ntilde;os que viven en su hogar:</strong> {{ $value->adultos_hogar .' adultos y '.$value->ninos_hogar.' ni&ntilde;os'  }}</p>
        <p><strong>Tiene Ud. Hermanos:</strong> {{ $value->hermanos }}</p>
        <p><strong>Tiene Ud. hijos:</strong> {{ $value->hijos }}</p>
        <br>
        <p><strong>Variable de Discapacidad</strong> </p>
        @if($value->discapacidad1==1) <p><strong>Ceguera Total</strong> </p> @endif
        @if($value->discapacidad2==1) <p><strong>Sordera Total</strong> </p> @endif
        @if($value->discapacidad3==1) <p><strong>Retardo Mental</strong> </p> @endif
        @if($value->discapacidad4==1) <p><strong>Discapacidad de extremidades superiores</strong> </p> @endif
        @if($value->discapacidad5==1) <p><strong>Discapacidad de extremidades inferiores</strong> </p> @endif
        @if($value->discapacidad6==1) <p><strong>Ninguna variable</strong> </p> @endif
        @if($value->desc_discapacidad!="") <p><strong>{{$value->desc_discapacidad}}</strong> </p> @endif
        <br>
        <p class="negrita raya_abajo"><strong>Informaci&oacute;n Adicional</strong> </p>
        <p><strong>Usted fuma:</strong> {{ sino_preinsc($value->fuma) }}</p>
        <p><strong>Pertenece Ud. a una Etnia:</strong> {{ sino_preinsc($value->etnia) }}</p>
        <p><strong>Escribe:</strong> {{ escribe($value->escribe) }}</p>
        <br>
        @if(count($asegurado)>0)
        <p class="negrita raya_abajo"><strong>Informaci&oacute;n de poliza de seguro.</strong></p>
        <p><strong>Entidad:</strong> {{ $asegurado->entidad }}</p>
        <p><strong>Nº de poliza:</strong> {{ $asegurado->num_poliza }}</p>
        @endif
        @if(count($no_asegurado)>0)
        <p class="negrita raya_abajo"><strong>Informaci&oacute;n de poliza de seguro.</strong></p>
        <p><strong>Entidad:</strong> C.A DE SEGUROS LA OCCIDENTAL</p>
        <p><strong>Beneficiario:</strong> {{ $no_asegurado->beneficiario }}</p>
        @endif
        <br><br><br><br>
        <table width="100%" align="center" style="padding-top:50px">
          <tr>
            <td width="40%" colspan="2" align="center" style="padding-left:30px">________________________________<br><br><br>Firma del Estudiante<br><br><br><br></td>
          </tr> 
          <tr>
            <td width="50%" align="center" style="padding-left:30px"><br><br><br><br><br><br><br><br><br><br>________________________________<br><br><br>Nombre y Apellido del Funcionario</td>
            <td width="50%" align="center"><br><br><br><br><br><br><br><br><br><br>________________________________<br><br><br>Firma y Sello</td>
          </tr>   
        </table>
        @if($value->tipo_solicitud==3)
        <hr>
        <table class="tabla" align="center" style="padding-top:50px">
        	<tr>
        		<td class="negrita centrado"><h3>DATOS DEL SOBRE ADJUNTO</h3></td>
        	</tr>
          <tr>
            <td>
            	<p><strong>N&uacute;mero de Sobre:</strong> {{ $value->numsobre}}</p>
		        <p><strong>Lapso Acad&eacute;mico:</strong> {{ $value->lapso}}</p>
		        <p><strong>C&eacute;dula de Identidad:</strong> {{ $value->cedula}}</p>
		        <p><strong>Apellidos y Nombres:</strong> {{ minuscula(trimEstudiante($value->apellidos.','.$value->nombres)) }}</p>
		        <p><strong>Celular:</strong> {{ $value->cod_celular.'-'.$value->celular }}</p>
		        <p><strong>Carrera a cursar:</strong> {{ minuscula(nombreEscuelalargo($value->carrera_cursar)) }}</p>
		        <p><strong>Instituto de procedencia:</strong> @if($value->instituto_superior!="") {{ minuscula(nombreInstitutosuperior($value->instituto_superior)) }} @else  ____________________________________________________________ @endif</p>
		        <p><strong>Carrera que estudi&oacute;:</strong> ____________________________________________________________</p>
		        <p><strong>Programa Createc de Origen:</strong> ____________________________________________________________</p>
            </td>
          </tr>   
        </table>
    @endif        
    </div> 
@endforeach
</body> 
</html>