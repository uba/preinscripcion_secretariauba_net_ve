<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{$title}}- Zona Estudiante</title>
        <meta name="description" content="">   
        <meta name="viewport" content="width=device-width">
        {{ HTML::style('css/normalize.css')}}
        {{ HTML::style('css/main.css')}}
        {{ HTML::style('css/bootstrap.min.css')}}
        {{ HTML::style('css/bootstrap.css')}}        
        {{ HTML::style('css/estilo.css')}}
    	<link rel="shortcut icon" href="{{ URL::to('images/favicon.ico')}}">
    	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>	
    	<style> 
			strong,span{
			font-family: 'Open Sans', sans-serif;
			}
		</style>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
	<div id="conteiner">
		<!-- Master Header -->
		<header id="master-header">
			<div id="master-header-bg"></div>
			<div id="master-header-wrap">
				<!-- Logo -->
				<div id="logo" class="logo_uba"><a href="#"><img style="background-color: white;" src="{{ URL::to('images/uba.png')}}" alt="" /></a></div>
				<!--  or, for logo image: <div id="logo"><a href="index.html"><img src="{{ URL::to('images/uba.png')}}" alt="" /></a></div> -->
				<!-- ..Logo -->
				<!-- Navi -->
			</div>
		</header>
		<!-- ..Master Header -->
		<section>
		<!-- Section -->
			<div class="container container-margin-top centrado">    
				<div> <img src="{{ URL::to('images/mantenimiento.jpg')}}" alt="mant"></div>
			</div>
		<!-- Section -->
		</section>
	</div>
		<!--/. Copyrights -->
		{{ HTML::script('js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') }}
		{{ HTML::script('js/vendor/jquery-1.10.1.min.js')}}
		{{ HTML::script('js/jquery.js')}}
		{{ HTML::script('js/bootstrap.min.js')}}
		{{ HTML::script('js/plugins/jquery.hoverIntent.minified.js')}}
		{{ HTML::script('js/main.js')}}
		{{ HTML::script('js/jquery.isotope.min.js')}}
		{{ HTML::script('js/jquery.flexslider-min.js')}}
    </body>
</html>