<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ $title }} | Universidad Bicentenaria de Aragua</title>
        <meta name="description" content="">   
        <meta name="viewport" content="width=device-width">
        {{ HTML::style('css/normalize.css')}}
        {{ HTML::style('css/main.css')}}
        {{ HTML::style('css/bootstrap.min.css')}}
        {{ HTML::style('css/bootstrap.css')}}        
        {{ HTML::style('css/estilo.css')}}
        @yield('css')
    	<link rel="shortcut icon" href="{{URL::to('images/favicon.ico')}}">	
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
	<div id="conteiner">
		<!-- Master Header -->
		<header id="master-header">
			<div id="master-header-bg"></div>
			<div id="master-header-wrap">
				<!-- Logo -->
				<div id="logo" class="logo_uba"><a href="{{ URL::to('/')}}"><img style="background-color: white;" src="{{ URL::to('images/uba.png')}}" alt="" /></a></div>
				<!--  or, for logo image: <div id="logo"><a href="index.html"><img src="{{ URL::to('images/uba.png')}}" alt="" /></a></div> -->
				<!-- ..Logo -->
				<!-- Navi -->
				<nav id="master-nav">
					<ul>
						<li><a href="{{URL::to('preinscripcion/inicio')}}">Inicio</a></li>
						<li><a href="{{URL::to('preinscripcion/modificarpreinscripcion')}}">Modificación de Datos</a></li>
						<li><a href="{{URL::to('preinscripcion/reimprimir')}}">Re-Impresión de la Planilla</a></li>
						<li><a href="{{URL::to('preinscripcion/inicio')}}">Reiniciar</a></li>
					</ul>
				</nav>
				<!-- ..Navi -->
				<!-- Mobile Button -->
				<a href="#" id="mobile-switch"><span class="icon-menu"></span></a>
				<!-- ..Mobile Button -->
			</div>
		</header>
		<!-- ..Master Header -->
		<section>
		<!-- Section -->
		@yield('content')
		<!-- Section -->
		</section>
	</div>
	<div class="footer">
		
		<!-- Footer -->
		<footer id="master-footer" >

			<!-- Container -->
			<div class="container">

				<div class="row">

					<div class="col-xs-8 col-sm-4 col-md-4">

						<h3 class="widget-title">Sitios de Inter&eacute;s</h3>

						<div class="tagcloud">
							<a href="http://uba.edu.ve">UBA</a>
							<a href="http://uba-extension.com">Direcci&oacute;n de Extensi&oacute;n</a>
							<!-- <a href="#">Academia CISCO</a> -->
							<a href="http://aulavirtual.uba.edu.ve">Aula Virtual</a>
							<a href="http://blog.uba.edu.ve">Blog</a>
						</div>

					</div>

					<div class="col-xs-8 col-sm-4 col-md-4">

						<h3 class="widget-title">Direcci&oacute;n</h3>

						<address class="icon icon-location">
							Av. Intercomunal Santiago Mari&ntilde;o c/c Av. Universidad. <br> Sector la Providencia. Zona Postal 2115. <br> Municipio Santiago Mari&ntilde;o, Turmero. Edo. Aragua. Venezuela.
						</address>

					</div>

					<div class="col-xs-8 col-sm-4 col-md-4">
						<h3 class="widget-title">Contacto</h3>
						<p class="icon icon-envelop">
							<strong class="colored">E-mail San Joaquin:</strong> <a href="mailto:soporte.academico@uba.edu.ve">soporte.secretaria@uba.edu.ve</a>
						</p>
						<p class="icon icon-envelop">
							<strong class="colored">E-mail San Antonio:</strong> <a href="mailto:soporte.academico@uba.edu.ve">soporte.secretaria.sa@uba.edu.ve</a>
						</p>
						<p class="icon icon-envelop">
							<strong class="colored">E-mail Apure:</strong> <a href="mailto:soporte.academico@uba.edu.ve">soporte.secretaria.sf@uba.edu.ve</a>
						</p>
						<p class="icon icon-envelop">
							<strong class="colored">E-mail Pto. Ordaz:</strong> <a href="mailto:soporte.academico@uba.edu.ve">soporte.secretaria.po@uba.edu.ve</a>
						</p>
						<p class="icon icon-phone">
							<strong class="colored">L&iacute;nea Gratuita:</strong> 0.500.UBA.0000
							<br>
							<strong class="colored">M&aacute;ster:</strong> +58 243.265.00.11
							<br>
							<strong class="colored">Fax:</strong> +58 243.265.00.01
						</p>
					</div>

				</div>
				<div class="modal fade ventanamodal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
				  			<div class="modal-header">
				    			<h4 class="modal-title" id="myModalLabel">Tiempo Inactivo</h4>
				  			</div>
				  			<div class="modal-body">
				    			<h4>Se ha cerrado su sesi&oacute;n por tiempo de inactividad</h4>
				  			</div>
					  		<div class="modal-footer">
					    		<a class="btn btn-success" href="{{ URL::to('/')}}">Aceptar</a>
					  		</div>
						</div>
					</div>
				</div>
			</div> <!--/. Container -->
		</footer>
		<!--/. Footer -->
		<!-- Copyrights -->
		<div id="copyrights">

			<!-- Container -->
			<div class="container">

				<div class="row">

					<div class="col-xs-12 col-md-12 center">
						<p>Copyright © 2014 - Universidad Bicentenaria de Aragua. Todos los derechos reservados. <br>
						Sitio desarrollado por: <strong>Departamento de Inform&aacute;tica</strong></p>
					</div>
				</div>

			</div> <!--/. Container -->

		</div>
	</div>

		<!--/. Copyrights -->
		{{ HTML::script('js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') }}
		{{ HTML::script('js/vendor/jquery-1.10.1.min.js')}}
		{{ HTML::script('js/jquery.js')}}
		{{ HTML::script('js/bootstrap.min.js')}}
		{{ HTML::script('js/plugins/jquery.hoverIntent.minified.js')}}
		{{ HTML::script('js/main.js')}}
		{{ HTML::script('js/jquery.isotope.min.js')}}
		{{ HTML::script('js/jquery.flexslider-min.js')}}
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-59704303-1', 'auto');
		  ga('send', 'pageview');

		</script>
		@yield('postscript')
    </body>
</html>