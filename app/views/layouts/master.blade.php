<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ $title; }} | Universidad Bicentenaria de Aragua</title>
        <meta name="description" content="">   
        <meta name="viewport" content="width=device-width">
        {{ HTML::style('css/normalize.css')}}
        {{ HTML::style('css/main.css')}}
        {{ HTML::style('css/bootstrap.min.css')}}
        {{ HTML::style('css/bootstrap.css')}}        
        {{ HTML::style('css/estilo.css')}}
        @yield('css')
    	<link rel="shortcut icon" href="{{ URL::to('images/favicon.ico')}}">	
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
	<div id="conteiner">
		<!-- Master Header -->
		<header id="master-header">
			<div id="master-header-bg"></div>
			<div id="master-header-wrap">
				<!-- Logo -->
				<div id="logo" class="logo_uba"><a href="{{ URL::route('inicio')}}"><img style="background-color: white;" src="{{ URL::to('images/uba.png')}}" alt="" /></a></div>
				<!--  or, for logo image: <div id="logo"><a href="index.html"><img src="{{ URL::to('images/uba.png')}}" alt="" /></a></div> -->
				<!-- ..Logo -->
				<!-- Navi -->
				<nav id="master-nav">
					<ul>
						<li><a href="{{ URL::route('inicio')}}">Inicio</a></li>
						<li class="dropdown hassubmenu">
							<a style="pointer-events: none;" href="#">Servicios Acad&eacute;mico <b class="caret"></b></a>
							<ul>
								<li><a href="{{URL::to('servacade/informacion')}}">Solicitud Servicio</a></li>
								<li><a href="{{URL::to('servacade/historial')}}">Historial Solicitudes</a></li>
								<li><a href="{{URL::to('servacade/costodias')}}">Costos y D&iacute;as de Entrega</a></li>
							</ul>
						</li>
						<li class="dropdown hassubmenu">
							<a style="pointer-events: none;" href="#">Solvencias <b class="caret"></b></a>
							<ul>
								<li><a href="{{URL::to('solvenciabi/solvencia')}}">Bibliotecas</a></li>
								<li><a href="{{URL::to('solvenciabi/solvenciadmi')}}">Administrativo</a></li>
								<li><a href="{{URL::to('solvenciabi/solvenciadoc')}}">Documentos</a></li>
								<li><a href="{{URL::to('solvenciabi/solvenciareq')}}">Req. Grado</a></li>
							</ul>
						</li>
						<li class="dropdown hassubmenu">
							<a style="pointer-events: none;" href="#">Procesos Acad&eacute;micos <b class="caret"></b></a>
							<ul>
								<li><a style="pointer-events: none;" href="#">Inscripci&oacute;n en L&iacute;nea<b class="caret"></b></a>
									<ul>
										@if(Session::has('Es_nuevo') && Session::get('Es_nuevo')==0)
											<li><a href="{{URL::to('inscripcionr/inicioinscripcionreg')}}">Inscripci&oacute;n Regulares</a></li>
											@if(Session::get('cod_reg') == 0)
												<li><a href="{{URL::to('adicion/inicioadicionmateria')}}">Adici&oacute;n de Asignaturas</a></li>
												<li><a href="{{URL::to('cambio/iniciocambioseccion')}}">Cambios de Secci&oacute;n</a></li>
											@endif
										@endif
										@if(Session::has('Es_nuevo') && Session::get('Es_nuevo')==1 && Session::get('proce')==0) 
											<li><a href="{{URL::to('inscripcion/informacionnuevoingreso')}}">Inscripci&oacute;n Nuevo Ingreso</a></li>																					
										@endif
										@if(Session::has('Es_nuevo') && Session::get('Es_nuevo')==1 && Session::get('proce')>0 && Session::get('cod_pen')==9)
											<li><a href="{{URL::to('inscripcionr/inicioinscripcionreg')}}">Inscripci&oacute;n Nuevos por Equivalencia</a></li>										
										@endif
										@if(Session::has('Es_nuevo') && Session::get('Es_nuevo')==1 && Session::get('proce')>0 && Session::get('cod_pen')==2)
											<li><a href="{{URL::to('inscripcion/informacionnuevoingreso')}}">Inscripci&oacute;n Nuevo Ingreso</a></li>										
										@endif		
										@if(Session::has('Es_nuevo') && Session::get('Es_nuevo')==1 && Session::get('proce')>0 && Session::get('cod_pen')==3)
											<li><a href="{{URL::to('inscripcion/informacionnuevoingreso')}}">Inscripci&oacute;n Nuevo Ingreso</a></li>										
										@endif																			
										<li><a href="{{URL::to('proceso/inicioretiro')}}">Retiro de Asignaturas</a></li>
									</ul>
								</li>
								<li><a href="#">Intensivo<b class="caret"></b></a>
									<ul>
										<li><a href="{{URL::to('verano/oferta')}}">Oferta verano</a></li>
										<li><a href="{{URL::to('verano/inicio')}}">Solicitud Verano</a></li>
										<li><a href="{{URL::to('verano/historialverano')}}">Historial / Estatus Solicitud</a></li>
									</ul>

								<li><a style="pointer-events: none;" href="#">Niveles de Inform&aacute;tica<b class="caret"></b></a>
									<ul>
										<li><a href="{{URL::to('proceso/verificaresc')}}">Inscripci&oacute;n</a></li>
										<li><a href="{{URL::to('proceso/inscripcioninf')}}">Consultar Inscripci&oacute;n</a></li>
										<li><a href="{{URL::to('proceso/califiinformatica')}}">Calificaciones</a></li>
									</ul>
								</li>
								<li><a style="pointer-events: none;" href="#">Niveles de Idiomas<b class="caret"></b></a>
									<ul>
										<li><a href="{{URL::to('proceso/nivelidioma')}}">Inscripci&oacute;n</a></li>
										<li><a href="{{URL::to('proceso/inscripcionidioma')}}">Consultar Inscripci&oacute;n</a></li>
										<li><a href="{{URL::to('proceso/califiidioma')}}">Calificaciones</a></li>
									</ul>
								</li>
								<li><a href="{{ URL::to('pasantia/inicio')}}">Solicitud Pasant&iacute;a</a></li>
								<li><a href="{{URL::to('proceso/datosmencion')}}">Selecci&oacute;n de Menci&oacute;n</a></li>
							</ul>
						</li>
						<li class="dropdown hassubmenu">
							<a style="pointer-events: none;" href="#"><img src="{{ URL::to('fotos/fotoest',array('cedula'=>Session::get('cedula'))) }}" height="32" class="img-rounded"> {{ Session::get('nom_ape') }}<b class="caret"></b></a>
							<ul>
								<li><a href="{{URL::to('estudiante/datosest')}}">Datos</a></li>
								<li><a href="{{URL::to('estudiante/mostrarlapso')}}">Horario</a></li>
								<li><a href="{{URL::to('estudiante/mostrarplan')}}">Plan de Evaluaci&oacute;n</a></li>
								<li><a style="pointer-events: none;" href="#">Calificaciones<b class="caret"></b></a>
									<ul>
										<li><a href="{{URL::to('estudiante/notasparciales')}}">Parciales</a></li>
										<li><a href="{{URL::to('estudiante/iniciocalifi')}}">Definitivas</a></li>
									</ul>
								</li>
								<li><a href="#">Ayuda</a></li>
								<li><a href="{{ URL::to('exit') }}"><span class="glyphicon glyphicon-off"></span> Salir</a></li>
							</ul>
						</li>
					</ul>
				</nav>
				<!-- ..Navi -->
				<!-- Mobile Button -->
				<a href="#" id="mobile-switch"><span class="icon-menu"></span></a>
				<!-- ..Mobile Button -->
			</div>
		</header>
		<!-- ..Master Header -->
		<section>
		<!-- Section -->
		@yield('content')
		<!-- Section -->
		</section>
	</div>
	<div class="footer">
		
		<!-- Footer -->
		<footer id="master-footer" >

			<!-- Container -->
			<div class="container">

				<div class="row">

					<div class="col-xs-8 col-sm-4 col-md-4">

						<h3 class="widget-title">Sitios de Inter&eacute;s</h3>

						<div class="tagcloud">
							<a href="http://uba.edu.ve">UBA</a>
							<a href="http://uba-extension.com">Direcci&oacute;n de Extensi&oacute;n</a>
							<!-- <a href="#">Academia CISCO</a> -->
							<a href="http://aulavirtual.uba.edu.ve">Aula Virtual</a>
							<a href="http://blog.uba.edu.ve">Blog</a>
						</div>

					</div>

					<div class="col-xs-8 col-sm-4 col-md-4">

						<h3 class="widget-title">Direcci&oacute;n</h3>

						<address class="icon icon-location">
							Av. Intercomunal Santiago Mari&ntilde;o c/c Av. Universidad. <br> Sector la Providencia. Zona Postal 2115. <br> Municipio Santiago Mari&ntilde;o, Turmero. Edo. Aragua. Venezuela.
						</address>

					</div>

					<div class="col-xs-8 col-sm-4 col-md-4">
						<h3 class="widget-title">Contacto</h3>
						<p class="icon icon-envelop">
							<strong class="colored">E-mail San Joaquin:</strong> <a href="mailto:soporte.secretaria@uba.edu.ve">soporte.secretaria@uba.edu.ve</a>
						</p>
						<p class="icon icon-envelop">
							<strong class="colored">E-mail San Antonio:</strong> <a href="mailto:soporte.secretaria.sa@uba.edu.ve">soporte.secretaria.sa@uba.edu.ve</a>
						</p>
						<p class="icon icon-envelop">
							<strong class="colored">E-mail Apure:</strong> <a href="mailto:soporte.secretaria.sf@uba.edu.ve">soporte.secretaria.sf@uba.edu.ve</a>
						</p>
						<p class="icon icon-envelop">
							<strong class="colored">E-mail Pto. Ordaz:</strong> <a href="mailto:soporte.secretaria.po@uba.edu.ve">soporte.secretaria.po@uba.edu.ve</a>
						</p>
						<p class="icon icon-phone">
							<strong class="colored">L&iacute;nea Gratuita:</strong> 0.500.UBA.0000
							<br>
							<strong class="colored">M&aacute;ster:</strong> +58 243.265.00.11
							<br>
							<strong class="colored">Fax:</strong> +58 243.265.00.01
						</p>
					</div>

				</div>
				<div class="modal fade ventanamodal" id="myModalmaster" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
				  			<div class="modal-header">
				    			<h4 class="modal-title" id="myModalLabel">Tiempo Inactivo</h4>
				  			</div>
				  			<div class="modal-body">
				    			<h4>Se ha cerrado su sesi&oacute;n por tiempo de inactividad</h4>
				  			</div>
					  		<div class="modal-footer">
					    		<a class="btn btn-success" href="{{ URL::route('inicio')}}">Aceptar</a>
					  		</div>
						</div>
					</div>
				</div>
			</div> <!--/. Container -->
		</footer>
		<!--/. Footer -->
		<!-- Copyrights -->
		<div id="copyrights">

			<!-- Container -->
			<div class="container">

				<div class="row">

					<div class="col-xs-12 col-md-12 center">
						<p>Copyright © 2014 - Universidad Bicentenaria de Aragua. Todos los derechos reservados. <br>
						Sitio desarrollado por: <strong>Departamento de Inform&aacute;tica</strong></p>
					</div>
				</div>

			</div> <!--/. Container -->

		</div>
	</div>

		<!--/. Copyrights -->
		{{ HTML::script('js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') }}
		{{ HTML::script('js/vendor/jquery-1.10.1.min.js')}}
		{{ HTML::script('js/jquery.js')}}
		{{ HTML::script('js/bootstrap.min.js')}}
		{{ HTML::script('js/plugins/jquery.hoverIntent.minified.js')}}
		{{ HTML::script('js/main.js')}}
		{{ HTML::script('js/jquery.isotope.min.js')}}
		{{ HTML::script('js/jquery.flexslider-min.js')}}
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-59704303-1', 'auto');
		  ga('send', 'pageview');

		</script>

		<script type="text/javascript">
			var timeout;	
			timeout = setTimeout(function(){
				$('#master-header').css("z-index", '0');
				$('#myModalmaster').modal({
					 backdrop: 'static',
			  		keyboard: false
				});
			}, 600000);
			$('#myModalmaster').on('shown.bs.modal', function (e) {
				$.ajax({
					type: "GET",
					url: "{{URL::action('ExitController@getExit')}}"
				});
			})
			document.onmousemove = function(){
			  	clearTimeout(timeout);
			  	timeout = setTimeout(
			  	function(){
			  		$('#master-header').css("z-index", '0');
			  		$('#myModalmaster').modal({
			  			backdrop: 'static',
			  			keyboard: false
			  		});
			  	}, 600000);
			}
		</script>
		@yield('postscript')
    </body>
</html>
