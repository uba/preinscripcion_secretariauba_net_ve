<?php

Validator::extend('mayor_cero', function($attribute, $value, $parameters)
{
    return preg_match('/^[1-9]+/', $value);
    
});

Validator::extend('space', function($attribute, $value, $parameters)
{
	$valor = 1;
	for ($i=0; $i < strlen($value) ; $i++) { 
		if(preg_match('/\s/', $value[$i])) {
			$valor = 0;
		}
	}
    return $valor;
    
});