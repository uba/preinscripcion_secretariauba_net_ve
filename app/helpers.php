<?php
//funcion para obviar el tiempo por defecto de ejecuciÃ³n de un archivo php
function max_execution_time(){
    //cero igual a tiempo ilimitado
    return ini_set('max_execution_time', 0);
}

//funcion para aumentar el tamaño limite del archivo php generado
function memory_limit(){
    //cero igual a tiempo ilimitado
    return ini_set('memory_limit','512M');
}
//funcion para buscar el nombre de la escuela corto, parametro el codigo de la escuela
function nombreEscuela($cod_esc){
    //SQL para buscar la descripcion de la escuela
    $escuela=DB::table('escuelas')
        ->select('Facultad')
        ->where('cod_esc','=',$cod_esc)
        ->first();
    return $escuela->Facultad;
}

function nombreInstitutosuperior($cod){
    //SQL para buscar la descripcion de la escuela
    $estudiante=DB::table('instituciones')
        ->select('instituciones')
        ->where('id','=',$cod)
        ->first();
    return $estudiante->instituciones;
}

function nombreCarrerosuperior($cod){
    //SQL para buscar la descripcion de la escuela
    $estudiante=DB::table('carreras')
        ->select('carrera')
        ->where('id','=',$cod)
        ->first();
    return $estudiante->carrera;
}


//funcion para buscar el nombre del nucleo, parametro el codigo del nucleo
function nombreNucleo($cod_nuc){
    //SQL para buscar la descripcion del nucleo
    $nucleo=DB::table('nucleos')
        ->select('des_nucleo')
        ->where('ID','=',$cod_nuc)
        ->first();
    return $nucleo->des_nucleo;
}
//funcion para buscar el nombre del nucleo, parametro el codigo del nucleo
function estadoNucleo($cod_nuc){
    //SQL para buscar la descripcion del nucleo
    $nucleo=DB::table('nucleos')
        ->select('estado')
        ->where('ID','=',$cod_nuc)
        ->first();
    return $nucleo->estado;
}
//funcion para buscar la foto del estudiante
function buscarfoto($x){
    if (file_exists('../../zonaEstudiante/UBA/'.$x.'.JPG')) {
            
        $filename = '../../zonaEstudiante/UBA/'.$x.'.JPG';

        $degrees = -90;

        // Content type
        header('Content-type: image/jpeg');

        // Load
        $source = imagecreatefromjpeg($filename);

        // // Rotate
        $rotate = imagerotate($source, $degrees, 0);

        // Output
        $foto=imagejpeg($rotate);
    }else{
        $filename = public_path().'/images/sinfoto.jpg';

        // Content type
        header('Content-type: image/jpeg');

        // Load
        $source = imagecreatefromjpeg($filename);

        // Output
        $foto=imagejpeg($source);
    }
    return $foto;
}

function minuscula($x){
    $min=array('á','é','í','ó','ú','ñ');
    $may=array('Á','É','Í','Ó','Ú','Ñ');
    $acentos=str_replace($may, $min, $x);
    $minuscula=ucwords(mb_strtolower($acentos));
    return $minuscula;
}

/*funcion para traer el nombre y apellido del estudiante*/
function primerNom_ape($x){
    $nom_ape=explode(',', $x); 
    $apellidos=trim($nom_ape[0]);
    $nombres=trim($nom_ape[1]);
    $nombre=explode(' ', $nombres);
    $apellido=explode(' ', $apellidos);
    $nom_ape=minuscula($nombre[0]).' '.minuscula($apellido[0]);
    return $nom_ape;  
}

/*funcion para traer los apellidos del estudiante*/
function apellidos($x){
    $nom_ape=explode(',', $x);
    $apellidos=trim($nom_ape[0]);
    return $apellidos;
}

/*funcion para traer el nombre completo del estudiante*/
function nombres($x){
    $nom_ape=explode(',', $x);
    $nombres=trim($nom_ape[1]);
    return $nombres;
}

//Funcion para retornar nombre de la materia
function nombreMateria($x,$y){
    $mat=DB::table('materias')
        ->select('des_mat')
        ->where('cod_mat','=',$x)
        ->where('cod_nuc','=',$y)
        ->first();
    if(count($mat)==0){
        return 'Sin nombre';
    }else{
        return $mat->des_mat;
    }
}

function nombreServicio($x){
    $sql = DB::table('servicios_academicos')
        ->select('servicio')
        ->where('cod_servicio','=',$x)
        ->first();
    return $sql->servicio;
}

function tiempoServicio($x,$y){
    if ($y == 0)
    {
        $sql = DB::table('servicios_academicos')
        ->select('lapso_entrega')
        ->where('cod_servicio','=',$x)
        ->first();
    return $sql->lapso_entrega;
    }else{
        $sql = DB::table('servicios_academicos')
        ->select('observaciones')
        ->where('cod_servicio','=',$x)
        ->first();
    return $sql->observaciones;
    }
}

function tipoPago($x){
  if ($x == 2020){
    $tipo = 'Punto de Venta';}
  else{$tipo = 'Deposito Bancario';}
    return $tipo;
}
//FUNCION QUE VOLTEA EL NOMBRE DEL ESTUDIANTE A NOMBRE - APELLIDO
function trimEstudiante($x){
    $nombre_completo=explode(',', $x);            
    $apellido_trim=trim($nombre_completo[0]);
    $nombre_trim=trim($nombre_completo[1]);
    $nom_ape=$nombre_trim.' '.$apellido_trim;
    return $nom_ape;
}

//Funcion que retorna el numero de las unidades minimas para realizar pasantias
function ucPasantias ($x){
    if ($x==1){
        $uc=135;
    }
    elseif($x==2){
        $uc=134;
    }
    elseif($x==3){
        $uc=144;
    }
    elseif($x==4){
        $uc=154;
    }
    elseif($x==5){
        $uc=149;
    }
    elseif($x==6){
        $uc=165;
    }
    elseif($x==7){
        $uc=139;
    }
    return $uc; 
}

//Funcion que devuelve el semestre de ubicacion del estudiante
function semestreUbicacion($x){

    $sql=DB::table('estudiantes_inscripciones')
        ->where('cedula','=',$x)
        ->max('ubi_sem');

    if ($sql == 1) {
        return "1er Semestre";
    }
    elseif ($sql == 2) {
        return "2do Semestre";
    }
    elseif ($sql == 3) {
        return "3er Semestre";
    }
    elseif  ($sql == 4) {
        return "4to Semestre";
    }
    elseif ($sql == 5) {
        return "5to Semestre";
    }
    elseif ($sql == 6) {
        return "6to Semestre";
    }
    elseif ($sql == 7) {
        return "7mo Semestre";
    }
    elseif ($sql == 8) {
        return "8vo Semestre";
    }
    elseif ($sql == 9) {
        return "9no Semestre";
    }
    elseif ($sql == 10) {
        return "10mo Semestre";
    }
}

//Funcion para calcular las unidades de credito del estudiante
function UCaprobadas($cedulaest, $escuela, $pensum, $mencion, $nucleo){
    $uc=DB::table('calificaciones')
        ->where('CEDULA','=',$cedulaest)
        ->where('calificaciones.COD_ESC','=',$escuela)
        ->where('calificaciones.COD_PEN','=',$pensum)
        ->where('calificaciones.COD_REG','=',0)
        ->where('calificaciones.COD_NUC','=',$nucleo)
        ->where('calificaciones.COD_MEN','<=',$mencion)
        ->join('materias', function($join)
            {
                $join->on('calificaciones.COD_PEN', '=', 'materias.cod_pen')
                ->on('calificaciones.COD_MAT','=','materias.cod_mat')
                ->on('calificaciones.CALIFI','>=','materias.min_apr')
                ->on('calificaciones.COD_NUC','=','materias.cod_nuc');
            })
        ->sum('materias.uni_cre');
        
    return  $uc;
}

//funcion para buscar el nombre de la escuela largo, parametro el codigo de la escuela
function nombreEscuelalargo($cod_esc){
    //SQL para buscar la descripcion de la escuela
    $escuela=DB::table('escuelas')
        ->select('Facultad')
        ->where('cod_esc','=',$cod_esc)
        ->first();
    return $escuela->Facultad;
}

//funcion para transformar un string de minuscula a mayuscula
function mayuscula($x){
    $may=array('Á','Í','É','Ó','Ú','Ñ');
    $min= array('á','é','í­','ó','ú','ñ');           
    $acentos=str_replace($min, $may, $x);
    $mayuscula=mb_strtoupper($acentos);
    return $mayuscula;
}

//Funcion para transformar una fecha a letras
function fechaALetras($fecha){
  $fecha_separada=explode("/", $fecha);

  $dia= strtolower(numtoletras($fecha_separada[0]));
  
  switch ($fecha_separada[1]) {
    
    case "01":
        $mes="enero";
      break;
    case "02":
        $mes="febrero";
      break;
    case "03":
        $mes="marzo";
      break;
    case "04":
        $mes="abril";
      break;
    case "05":
        $mes="mayo";
      break;
    case "06":
        $mes="junio";
      break;
    case "07":
        $mes="julio";
      break;
    case "08":
        $mes="agosto";
      break;
    case "09":
        $mes="septiembre";
      break;
    case "10":
        $mes="octubre";
      break;
    case "11":
        $mes="noviembre";
      break;
    case "12":
        $mes="diciembre";
      break;

    default:
      break;
  }
  
  $anio= strtolower(numtoletras($fecha_separada[2]));
    if ($dia=='un') {
        return "al primer día del mes de $mes de $anio.";
    }else{
        return "a los $dia días del mes de $mes de $anio.";
    }
}

//Funcion para transformar numero a letras dentro de la funcion fechaAletras
function numtoletras($xcifra)
{
    $xarray = array(0 => "Cero",
        1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
        "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
        "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
        100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
    );
//
    $xcifra = trim($xcifra);
    $xlength = strlen($xcifra);
    $xpos_punto = strpos($xcifra, ".");
    $xaux_int = $xcifra;
    $xdecimales = "00";
    if (!($xpos_punto === false)) {
        if ($xpos_punto == 0) {
            $xcifra = "0" . $xcifra;
            $xpos_punto = strpos($xcifra, ".");
        }
        $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
        $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
    }
 
    $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
    $xcadena = "";
    for ($xz = 0; $xz < 3; $xz++) {
        $xaux = substr($XAUX, $xz * 6, 6);
        $xi = 0;
        $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
        $xexit = true; // bandera para controlar el ciclo del While
        while ($xexit) {
            if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
                break; // termina el ciclo
            }
 
            $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
            $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
            for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
                switch ($xy) {
                    case 1: // checa las centenas
                        if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
                             
                        } else {
                            $key = (int) substr($xaux, 0, 3);
                            if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                                $xseek = $xarray[$key];
                                $xsub = subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
                                if (substr($xaux, 0, 3) == 100)
                                    $xcadena = " " . $xcadena . " CIEN " . $xsub;
                                else
                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
                            }
                            else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
                                $key = (int) substr($xaux, 0, 1) * 100;
                                $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
                                $xcadena = " " . $xcadena . " " . $xseek;
                            } // ENDIF ($xseek)
                        } // ENDIF (substr($xaux, 0, 3) < 100)
                        break;
                    case 2: // checa las decenas (con la misma lógica que las centenas)
                        if (substr($xaux, 1, 2) < 10) {
                             
                        } else {
                            $key = (int) substr($xaux, 1, 2);
                            if (TRUE === array_key_exists($key, $xarray)) {
                                $xseek = $xarray[$key];
                                $xsub = subfijo($xaux);
                                if (substr($xaux, 1, 2) == 20)
                                    $xcadena = " " . $xcadena . " VEINTE " . $xsub;
                                else
                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                $xy = 3;
                            }
                            else {
                                $key = (int) substr($xaux, 1, 1) * 10;
                                $xseek = $xarray[$key];
                                if (20 == substr($xaux, 1, 1) * 10)
                                    $xcadena = " " . $xcadena . " " . $xseek;
                                else
                                    $xcadena = " " . $xcadena . " " . $xseek . " Y ";
                            } // ENDIF ($xseek)
                        } // ENDIF (substr($xaux, 1, 2) < 10)
                        break;
                    case 3: // checa las unidades
                        if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
                             
                        } else {
                            $key = (int) substr($xaux, 2, 1);
                            $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
                            $xsub = subfijo($xaux);
                            $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                        } // ENDIF (substr($xaux, 2, 1) < 1)
                        break;
                } // END SWITCH
            } // END FOR
            $xi = $xi + 3;
        } // ENDDO
 
        if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
            $xcadena.= " DE";
 
        if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
            $xcadena.= " DE";
 
        // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
        if (trim($xaux) != "") {
            switch ($xz) {
                case 0:
                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                        $xcadena.= "UN BILLON ";
                    else
                        $xcadena.= " BILLONES ";
                    break;
                case 1:
                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                        $xcadena.= "UN MILLON ";
                    else
                        $xcadena.= " MILLONES ";
                    break;
                case 2:
                    if ($xcifra < 1) {
                        //$xcadena = "CERO PESOS $xdecimales/100 M.N.";
                    }
                    if ($xcifra >= 1 && $xcifra < 2) {
                        //$xcadena = "UN PESO $xdecimales/100 M.N. ";
                    }
                    if ($xcifra >= 2) {
                        //$xcadena.= " PESOS $xdecimales/100 M.N. "; //
                    }
                    break;
            } // endswitch ($xz)
        } // ENDIF (trim($xaux) != "")
        // ------------------      en este caso, para México se usa esta leyenda     ----------------
        $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
        $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
        $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
        $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
        $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
    } // ENDFOR ($xz)
    return trim($xcadena);
}
 
//Funcion para generar el subfijo dentro de la funcion fechaAletras
function subfijo($xx)
{ // esta función regresa un subfijo para la cifra
    $xx = trim($xx);
    $xstrlen = strlen($xx);
    if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
        $xsub = "";
    //
    if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
        $xsub = "MIL";
    //
    return $xsub;
}

//FUNCION PARA BUSCAR EL NOMBRE DE LA EMPRESA EN LA QUE EL ESTUDIANTE ESTA ASIGNADO
function nombreInstitucion($x){
    $consulta=DB::table('empresas')
        ->where('codEmp','=',$x)
        ->first();

    if (count($consulta)==0) {
        $msj='Instituci&oacute;n por asignar';
    }else{
        $msj=$consulta->nombre;
    }
    return $msj;
}

//FUNCION PARA BUSCAR EL NOMBRE DEL AREA EN LA QUE EL ESTUDIANTE ESTA ASIGNADO
function nombreArea($x){
    $consulta=DB::table('areas_pasantias')
        ->where('codigo','=',$x)
        ->first();

    if (count($consulta)==0) {
        $msj='&Aacute;rea por asignar';
    }else{
        $msj=$consulta->area;
    }
    return $msj;
}

//FUNCION PARA BUSCAR EL ESTATUS QUE SE ENCUENTRA LA PASANTIA DEL ESTUDIANTE
function nombreEstatus($x){
    $lapso=DB::table('lapso_fechas')
            ->where('activo','=',1)
            ->first();

    $consulta=DB::table('pasantes')
        ->where('cedula','=',$x)
        ->where('lapso','=',$lapso->lapso)
        ->first();

    if ($consulta->cod_estatus == 0) {
        $msj='En espera de aprobaci&oacute;n';
    }else{
        $msj=$consulta->area;
    }
    return $msj;
}

// FUNCION PARA VERIFICAR EN LA TABLA config_web
function config(){    
    //SQL para verificar si esta activo y el lapso
    $sqlConfig = DB::select('SELECT * FROM config_web WHERE cod_modulo="RMSEM09" LIMIT 0,1');
        
    if (count($sqlConfig)>=1){
        $materia = array();
        foreach ($sqlConfig as $key => $value){
            $arreglo = new stdClass();

            $arreglo->id = $value->id;
            $arreglo->modulo = $value->cod_modulo;
            $arreglo->estado = $value->estado;
            $arreglo->lapso = $value->lapso;
            $arreglo->lapso_letra = $value->lapso_letra;
            $arreglo->fecha_ini = $value->fecha_ini;
            $arreglo->fecha_fin = $value->fecha_fin;
            $arreglo->observacion = $value->observacion;
            $arreglo->escuela_aplica = $value->escuela_aplica;

            $materia[] = $arreglo;
        }      
      return ($materia);      
    }
}

function ConfigWeb(){
    $sqlrei = DB::select('SELECT * FROM config_web WHERE cod_modulo="SEMVERA" LIMIT 0,1');
    if(count($sqlrei)>=1){
        $materia = array();
        foreach ($sqlrei as $key => $value){
            $arreglo = new stdClass();

            $arreglo->id = $value->id;
            $arreglo->modulo = $value->cod_modulo;
            $arreglo->estado = $value->estado;
            $arreglo->lapso = $value->lapso;
            $arreglo->lapso_letra = $value->lapso_letra;
            $arreglo->fecha_ini = $value->fecha_ini;
            $arreglo->fecha_fin = $value->fecha_fin;
            $arreglo->observacion = $value->observacion;
            $arreglo->escuela_aplica = $value->escuela_aplica;
            $arreglo->ultimo_lapso_reg_activo = $value->ultimo_lapso_reg_activo;

            $materia[] = $arreglo;
        }      
      return ($materia);      
    }
}

//FUNCION PARA SABER LAS MATERIAS QUE TIENE INSCRITA EN EL PROCESO DE RETIRO DE MATERIAS
function materiasInscritas($cedula, $escuela, $pensum, $nucleo, $lapso){

    $sqlArreglo = DB::select('SELECT DISTINCT COD_MAT, (SELECT des_mat FROM materias WHERE cod_mat=inscripciones.cod_mat LIMIT 0,1) as DES_MAT, (SELECT uni_cre FROM materias WHERE cod_mat=inscripciones.cod_mat LIMIT 0,1) as UNI_CRE FROM inscripciones WHERE cedula=? AND cod_esc=? AND lapso=? AND cod_nuc=? AND cod_pen=? AND NOT EXISTS(SELECT * FROM materias_retiradas WHERE cedula=inscripciones.cedula AND cod_mat=inscripciones.cod_mat AND temp<>3 AND lapso=inscripciones.lapso)',array($cedula,$escuela,$lapso,$nucleo,$pensum));

    $materias = array();
    if (count($sqlArreglo)>=1){
            foreach ($sqlArreglo as $key => $value) {
            $arreglo = new stdClass();

            $arreglo->COD_MAT = $value->COD_MAT;
            $arreglo->DES_MAT = $value->DES_MAT;
            $arreglo->UNI_CRE = $value->UNI_CRE;

            $materias[] = $arreglo;
        }
        return ($materias);
    }
}


function Bsc_monto_pago($cedula, $lapso, $tipo) {
    $query = DB::select('SELECT monto as pago,(select monto from costos_procesos where id=1) as monto_proceso FROM solvencias WHERE cedula=? and lapso=? and tipo=?',array($cedula, $lapso, $tipo));
    
        if (count($query)>0) {
            foreach ($query as $key => $value) {
                $pago = $value->pago;
                $montop = $value->monto_proceso;
            }
            return $pago;
        }
    return 0;
}

function Bsc_Cant_Materias_Temporal($cedula,$lapso_actual) {
    $total = 0;
    $sql=DB::select('SELECT count(*) as total FROM adicion_temporal WHERE cedula = ? and lapso = ?',array($cedula,$lapso_actual));
    if (count($sql)>0) {
        foreach ($sql as $key => $value) {
            $total = $value->total;
        }
        return $total;
    }
    return $total;
}

function Bsc_Serial_Insc($cedula,$lapso) {
    
    $sql = DB::select('SELECT id,serial,fecha from inscripcion_ratificacion where cedula = ? and lapso = ?',array($cedula,$lapso));

    $seriales = array();
    foreach ($sql as $key => $value) {
            if(count($sql)>0){
                $fila = new stdClass();

                $fila->id = $value->id;
                $fila->serial = $value->serial;
                $fila->fecha = $value->fecha;

                $seriales[] = $fila;
            }
    }    
    return  ($seriales);
}

function indiceAC($cod_nuc,$cod_pen,$cod_esc,$cedula){

    //SQL para calcular el indice
    $sql_indice= DB::select('SELECT ((SUM(producto)) / ((SELECT SUM(uni_cre) AS total FROM materias WHERE materias.E'.$cod_esc.'>0 And (cod_nuc = ?) And (cod_pen = ?) And Exists (SELECT max(cod_mat) From calificaciones WHERE cedula = ? AND cod_mat = materias.cod_mat  AND cod_esc= ? AND cod_pen = materias.cod_pen AND cod_nuc = materias.cod_nuc AND  COD_TIP<>4 GROUP BY cod_mat)))) AS IA FROM (SELECT MAX(calificaciones.CALIFI) * materias.uni_cre AS producto FROM  calificaciones INNER JOIN materias ON calificaciones.COD_PEN = materias.cod_pen AND calificaciones.COD_MAT = materias.cod_mat AND calificaciones.cod_nuc = materias.cod_nuc WHERE (calificaciones.CEDULA = ?) AND (calificaciones.COD_PEN = ?) AND (calificaciones.COD_ESC = ?) AND (calificaciones.COD_NUC = ?) AND (calificaciones.COD_TIP <> 4) AND (materias.E'.$cod_esc.' > 0) GROUP BY materias.cod_mat, materias.uni_cre) producto',array($cod_nuc,$cod_pen,$cedula,$cod_esc,$cedula,$cod_pen,$cod_esc,$cod_nuc));

    $indice = array();
    if(count($sql_indice)>0){
    foreach ($sql_indice as $key => $value) {
        $fila = new stdClass();

        $fila->IA = $value->IA;

        $indice[]=$fila;
    }
    return ($indice);
    }else{
        return -1;
    }


}
//FUNCIONES DE ADICION DE MATERIAS
function MateriasAdicion($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso, $lapso_ingreso,$traerMaterias=false,$condicion){
     //valido que los datos esten llenos y sean numericos
    $sqlBajaCupAproAd = DB::select('UPDATE secciones SET cup_apro=cup_apro-1 WHERE EXISTS (SELECT cod_mat FROM adicion_temporal WHERE cedula=? AND COD_MAT=secciones.COD_MAT AND SEC_MAT=secciones.SEC_MAT AND lapso=? AND cod_nuc=? AND cod_esc=?) AND LAPSO=? AND cod_nuc=?',array($cedula,$lapso,$nucleo,$escuela,$lapso,$nucleo));


    //Borro la tabla temporal de la inscripcion
    if($condicion == 0)
    {
        $sqlEliminarAdicionTemporal = DB::table('adicion_temporal')
        ->where('cedula','=',$cedula)
        ->where('lapso','=',$lapso)
        ->where('procesado','=',0)
        ->delete();
    }

    $sqlMateriassAdicion = DB::select('SELECT cedula,cod_mat,des_mat,lapso,cod_esc,cod_sem,uni_cre FROM codigos WHERE cedula = ? AND lapso = ? AND (NOT cod_mat IN (SELECT cod_mat FROM inscripciones WHERE cedula = ? AND lapso = ?)) AND (NOT cod_mat IN (SELECT cod_mat FROM adicion_temporal WHERE cedula = ? AND lapso = ?)) ORDER BY cod_sem, cod_mat',array($cedula,$lapso,$cedula,$lapso,$cedula,$lapso));

    $numero_filas = count($sqlMateriassAdicion);
    $arreglo = array();
    if ($numero_filas>0){
        foreach ($sqlMateriassAdicion as $key => $value) {
            $materiasA = new stdClass();

            $materiasA->cedula = $value->cedula;
            $materiasA->cod_mat = $value->cod_mat;
            $materiasA->des_mat = $value->des_mat;
            $materiasA->cod_sem = $value->cod_sem;
            $materiasA->uni_cre = $value->uni_cre;

            $arreglo[] = $materiasA;
        }
        return ($arreglo);                  
    }else{
        return 0;
    }
}
//FUNCION PARA MOSTRAR EL HORARIO INSCRITO EN (VISTA:ADICION)
function HorarioInscrito($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso){

    $sqlTablaMateriasInscritas = DB::select('SELECT DISTINCT i.cedula, i.cod_mat,m.des_mat,i.sec_mat,i.cod_esc,i.cod_pen,i.cod_nuc,m.uni_cre,m.sem_mat,s.DIA1,s.DIA2 FROM secciones s INNER JOIN horamate h on h.COD_MAT=s.COD_MAT and h.LAPSO=s.lapso and h.SEC_MAT=s.SEC_MAT and s.cod_nuc= h.COD_NUC INNER JOIN inscripciones i on i.lapso=s.lapso and i.cod_mat=s.COD_MAT INNER JOIN materias m on m.cod_mat=s.COD_MAT AND m.cod_nuc=s.cod_nuc AND m.cod_pen=i.cod_pen AND m.cod_nuc=i.cod_nuc WHERE s.lapso=? and s.SEC_MAT=i.sec_mat AND s.COD_MAT=i.cod_mat and cedula=? ORDER BY sem_mat,cod_mat',array($lapso,$cedula));
    $total_uc=0;
    $i = 0;
    $HorarioLins = array();

    if (count($sqlTablaMateriasInscritas)>0){

        foreach ($sqlTablaMateriasInscritas as $key => $value) {
            $HorarioL = new stdClass();

            $HorarioL->cod_mat = $value->cod_mat;
            $HorarioL->des_mat = $value->des_mat;
            $HorarioL->sec_mat = $value->sec_mat;
            $HorarioL->sem_mat = $value->sem_mat;
            $HorarioL->uni_cre = $value->uni_cre;
            $HorarioL->DIA1 = $value->DIA1;
            $HorarioL->DIA2 = $value->DIA2;
            $total_uc+= $value->uni_cre;
            $HorarioL->suma = $total_uc;
            $HorarioLins[] = $HorarioL;
        }
        return ($HorarioLins);
    }
    return 0;    
}
//FUNCION PARA MOSTRAR ELECTIVAS DE ADICION
function ElectivasAdicion($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso){

    $sqlElectivasAdicion = DB::select('SELECT DISTINCT cedula,cod_mat,des_mat,lapso,cod_esc,cod_sem,uni_cre FROM codigos_electivas WHERE cedula = ? AND lapso = ? AND (NOT cod_mat IN (SELECT cod_mat FROM inscripciones WHERE cedula = ? AND lapso = ?)) AND (NOT cod_mat IN (SELECT cod_mat FROM adicion_temporal WHERE cedula = ? AND lapso = ?)) AND EXISTS (SELECT cod_mat FROM secciones WHERE cod_mat=codigos_electivas.cod_mat AND lapso=codigos_electivas.lapso AND cod_nuc=codigos_electivas.cod_nuc) ORDER BY cod_sem, cod_mat',array($cedula,$lapso,$cedula,$lapso,$cedula,$lapso));
    $electivaadi = array();
    if (count($sqlElectivasAdicion)>0){
        foreach ($sqlElectivasAdicion as $key => $value) {
            $arreglo = new stdClass();
            $arreglo->cedula = $value->cedula;
            $arreglo->cod_mat = $value->cod_mat;
            $arreglo->des_mat = $value->des_mat;
            $arreglo->cod_sem = $value->cod_sem;
            $arreglo->uni_cre = $value->uni_cre;

            $electivaadi[]=$arreglo;
        }                  
        return $electivaadi;                  
    }else{
      return 0;
    }        
}

//FUNCION PARA MOSTRAR SI CUMPLE CON LOS REQUISITOS PARA LA ADICION
function RequisitosAdicion($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso){
    if (!is_numeric($mencion) || !is_numeric($escuela) || !is_numeric($pensum) || !is_numeric($regimen) || !is_numeric($lapso)){
        return false;
        }else{
         $sqlRequisitos = DB::select('SELECT cedula,cod_mat,des_mat,lapso,cod_esc,cod_sem FROM codigos_requisitos WHERE cedula = ? AND lapso = ? AND (NOT cod_mat IN (SELECT cod_mat FROM inscripciones WHERE cedula = ? AND lapso = ?)) AND (NOT cod_mat IN (SELECT cod_mat FROM adicion_temporal WHERE cedula = ? AND lapso = ?)) ORDER BY cod_sem, cod_mat',array($cedula,$lapso,$cedula,$lapso,$cedula,$lapso));
         $numero_filas1 = count($sqlRequisitos);
         $arreglo = array();
         if ($numero_filas1>=1){
            foreach ($sqlRequisitos as $key => $value) {
                $requisitoadi = new stdClass();
                $requisitoadi->cedula = $value->cedula;
                $requisitoadi->cod_mat = $value->cod_mat;
                $requisitoadi->des_mat = $value->des_mat;
                $requisitoadi->cod_sem = $value->cod_sem;

                $arreglo[]=$requisitoadi;
            }
            return ($arreglo);
        }else{
            return 0;
        }
    }
}
// *************** En caso de los Solventes Con monto de pago cero (0) ***************
function MateriasAdicionSolvente($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso, $lapso_ingreso,$traerMaterias=false){
//valido que los datos esten llenos y sean numericos
    if (!is_numeric($mencion) || !is_numeric($escuela) || !is_numeric($pensum) || !is_numeric($regimen) || !is_numeric($lapso)){
        return false;
    }else{
            $sqlMateriassAdicionSol = DB::select('SELECT cedula,cod_mat,des_mat,lapso,cod_esc,cod_sem,uni_cre FROM codigos WHERE cedula = ? AND lapso = ? AND (NOT cod_mat IN (SELECT cod_mat FROM inscripciones WHERE cedula = ? AND lapso = ?)) AND (NOT cod_mat IN (SELECT cod_mat FROM adicion_temporal WHERE cedula = ? AND lapso = ?)) AND cod_mat IN ("SCOM01","SCOM02","SCOM03","SCOM04","SCOM05","SCOM06","SCOM07") ORDER BY cod_sem, cod_mat',array($cedula,$lapso,$cedula,$lapso,$cedula,$lapso));

            $numero_filas = count($sqlMateriassAdicionSol);
            $arreglo = array();
                if ($numero_filas >= 1){
                    foreach ($sqlMateriassAdicionSol as $key => $value) {
                        $adicion_solvente = new stdClass();

                        $adicion_solvente->cedula = $value->cedula;
                        $adicion_solvente->cod_mat = $value->cod_mat;
                        $adicion_solvente->des_mat = $value->des_mat;
                        $adicion_solvente->cod_sem = $value->cod_sem;
                        $adicion_solvente->uni_cre = $value->uni_cre;

                        $arreglo[] = $adicion_solvente;
                    }
                  return ($arreglo);
                }else{
                    return 0;
                }
        }
}
//ADICION DE ELECTIVAS EN EL CASO DE LOS SOLVENTES
function ElectivasAdicionSolvente($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso){

    $sqlElectivasAdicion = DB::select('SELECT cedula,cod_mat,des_mat,lapso,cod_esc,cod_sem,uni_cre FROM codigos_electivas WHERE cedula = ? AND lapso = ? AND (NOT cod_mat IN (SELECT cod_mat FROM inscripciones WHERE cedula = ? AND lapso = ?)) AND (NOT cod_mat IN (SELECT cod_mat FROM adicion_temporal WHERE cedula = ? AND lapso = ?)) ORDER BY cod_sem, cod_mat',array($cedula,$lapso,$cedula,$lapso,$cedula,$lapso));
    $arreglo = array();
    if (count($sqlElectivasAdicion)>0){
        foreach ($sqlElectivasAdicion as $key => $value) {
            $electivaadisol = new stdClass();
            $electivaadisol->cedula = $value->cedula;
            $electivaadisol->cod_mat = $value->cod_mat;
            $electivaadisol->des_mat = $value->des_mat;
            $electivaadisol->cod_sem = $value->cod_sem;
            $electivaadisol->uni_cre = $value->uni_cre;

            $arreglo[]=$electivaadisol;
        }
        return ($arreglo);
    }else{
        return 0;
    }
}
//FUNCION DE ELECTIVAS REQUISITOS DE ADICION CASO DE LOS SOLVENTES
function RequisitosAdicionSolvente($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso){
    if (!is_numeric($mencion) || !is_numeric($escuela) || !is_numeric($pensum) || !is_numeric($regimen) || !is_numeric($lapso)){
        return false;
    }else{
        $sqlRequisitosSol = DB::select('SELECT cedula,cod_mat,des_mat,lapso,cod_esc,cod_sem FROM codigos_requisitos WHERE cedula = ? AND lapso = ? AND (NOT cod_mat IN (SELECT cod_mat FROM inscripciones WHERE cedula = ? AND lapso = ?)) AND (NOT cod_mat IN (SELECT cod_mat FROM adicion_temporal WHERE cedula = ? AND lapso = ?)) AND cod_mat IN ("DHP000","DHP200","DHP300") ORDER BY cod_sem, cod_mat',array($cedula,$lapso,$cedula,$lapso,$cedula,$lapso));

        $numero_filas1 = count($sqlRequisitosSol);
        $arreglo = array();
        if ($numero_filas1>=1){
            foreach ($sqlRequisitosSol as $key => $value) {
                $requisitoadisol = new stdClass();
                $requisitoadisol->cedula = $value->cedula;
                $requisitoadisol->cod_mat = $value->cod_mat;
                $requisitoadisol->des_mat = $value->des_mat;
                $requisitoadisol->cod_sem = $value->cod_sem;

                $arreglo[]=$requisitoadisol;
            }
            return ($arreglo);
        }else{
            return 0;
        }
    }
}
//FUNCION PARA LOS COSTOS DE LOS PROCESOS DE ADICION
function CostosProcesosAdicion(){
    $sqlCostos = DB::table('costos_procesos')
        ->where('id','=',1)
        ->where('vigente','=',1)
        ->get();

    $numero_filas1 = count($sqlCostos);
    $costos = array();
        if ($numero_filas1>=1){
            foreach ($sqlCostos as $key => $value) {
                $costosadicion = new stdClass();
                $costosadicion->id = $value->id;
                $costosadicion->descripcion = $value->descripcion;
                $costosadicion->monto = $value->monto;
                $costosadicion->vigente = $value->vigente;

                $costos[]=$costosadicion;
            }
         return ($costos);
        }else{
            return false;
        }
}

function FechasLapso() {
    $fechas = array(); 
    $sql= DB::table('lapso_fechas')
        ->get();
    
       if (count($sql)>0) {
        foreach ($sql as $key => $value) {
            $fecha = new stdClass();

            $fecha->lapso = $value->lapso;
            $fecha->lapso_letra = $value->lapso_letras;
            $fecha->fecha_i_inscripcion = $value->fecha_i_inscripcion;
            $fecha->fecha_f_inscripcion = $value->fecha_f_inscripcion;
            $fecha->fecha_rezagados = $value->fecha_rezagados;
            $fecha->fecha_i_nuevos_equiv_reinc = $value->fecha_i_nuevos_equiv_reinc;
            $fecha->fecha_i_adicion_cambio = $value->fecha_i_adicion_cambio;
            $fecha->fecha_f_adicion_cambio = $value->fecha_f_adicion_cambio;
            $fecha->fecha_i_retiro_asignatura = $value->fecha_i_retiro_asignatura;
            $fecha->fecha_f_retiro_asignatura = $value->fecha_f_retiro_asignatura;

            $fechas[] = $fecha;
        }
        return $fechas;
    }else{
        return -1;
    }   

}

//Clase para el proceso de inscripcion de estudiantes regulares
function proyeccion($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso, $lapso_ingreso,$traerMaterias=false,$proce,$cod_origen){
      //valido que los datos esten llenos y sean numericos
  if (!is_numeric($mencion) || !is_numeric($escuela) || !is_numeric($pensum) || !is_numeric($regimen) || !is_numeric($lapso)) {
    return false;
}else{

        /*Pasos:
        1-Materias que puede cursar validando prelaciones
          1.1-indicando materias obligatorias
        2-Electivas
        3-Requisitos
        4-Ingresar las materias que sean separadas en codigos
        5-Elimino las materias que estan por encima de 4 semestres
        */
        
        //Antes de todo limpio la tabla de codigos, codigos_electivas, codigos_requisitos, donde la cedula sea la del estudiante
        //Bajo los contadores
        $restarsecciones=DB::select('UPDATE secciones SET cup_apro=cup_apro-1 WHERE EXISTS (SELECT cod_mat FROM inscripciones_temporal WHERE cedula=? AND COD_MAT=secciones.COD_MAT AND SEC_MAT=secciones.SEC_MAT AND lapso=? AND cod_nuc=? AND cod_esc=?) AND LAPSO=? AND cod_nuc=?',array($cedula,$lapso,$nucleo,$escuela,$lapso,$nucleo));

        //Borro la tabla temporal de la inscripcion
        $sqlEliminarInscripcionTemporal = DB::table('inscripciones_temporal')
            ->where('cedula','=',$cedula)
            ->delete();
        
        $sqlEliminarProyeccion = DB::table('codigos')
            ->where('cedula','=',$cedula)
            ->delete();
        
        $sqlEliminarProyeccion = DB::table('codigos_electivas')
            ->where('cedula','=',$cedula)
            ->delete();
        
        $sqlEliminarProyeccion = DB::table('codigos_requisitos')
            ->where('cedula','=',$cedula)
            ->delete();

        //1-Materias que puede cursar validando prelaciones
        $sqlCodigos = DB::insert('INSERT INTO codigos (cedula, cod_mat, des_mat, uni_cre, opcional, electiva, cod_pen, cod_reg, cod_nuc, cod_esc, lapso, cod_sem, cod_men)
           SELECT ? as CEDULA, COD_MAT,  (SELECT des_mat From materias WHERE cod_mat = pensum.cod_mat LIMIT 0,1) as DES_MAT, UC, 1, 0, ?, ?, ?, ?, ?, pensum.cod_sem as COD_SEM, ?
           FROM pensum
           WHERE (COD_PEN = ?)
           AND (COD_ESC = ?)
           AND (COD_MEN = ? OR COD_MEN = 0)
           AND (PREUC <= (SELECT SUM(materias.uni_cre) AS TotalAc
            FROM calificaciones INNER JOIN materias
            ON calificaciones.COD_PEN = materias.cod_pen
            AND calificaciones.COD_MAT = materias.cod_mat
            AND calificaciones.CALIFI >= materias.min_apr
            AND calificaciones.COD_NUC = materias.cod_nuc
            WHERE  (calificaciones.CEDULA = ?)
            AND (calificaciones.COD_ESC = ?)
            AND (calificaciones.COD_PEN = ?)
            AND (calificaciones.COD_REG = ?)
            AND (calificaciones.COD_NUC = ?)
            AND (calificaciones.COD_MEN = ? OR calificaciones.COD_MEN=0)) OR PREUC = 0)
            AND (NOT EXISTS (SELECT cod_mat
              FROM calificaciones
              WHERE cod_Esc = ?
              AND cedula = ?
              AND cod_mat = pensum.cod_mat
              AND califi >=(SELECT DISTINCT min_apr
                FROM materias
                WHERE cod_mat = calificaciones.cod_mat LIMIT 0,1)))
            AND (EXISTS ((SELECT cod_mat
              FROM calificaciones
              WHERE cedula = ?
              AND cod_esc = ?
              AND cod_pen = pensum.cod_pen
              AND (cod_men = ?  or cod_men=0)
              AND ((cod_mat = pensum.prel1) AND califi >= (SELECT DISTINCT min_apr
                  FROM  materias
                  WHERE cod_mat = calificaciones.cod_mat)))) OR pensum.prel1="")
                AND (EXISTS ((SELECT cod_mat
                FROM calificaciones
                WHERE cedula = ?
                AND cod_esc = ?
                AND cod_pen = pensum.cod_pen
                AND (cod_men = ?  or cod_men=0)
                AND ((cod_mat = pensum.prel2) AND califi >=(SELECT DISTINCT min_apr
                  FROM materias WHERE cod_mat = calificaciones.cod_mat)))) OR pensum.prel2="")
                AND (EXISTS ((SELECT cod_mat
                FROM calificaciones
                WHERE cedula = ?
                AND cod_esc = ?
                AND cod_pen = pensum.cod_pen
                AND (cod_men = ?  or cod_men=0)
                AND ((cod_mat = pensum.prel3) AND califi >= (SELECT DISTINCT min_apr
                  FROM materias WHERE cod_mat = calificaciones.cod_mat)))) OR pensum.prel3="")
                AND (EXISTS ((SELECT cod_mat FROM  calificaciones WHERE cedula = ? AND cod_esc = ?  AND cod_pen = pensum.cod_pen AND (cod_men = ?  or cod_men=0) AND ((cod_mat = pensum.prel4) AND califi >= (SELECT DISTINCT min_apr FROM materias WHERE cod_mat = calificaciones.cod_mat)))) OR pensum.prel4="") AND (EXISTS ((SELECT cod_mat FROM calificaciones WHERE cedula = ? AND cod_esc = ? AND cod_pen = pensum.cod_pen AND (cod_men = ?  or cod_men=0) AND ((cod_mat = pensum.prel5) AND califi >= (SELECT DISTINCT min_apr FROM materias WHERE cod_mat = calificaciones.cod_mat)))) OR pensum.prel5="") AND (EXISTS ((SELECT cod_mat FROM calificaciones WHERE cedula = ? AND cod_esc = ?
                AND cod_pen = pensum.cod_pen AND (cod_men = ? or cod_men=0) AND ((cod_mat = pensum.prel6) AND califi >= (SELECT DISTINCT min_apr FROM materias WHERE cod_mat = calificaciones.cod_mat)))) OR pensum.prel6="") AND (EXISTS ((SELECT cod_mat FROM calificaciones WHERE cedula = ? AND cod_esc = ?  AND cod_pen = pensum.cod_pen AND (cod_men = ? or cod_men=0) AND ((cod_mat = pensum.prel7) AND califi >= (SELECT DISTINCT min_apr FROM materias WHERE cod_mat = calificaciones.cod_mat)))) OR pensum.prel7="") AND (EXISTS ((SELECT cod_mat FROM calificaciones WHERE cedula = ? AND cod_esc = ? AND cod_pen = pensum.cod_pen AND (cod_men = ? or cod_men=0) AND ((cod_mat = pensum.prel8) AND califi >= (SELECT DISTINCT min_apr FROM  materias WHERE cod_mat = calificaciones.cod_mat)))) OR pensum.prel8="") ORDER BY COD_SEM, COD_MAT',array($cedula,$pensum,$regimen,$nucleo,$escuela,$lapso,$mencion,$pensum,$escuela,$mencion,
                $cedula,$escuela,$pensum,$regimen,$nucleo,$mencion,$escuela,$cedula,$cedula,$escuela,$mencion,$cedula,$escuela,$mencion,$cedula,$escuela,$mencion,$cedula,$escuela,$mencion,$cedula,$escuela,$mencion,$cedula,$escuela,$mencion,$cedula,$escuela,$mencion,$cedula,$escuela,$mencion));

                
        /****Borramos los codigos de las electivas repitidas en calificaciones***/
        $sqlEliminaElectivas = DB::select('DELETE FROM codigos WHERE (cedula = ?) AND (cod_esc = ?) AND (cod_pen = ?)  AND EXISTS (SELECT  * FROM  electivas WHERE  cod_gen = codigos.cod_mat AND EXISTS (SELECT * FROM calificaciones WHERE cedula = codigos.cedula AND cod_esc = codigos.cod_esc AND cod_mat = electivas.cod_mat))',array($cedula,$escuela,$pensum));             

        //1.1 indicando materias que son OBLIGATORIAS
        //Verificamos el DAU si la escuela es diferente de 6 y el lapso es >=20012
        $Es_nuevo = Session::get('Es_nuevo');

            if ($lapso_ingreso>=20141 && $escuela!=6 && $Es_nuevo!=1){
                $sqlDHP = DB::select('UPDATE codigos SET opcional=1, cod_sem=5 WHERE cedula=? AND LEFT(cod_mat,3)="DAU" AND lapso=? AND cod_esc=?',array($cedula,$lapso,$escuela));          
            }elseif ($lapso_ingreso<20141 && $escuela!=6 && $Es_nuevo!=1){
                $sqlDHP = DB::select('UPDATE codigos SET opcional=1, cod_sem=7 WHERE cedula=? AND LEFT(cod_mat,3)="DAU" AND lapso=? AND cod_esc=?',array($cedula,$lapso,$escuela));
            }elseif($escuela==6 && $Es_nuevo!=1 && $pensum == 9){
                $sqlDHP = DB::select('DELETE FROM codigos WHERE cedula=? AND cod_mat="ECS000" AND lapso=? AND cod_esc=?',array($cedula,$lapso,$escuela));            
            }


        //Verificamos el DAU si la escuela es 5 y el lapso es <20021
        if ($lapso_ingreso<20021 && $escuela==5){
            $sqlDHP = DB::select('DELETE FROM codigos WHERE cedula=? AND LEFT(cod_mat,3)="DAU" AND lapso=? AND cod_esc=?',array($cedula,$lapso,$escuela));
        }

        //2-Electivas
            $sqlElectivas = DB::insert('INSERT INTO codigos_electivas (cedula, cod_mat, cod_gen, des_mat, uni_cre, opcional, cod_men, cod_pen, cod_esc, cod_reg, cod_nuc, lapso, cod_sem,preuc)
              SELECT DISTINCT ?, pensum_electivas.cod_mat, electivas.cod_gen, /*(SELECT cod_gen FROM electivas WHERE cod_mat=pensum_electivas.COD_MAT and cod_pen=pensum_electivas.cod_pen AND cod_esc=pensum_electivas.COD_ESC and (cod_men=pensum_electivas.cod_men OR cod_men=0) LIMIT 0,1),*/
              pensum_electivas.des_mat, (SELECT uni_cre FROM materias WHERE cod_mat=pensum_electivas.cod_mat LIMIT 0,1), 0, ?,?,?,?,?,?, sem_mat,PREUC
              FROM pensum_electivas  inner join electivas  on electivas.cod_mat = pensum_electivas.cod_mat and  electivas.cod_pen = pensum_electivas.cod_pen and electivas.cod_nuc = pensum_electivas.cod_nuc and electivas.cod_esc = pensum_electivas.cod_esc and electivas.cod_men = pensum_electivas.cod_men 
              WHERE (pensum_electivas.COD_PEN = ?) 
              AND (pensum_electivas.COD_ESC = ?) 
              AND (pensum_electivas.COD_NUC = ?) 
              AND (pensum_electivas.COD_MEN = ? OR pensum_electivas.COD_MEN = 0)                           
              AND (NOT EXISTS (SELECT cod_mat 
                FROM calificaciones 
                WHERE cedula = ?
                AND cod_esc = ? 
                AND cod_pen = pensum_electivas.cod_pen 
                AND cod_mat = pensum_electivas.cod_mat
                AND cod_nuc = pensum_electivas.cod_nuc 
                AND califi >=(SELECT DISTINCT min_apr 
                    FROM materias 
                    WHERE cod_mat = calificaciones.cod_mat))) 
                AND (EXISTS ((SELECT cod_mat 
                FROM calificaciones 
                WHERE cedula = ?
                AND cod_esc = ? 
                AND cod_pen = pensum_electivas.cod_pen 
                AND (cod_men = ? or cod_men=0) 
                AND ((cod_mat = pensum_electivas.prel1) AND califi >= (SELECT DISTINCT min_apr 
                    FROM materias 
                    WHERE cod_mat = calificaciones.cod_mat)))) OR pensum_electivas.prel1="") 
                AND (EXISTS ((SELECT cod_mat 
                FROM calificaciones 
                WHERE cedula = ? 
                AND cod_esc = ?
                AND cod_pen = pensum_electivas.cod_pen 
                AND cod_nuc = pensum_electivas.cod_nuc
                AND (cod_men = ? or cod_men=0) 
                AND ((cod_mat = pensum_electivas.prel2) AND califi >=(SELECT DISTINCT min_apr 
                    FROM materias 
                    WHERE cod_mat = calificaciones.cod_mat)))) OR pensum_electivas.prel2="") 
                AND (EXISTS (SELECT cod_est FROM electivas WHERE cod_mat=pensum_electivas.cod_mat AND cod_est=0))
                ORDER BY SEM_MAT, COD_MAT',array($cedula,$mencion,$pensum,$escuela,$regimen,$nucleo,$lapso,$pensum,$escuela,$nucleo,$mencion,$cedula,$escuela,$cedula,$escuela,$mencion,$cedula,$escuela,$mencion));
                
                //Eliminar las electivas que tiene codigo gen en nulo
                $sqlEliminaElectivasNulas = DB::select('DELETE from codigos_electivas where cod_gen IS NULL and cedula = ? and lapso = ? and cod_nuc = ? and cod_esc = ?',array($cedula,$lapso,$nucleo,$escuela));

                //Elimina los codigos electiva de codigos
                 $sqlEliminaCodigoGen=DB::select('DELETE FROM codigos WHERE EXISTS (SELECT * FROM electivas WHERE cod_gen=codigos.cod_mat) AND cedula=? AND lapso=? AND cod_nuc=? AND cod_esc=?',array($cedula,$lapso,$nucleo,$escuela));
            /**Procedimiento para quitar las materias de semestres sup SOLO PARA NUEVOS***/

            if ($Es_nuevo==1) {
                if ($pensum==9) {
                   /* SE QUEDA COMO ESTA*/                            
                }else{
                   $sqlSemestreSuperiores = DB::table('codigos')
                       ->where('cedula','=',$cedula)
                       ->where('cod_esc','=',$escuela)
                       ->where('cod_pen','=',$pensum)
                       ->where('cod_sem','>',1)
                       ->where('uni_cre','>',0)
                       ->delete();                     
                }
            }

            /*Procedimiento para verificar si puede cursar la electiva que esta proyectada en codigos_electivas
            segun la carrera o mencion.*/
            $codigoGen=DB::select('SELECT COUNT(*) AS electivas FROM calificaciones WHERE cedula=? AND cod_esc=? AND cod_nuc=? AND EXISTS (SELECT * FROM electivas WHERE cod_mat=calificaciones.cod_mat AND cod_esc=calificaciones.cod_esc AND cod_nuc=calificaciones.cod_nuc) AND califi>=(SELECT min_apr FROM materias WHERE cod_mat=calificaciones.cod_mat LIMIT 0,1)',array($cedula,$escuela,$nucleo));
             foreach ($codigoGen as $key => $value) {
                $ElectivaCursar = $value->electivas+1;
            }

            $UCAprobadas =UCAprobadas($cedula, $escuela, $pensum, $mencion, $nucleo);

            $cantCursadas = '00'.($ElectivaCursar); //a cursar 
            switch ($escuela) {
            case 1: //sistemas
                $electivas_carrera=2;
                if ($cantCursadas>$electivas_carrera) {
                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                      ->where('cedula','=',$cedula)
                      ->where('lapso','=',$lapso)
                      ->where('cod_esc','=',$escuela)
                      ->where('cod_pen','=',$pensum)
                      ->delete();                  
                }else{
                    switch ($cantCursadas){
                    case 1: //no ha cursado ninguna esta es le primera a cursar.               
                        if($UCAprobadas<110) {
                          $sqlEliminaElectivasNoCursa =DB::table('codigos_electivas')
                              ->where('cedula','=',$cedula)
                              ->where('lapso','=',$lapso)
                              ->where('cod_esc','=',$escuela)
                              ->where('cod_pen','=',$pensum)
                              ->whereIn('cod_gen', array("ES-111","ES-222"))
                              ->delete();
                        }
                        if($UCAprobadas>=110 && $UCAprobadas<150){
                            $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                              ->where('cedula','=',$cedula)
                              ->where('lapso','=',$lapso)
                              ->where('cod_esc','=',$escuela)
                              ->where('cod_pen','=',$pensum)
                              ->whereIn('cod_gen',array("ES-222"))
                              ->delete();

                            $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                              ->where('cedula','=',$cedula)
                              ->where('lapso','=',$lapso)
                              ->where('cod_esc','=',$escuela)
                              ->where('cod_pen','=',$pensum)
                              ->where('preuc','>',$UCAprobadas)
                              ->whereIn('cod_gen',array("ES-111"))
                              ->delete();                
                        }
                    break;
                    case 2: //segunda electiva.
                        if($UCAprobadas>=110 && $UCAprobadas<150){
                          $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                              ->where('cedula','=',$cedula)
                              ->where('lapso','=',$lapso)
                              ->where('cod_esc','=',$escuela)
                              ->where('cod_pen','=',$pensum)
                              ->whereIn('cod_gen', array("ES-111","ES-222"))
                              ->delete();
                        }
                        if($UCAprobadas>=150){
                          $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                              ->where('cedula','=',$cedula)
                              ->where('lapso','=',$lapso)
                              ->where('cod_esc','=',$escuela)
                              ->where('cod_pen','=',$pensum)
                              ->whereIn('cod_gen',array("ES-111"))
                              ->delete();
                        }
                    break;
                    }
                } //else  
            break;     
            case 2: //Electrica
            $electivas_carrera=7;
            if ($cantCursadas>$electivas_carrera) {
              $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                  ->where('cedula','=',$cedula)
                  ->where('lapso','=',$lapso)
                  ->where('cod_esc','=',$escuela)
                  ->where('cod_pen','=',$pensum)
                  ->delete();
            }else{
                switch ($cantCursadas){
                case 1: //no ha cursado ninguna esta es le primera a cursar.               
                    if($UCAprobadas<109){
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EE-111","EE-222","EE-333","EE-444","EE-555","EE-666","EE-777"))
                            ->delete();
                    }
                    if($UCAprobadas>=109 && $UCAprobadas<128){
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EE-222","EE-333","EE-444","EE-555","EE-666","EE-777"))
                            ->delete();
                    }
                break;
                case 2: //segunda electiva.
                    if($UCAprobadas>=109 && $UCAprobadas<128){
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EE-111","EE-222","EE-333","EE-444","EE-555","EE-666","EE-777"))
                            ->delete();
                    }
                    if($UCAprobadas>=128){
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen',array("EE-111"))
                            ->delete();
                    }
                break;
                case 3: //tercera electiva.
                    if($UCAprobadas>=128){
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array('EE-111','EE-222'))
                            ->delete();
                    }
                break;
                case 4: //cuarta electiva.
                    if($UCAprobadas>=128){
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array('EE-111','EE-222','EE-333'))
                            ->delete();
                    }
                break;
                case 5: //QUINTA electiva.
                    if($UCAprobadas>=128){
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EE-111","EE-222","EE-333","EE-444"))
                            ->delete();
                    }
                break;              
                case 6: //sexta electiva.
                    if($UCAprobadas>=128){
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EE-111","EE-222","EE-333","EE-444","EE-555"))
                            ->delete();
                    }
                break;   
                case 7: //septima electiva.
                    if($UCAprobadas>=128){
                            $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EE-111","EE-222","EE-333","EE-444","EE-555","EE-666"))
                            ->delete();
                    }
                break; 
                }
            } //else
            break;
            case 3: //Administracion
            $electivas_carrera=3;
            if ($cantCursadas>$electivas_carrera) {
                $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                    ->where('cedula','=',$cedula)
                    ->where('lapso','=',$lapso)
                    ->where('cod_esc','=',$escuela)
                    ->where('cod_pen','=',$pensum)
                    ->delete();
            }else{
                switch ($cantCursadas){
                    case 1: //no ha cursado ninguna esta es le primera a cursar.               
                        if($UCAprobadas<73) {
                            $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                ->where('cedula','=',$cedula)
                                ->where('lapso','=',$lapso)
                                ->where('cod_esc','=',$escuela)
                                ->where('cod_pen','=',$pensum)
                                ->whereIn('cod_gen', array("EA-111","EA-222","EA-333"))
                                ->delete();
                        }
                        if($UCAprobadas>=73 && $UCAprobadas<92){
                            $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                ->where('cedula','=',$cedula)
                                ->where('lapso','=',$lapso)
                                ->where('cod_esc','=',$escuela)
                                ->where('cod_pen','=',$pensum)
                                ->whereIn('cod_gen', array("EA-222","EA-333"))
                                ->delete();

                            $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                ->where('cedula','=',$cedula)
                                ->where('lapso','=',$lapso)
                                ->where('cod_esc','=',$escuela)
                                ->where('cod_pen','=',$pensum)
                                ->whereIn('cod_gen', array("EA-111"))
                                ->where('preuc','>',$UCAprobadas)
                                ->delete();
                        }

                        if($UCAprobadas>92){
                            $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                ->where('cedula','=',$cedula)
                                ->where('lapso','=',$lapso)
                                ->where('cod_esc','=',$escuela)
                                ->where('cod_pen','=',$pensum)
                                ->whereIn('cod_gen', array("EA-333"))
                                ->delete();
                        }
                    break;
                    case 2: 
                        if($UCAprobadas>=92 && $UCAprobadas<126){
                            $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                ->where('cedula','=',$cedula)
                                ->where('lapso','=',$lapso)
                                ->where('cod_esc','=',$escuela)
                                ->where('cod_pen','=',$pensum)
                                ->whereIn('cod_gen', array("EA-111","EA-333"))
                                ->delete();
                        }
                    break;
                    case 3: 
                        if($UCAprobadas>=126){
                                $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                ->where('cedula','=',$cedula)
                                ->where('lapso','=',$lapso)
                                ->where('cod_esc','=',$escuela)
                                ->where('cod_pen','=',$pensum)
                                ->whereIn('cod_gen', array("EA-111","EA-222"))
                                ->delete();
                        }
                    break;
                    }
                }   
            break;
            case 4: //Contaduria
            $electivas_carrera=3;
                if ($cantCursadas>$electivas_carrera) {
                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                        ->where('cedula','=',$cedula)
                        ->where('lapso','=',$lapso)
                        ->where('cod_esc','=',$escuela)
                        ->where('cod_pen','=',$pensum)
                        ->delete();
                }else{
                   switch ($cantCursadas){
                    case 1: //no ha cursado ninguna esta es le primera a cursar.               
                    if($UCAprobadas<72) {
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EC-111","EC-222","EC-333"))
                            ->delete();
                    }
                    if($UCAprobadas>=72 && $UCAprobadas<109){
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EC-222","EC-333"))
                            ->delete();
                    }
                    break;
                    case 2:
                    if($UCAprobadas<109 ){
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EC-111","EC-222","EC-333"))
                            ->delete();
                    }           
                    if($UCAprobadas>=109 && $UCAprobadas<130){                                       
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EC-111","EC-333"))
                            ->delete();
                    }

                    if($UCAprobadas>=130){                                       
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EC-111"))
                            ->delete();
                    }
                    break;            
                    case 3: //segunda electiva.
                    if($UCAprobadas>=130){
                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                            ->where('cedula','=',$cedula)
                            ->where('lapso','=',$lapso)
                            ->where('cod_esc','=',$escuela)
                            ->where('cod_pen','=',$pensum)
                            ->whereIn('cod_gen', array("EC-111","EC-222"))
                            ->delete();
                    }
                    break;
                    }   
                }
            break;
            case 5: //Derecho
            $electivas_carrera=1;
                if ($cantCursadas>$electivas_carrera) {
                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                        ->where('cedula','=',$cedula)
                        ->where('lapso','=',$lapso)
                        ->where('cod_esc','=',$escuela)
                        ->where('cod_pen','=',$pensum)
                        ->delete();
                }else{
                    switch ($cantCursadas){
                    case 1: //no ha cursado ninguna esta es le primera a cursar.               
                        if($UCAprobadas<149) {
                            $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                ->where('cedula','=',$cedula)
                                ->where('lapso','=',$lapso)
                                ->where('cod_esc','=',$escuela)
                                ->where('cod_pen','=',$pensum)
                                ->whereIn('cod_gen',array("ED-111"))
                                ->delete();
                        }
                    break;
                    }   
                }
            break;
            case 6: //Comunicacion social
            $electivas_carrera=3;
            //echo 'eeee'.$cantCursadas;              
                if ($cantCursadas>$electivas_carrera){
                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                        ->where('cedula','=',$cedula)
                        ->where('lapso','=',$lapso)
                        ->where('cod_esc','=',$escuela)
                        ->where('cod_pen','=',$pensum)
                        ->delete();
                }else{
                    switch ($mencion){
                    case 1: //mencion Periodismo    
                        switch ($cantCursadas){
                        case 1: //no ha cursado ninguna esta es le primera a cursar.
                            if($UCAprobadas<134){
                                $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                    ->where('cedula','=',$cedula)
                                    ->where('lapso','=',$lapso)
                                    ->where('cod_esc','=',$escuela)
                                    ->where('cod_pen','=',$pensum)
                                    ->whereIn('cod_gen', array("MPS001","MPS002","MPS003"))
                                    ->delete();
                            }
                            if($UCAprobadas>=134 && $UCAprobadas<149){
                                $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                    ->where('cedula','=',$cedula)
                                    ->where('lapso','=',$lapso)
                                    ->where('cod_esc','=',$escuela)
                                    ->where('cod_pen','=',$pensum)
                                    ->whereIn('cod_gen', array("MPS002","MPS003"))
                                    ->delete();
                                $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                    ->where('cedula','=',$cedula)
                                    ->where('lapso','=',$lapso)
                                    ->where('cod_esc','=',$escuela)
                                    ->where('cod_pen','=',$pensum)
                                    ->whereIn('cod_gen',array("MPS001"))
                                    ->where('preuc','>',$UCAprobadas)
                                    ->delete();
                            }
                        break;
                        case 2:   //Segunda electiva  
                            if($UCAprobadas>=149 ){
                                $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                    ->where('cedula','=',$cedula)
                                    ->where('lapso','=',$lapso)
                                    ->where('cod_esc','=',$escuela)
                                    ->where('cod_pen','=',$pensum)
                                    ->whereIn('cod_gen',array('MPS001'))
                                    ->delete();

                                $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                    ->where('cedula','=',$cedula)
                                    ->where('lapso','=',$lapso)
                                    ->where('cod_esc','=',$escuela)
                                    ->where('cod_pen','=',$pensum)
                                    ->where('preuc','<',149)
                                    ->whereIn('cod_gen', array('MPS002','MPS003'))
                                    ->delete();
                            }
                        break;            
                        case 3: //tercera electiva.
                            if($UCAprobadas>=149){
                                $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                    ->where('cedula','=',$cedula)
                                    ->where('lapso','=',$lapso)
                                    ->where('cod_esc','=',$escuela)
                                    ->where('cod_pen','=',$pensum)
                                    ->whereIn('cod_gen', array("MPS001","MPS002"))
                                    ->delete();

                                $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                    ->where('cedula','=',$cedula)
                                    ->where('lapso','=',$lapso)
                                    ->where('cod_esc','=',$escuela)
                                    ->where('cod_pen','=',$pensum)
                                    ->where('preuc','<',149)
                                    ->whereIn('cod_gen',array("MPS003"))
                                    ->delete();
                            }
                        break;
                    }   //switch cantcursadas
                    break;
                    case 2: //mencion 2. Publicacion y Relaciones Publicas  
                        switch ($cantCursadas){
                            case 1: //no ha cursado ninguna esta es le primera a cursar.               
                                if($UCAprobadas<131) {
                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MPR001","MPR002","MPR003"))
                                        ->delete();
                                }
                                if($UCAprobadas>=131 && $UCAprobadas<149){
                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MPR002","MPR003"))
                                        ->delete();
                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen',array("MPR001"))
                                        ->where('preuc','>',$UCAprobadas)
                                        ->delete();
                                }
                            break;
                            case 2: 
                          //segunda electiva         
                                if($UCAprobadas>=149 ){
                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MPR001"))
                                        ->delete();

                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MPR002","MPR003"))
                                        ->where('preuc','<',149)
                                        ->delete();
                                }
                            break;            
                            case 3: //tercera electiva.
                                if($UCAprobadas>=149){
                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MPR001","MPR002"))
                                        ->delete();

                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MPR003"))
                                        ->where('preuc','<',149)
                                        ->delete();
                                }
                            break;
                        }   //switch cantcursadas
                    break;
                    case 3: //mencion 3. Comunicacion y Desarrollo  
                        switch ($cantCursadas){
                            case 1: //no ha cursado ninguna esta es le primera a cursar.               
                                if($UCAprobadas<131) {
                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MCD001","MCD002","MCD003"))
                                        ->delete();
                                }
                                if($UCAprobadas>=131 && $UCAprobadas<149){
                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MCD002","MCD003"))
                                        ->delete();

                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen',array("MCD001"))
                                        ->where('preuc','>',$UCAprobadas)
                                        ->delete();
                                }
                            break;
                            case 2: //segunda electiva         
                                if($UCAprobadas>=149 ){
                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MCD001"))
                                        ->delete();

                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MCD002","MCD003"))
                                        ->where('preuc','<',149)
                                        ->delete();
                                }
                            break;            
                            case 3: //tercera electiva.
                                if($UCAprobadas>=149){
                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MCD001","MCD002"))
                                        ->delete();

                                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                        ->where('cedula','=',$cedula)
                                        ->where('lapso','=',$lapso)
                                        ->where('cod_esc','=',$escuela)
                                        ->where('cod_pen','=',$pensum)
                                        ->whereIn('cod_gen', array("MCD003"))
                                        ->where('preuc','<',149)
                                        ->delete();
                                }
                            break;
                        }   //switch cantcursadas
                    break;
                    } //else
                }   //switch mencion
            break;
            case 7: //Psicologia
            $electivas_carrera=2;
                if ($cantCursadas>$electivas_carrera){
                    $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                        ->where('cedula','=',$cedula)
                        ->where('lapso','=',$lapso)
                        ->where('cod_esc','=',$escuela)
                        ->where('cod_pen','=',$pensum)
                        ->delete();
                }else{
                    switch ($mencion){
                        case 1: //mencion Educativa    
                            switch ($cantCursadas){
                                case 1: //no ha cursado ninguna esta es le primera a cursar.               
                                    if($UCAprobadas<85) {
                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array("EDU001","EDU002"))
                                            ->delete();
                                    }
                                    if($UCAprobadas>=85 && $UCAprobadas<101){
                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen',array("EDU002"))
                                            ->delete();

                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen',array("EDU001"))
                                            ->delete();
                                    }
                                break;
                                case 2:   //Segunda electiva  
                                    if($UCAprobadas>=101 ){
                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array("EDU001"))
                                            ->delete();
                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array("EDU001"))
                                            ->where('preuc','<',101)
                                            ->delete();
                                    }
                                break;            
                            }   //switch cantcursadas
                        break;
                        case 2: //mencion 2. Clinica  
                            switch ($cantCursadas){                               
                                case 1:  //no ha cursado ninguna esta es le primera a cursar.               
                                    if($UCAprobadas<85){
                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array('CLI001','CLI002'))
                                            ->delete();
                                    }
                                    if($UCAprobadas>=85 && $UCAprobadas<101){
                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen',array("CLI002"))
                                            ->delete();

                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('PREUC','>',$UCAprobadas)
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen',array("CLI001"))
                                            ->delete();
                                    }
                                break;
                                case 2:   //Segunda electiva  
                                    if($UCAprobadas>=101 ){
                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array('CLI001'))
                                            ->delete();

                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array('CLI002'))
                                            ->where('preuc','<',101)
                                            ->delete();
                                    }
                                break; 
                            }   //switch cantcursadas
                        break;
                        case 3: //mencion 3 Industrial  
                            switch ($cantCursadas){
                                case 1: //no ha cursado ninguna esta es le primera a cursar.               
                                    if($UCAprobadas<85) {
                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array('IND001','IND002'))
                                            ->delete();
                                    }
                                    if($UCAprobadas>=85 && $UCAprobadas<101){
                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array('IND002'))
                                            ->delete();

                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array('IND001'))
                                            ->where('preuc','>',$UCAprobadas)
                                            ->delete();
                                    }
                                break;
                                case 2:   //Segunda electiva  
                                    if($UCAprobadas<101){
                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array('IND001','IND002'))
                                            ->delete();
                                    }
                                    if($UCAprobadas>=101 ){
                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array('IND001'))
                                            ->delete();

                                        $sqlEliminaElectivasNoCursa = DB::table('codigos_electivas')
                                            ->where('cedula','=',$cedula)
                                            ->where('lapso','=',$lapso)
                                            ->where('cod_esc','=',$escuela)
                                            ->where('cod_pen','=',$pensum)
                                            ->whereIn('cod_gen', array('IND002'))
                                            ->where('preuc','<',101)
                                            ->delete();
                                    }
                                break; 
                            }   //switch cantcursadas
                        break;
                    } //else
                }   //switch mencion
            break;

            default:
            # code...
            break;
            }
        //3-Requisitos    
            $sqlRequisitos = DB::insert('INSERT INTO codigos_requisitos (cedula, cod_mat, des_mat, cod_pen, cod_reg, cod_nuc, cod_esc, lapso, cod_sem, cod_est, opcional, cod_men)
                SELECT ?, COD_MAT, (SELECT des_mat From materias WHERE cod_mat = pensum_requisitos.cod_mat LIMIT 0,1),?, ?, ?, ? ,?, cod_sem ,0,0, ?
                FROM pensum_requisitos WHERE (COD_PEN = ?) AND (COD_ESC = ?) AND (NOT EXISTS (SELECT cod_mat FROM calificaciones WHERE COD_ESC = ? AND cedula = ? AND cod_mat = pensum_requisitos.cod_mat AND califi >= (SELECT DISTINCT min_apr FROM materias WHERE  cod_mat = calificaciones.cod_mat))) AND (EXISTS ((SELECT cod_mat FROM calificaciones WHERE cedula = ? AND COD_ESC = ? AND cod_pen = pensum_requisitos.cod_pen AND (COD_MEN = ? OR cod_men = 0) AND ((cod_mat = pensum_requisitos.cod_pr1) AND califi >= (SELECT DISTINCT min_apr FROM materias WHERE cod_mat = calificaciones.cod_mat)))) OR pensum_requisitos.cod_pr1 = "") AND (EXISTS ((SELECT cod_mat FROM  calificaciones WHERE cedula = ? AND COD_ESC = ? AND cod_pen = pensum_requisitos.cod_pen AND (COD_MEN = ? OR cod_men = 0) AND ((cod_mat = pensum_requisitos.cod_pr2) AND califi >= (SELECT DISTINCT min_apr FROM  materias WHERE  cod_mat = calificaciones.cod_mat)))) OR pensum_requisitos.cod_pr2 = "") AND (EXISTS ((SELECT cod_mat FROM calificaciones WHERE cedula = ? AND COD_ESC = ? AND cod_pen = pensum_requisitos.cod_pen AND (COD_MEN = ? OR cod_men = 0) AND ((cod_mat = pensum_requisitos.cod_pr3) AND califi >= (SELECT DISTINCT min_apr FROM materias WHERE cod_mat = calificaciones.cod_mat)))) OR pensum_requisitos.cod_pr3 = "") AND (pensum_requisitos.COD_LP1<=20102) ORDER BY COD_SEM, COD_MAT',array($cedula,$pensum,$regimen,$nucleo,$escuela,$lapso,$mencion,$pensum,$escuela,$escuela,$cedula,$cedula,$escuela,$mencion,$cedula,$escuela,$mencion,$cedula,$escuela,$mencion));

            $sqlRequisitos = DB::select('DELETE FROM codigos_requisitos WHERE cedula=? AND cod_esc=? AND cod_nuc=? AND LEFT(cod_mat,3)<>"DHP"',array($cedula,$escuela,$nucleo));

            $sqlsem = DB::select('SELECT min(cod_sem) AS semestre FROM (SELECT cod_sem FROM codigos WHERE cedula=? AND SUBSTR(cod_mat,1,4)<>"SCOM") sem ORDER BY cod_sem',array($cedula));

            foreach ($sqlsem as $key => $value) {
                if ($value->semestre<10) {
                    $sqlRequisitos = DB::select('UPDATE codigos_requisitos SET opcional=1 WHERE cedula=? AND cod_esc=? AND cod_nuc=? AND cod_mat="DHP300" ',array($cedula,$escuela,$nucleo));
                }
            }
 
          //Verificamos el DAU si la escuela es 5 y el lapso es <20021
            if ($lapso_ingreso<19972){
                $sqlDHP = DB::select('DELETE FROM codigos_requisitos WHERE cedula=? AND LEFT(cod_mat,3)="DHP" AND lapso=? AND cod_esc=?',array($cedula,$lapso,$escuela));
            }

            if ($proce>0 && $pensum == 2){
                $sqlRequisitos = DB::select('DELETE FROM codigos_requisitos WHERE cedula=? AND cod_esc=? AND cod_nuc=? AND LEFT(cod_mat,3)="DHP"',array($cedula,$escuela,$nucleo));
                $sqlDAU_prof = DB::select('DELETE FROM codigos WHERE cedula=? AND LEFT(cod_mat,3)="DAU" AND lapso=? AND cod_esc=?',array($cedula,$lapso,$escuela));
            }

            if ($proce>0 && $pensum == 1 && $escuela==6){
                $sqlRequisitos = DB::select('DELETE FROM codigos_requisitos WHERE cedula=? AND cod_esc=? AND cod_nuc=? AND LEFT(cod_mat,3)="DHP"',array($cedula,$escuela,$nucleo));
                $sqlDAU_prof = DB::select('DELETE FROM codigos WHERE cedula=? AND cod_mat="ECS000" AND lapso=? AND cod_esc=?',array($cedula,$lapso,$escuela));
            }

            if ($cod_origen>50){
                //esto es opcional para que los del tachira y valle la pascua no cursen DHP y DAU
                $sqlRequisitos = DB::select('DELETE FROM codigos_requisitos WHERE cedula=? AND cod_esc=? AND cod_nuc=? AND LEFT(cod_mat,3)="DHP" and lapso=?',array($cedula,$escuela,$nucleo,$lapso));
                $sqlDHP = DB::select('DELETE FROM codigos WHERE cedula=? AND cod_mat="DAU000" AND lapso=? AND cod_esc=?',array($cedula,$lapso,$escuela));
            }


        //4-Materias que se ven separadas Esto se aplica a FPB02F y FPB03F del nucleo MARACAY y PESI
            $sqlComplementos = DB::insert('INSERT INTO codigos (cedula, cod_mat, des_mat, uni_cre, opcional, electiva, cod_pen, cod_reg, cod_nuc, cod_esc, lapso, cod_sem, cod_men)
                SELECT ?,cod_mat,des_mat,  (SELECT DISTINCT uni_cre FROM materias WHERE cod_mat = complementos.cod_mat) AS uni_cre, 0, 0, ?,?,?,?,?, (SELECT DISTINCT sem_mat FROM materias WHERE cod_mat = complementos.cod_mat AND cod_nuc = ? AND cod_pen = ? /*AND cod_men = $mencion*/) AS sem_mat ,? 
                FROM complementos
            WHERE (cod_nuc = ?) AND (cod_esc = ?) AND (cod_pen = ?) AND (dividida = ?) AND EXISTS
                (SELECT * FROM  codigos
                WHERE cedula = ? AND lapso = ? AND cod_nuc in (0,4) AND cod_mat = complementos.cpensum) AND cpensum in ("FPB02F", "FPB03F","ECS363","ECS463")',array($cedula,$pensum,$regimen,$nucleo,$escuela,$lapso,$nucleo,$pensum,$mencion,$nucleo,$escuela,$pensum,1,$cedula,$lapso));
            //echo $sqlComplementos;
            $sqlComplementos = DB::select('DELETE FROM codigos WHERE cedula=? AND cod_nuc=? AND cod_mat in ("FPB02F","FPB03F","ECS363","ECS463") AND cod_esc=? AND cod_nuc in (0,4)',array($cedula,$nucleo,$escuela));

        //5 Eliminando materias por encima de 2 semestres
            //Necesito ver si tiene pensum 9 y su resta da 19
                if ($pensum==9){
                    if(($lapso-$lapso_ingreso)<19){
                        //Dejo las materias tal y como estan
                    } else {
                        $sqlMateriasEliminaN3 = DB::select('DELETE FROM codigos WHERE cedula=? AND cod_sem> (SELECT min(cod_sem) FROM (SELECT cod_sem FROM codigos WHERE cedula=? AND SUBSTR(cod_mat,1,4)<>"SCOM" ORDER BY cod_sem) AS materias)+3 AND lapso=? AND cod_esc=?',array($cedula,$cedula,$lapso,$escuela));
                    }
                }else{
                    //Verifico si tiene movimiento de cambio de pensum o carrera
                      $sql = DB::table('movimiento_estudiantes')
                      ->select('cedula','cod_mov','lapso')
                      ->where('cedula','=',$cedula)
                      ->whereIn('cod_mov',array(10,20))
                      ->get();

                    $numrowMov = count($sql);
                    if ($numrowMov>0){
                    //Dejo las materias tal y como estan
                    } else {
                        //Borro las materias
                        $sqlMateriasEliminaN3 = DB::select('DELETE FROM codigos WHERE cedula=? AND cod_sem> (SELECT min(cod_sem) FROM (SELECT cod_sem FROM codigos WHERE cedula=? AND SUBSTR(cod_mat,1,4)<>"SCOM" ORDER BY cod_sem) AS materias)+3 AND lapso=? AND cod_esc=?',array($cedula,$cedula,$lapso,$escuela));
                    }
                }

            //Verifico si el indice es inferior a 14 punto y si no tiene el movimiento 35
            $indice_academico = indiceAC($nucleo,$pensum,$escuela,$cedula);
            foreach ($indice_academico as $key => $value) {
                $IA = $value->IA;
            }

            //Verifico si el semestre en que puede inscribirse es mayor o igual a 8
            $sql =DB::select('SELECT  cedula, COUNT(*) as popular, cod_sem FROM codigos WHERE cedula=? AND lapso=? AND cod_esc=? GROUP BY cod_sem ORDER BY popular DESC, cod_sem ASC LIMIT 0,1',array($cedula,$lapso,$escuela));

            if (count($sql)==0 && $IA<"14"){
                $sql = DB::select('SELECT cedula, lapso, cod_mov FROM movimiento_estudiantes WHERE cedula=? AND cod_esc=? AND cod_mov=?',array($cedula,$escuela,35));
                if(count($sql)>0){
                    //Tiene aprobado el ania no deberia salir en la proyeccion
                }else{
                       $sql = DB::table('codigos')
                           ->insert(array(
                               'cedula' => $cedula,
                               'cod_mat' => 'ANIA00',
                               'des_mat' => 'ACTIVIDAD NIVELADORA',
                               'uni_cre' => 0,
                               'opcional' => 0,
                               'electiva' => 0,
                               'cod_pen' => $pensum,
                               'cod_reg' => $regimen,
                               'cod_nuc' => $nucleo,
                               'cod_esc' => $escuela,
                               'lapso' => $lapso,
                               'cod_sem' => 8,
                               'cod_men' => $mencion
                           ));

                        //No tiene el ania y debe cursarlo, hay que chequear tambien si tiene alguna materia de proyecto eliminarla.
                        $sqlEliminaProyecto = DB::table('codigos')
                           ->where('cedula','=',$cedula)
                           ->where('cod_esc','=',$escuela)
                           ->where('lapso','=',$lapso)
                           ->where('cod_nuc','=',$nucleo)
                           ->whereIn('cod_mat',array('AI10PR', 'AI510P','MPS025','MCD025','MPR025','FPE70P','FPE70E','FPE30N','FPE30G','FPE40A','FPE40N','FPE60D','FPE60I','FPE10A','FPE10P','FPE20A','FPE20P','FPE50E','FPE50J','PFE410'))
                           ->delete();

                        $sqlObligaANIA = DB::select('UPDATE codigos SET opcional=0 WHERE cedula=? AND cod_esc=? AND lapso=? AND cod_nuc=? AND cod_mat in ("ANIA00")',array($cedula,$escuela,$lapso,$nucleo,$cedula,$escuela,$lapso,$nucleo));
                    }
            }else{

                foreach ($sql as $key => $value) {
                    $fila = $value->cod_sem;
                }

                if($IA<"14" && $fila>=8 && $Es_nuevo!=1){
                    //Verifico si no tiene un movimiento en particular
                    $sql = DB::select('SELECT cedula, lapso, cod_mov FROM movimiento_estudiantes WHERE cedula=? AND cod_esc=? AND cod_mov=?',array($cedula,$escuela,35));
                    if(count($sql)>0){
                    //Tiene aprobado el ania no deberia salir en la proyeccion
                    }else{
                        $sql = DB::table('codigos')
                            ->insert(array(
                                'cedula' => $cedula,
                                'cod_mat' => 'ANIA00',
                                'des_mat' => 'ACTIVIDAD NIVELADORA',
                                'uni_cre' => 0,
                                'opcional' => 0,
                                'electiva' => 0,
                                'cod_pen' => $pensum,
                                'cod_reg' => $regimen,
                                'cod_nuc' => $nucleo,
                                'cod_esc' => $escuela,
                                'lapso' => $lapso,
                                'cod_sem' => 8,
                                'cod_men' => $mencion
                            ));

                        //No tiene el ania y debe cursarlo, hay que chequear tambien si tiene alguna materia de proyecto eliminarla.
                        $sqlEliminaProyecto = DB::table('codigos')
                        ->where('cedula','=',$cedula)
                        ->where('cod_esc','=',$escuela)
                        ->where('lapso','=',$lapso)
                        ->where('cod_nuc','=',$nucleo)
                        ->whereIn('cod_mat',array('AI10PR', 'AI510P','MPS025','MCD025','MPR025','FPE70P','FPE70E','FPE30N','FPE30G','FPE40A','FPE40N','FPE60D','FPE60I','FPE10A','FPE10P','FPE20A','FPE20P','FPE50E','FPE50J','PFE410'))
                        ->delete();

                     $sqlObligaANIA = DB::select('UPDATE codigos SET opcional=0 WHERE cedula=? AND cod_esc=? AND lapso=? AND cod_nuc=? AND cod_mat in ("ANIA00")',array($cedula,$escuela,$lapso,$nucleo,$cedula,$escuela,$lapso,$nucleo));
                    }
                }
            }

            //Colonando las materias obligatorias segun el semestre
            $sqlMateriasOpcionales = DB::select('UPDATE codigos SET opcional=0 WHERE cedula=? AND LEFT(cod_mat,3)<>"DAU" AND  cod_sem<=(SELECT min(cod_sem) FROM (SELECT cod_sem FROM codigos WHERE cedula=? ORDER BY cod_sem) AS materias)',array($cedula,$cedula));

         //Quitando ecs000 de codigos a estudiantes pensum 9
            if ( ($pensum==9) && ($Es_nuevo==1) ){
                $sqlMateriasECS000 =DB::table('codigos')
                    ->where('cedula','=',$cedula)
                    ->where('cod_esc','=',$escuela)
                    ->where('lapso','=',$lapso)
                    ->where('cod_nuc','=',$nucleo)
                    ->whereIn('cod_mat',array('ECS000'))
                    ->delete();
            }

            //Colocando las materias del decimo como NO Obligatorias
            $sqlMateriasDecimo =DB::table('codigos')
                ->where('cedula','=',$cedula)
                ->where('cod_sem','=',10)
                ->update(array(
                    'opcional' => 1));

            //verificando el SCOM exonerado con cod_mov 45
            $sqlSCOM = DB::table('movimiento_estudiantes')
                ->select('cedula','cod_mov','lapso')
                ->where('cedula','=',$cedula)
                ->whereIn('cod_mov',array(45))
                ->get();
            $numrowSCOMMov = count($sqlSCOM);
            if ($numrowSCOMMov>0){
             $sqlSCOM2 = DB::select('DELETE FROM codigos WHERE cedula=? AND LEFT(cod_mat,4)="SCOM" AND lapso=? AND cod_esc=?',array($cedula,$lapso,$escuela));
            }

            if ($traerMaterias==true){
                
                $sqlArreglo = DB::select('SELECT CEDULA, COD_MAT, DES_MAT,COD_SEM, UNI_CRE, OPCIONAL FROM codigos WHERE cedula=? AND lapso=? AND cod_esc=? AND cod_pen=? AND cod_men=? AND cod_nuc=? AND NOT EXISTS (SELECT * FROM electivas WHERE cod_gen=codigos.cod_mat) ORDER BY COD_SEM, COD_MAT, OPCIONAL',array($cedula,$lapso,$escuela,$pensum,$mencion,$nucleo));
                      //Traemos todo en un arreglo
                      //echo $sqlArreglo;
                 $arreglo = array();
                if (count($sqlArreglo)>0){
                    foreach ($sqlArreglo as $key => $value) {
                        $traerM = new stdClass();
                        $traerM->CEDULA = $value->CEDULA;
                        $traerM->COD_MAT = $value->COD_MAT;
                        $traerM->DES_MAT = $value->DES_MAT;
                        $traerM->COD_SEM = $value->COD_SEM;
                        $traerM->UNI_CRE = $value->UNI_CRE;
                        $arreglo[]=$traerM;
                    }
                    return ($arreglo);
                }else{
                    return false;
                }
            }
        return true;
    }  
}

function traerProyeccionElectivas($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso){
    $sqlArregloE=DB::select('SELECT CEDULA, COD_MAT, DES_MAT, UNI_CRE as UC, COD_SEM, OPCIONAL from codigos_electivas where cedula = ? and lapso = ? and cod_esc = ? and cod_pen = ? and cod_men = ? and cod_nuc = ? and exists (select cod_mat from secciones where secciones.cod_mat = codigos_electivas.cod_mat and secciones.lapso = codigos_electivas.lapso and secciones.cod_nuc = codigos_electivas.cod_nuc) order by cod_sem asc, cod_mat asc',array($cedula,$lapso,$escuela,$pensum,$mencion,$nucleo));

    $arreglo = array();
    if (count($sqlArregloE)>0){
        foreach ($sqlArregloE as $key => $value) {
            $traerE = new stdClass();
            $traerE->cedula = $value->CEDULA;
            $traerE->cod_mat = $value->COD_MAT;
            $traerE->des_mat = $value->DES_MAT;
            $traerE->cod_sem = $value->COD_SEM;
            $traerE->uc = $value->UC;
            $arreglo[]=$traerE;
        }
        return $arreglo;
    }else{
        return 0;
    }
}

function traerProyeccionRequisitos($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso){

    $sqlArreglo= DB::select('SELECT CEDULA, COD_MAT, DES_MAT, COD_SEM, OPCIONAL FROM codigos_requisitos WHERE cedula = ? AND lapso = ? AND cod_esc = ? AND cod_pen = ? AND cod_men = ? AND cod_nuc = ? AND EXISTS (SELECT cod_mat FROM secciones WHERE secciones.cod_mat = codigos_requisitos.cod_mat AND secciones.lapso = codigos_requisitos.lapso AND secciones.cod_nuc = codigos_requisitos.cod_nuc)',array($cedula,$lapso,$escuela,$pensum,$mencion,$nucleo));

    $arregloR = array();
    if (count($sqlArreglo)>0){
        foreach ($sqlArreglo as $key => $value) {
            $traerR = new stdClass();
            $traerR->cedula = $value->CEDULA;
            $traerR->cod_mat = $value->COD_MAT;
            $traerR->des_mat = $value->DES_MAT;
            $traerR->cod_sem = $value->COD_SEM;

            $arregloR[]=$traerR;
        }
        return $arregloR;
    }else{
        return 0;
    }
}

function VerificarInscripcion($cedula,$escuela,$nucleo,$lapso){
    $sqlInscripcion = DB::table('estudiantes_inscripciones')
        ->select('cedula','fec_ins')
        ->where('cedula','=',$cedula)
        ->where('cod_esc','=',$escuela)
        ->where('lapso','=',$lapso)
        ->where('cod_nuc','=',$nucleo)
        ->whereExists(function($query)
            {
                $query->select(DB::raw('DISTINCT cedula'))
                    ->from('inscripciones')
                    ->whereRaw('cedula','=',$cedula)
                    ->whereRaw('cod_esc','=',$escuela)
                    ->whereRaw('cod_nuc','=',$nucleo)
                    ->whereRaw('lapso','=',$lapso);
            })
        ->get();

    if(count($sqlInscripcion)>0){
        $filas = $sqlInscripcion;
        return $filas;
    }
}

function incrementaCupApro($cod_mat,$sec_mat,$lapso,$cod_nuc){
  //Verificando si la materia esta en CONTEO
  $sqlConteo = DB::table('conteo')
      ->select('cod_mat','sec_mat','sec_ant')
      ->where('cod_mat','=',$cod_mat)
      ->where('sec_mat','=',$sec_mat)
      ->where('lapso','=',$lapso)
      ->where('cod_nuc','=',$cod_nuc)
      ->get();
//Existe la materia
    if(count($sqlConteo)>0){
        foreach ($sqlConteo as $key => $value) {
            $sec_mat = $value->sec_mat;
        }
        $sqlAumentaCupApro = DB::select('UPDATE secciones SET cup_apro=(SELECT count(cedula) FROM adicion_temporal WHERE lapso=secciones.lapso AND cod_mat=secciones.cod_mat AND sec_mat=secciones.SEC_MAT AND cod_nuc=?)+1 WHERE cod_mat=? AND (sec_mat=? OR sec_mat=?) AND lapso=? AND cod_nuc=?',array($cod_nuc,$cod_mat,$sec_mat,$sec_mat,$lapso,$cod_nuc));
    }else{
        $sqlAumentaCupApro = DB::select('UPDATE secciones SET cup_apro=(SELECT count(cedula) FROM adicion_temporal WHERE lapso=secciones.lapso AND cod_mat=secciones.cod_mat AND sec_mat=secciones.SEC_MAT AND cod_nuc=?)+1 WHERE cod_mat=? AND sec_mat=? AND lapso=? AND cod_nuc=?',array($cod_nuc,$cod_mat,$sec_mat,$lapso,$cod_nuc));
    }
}

function incrementaCupAproInsc($cod_mat,$sec_mat,$lapso,$cod_nuc){
  //Verificando si la materia esta en CONTEO
  $sqlConteo = DB::table('conteo')
      ->select('cod_mat','sec_mat','sec_ant')
      ->where('cod_mat','=',$cod_mat)
      ->where('sec_mat','=',$sec_mat)
      ->where('lapso','=',$lapso)
      ->where('cod_nuc','=',$cod_nuc)
      ->get();
//Existe la materia
    if(count($sqlConteo)>0){
        foreach ($sqlConteo as $key => $value) {
            $sec_mat = $value->sec_mat;
        }
        $sqlAumentaCupApro = DB::select('UPDATE secciones SET cup_apro=(SELECT count(cedula) FROM inscripciones_temporal WHERE lapso=secciones.lapso AND cod_mat=secciones.cod_mat AND sec_mat=secciones.SEC_MAT AND cod_nuc=?)+1 WHERE cod_mat=? AND (sec_mat=? OR sec_mat=?) AND lapso=? AND cod_nuc=?',array($cod_nuc,$cod_mat,$sec_mat,$sec_mat,$lapso,$cod_nuc));
    }else{
        $sqlAumentaCupApro = DB::select('UPDATE secciones SET cup_apro=(SELECT count(cedula) FROM inscripciones_temporal WHERE lapso=secciones.lapso AND cod_mat=secciones.cod_mat AND sec_mat=secciones.SEC_MAT AND cod_nuc=?)+1 WHERE cod_mat=? AND sec_mat=? AND lapso=? AND cod_nuc=?',array($cod_nuc,$cod_mat,$sec_mat,$lapso,$cod_nuc));
    }
}

function descuentaCupApro($cod_mat, $sec_mat,$lapso,$cod_nuc){
  //Verificando si la materia esta en CONTEO
  $sqlConteo = DB::table('conteo')
      ->select('cod_mat','sec_mat','sec_ant')
      ->where('cod_mat','=',$cod_mat)
      ->where('sec_mat','=',$sec_mat)
      ->where('lapso','=',$lapso)
      ->where('cod_nuc','=',$cod_nuc)
      ->get();
  
    if(count($sqlConteo)>0){ //Existe la materia
        foreach ($sqlConteo as $key => $value) {
            $sec_ant = $value->sec_mat;
        }
        $sqlAumentaCupApro = DB::raw('UPDATE secciones SET cup_apro=cup_apro+1 WHERE cod_mat=? AND (sec_mat=? OR sec_mat=?) AND lapso=? AND cod_nuc=?',array($cod_mat,$sec_mat,$sec_mat,$lapso,$cod_nuc));
    }else{
        $sqlAumentaCupApro = DB::raw('UPDATE secciones SET cup_apro=cup_apro+1 WHERE cod_mat=? AND sec_mat=? AND lapso=? AND cod_nuc=?',array($cod_mat,$sec_mat,$lapso,$cod_nuc));
    }
}

function llenarAsignaturas($cedula, $lapso){
    $sqlTabla = DB::select('SELECT cod_mat, des_mat,sec_mat,sem_mat, uni_cre, electiva, (SELECT dia1 FROM secciones WHERE cod_mat=adicion_temporal.cod_mat AND sec_mat=adicion_temporal.sec_mat AND lapso=? AND cod_nuc=adicion_temporal.cod_nuc) AS dia1, (SELECT dia2 FROM secciones WHERE cod_mat=adicion_temporal.cod_mat AND sec_mat=adicion_temporal.sec_mat AND lapso=? AND cod_nuc=adicion_temporal.cod_nuc) AS dia2 FROM adicion_temporal WHERE cedula=? AND lapso=? ORDER BY cod_mat',array($lapso,$lapso,$cedula,$lapso));

    $tabla_adicion = '<table class="table table-bordered">
    <thead>
        <tr class="info">
            <th class="text-center">C&oacute;digo</th>
            <th class="text-center">Descripci&oacute;n</th>
            <th class="text-center">Secci&oacute;n</th>
            <th class="text-center">U.C.</th>
            <th class="text-center">Semestre</th>
            <th class="text-center">Horario</th>
            <th class="text-center">Eliminar</th>
        </tr>
    </thead><tbody>';
    if (count($sqlTabla)>0){       

        foreach ($sqlTabla as $key => $value) {
            if (!empty($value->dia2)) {
                $dia2=' - '.$value->dia2;
            }else{
                $dia2='';
            }
            $tabla_adicion.='<tr>
            <td class="centrado">'.$value->cod_mat.'</td>
            <td class="centrado">'.$value->des_mat.'</td>
            <td class="centrado">'.$value->sec_mat.'</td>
            <td class="centrado">'.$value->uni_cre.'</td>
            <td class="centrado">'.$value->sem_mat.'</td>
            <td class="centrado">'.$value->dia1.''.$dia2.'</td>
            <td class="centrado"><button type="button" class="btn btn-danger" id="'.$value->cod_mat.'" onclick="eliminar_retiro(this.id)"><span class="glyphicon glyphicon-remove"></span></button></td>';
        }
        $tabla_adicion.='</tbody></table>';
    }else{
        $tabla_adicion.='<tr> <td class="text-center" colspan="7">No hay materias adicionadas</td> </tr>';
    }
    return $tabla_adicion;
}


function nombreturno($t){

  switch ($t) {
    
    case 1:
        $dturno="Mañana";
      break;
    case 2:
        $dturno="Tarde";
      break;
    case 3:
        $dturno="Noche";
      break;
    case 4:
        $dturno="Flexible";
      break;
 
    default:
        $dturno="Turno no Existe";
      break;

  }

  return $dturno;

}

function tipoevaluacion($x){

    $sql=DB::table('tipos_evaluacion')
        ->select('descripcion')
        ->where('cod_eval','=',$x)
        ->first();
    if (count($sql)==1) {
        return $sql->descripcion;
    } else {
        return '-';
    }
}

function Bsc_hizo_solicitud_cambio($cedula, $lapso) {
       
    $query = DB::select('SELECT cedula,lapso,aprobado,fecha_ratificacion FROM solicitudes_cambios_seccion WHERE cedula=? and lapso=? and aprobado=? and not fecha_aprobacion is null and fecha_ratificacion > ?',array($cedula, $lapso,1,0));

    if(count($query)>0){
        foreach ($query as $key => $value) {
            $fecha_cambio = $value->fecha_ratificacion;
        }
        return 1;
    }else {
        return 0;
    }
    
}

function Bsc_Solvencias($tipo,$cedula,$lapso_actual) {

$sql=DB::select('SELECT count(*) as total from solvencias where cedula = ? and lapso = ? and tipo=?',array($cedula,$lapso_actual,$tipo));
    if (count($sql)>0) {
        foreach ($sql as $key => $value) {
            $total = $value->total;
        }
        if($total>0){
            return true;
        }
    }
    return false;
}

function Bsc_hizo_adicion($cedula, $lapso) {

    $query = DB::select('SELECT cedula,lapso,fecha_adicion FROM adicion_temporal WHERE cedula=? and lapso=?',array($cedula, $lapso));

    if(count($query)>0){
        foreach ($query as $key => $value) {
            $fecha_adicion = $value->fecha_adicion;
        }
    }else {
        return 0;
    }
    return $fecha_adicion;
}

function generateCode($length = 10) {
    
    $code = "";
    $possible = "012346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
    $maxlength = strlen($possible);
    if ($length > $maxlength) {
      $length = $maxlength;
    }
    $i = 0; 
    while ($i < $length) { 
      $char = substr($possible, mt_rand(0, $maxlength-1), 1);
      if (!strstr($code, $char)) { 
        $code .= $char;
        $i++;
      }
    }
    $code .= $code.date('y');
    return $code;
}

function Bsc_MaxNro($numestudiante,$cod_esc,$ahora) {
    $sql = DB::select('SELECT fecha_inscripcion,hora_inicio,hora_fin,min_nro,max_nro,escuela,concat_ws(" ",fecha_inscripcion,cast(hora_inicio as time)) as fechai,concat_ws(" ",fecha_inscripcion,cast(hora_fin as time)) as fechaf FROM numeros_inscripcion inner join grupos_numeros_inscripcion on grupos_numeros_inscripcion.id_grupo = numeros_inscripcion.id_grupo WHERE grupos_numeros_inscripcion.escuela=? and ? between min_nro and max_nro',array($cod_esc,$numestudiante));

    $consulta = array();
    if (count($sql)>0) {
        foreach ($sql as $key => $value) {
            $consul = new stdClass();
            $consul->fecha_inscripcion = $value->fecha_inscripcion;
            $consul->hora_inicio = $value->hora_inicio;           
            $consul->hora_fin = $value->hora_fin;
            $consul->min_nro = $value->min_nro;
            $consul->max_nro = $value->max_nro;
            $consul->escuela = $value->escuela;
            $consul->fechai = $value->fechai;
            $consul->fechaf = $value->fechaf;

            $consulta[]=$consul;
        } 
        return  $consulta;
    } else {
        return 0;
    }  
}

function UltFechaInscripcion($nuevo,$lapso_actual){
    $ultFecha = '1999-01-01';
    
    if ($nuevo==1) {
        $sql = DB::select('SELECT fecha_i_nuevos_equiv_reinc as ultFecha from lapso_fechas where activo=1');
    } else {
        $sql = DB::select('SELECT fecha_f_inscripcion as ultFecha from lapso_fechas where  activo=1');
    }

    if (count($sql)>0) {
        foreach ($sql as $key => $value) {
            $ultFecha = $value->ultFecha;
        }
    }
    return $ultFecha;
}

function recargar_horario($cedula,$lapso,$nucleo,$escuela){

    for ($i=1; $i<=21;$i++){
        for ($j=1; $j<=6;$j++){
            if ($i<10) {
                ${$j.'0'.$i}='';
            }else{
                ${$j.$i}='';
            }  
        }
    }
    //SQl que retorna la materia que tiene en inscripcion_temporal
    $sqlTabla = DB::select('SELECT cod_mat, des_mat,sec_mat,sem_mat, uni_cre, electiva, (SELECT dia1 FROM secciones WHERE cod_mat=inscripciones_temporal.cod_mat AND sec_mat=inscripciones_temporal.sec_mat AND lapso=? AND cod_nuc=inscripciones_temporal.cod_nuc) AS dia1, (SELECT dia2 FROM secciones WHERE cod_mat=inscripciones_temporal.cod_mat AND sec_mat=inscripciones_temporal.sec_mat AND lapso=? AND cod_nuc=inscripciones_temporal.cod_nuc) AS dia2 FROM inscripciones_temporal WHERE cedula=? AND lapso=? ORDER BY cod_mat',array($lapso,$lapso,$cedula,$lapso));

    if (count($sqlTabla)<=0){
        return 0;
    } 

    $tablaHorario='<table class="table table-bordered">
    <thead>
        <tr class="info">
            <th class="text-center">Hora</th>
            <th class="text-center">Lunes</th>
            <th class="text-center">Martes</th>
            <th class="text-center">Mi&eacute;rcoles</th>
            <th class="text-center">Jueves</th>
            <th class="text-center">Viernes</th>
            <th class="text-center">S&aacute;bado</th>
        </tr>
    </thead>';

    $sqlOcupacion = DB::select('SELECT COD_MAT, HOR_MAT from horamate where lapso=? and cod_nuc=? AND EXISTS (SELECT * from inscripciones_temporal where cedula=? and cod_nuc=? and cod_esc=? and lapso=? AND cod_mat = horamate.cod_mat AND sec_mat = horamate.sec_mat) ORDER BY hor_mat',array($lapso,$nucleo,$cedula,$nucleo,$escuela,$lapso));

    $fila=array();
    foreach ($sqlOcupacion as $key => $value) {
        ${$value->HOR_MAT}=$value->HOR_MAT;
        ${'materia'.$value->HOR_MAT}='<a href="#" title="'.nombreMateria($value->COD_MAT,$nucleo).'" style="color:#FFFFFF;">'.$value->COD_MAT.'</a>';
        ${'codigoMateria'.$value->HOR_MAT}=$value->COD_MAT;
    }

    $colores = array (1=>"#428bca", 2=>"#89B589",3=>"#DA80E8",4=>"#f0ad4e",5=>"#d9534f",6=>"#cb5303",7=>"#6C5CB8",8=>"#346969",9=>"#5b5b5b",10=>"#C3B950",11=>"#A250C3",12=>"#B0981C");
    $mateInscritas = DB::select('SELECT COD_MAT FROM  inscripciones_temporal WHERE cedula = ? AND lapso = ? AND cod_esc = ? ORDER BY COD_MAT',array($cedula,$lapso,$escuela));
    $i=1;
    foreach ($mateInscritas as $key => $value) {
        ${'color'.$value->COD_MAT}= $colores[$i];
        $i++;
    }

    $diasHoras = array(1=>"07:15 am - 07:55 am",2=>"08:00 am - 08:40 am",3=>"08:45 am - 09:25 am",4=>"09:30 am - 10:10 am",5=>"10:15 am - 10:55 am",6=>"11:00 am - 11:40 am",7=>"11:45 am - 12:25 pm",8=>"12:30 pm - 01:10 pm",9=>"01:15 pm - 01:55 pm",10=>"02:00 pm - 02:40 pm",11=>"02:45 pm - 03:25 pm",12=>"03:30 pm - 04:10 pm",13=>"04:15 pm - 04:55 pm",14=>"05:00 pm - 05:40 pm",15=>"05:45 pm - 06:25 pm",16=>"06:30 pm - 07:10 pm",17=>"07:15 pm - 07:55 pm",18=>"08:00 pm - 08:40 pm",19=>"08:45 pm - 09:25 pm",20=>"09:30 pm - 10:10 pm", 21=>"10:15 pm - 10:55 pm");
    for ($i=1; $i<=21;$i++){
        $tablaHorario.='<tr>';
        for ($j=0; $j<=6;$j++){
            if($j==0){
                $tablaHorario.='<td width="20%">'.$diasHoras[$i].'</td>';
            }else{ 
                $sqlWhereHoras = $j.str_pad($i,2,0,STR_PAD_LEFT); 

                if (${$sqlWhereHoras}>0){ 
                    $tablaHorario.='<td id="'.$sqlWhereHoras.'" style="background-color:'.${"color".${"codigoMateria".$sqlWhereHoras}}.';border:none;text-align:center;color:white;">'.${'materia'.$sqlWhereHoras}.'</td>';

                } else {
                    $tablaHorario.='<td id="'.$sqlWhereHoras.'" style="background-color:white;"></td>';
                }
            }
        }
        $tablaHorario.='</tr>';
    }

    return $tablaHorario;
}

/************************PROCESO DE CAMBIO DE SECCION*************/
//Funciones de Insertar las Materias de Inscripcion a Temporal cambio
function MateriasCambios($cedula,$mencion,$escuela,$pensum,$nucleo,$regimen,$lapso){

    //Borro la tabla temporal de la inscripcion
    $sqlEliminarcambiosProcesos = DB::select('DELETE FROM cambios_secciones_proceso WHERE cedula=? AND lapso=?',array($cedula,$lapso));
    $sqlEliminarcambiosTemporal = DB::select('DELETE FROM cambios_secciones_temporal WHERE cedula=? AND lapso=?',array($cedula,$lapso));
    // esta secuencia es para verificar si existen datos para validar ingreso       
    $sqlCambiosVer = DB::select('SELECT lapso,cedula,cod_mat,des_mat FROM cambios_secciones_proceso where lapso=? AND cedula=? AND cod_nuc=? AND cod_esc=?',array($lapso,$cedula,$nucleo,$escuela));
    $cant_filas = count($sqlCambiosVer);
    //esta secuencia es para verificar si existen datos para validar ingreso
    if ($cant_filas==0){
      $sqlInsertarCambios = DB::select('INSERT INTO cambios_secciones_proceso (lapso,cedula,cod_mat,des_mat,sec_mat,cod_esc,cod_pen,cod_reg,cod_nuc,cod_men,electiva,sem_mat,uni_cre) SELECT inscripciones.lapso,inscripciones.cedula,inscripciones.cod_mat,inscripciones.des_mat,inscripciones.sec_mat,inscripciones.cod_esc,inscripciones.cod_pen, inscripciones.cod_reg,inscripciones.cod_nuc,inscripciones.cod_men,0,materias.sem_mat,materias.uni_cre FROM inscripciones,materias WHERE inscripciones.cedula=? AND inscripciones.cod_esc=? AND inscripciones.cod_pen=? AND inscripciones.cod_nuc=? AND inscripciones.lapso=?  
        AND inscripciones.cod_mat=materias.cod_mat AND inscripciones.cod_pen=materias.cod_pen AND inscripciones.cod_nuc=materias.cod_nuc',array($cedula,$escuela,$pensum,$nucleo,$lapso));
  } else {
      return false;
  }
}

//Funciones de Cambio de Materias
function MateriasInscritasCambio($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso, $lapso_ingreso,$traerMaterias=false){
  $sqlMateriasInscritas = DB::select('SELECT lapso,cedula,cod_mat,des_mat,sec_mat,cod_esc,cod_pen,cod_reg,cod_nuc,cod_men, (select uc from pensum where cod_mat=inscripciones.cod_mat and cod_pen=inscripciones.cod_pen and cod_esc=inscripciones.cod_esc AND cod_nuc=inscripciones.cod_nuc) as uni_cre, (select COD_SEM from pensum where cod_mat=inscripciones.cod_mat and cod_pen=inscripciones.cod_pen and cod_esc=inscripciones.cod_esc AND cod_nuc=inscripciones.cod_nuc) as sem FROM inscripciones WHERE lapso=? AND cedula=?',array($lapso,$cedula));

  $arreglo = array();
  if (count($sqlMateriasInscritas)>=1){
        foreach ($sqlMateriasInscritas as $key => $value) {
            $materias_inscritas = new stdClass();
            $materias_inscritas->cedula = $value->cedula;
            $materias_inscritas->cod_mat = $value->cod_mat;
            $materias_inscritas->des_mat = $value->des_mat;
            $materias_inscritas->sec_mat = $value->sec_mat;
            $materias_inscritas->uni_cre = $value->uni_cre;

            $arreglo[]=$materias_inscritas;
        }      
        return ($arreglo);
    }else{
        return false;
        }
}
//*******
function MateriasInscritasCombo($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso, $lapso_ingreso,$traerMaterias=false){
  $sqlMateriasInscritas = DB::select('SELECT lapso,cedula,cod_mat,des_mat,sec_mat,cod_esc,cod_pen,cod_reg,cod_nuc,cod_men, (select uc from pensum where cod_mat=cambios_secciones_proceso.cod_mat and cod_pen=cambios_secciones_proceso.cod_pen and cod_esc=cambios_secciones_proceso.cod_esc AND cod_nuc=cambios_secciones_proceso.cod_nuc Limit 0,1) as uni_cre, (select COD_SEM from pensum where cod_mat=cambios_secciones_proceso.cod_mat and cod_pen=cambios_secciones_proceso.cod_pen and cod_esc=cambios_secciones_proceso.cod_esc AND cod_nuc=cambios_secciones_proceso.cod_nuc Limit 0,1) as sem from cambios_secciones_proceso where lapso=? and cedula=?',array($lapso,$cedula));

  $arreglo = array();
  if (count($sqlMateriasInscritas)>=1){
        foreach ($sqlMateriasInscritas as $key => $value) {
          $materias_inscritas_combo = new stdClass();

          $materias_inscritas_combo->cedula = $value->cedula;
          $materias_inscritas_combo->cod_mat = $value->cod_mat;
          $materias_inscritas_combo->des_mat = $value->des_mat;
          $materias_inscritas_combo->sec_mat = $value->sec_mat;
          $materias_inscritas_combo->uni_cre = $value->uni_cre;

          $arreglo[]=$materias_inscritas_combo;
        }
        return ($arreglo);
    }else{
        return false;
    }
}
//******
function HorarioInscritoCambio($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso){
  $sqlTablaMateriasInscritas = DB::select('SELECT DISTINCT i.cedula, i.cod_mat,m.des_mat,i.sec_mat,i.cod_esc,i.cod_pen,i.cod_nuc,m.uni_cre,m.sem_mat,s.DIA1,s.DIA2 FROM secciones s INNER JOIN horamate h on h.COD_MAT=s.COD_MAT and h.LAPSO=s.lapso and h.SEC_MAT=s.SEC_MAT and s.cod_nuc= h.COD_NUC INNER JOIN inscripciones i on i.lapso=s.lapso and i.cod_mat=s.COD_MAT INNER JOIN materias m on m.cod_mat=s.COD_MAT AND m.cod_nuc=s.cod_nuc AND m.cod_pen=i.cod_pen AND m.cod_nuc=i.cod_nuc WHERE s.lapso=? and s.SEC_MAT=i.sec_mat AND s.COD_MAT=i.cod_mat and cedula=? ORDER BY sem_mat,cod_mat',array($lapso,$cedula));

  $total_uc=0;
  $arreglo = array();
  if (count($sqlTablaMateriasInscritas)>0){
        foreach ($sqlTablaMateriasInscritas as $key => $value) {
            $horario_inscrito = new stdClass();

            $horario_inscrito->cedula = $value->cedula;
            $horario_inscrito->cod_mat = $value->cod_mat;
            $horario_inscrito->des_mat = $value->des_mat;
            $horario_inscrito->sec_mat = $value->sec_mat;
            $horario_inscrito->sem_mat = $value->sem_mat;
            $horario_inscrito->uni_cre = $value->uni_cre;
            $horario_inscrito->DIA1 = $value->DIA1;
            $horario_inscrito->DIA2 = $value->DIA2;
            $total_uc+= $value->uni_cre;
            $dia1 = $value->DIA1;
            $dia2 = $value->DIA2;

            $arreglo[]=$horario_inscrito;
        }
        return ($arreglo);
    }else{
        return 0;
    }
}
//******
function HorarioCambios($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso){
    $sqlTablaMateriasCambios = DB::select('SELECT sc.cedula, sc.cod_mat, (select des_mat from materias where cod_mat=sc.cod_mat limit 0,1) as des_mat, (select uni_cre from materias where cod_mat=sc.cod_mat limit 0,1) as uni_cre, (select sem_mat from materias where cod_mat=sc.cod_mat limit 0,1) as sem_mat, CASE when sc.aprobado=? then "Sin respuesta" when sc.aprobado=? then "No Aprobado" when sc.aprobado=? then "Aprobado" when sc.aprobado=? then "Procesado" end as aprobadoC,sc.aprobado, sc.sec_mat,sc.cod_esc,sc.cod_pen,sc.cod_nuc,se.dia1 as DIA1,se.dia2 as DIA2 from solicitudes_cambios_seccion sc inner join secciones se on se.cod_mat=sc.cod_mat and se.sec_mat=sc.sec_mat and se.lapso=sc.lapso and se.cod_nuc=sc.cod_nuc where cedula = ? and sc.lapso = ?',array(0,3,1,2,$cedula,$lapso));

    $total_uc=0;
    $arreglo = array();
    if (count($sqlTablaMateriasCambios)>0){
        foreach ($sqlTablaMateriasCambios as $key => $value) {
            $horario_cambio = new stdClass();

            $uc = $value->uni_cre;
            $horario_cambio->cedula = $value->cedula;
            $horario_cambio->cod_mat = $value->cod_mat;
            $horario_cambio->des_mat = $value->des_mat;
            $horario_cambio->sec_mat = $value->sec_mat;
            $horario_cambio->uni_cre = $value->uni_cre;
            $horario_cambio->sem_mat = $value->sem_mat;
            $horario_cambio->DIA1 = $value->DIA1;
            $horario_cambio->DIA2 = $value->DIA2;
            $horario_cambio->aprobadoC = $value->aprobadoC;
            $horario_cambio->aprobado = $value->aprobado;
            $dia1 = $value->DIA1;
            $dia2 = $value->DIA2;
            $total_uc+=$value->uni_cre;

            $arreglo[]=$horario_cambio;
        }
        return $arreglo;
    }else{
        return false;
    }
}

 //*******    
function CostosCambios($cedula, $mencion, $escuela, $pensum, $nucleo, $regimen, $lapso){
    $sqlCostos =DB::select('SELECT DISTINCT i.id,i.cod_mat,materias.des_mat, costos_procesos.monto, i.aprobado,i.lapso FROM solicitudes_cambios_seccion as i,costos_procesos,materias WHERE i.cedula=? AND i.lapso=? AND costos_procesos.id=2 AND i.aprobado=? AND i.cod_mat=materias.cod_mat',array($cedula,$lapso,1));

    $arreglo=array();
    if (count($sqlCostos)) {
        foreach ($sqlCostos as $key => $value) {
            $costos_cambios = new stdClass();

            $costos_cambios->cod_mat=$value->cod_mat;
            $costos_cambios->des_mat=$value->des_mat;
            $costos_cambios->monto=$value->monto;
            $costos_cambios->aprobado=$value->aprobado;

            $arreglo[]=$costos_cambios;
        }
        return $arreglo;
    }else{
        return false;
    }
}
//********
function SeccCambio($cedula, $lapso, $cod_mat){
  $sqlS = DB::table('inscripciones')
  ->where('cod_mat','=',$cod_mat)
  ->where('cedula','=',$cedula)
  ->where('lapso','=',$lapso)
  ->get();

  $arreglo=array();
  if(count($sqlS)){
    foreach ($sqlS as $key => $value) {
      $secCam = new stdClass();
      $seccion = $value->sec_mat;
      $secCam->sec_mat = $value->sec_mat;
  }
}else{
    return false;
}
}


function nombre_solicitud($var)
{
    if ($var==1){
        return 'Por Primera Vez';
    }elseif ($var==2){
        return 'Reincorporaci&oacute;n';
    }elseif ($var==3){
        return 'Equivalencia';
    }else{
        return 'Sin Nombre';
    }
}

function tipo_sexo($var)
{
    if($var=='M'){
        return 'Masculino';
    }elseif ($var=='F'){
        return 'Femenino';
    }else{
        return 'Sin Nombre';
    }
}

function pais_nacimiento($var)
{
    $sql=DB::table('db_paises')
        ->select('des_paises')
        ->where('id','=',$var)
        ->first();
    if (count($sql)==1) {
        return $sql->des_paises;
    } else {
        return '-';
    }
}

function estado_nacimiento($var)
{
    $sql=DB::table('db_estados')
        ->select('des_estados')
        ->where('id','=',$var)
        ->first();
    if (count($sql)==1) {
        return $sql->des_estados;
    } else {
        return '-';
    }
}

function ciudad_nacimiento($var)
{
    $sql=DB::table('db_ciudades')
        ->select('des_ciudades')
        ->where('id','=',$var)
        ->first();
    if (count($sql)==1) {
        return $sql->des_ciudades;
    } else {
        return '-';
    }
}

function estados($var)
{
    $sql=DB::table('db_estados')
        ->select('des_estados')
        ->where('id','=',$var)
        ->first();
    if (count($sql)==1) {
        return $sql->des_estados;
    } else {
        return '-';
    }
}

function ciudades($var)
{
    $sql=DB::table('db_ciudades')
        ->select('des_ciudades')
        ->where('id','=',$var)
        ->first();
    if (count($sql)==1) {
        return $sql->des_ciudades;
    } else {
        return '-';
    }
}

function estados_bach($var)
{
    $id_pais=230;
    $sql=DB::table('db_estados')
        ->select('des_estados')
        ->where('id','=',$var)
        ->where('id_pais','=',$id_pais)
        ->first();

    if (count($sql)==1) {
        return $sql->des_estados;
    } else {
        return '-';
    }
}

function tipo_institucion($var)
{
    if ($var==1){
        return 'Privado';
    }elseif($var==2){
        return 'Publico';
    }elseif($var==3){
        return 'Misi&oacute;n';
    } else {
        return '-';
    }
}

function especialidad($var)
{
    if ($var==1){
        return 'Ciencias';
    }elseif($var==2){
        return 'Humanidades';
    }elseif($var==3){
        return 'Otra';
    }
}

function turno($var)
{
    if($var==1){
        return 'Mañana';
    }elseif($var==2){
        return 'Tarde';
    }elseif($var==3){
        return 'Noche';
    }elseif($var==4){
        return 'Regimen flexible';        
    } else {
        return '-';
    }
}

function modalidad($var)
{
    if($var==1){
        return 'Presencial';
    }elseif($var==2){
        return 'Semi-Presencial';
    }elseif($var==3){
        return 'Virtual';
    }else {
        return '-';
    }
}

function sino_preinsc($var)
{
    if($var==1){
        return 'Si';
    }elseif($var==2){
        return 'No';
    } else {
        return '-';
    }
}

function vivira_con($var)
{
    if($var==1){
        return 'CON SUS PADRES';
    }elseif($var==2){
        return 'OTRO FAMILIAR';
    }elseif($var==3){
        return 'VIVIENDO ALQUILADA O PROPIA';
    }elseif($var==4){
        return 'RESIDENCIA O PENSIÓN';
    }elseif($var==5){
        return 'OTRA VIVIENDA';
    } else {
        return '-';
    }

}


function costea_gastos($var)
{
    if($var==1){
        return 'PADRE';
    }elseif($var==2){
        return 'MADRE';
    }elseif($var==3){
        return 'BECA';
    }elseif($var==4){
        return 'UD. MISMO';
    }elseif($var==5){
        return 'OTROS';
    } else {
        return '-';
    }
}

function disfruta_de($var)
{
    if($var==1){
        return 'CRÉDITO EDUCATIVO';
    }elseif($var==2){
        return 'BECA';
    }elseif($var==3){
        return 'MEDIA BECA';
    }elseif($var==4){
        return 'NINGUN TIPO DE AYUDA';
    }elseif($var==5){
        return 'OTRO TIPO DE AYUDA';
    } else {
        return '-';
    }
}
function ingreso_mensual($var)
{
    if($var==1){
        return 'DESDE 5000 HASTA 8000';
    }elseif($var==2){
        return 'DESDE 8001 HASTA 11000';
    }elseif($var==3){
        return 'DESDE 11001 HASTA 14000';
    }elseif($var==4){
        return 'DESDE 14001 HASTA 17000';
    }elseif($var==5){
        return 'DESDE 17001 Ó MÁS';
    } else {
        return '-';
    }
}

function viven_padres($var)
{
    if($var==1){
        return 'AMBOS';
    }elseif($var==2){
        return 'MADRE';
    }elseif($var==3){
        return 'PADRE';
    }elseif($var==4){
        return 'NINGUNO';
    } else {
        return '-';
    }
}


function estado_civil($var)
{
    if($var=='S'){
        return 'SOLTERO';
    }elseif($var=='C'){
        return 'CASADO';
    }elseif($var=='D'){
        return 'DIVORCIADO';
    }elseif($var=='V'){
        return 'VIUDO';
    } else {
        return '-';
    }
}

function estado_civil_padres($var)
{
    if($var=='1'){
        return 'SOLTERO';
    }elseif($var=='2'){
        return 'CASADO';
    }elseif($var=='3'){
        return 'DIVORCIADO';
    }elseif($var=='4'){
        return 'VIUDO';
    } else {
        return '-';
    }
}
function vive_con($var)
{
    if($var==1){
        return 'AMBOS PADRES';
    }elseif($var==2){
        return 'PADRE';
    }elseif($var==3){
        return 'MADRE';
    }elseif($var==4){
        return 'ABUELO(A)';
    }elseif($var==5){
        return 'TIO(A)';
    }elseif($var==6){
        return 'HERMANO(A)';
    }elseif($var==7){
        return 'OTRA PERSONA';
    } else {
        return '-';
    }
}

function parentesco($var)
{
    if($var==1){
        return 'PADRE';
    }elseif($var==2){
        return 'MADRE';
    }elseif($var==3){
        return 'TIO(A)';
    }elseif($var==4){
        return 'HERMANO(A)';
    }elseif($var==5){
        return 'ABUELO(A)';
    }elseif($var==6){
        return 'PADRASTRO';
    }elseif($var==7){
        return 'MADRASTRA';
    }elseif($var==8){
        return 'AMIGO DE LA FAMILIA';
    }elseif($var==9){
        return 'OTRA PERSONA...';
    } else {
        return '-';
    }
}

function escribe($var)
{
    if($var==1){
        return 'ZURDO';
    }elseif($var==2){
        return 'DERECHO';
    } else {
        return '-';
    }
}

function nacionalidad($var)
{
    if($var=='V' || $var=='v'){
        return 'Venezolano(a)';
    }elseif($var=='E' || $var=='e'){
        return 'Extranjero(a)';
    } else {
        return '-';
    }
}

function orientacion_carrera($var)
{
    if($var==1){
        return 'Si recibi orientaci&oacute;n';
    }elseif($var==2){
        return 'No recibi orientaci&oacute;n';
    } else {
        return '-';
    }
}

function decidido_carrera($var)
{
    if($var==1){
        return 'Si lo estoy';
    }elseif($var==2){
        return 'No lo estoy';
    } else {
        return '-';
    }
}

?>